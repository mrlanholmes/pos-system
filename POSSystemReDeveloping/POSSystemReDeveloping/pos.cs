﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Management;
namespace POSSystemReDeveloping
{
    public class pos
    {
        protected String qry;
        protected String[,] dt;
        protected ManagementObjectSearcher sch;
        public String getDate() { return DateTime.Today.ToString("yyyy-MM-dd"); }
        public String getDate(String dateFormat) { return DateTime.Today.ToString(dateFormat); }
        public String getMachineName() { return Environment.MachineName; }
        public String getDateTime() { return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); }
        public String getTime() { return DateTime.Now.ToString("HH:mm:ss"); }
        public String getConvertedDate(String date) {
            date.Trim();
            if (date.Contains("/")) {
                String dd=date.Substring(0, 2);
                String mm = date.Substring(3, 2);
                String yy = date.Substring(6, 4);
                return yy+"-"+mm+"-"+dd; }
            else return date;
        }
    }
}
