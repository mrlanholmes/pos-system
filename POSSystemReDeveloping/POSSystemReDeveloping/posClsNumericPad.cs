﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public class posClsNumericPad 
    {
        // Number pad button value
        private String posNumOne = null;
        private String posNumTwo = null;
        private String posNumThree = null;
        private String posNumFour = null;
        private String posNumFive = null;
        private String posNumSix = null;
        private String posNumSeven = null;
        private String posNumEight = null;
        private String posNumNine = null;
        private String posNumZero = null;
        private String posNumEnter = null;
        private String posNumBackSpace = null;
        private String posNumDot = null;
        private posGuiGuestOrder guestOrder;
        private posGuiNumericPad numPad = null;
        private posClsFullKeyBoard fullKeyBoard;
        private posGuiBill bill;
        private posClsBillSeperate billSeperate;
        
        public posClsNumericPad() {           
            
            this.numPad = new posGuiNumericPad();
            this.posSetButtonSendKeys();
            this.numPad.Load += this.posClsNumericPadFormLoad;
            this.numPad.posBtnNumericPadOne.Click += this.posClsNumericPadNumOneClick;
            this.numPad.posBtnNumericPadTwo.Click += this.posClsNumericPadNumTwoClick;
            this.numPad.posBtnNumericPadThree.Click += this.posClsNumericPadNumThreeClick;
            this.numPad.posBtnNumericPadFour.Click += this.posClsNumericPadNumFourClick;
            this.numPad.posBtnNumericPadFive.Click += this.posClsNumericPadNumFiveClick;
            this.numPad.posBtnNumericPadSix.Click += this.posClsNumericPadNumSixClick;
            this.numPad.posBtnNumericPadSeven.Click += this.posClsNumericPadNumSevenClick;
            this.numPad.posBtnNumericPadEight.Click += this.posClsNumericPadNumEightClick;
            this.numPad.posBtnNumericPadNine.Click += this.posClsNumericPadNumNineClick;
            this.numPad.posBtnNumericPadZero.Click += this.posClsNumericPadNumZeroClick;
            this.numPad.posBtnNumericPadEnter.Click += this.posClsNumericPadEnterClick;
            this.numPad.posBtnNumericPadBackSpace.Click += this.posClsNumericPadBackSpaceClick;
            this.numPad.posBtnNumericPadDecimal.Click += this.posClsNumericPadDotClick;
            this.numPad.posBtnNumericPadExit.Click += this.posClsNumericPadExitClick;
            this.numPad.posBtnNumericPadFullKeyBoard.Click += this.posClsFullKeyBoardClick;
        }
        private void posSetFocus()
        {
            try { this.guestOrder.Focus(); }
            catch (Exception) { }
            try { this.bill.Focus(); }
            catch (Exception) { }
            try { this.billSeperate.posGetGuiBillSeperateObject().Focus(); }
            catch (Exception) { }
        }
        public void posSetFullKeyBoard(posClsFullKeyBoard fullKeyBoardObject)
        {
            this.fullKeyBoard = fullKeyBoardObject;
        }
        public void posSetGuestOrderObject(posGuiGuestOrder guestOrderObject){
            this.guestOrder = guestOrderObject;
        }
        public void posSetBillObject(posGuiBill billObject) {
            this.bill = billObject;
            this.bill.TopMost = false;
        }
        public void posSetBillSeperateGuiObject(posClsBillSeperate billSeperateObject) {
            this.billSeperate = billSeperateObject;
        }
        private void posSetPositionNumericPad() {
            try
            {
                int x = this.guestOrder.Location.X;
                int y = this.guestOrder.Location.Y;
                this.numPad.Location = new Point(x + 688, y + 38);
            }
            catch (Exception) { }
            try
            {
                int x = this.bill.Location.X;
                int y = this.bill.Location.Y;
                this.numPad.Location = new Point(x + 688, y + 38);
            }
            catch (Exception) { }
        }
        private void posSetButtonSendKeys() {
            // set button send keys value
            this.posNumOne = "1";
            this.posNumTwo = "2";
            this.posNumThree = "3";
            this.posNumFour = "4";
            this.posNumFive = "5";
            this.posNumSix = "6";
            this.posNumSeven = "7";
            this.posNumEight = "8";
            this.posNumNine = "9";
            this.posNumZero = "0";
            this.posNumDot = ".";
            this.posNumEnter = "{ENTER}";
            this.posNumBackSpace = "{BACKSPACE}";
        }
        // Event for buttons
        private void posClsNumericPadNumOneClick(object sender, EventArgs e){
            this.posSetFocus();
            SendKeys.Send(this.posNumOne);
        }
        private void posClsNumericPadNumTwoClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(this.posNumTwo);
        }
        private void posClsNumericPadNumThreeClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(this.posNumThree);
        }
        private void posClsNumericPadNumFourClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(this.posNumFour);
        }
        private void posClsNumericPadNumFiveClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(this.posNumFive);
        }
        private void posClsNumericPadNumSixClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(this.posNumSix);
        }
        private void posClsNumericPadNumSevenClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(this.posNumSeven);
        }
        private void posClsNumericPadNumEightClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(this.posNumEight);
        }
        private void posClsNumericPadNumNineClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(this.posNumNine);
        }
        private void posClsNumericPadNumZeroClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(this.posNumZero);
        }
        private void posClsNumericPadEnterClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(this.posNumEnter);
        }
        private void posClsNumericPadDotClick(object sender, EventArgs e) {
            this.posSetFocus();
            SendKeys.Send(this.posNumDot);
        }
        private void posClsNumericPadBackSpaceClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(this.posNumBackSpace);
        }
        private void posClsNumericPadExitClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            this.posHideNumericPad();
            try { this.bill.TopMost = true; }
            catch (Exception) { }
        }
        private void posClsFullKeyBoardClick(object sender, EventArgs e) {
            this.posSetFocus();
            this.fullKeyBoard.posShowFullKeyBoardPad();            
        }
        // form load event
        private void posClsNumericPadFormLoad(object sender, EventArgs e) {
            try { this.posSetPositionNumericPad(); }
            catch (Exception) { }
            
        }
        // other
        public void posSetViewLocationNumericPad(int[] position) {
            this.numPad.Location = new Point(position[0], position[1]);
        }
        public void posShowNumericPad() { this.numPad.Owner = this.guestOrder; this.numPad.TopMost = true; this.numPad.Show();

        try { this.posSetPositionNumericPad(); }
        catch (Exception) { }
        }
        public bool posIsNumericPadShow() {
            if (this.numPad.Visible) { return true; }
            return false;
        }
        public void posHideNumericPad() { this.numPad.Hide(); }
        public void posDisposeNumericPad() { this.numPad.Dispose(); }
    }
}
