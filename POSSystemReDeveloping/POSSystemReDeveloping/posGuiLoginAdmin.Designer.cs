﻿namespace POSSystemReDeveloping
{
    partial class posGuiLoginAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.posBtnLoginAdminLogin = new System.Windows.Forms.Button();
            this.posBtnLoginAdminCancel = new System.Windows.Forms.Button();
            this.posTxtLoginAdminPassword = new System.Windows.Forms.TextBox();
            this.posTxtLoginAdminUsername = new System.Windows.Forms.TextBox();
            this.posLblLoginAdminUserName = new System.Windows.Forms.Label();
            this.posLblLoginAdminPassword = new System.Windows.Forms.Label();
            this.posBtnLoginAdminKeyBoard = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // posBtnLoginAdminLogin
            // 
            this.posBtnLoginAdminLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnLoginAdminLogin.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.posBtnLoginAdminLogin.Location = new System.Drawing.Point(308, 90);
            this.posBtnLoginAdminLogin.Name = "posBtnLoginAdminLogin";
            this.posBtnLoginAdminLogin.Size = new System.Drawing.Size(100, 46);
            this.posBtnLoginAdminLogin.TabIndex = 2;
            this.posBtnLoginAdminLogin.Text = "Login";
            this.posBtnLoginAdminLogin.UseVisualStyleBackColor = true;
            this.posBtnLoginAdminLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // posBtnLoginAdminCancel
            // 
            this.posBtnLoginAdminCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnLoginAdminCancel.ForeColor = System.Drawing.Color.HotPink;
            this.posBtnLoginAdminCancel.Location = new System.Drawing.Point(206, 90);
            this.posBtnLoginAdminCancel.Name = "posBtnLoginAdminCancel";
            this.posBtnLoginAdminCancel.Size = new System.Drawing.Size(100, 46);
            this.posBtnLoginAdminCancel.TabIndex = 3;
            this.posBtnLoginAdminCancel.Text = "Cancel";
            this.posBtnLoginAdminCancel.UseVisualStyleBackColor = true;
            this.posBtnLoginAdminCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // posTxtLoginAdminPassword
            // 
            this.posTxtLoginAdminPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtLoginAdminPassword.ForeColor = System.Drawing.Color.Maroon;
            this.posTxtLoginAdminPassword.Location = new System.Drawing.Point(130, 45);
            this.posTxtLoginAdminPassword.Name = "posTxtLoginAdminPassword";
            this.posTxtLoginAdminPassword.PasswordChar = '*';
            this.posTxtLoginAdminPassword.Size = new System.Drawing.Size(278, 44);
            this.posTxtLoginAdminPassword.TabIndex = 1;
            this.posTxtLoginAdminPassword.WordWrap = false;
            this.posTxtLoginAdminPassword.TextChanged += new System.EventHandler(this.posTxtLoginAdminPassword_TextChanged);
            // 
            // posTxtLoginAdminUsername
            // 
            this.posTxtLoginAdminUsername.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.posTxtLoginAdminUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtLoginAdminUsername.ForeColor = System.Drawing.Color.Maroon;
            this.posTxtLoginAdminUsername.Location = new System.Drawing.Point(130, 2);
            this.posTxtLoginAdminUsername.Name = "posTxtLoginAdminUsername";
            this.posTxtLoginAdminUsername.Size = new System.Drawing.Size(278, 40);
            this.posTxtLoginAdminUsername.TabIndex = 0;
            this.posTxtLoginAdminUsername.WordWrap = false;
            this.posTxtLoginAdminUsername.TextChanged += new System.EventHandler(this.posTxtLoginAdminUsername_TextChanged);
            // 
            // posLblLoginAdminUserName
            // 
            this.posLblLoginAdminUserName.AutoSize = true;
            this.posLblLoginAdminUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblLoginAdminUserName.Location = new System.Drawing.Point(-1, 10);
            this.posLblLoginAdminUserName.Name = "posLblLoginAdminUserName";
            this.posLblLoginAdminUserName.Size = new System.Drawing.Size(126, 24);
            this.posLblLoginAdminUserName.TabIndex = 4;
            this.posLblLoginAdminUserName.Text = "User Name :";
            // 
            // posLblLoginAdminPassword
            // 
            this.posLblLoginAdminPassword.AutoSize = true;
            this.posLblLoginAdminPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblLoginAdminPassword.Location = new System.Drawing.Point(12, 52);
            this.posLblLoginAdminPassword.Name = "posLblLoginAdminPassword";
            this.posLblLoginAdminPassword.Size = new System.Drawing.Size(112, 24);
            this.posLblLoginAdminPassword.TabIndex = 5;
            this.posLblLoginAdminPassword.Text = "Password :";
            // 
            // posBtnLoginAdminKeyBoard
            // 
            this.posBtnLoginAdminKeyBoard.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnLoginAdminKeyBoard.ForeColor = System.Drawing.Color.HotPink;
            this.posBtnLoginAdminKeyBoard.Image = global::POSSystemReDeveloping.Properties.Resources.keyboard12;
            this.posBtnLoginAdminKeyBoard.Location = new System.Drawing.Point(129, 90);
            this.posBtnLoginAdminKeyBoard.Name = "posBtnLoginAdminKeyBoard";
            this.posBtnLoginAdminKeyBoard.Size = new System.Drawing.Size(76, 46);
            this.posBtnLoginAdminKeyBoard.TabIndex = 6;
            this.posBtnLoginAdminKeyBoard.UseVisualStyleBackColor = true;
            // 
            // posGuiLoginAdmin
            // 
            this.AcceptButton = this.posBtnLoginAdminLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.posBtnLoginAdminCancel;
            this.ClientSize = new System.Drawing.Size(406, 134);
            this.ControlBox = false;
            this.Controls.Add(this.posBtnLoginAdminKeyBoard);
            this.Controls.Add(this.posLblLoginAdminPassword);
            this.Controls.Add(this.posLblLoginAdminUserName);
            this.Controls.Add(this.posTxtLoginAdminUsername);
            this.Controls.Add(this.posTxtLoginAdminPassword);
            this.Controls.Add(this.posBtnLoginAdminCancel);
            this.Controls.Add(this.posBtnLoginAdminLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "posGuiLoginAdmin";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EXECUTIVE LOGIN";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.posGuiLoginAdmin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label posLblLoginAdminUserName;
        private System.Windows.Forms.Label posLblLoginAdminPassword;
        internal System.Windows.Forms.Button posBtnLoginAdminLogin;
        internal System.Windows.Forms.Button posBtnLoginAdminCancel;
        internal System.Windows.Forms.TextBox posTxtLoginAdminPassword;
        internal System.Windows.Forms.TextBox posTxtLoginAdminUsername;
        internal System.Windows.Forms.Button posBtnLoginAdminKeyBoard;
    }
}