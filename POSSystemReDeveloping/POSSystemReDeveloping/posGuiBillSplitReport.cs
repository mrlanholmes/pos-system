﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public partial class posGuiBillSplitReport : Form
    {
        posGuiReportBillSplitDetail billSplitDetail;
        posDsReportBill dsBillSplit;
        public posGuiBillSplitReport()
        {
            InitializeComponent();
            c_centerNameLoad();
            this.dsBillSplit = new posDsReportBill();
            this.billSplitDetail = new posGuiReportBillSplitDetail();
        }

        private void posCmbReportBillSplitCcenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (posCmbReportBillSplitCcenter.SelectedIndex == 0)
            {
                posTxtBillSplitReportHiddenCenter.Text = "0063";
            }
            else
            {
                posTxtBillSplitReportHiddenCenter.Text = "0080";
            }

        }

        void c_centerNameLoad()
        {
            posStaticLoadOnce.getConnection();
            String sqlCmb = null;
            sqlCmb = "Select CCent_Code,CCent_Name From FOCostCenter Where CCent_POS = '1'";
            SqlDataAdapter Adpt = new SqlDataAdapter(sqlCmb, posStaticLoadOnce.getConnection());
            //Adp.Fill(this.CmbDs, "c_center");
            DataTable dt1 = new DataTable();
            Adpt.Fill(dt1);
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                //posCmbReportBillCcenter.ValueMember = dt.Columns["CCent_Code"].ToString();
                posCmbReportBillSplitCcenter.Items.Add(dt1.Rows[i]["CCent_Name"]);
            }
            posCmbReportBillSplitCcenter.SelectedIndex = 0;
        }

        private void posDtpReportBillSplitFromDate_ValueChanged(object sender, EventArgs e)
        {
            posTxtReportBillSplitFromDate.Text = posDtpReportBillSplitFromDate.Value.Date.ToString("yyyy-MM-dd");
        }

        private void posDtpBillSplitReportToDate_ValueChanged(object sender, EventArgs e)
        {
            posTxtReportBillSplitToDate.Text = posDtpBillSplitReportToDate.Value.Date.ToString("yyyy-MM-dd");
        }

        private void posBtnBillSplitReportSearch_Click(object sender, EventArgs e)
        {
            if (posTxtBillSplitReportHiddenCenter.Text != "" || posTxtReportBillSplitBillNo.Text != "" || posTxtReportBillSplitSerialNo.Text != "" || posTxtReportBillSplitRoomNo.Text != "" || posTxtReportBillSplitStaffNo.Text != "" || posTxtReportBillSplitStewardNo.Text != "" || posTxtReportBillSplitFromDate.Text != "" || posTxtReportBillSplitToDate.Text != "" && DateTime.Parse(posTxtReportBillSplitFromDate.Text) >= DateTime.Parse(posTxtReportBillSplitToDate.Text))
            {
                ReportDocument cryRpt = new ReportDocument();
                TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
                TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
                ConnectionInfo crConnectionInfo = new ConnectionInfo();
                Tables CrTables;

                crConnectionInfo.ServerName = "SAJANI\\MSSQLSAJANI";
                crConnectionInfo.DatabaseName = "SMARTTBH";
                crConnectionInfo.UserID = "sa";
                crConnectionInfo.Password = "123";

                posStaticLoadOnce.getConnection();
                String sqlBill = null;
                int counter = 0;
                sqlBill = " SELECT FOMOutletBill.OblM_BillNo,FOMOutletBill.POS_SplitOldBillNo,FOMOutletBill.OblM_SerialNo,FOMOutletBill.OblM_ERoom,FOMOutletBill.OblM_EStaff,"
                           + "FOMOutletBill.OblM_Date,FOMOutletBill.OblM_Stw,FOMOutletBill.OblM_CusType,FOMOutletBill.OblM_Status,FOMOutletBill.OblM_Stot,"
                           + "FOCostCenter.CCent_Name, FOMOutletBill.POS_SplitStatus FROM FOMOutletBill,FOCostCenter "
                           + "WHERE FOMOutletBill.CCent_Code = FOCostCenter.CCent_Code AND FOMOutletBill.POS_SplitStatus = 'SP' AND ";

                if (!posTxtBillSplitReportHiddenCenter.Text.Equals(""))
                {
                    sqlBill += "FOMOutletBill.CCent_Code='" + posTxtBillSplitReportHiddenCenter.Text + "'";
                    counter++;
                }

                if (!posTxtReportBillSplitBillNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sqlBill += " and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_BillNo='" + posTxtReportBillSplitBillNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportBillSplitSerialNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sqlBill += " and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_SerialNo='" + posTxtReportBillSplitSerialNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportBillSplitRoomNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sqlBill += " and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_ERoom ='" + posTxtReportBillSplitRoomNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportBillSplitStaffNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sqlBill += " and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_EStaff ='" + posTxtReportBillSplitStaffNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportBillSplitStewardNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sqlBill += "and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_Stw='" + posTxtReportBillSplitStewardNo.Text + "'";
                    counter++;
                }

                if ((!posTxtReportBillSplitFromDate.Text.Equals("")) && (!posTxtReportBillSplitToDate.Text.Equals("")))
                {
                    if (counter == 1)
                    {
                        sqlBill += " and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_Date BETWEEN '" + posTxtReportBillSplitFromDate.Text.ToString() + "' and '" + posTxtReportBillSplitToDate.Text + "'";
                }


                SqlDataAdapter AdpBillSplit = new SqlDataAdapter(sqlBill, posStaticLoadOnce.getConnection());

                //dscmd.Fill(this.ds, "order");

                posStaticLoadOnce.getConnection().Close();
                AdpBillSplit.Fill(this.dsBillSplit, "BillSplit");
                posDgvBillSplitReport.DataSource = this.dsBillSplit.Tables[1];

            }

               
                
        }

        private void posBtnReportBillSplitDetail_Click(object sender, EventArgs e)
        {
            if (billSplitDetail == null)
            {
                billSplitDetail = new posGuiReportBillSplitDetail();

            }
            String BillIdNew = posDgvBillSplitReport.CurrentRow.Cells[0].Value.ToString();
            this.billSplitDetail.posSetBillNo(new String[] { BillIdNew });
            this.billSplitDetail.Show();
        }

        private void posBtnBillSplitReportPrint_Click(object sender, EventArgs e)
        {
            posGuiCrvBillSplitReport CrvBillSplit = new posGuiCrvBillSplitReport();
            CrvBillSplit.Show();
            ReportDocument cryRpt = new ReportDocument();
            posCrystalReportBillSplit billSplitRpt = new posCrystalReportBillSplit();
            billSplitRpt.SetDataSource(dsBillSplit.Tables[1]);
            CrvBillSplit.posGetBillSplitReportViewer().ReportSource = billSplitRpt;
            CrvBillSplit.posGetBillSplitReportViewer().Refresh();
        }

    }
}
