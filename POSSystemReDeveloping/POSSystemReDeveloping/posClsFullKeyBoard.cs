﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public class posClsFullKeyBoard
    {
        private posGuiFullKeyBoard keyBoard;
        private posGuiGuestOrder guestOrder;
        private posGuiLoginAdmin admin;
        private posGuiLoginSteward steward;
        private posGuiBill bill;
        private posGuiRemark remark;
        public posClsFullKeyBoard() {
            this.keyBoard = new posGuiFullKeyBoard();
            this.posSetBtnEvent();
        }
        // set object
        public void posSetGuestOrderObject(posGuiGuestOrder guestOrderObject) {
            try { this.guestOrder = guestOrderObject; }
            catch (Exception) { }
            
        }
        public void posSetLoginAdminObject(posGuiLoginAdmin adminObject) {
            this.admin = adminObject;
        }
        public void posSetLoginStewardObject(posGuiLoginSteward stewardObject)
        {
            this.steward = stewardObject;
        }
        public void posSetBillObject(posGuiBill billObject)
        {
            this.bill = billObject;
            this.bill.TopMost = false;
        }
        public void posSetRemarkObject(posGuiRemark remarkObject) {
            this.remark = remarkObject;
        }
        //###############################################################################
        private void posSetBtnEvent() {
            this.keyBoard.posBtnFullKeyBoardCurrentSign.Click += this.posBtnFullKeyBoardCurrentSignClick;
            this.keyBoard.posBtnFullKeyBoardOne.Click += this.posBtnFullKeyBoardOneClick;
            this.keyBoard.posBtnFullKeyBoardTwo.Click += this.posBtnFullKeyBoardTwoClick;
            this.keyBoard.posBtnFullKeyBoardThree.Click += this.posBtnFullKeyBoardThreeClick;
            this.keyBoard.posBtnFullKeyBoardFour.Click += this.posBtnFullKeyBoardFourClick;
            this.keyBoard.posBtnFullKeyBoardFive.Click += this.posBtnFullKeyBoardFiveClick;
            this.keyBoard.posBtnFullKeyBoardSix.Click += this.posBtnFullKeyBoardSixClick;
            this.keyBoard.posBtnFullKeyBoardSeven.Click += this.posBtnFullKeyBoardSevenClick;
            this.keyBoard.posBtnFullKeyBoardEight.Click += this.posBtnFullKeyBoardEightClick;
            this.keyBoard.posBtnFullKeyBoardNine.Click += this.posBtnFullKeyBoardNineClick;
            this.keyBoard.posBtnFullKeyBoardZero.Click += this.posBtnFullKeyBoardZeroClick;
            this.keyBoard.posBtnFullKeyBoardHyphen.Click += this.posBtnFullKeyBoardHyphenClick;
            this.keyBoard.posBtnFullKeyBoardEqual.Click += this.posBtnFullKeyBoardEqualClick;
            this.keyBoard.posBtnFullKeyBoardClose.Click += this.posBtnFullKeyBoardCloseClick;
            this.keyBoard.posBtnFullKeyBoardTab.Click += this.posBtnFullKeyBoardTabClick;
            this.keyBoard.posBtnFullKeyBoardQ.Click += this.posBtnFullKeyBoardQClick;
            this.keyBoard.posBtnFullKeyBoardW.Click += this.posBtnFullKeyBoardWClick;
            this.keyBoard.posBtnFullKeyBoardE.Click += this.posBtnFullKeyBoardEClick;
            this.keyBoard.posBtnFullKeyBoardR.Click += this.posBtnFullKeyBoardRClick;
            this.keyBoard.posBtnFullKeyBoardT.Click += this.posBtnFullKeyBoardTClick;
            this.keyBoard.posBtnFullKeyBoardY.Click += this.posBtnFullKeyBoardYClick;
            this.keyBoard.posBtnFullKeyBoardU.Click += this.posBtnFullKeyBoardUClick;
            this.keyBoard.posBtnFullKeyBoardI.Click += this.posBtnFullKeyBoardIClick;
            this.keyBoard.posBtnFullKeyBoardO.Click += this.posBtnFullKeyBoardOClick;
            this.keyBoard.posBtnFullKeyBoardP.Click += this.posBtnFullKeyBoardPClick;
            this.keyBoard.posBtnFullKeyBoardLeftSquareBrackets.Click += this.posBtnFullKeyBoardLeftSquareBracketClick;
            this.keyBoard.posBtnFullKeyBoardRightSquareBracket.Click += this.posBtnFullKeyBoardRightSquareBracketClick;
            this.keyBoard.posBtnFullKeyBoardBackSlash.Click += this.posBtnFullKeyBoardBackSlashClick;
            this.keyBoard.posBtnFullKeyBoardA.Click += this.posBtnFullKeyBoardAClick;
            this.keyBoard.posBtnFullKeyBoardS.Click += this.posBtnFullKeyBoardSClick;
            this.keyBoard.posBtnFullKeyBoardD.Click += this.posBtnFullKeyBoardDClick;
            this.keyBoard.posBtnFullKeyBoardF.Click += this.posBtnFullKeyBoardFClick;
            this.keyBoard.posBtnFullKeyBoardG.Click += this.posBtnFullKeyBoardGClick;
            this.keyBoard.posBtnFullKeyBoardH.Click += this.posBtnFullKeyBoardHClick;
            this.keyBoard.posBtnFullKeyBoardJ.Click += this.posBtnFullKeyBoardJClick;
            this.keyBoard.posBtnFullKeyBoardK.Click += this.posBtnFullKeyBoardKClick;
            this.keyBoard.posBtnFullKeyBoardL.Click += this.posBtnFullKeyBoardLClick;
            this.keyBoard.posBtnFullKeyBoardColon.Click += this.posBtnFullKeyBoardColonClick;
            this.keyBoard.posBtnFullKeyBoardQuotation.Click += this.posBtnFullKeyBoardQuotationClick;
            this.keyBoard.posBtnFullKeyBoardEnter.Click += this.posBtnFullKeyBoardEnterClick;
            this.keyBoard.posBtnFullKeyBoardZ.Click += this.posBtnFullKeyBoardZClick;
            this.keyBoard.posBtnFullKeyBoardX.Click += this.posBtnFullKeyBoardXClick;
            this.keyBoard.posBtnFullKeyBoardC.Click += this.posBtnFullKeyBoardCClick;
            this.keyBoard.posBtnFullKeyBoardV.Click += this.posBtnFullKeyBoardVClick;
            this.keyBoard.posBtnFullKeyBoardB.Click += this.posBtnFullKeyBoardBClick;
            this.keyBoard.posBtnFullKeyBoardN.Click += this.posBtnFullKeyBoardNClick;
            this.keyBoard.posBtnFullKeyBoardM.Click += this.posBtnFullKeyBoardMClick;
            this.keyBoard.posBtnFullKeyBoardComma.Click += this.posBtnFullKeyBoardCommaClick;
            this.keyBoard.posBtnFullKeyBoardDot.Click += this.posBtnFullKeyBoardDotClick;
            this.keyBoard.posBtnFullKeyBoardFowardSlash.Click += this.posBtnFullKeyBoardForwardSlashClick;
            this.keyBoard.posChkFullKeyBoardLeftControl.Click += this.posBtnFullKeyBoardLeftControlClick;
            this.keyBoard.posChkFullKeyBoardLeftAlt.Click += this.posBtnFullKeyBoardLeftAltClick;
            this.keyBoard.posBtnFullKeyBoardSpaceBar.Click += this.posBtnFullKeyBoardSpaceBarClick;
            this.keyBoard.posBtnFullKeyBoardBackSpace.Click += this.posBtnFullKeyBoardBackSpaceClick;
            this.keyBoard.posChkFullKeyBoardRightAlt.Click += this.posBtnFullKeyBoardRightAltClick;
            
            
        }
        public void posSetViewLocationFullKeyBoard(int[] position) {
            this.keyBoard.Location = new Point(position[0],position[1]);
        }
        public void posSetPositionFullKeyBoardPad()
        {
            try {
                int x = this.guestOrder.Location.X;
                int y = this.guestOrder.Location.Y;
                int height = this.guestOrder.Height;
                this.keyBoard.Location = new Point(0, height - 330);
            }
            catch (Exception) { }

            try
            {
                int x = this.admin.Location.X;
                int y = this.admin.Location.Y;
                int height = this.admin.Height;                
                this.keyBoard.Location = new Point(0, (height +y));
               // MessageBox.Show(x + "-" + y + "-" + height + "=" + this.keyBoard.Location.Y);
            }
            catch (Exception) { }
            try
            {
                int x = this.steward.Location.X;
                int y = this.steward.Location.Y;
                int height = this.steward.Height;
                this.keyBoard.Location = new Point(0, height + y);
            }
            catch (Exception) { }

            try
            {
                int x = this.bill.Location.X;
                int y = this.bill.Location.Y;
                int height = this.bill.Height;
                this.keyBoard.Location = new Point(0, height -300);
            }
            catch (Exception) { }
        }
        
        public void posShowFullKeyBoardPad() {
            this.keyBoard.Owner = this.guestOrder;
            this.keyBoard.TopMost = true; 
            this.keyBoard.Show();
            this.posSetPositionFullKeyBoardPad();
        }
        public void posHideFullKeyBoardPad() { this.keyBoard.Hide();
        try { this.bill.TopMost = true; }
        catch (Exception) { }
        }
        public void posDisposeFullKeyBoardPad() { this.keyBoard.Dispose(); }

        //###############################################################################
        private void posSetFocus()
        {
            try { this.guestOrder.Focus(); }
            catch (Exception) { }
            try { this.admin.Focus(); }
            catch (Exception) { }
            try { this.steward.Focus(); this.steward.posTxtLoginStewardPassword.Focus(); }
            catch (Exception) { }
            try { this.bill.Focus(); }
            catch (Exception) { }
            try { this.remark.Focus(); }
            catch (Exception) { }
        }
        private void posBtnFullKeyBoardQClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("Q");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("q");
            }
        }        

        private void posBtnFullKeyBoardForwardSlashClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{?}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("{/}");
            }
        }

        private void posBtnFullKeyBoardIClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("I");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("i");
            }
        }

        private void posBtnFullKeyBoardWClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("W");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("w");
            }
        }

        private void posBtnFullKeyBoardEClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("E");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("e");
            }
        }

        private void posBtnFullKeyBoardRClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("R");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("r");
            }
        }

        private void posBtnFullKeyBoardTClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("T");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("t");
            }
        }

        private void posBtnFullKeyBoardYClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("Y");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("y");
            }
        }

        private void posBtnFullKeyBoardUClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("U");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("u");
            }
        }

        private void posBtnFullKeyBoardAClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("A");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("a");
            }
        }

        private void posBtnFullKeyBoardOClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("O");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;

            }
            else
            {
                SendKeys.Send("o");
            }
        }

        private void posBtnFullKeyBoardPClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("P");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("p");
            }
        }

        private void posBtnFullKeyBoardSClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("S");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("s");
            }
        }

        private void posBtnFullKeyBoardDClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("D");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("d");
            }
        }

        private void posBtnFullKeyBoardFClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("F");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("f");
            }
        }

        private void posBtnFullKeyBoardGClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("G");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("g");
            }
        }

        private void posBtnFullKeyBoardHClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("H");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("h");
            }
        }

        private void posBtnFullKeyBoardJClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("J");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("j");
            }
        }

        private void posBtnFullKeyBoardKClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("K");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("k");
            }
        }

        private void posBtnFullKeyBoardLClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("L");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("l");
            }
        }

        private void posBtnFullKeyBoardZClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("Z");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("z");
            }
        }

        private void posBtnFullKeyBoardXClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("X");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("x");
            }
        }

        private void posBtnFullKeyBoardCClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("C");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("c");
            }

        }


        private void posBtnFullKeyBoardVClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("V");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("v");
            }

        }

        private void posBtnFullKeyBoardBClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("B");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;

            }
            else
            {
                SendKeys.Send("b");
            }
        }

        private void posBtnFullKeyBoardNClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("N");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("n");
            }
        }

        private void posBtnFullKeyBoardMClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardCapsLock.Checked || this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("M");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("m");
            }
        }

        private void posBtnFullKeyBoardLeftSquareBracketClick(object sender, EventArgs e)
        {
            this.posSetFocus();

            if ( this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{{}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("[");
            }
        }

        private void posBtnFullKeyBoardRightSquareBracketClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{}}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("]");
            }

        }

        private void posBtnFullKeyBoardBackSlashClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{|}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("\\");
            }
        }

        private void posBtnFullKeyBoardColonClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{:}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("{;}");
            }
        }

        private void posBtnFullKeyBoardQuotationClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("\"");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("{'}");
            }
        }

        private void posBtnFullKeyBoardCommaClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{<}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("{,}");
            }
        }

        private void posBtnFullKeyBoardDotClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{>}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("{.}");
            }
        }

        private void posBtnFullKeyBoardCurrentSignClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{~}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("{`}");
            }
        }

        private void posBtnFullKeyBoardHyphenClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{_}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("{-}");
            }
        }

        private void posBtnFullKeyBoardEqualClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{+}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("{=}");
            }
        }

        private void posBtnFullKeyBoardCloseClick(object sender, EventArgs e)
        {
            this.posHideFullKeyBoardPad();
        }

        private void posBtnFullKeyBoardOneClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{!}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("1");
            }
        }

        private void posBtnFullKeyBoardTwoClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{@}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("2");
            }
        }

        private void posBtnFullKeyBoardThreeClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{#}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("3");
            }
        }

        private void posBtnFullKeyBoardFourClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{$}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("4");
            }
        }

        private void posBtnFullKeyBoardFiveClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{%}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("5");
            }
        }

        private void posBtnFullKeyBoardSixClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{^}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("6");
            }
        }

        private void posBtnFullKeyBoardSevenClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{&}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("7");
            }
        }

        private void posBtnFullKeyBoardEightClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{*}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("8");
            }
        }

        private void posBtnFullKeyBoardNineClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{(}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("9");
            }
        }

        private void posBtnFullKeyBoardZeroClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            if (this.keyBoard.posChkFullKeyBoardLeftShift.Checked || this.keyBoard.posChkFullKeyBoardRightShift.Checked)
            {
                SendKeys.Send("{)}");
                this.keyBoard.posChkFullKeyBoardLeftShift.Checked = false;
                this.keyBoard.posChkFullKeyBoardRightShift.Checked = false;
            }
            else
            {
                SendKeys.Send("0");
            }
        }

        private void posBtnFullKeyBoardEnterClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send("{ENTER}");


        }

        private void posBtnFullKeyBoardTabClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send("{TAB}");
        }

        private void posBtnFullKeyBoardBackSpaceClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send("{BACKSPACE}");
        }

        private void posBtnFullKeyBoardLeftControlClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send("{LCTRL}");
        }

        private void posBtnFullKeyBoardRightControlClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send("{RCTRL}");
        }

        private void posBtnFullKeyBoardSpaceBarClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send(" ");
        }

        private void posBtnFullKeyBoardLeftAltClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send("{LALT}");
        }

        private void posBtnFullKeyBoardRightAltClick(object sender, EventArgs e)
        {
            this.posSetFocus();
            SendKeys.Send("{RALT}");
        }

    }
}
