﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
   public class posClsHome : pos
    {
        private posGuiHome home;
        private posClsGuestOrder guestOrder;
        private posClsBill bill;
        private posClsSettings setting;
        public posClsLoginAdmin admin;
        private posClsLoginSteward steward;
        private posClsNumericPad numPad;
        private posClsFullKeyBoard fullKeyBoard;
        private posClsBillSeperate billSeperate;
        Boolean[] userRight;
        Button[] btnsHome;
        public posClsHome(posClsLoginSteward stewardObject,posClsLoginAdmin adminObject) {
            this.admin = adminObject;
            this.steward = stewardObject;
            this.home = new posGuiHome();
            //this.numPad = new posClsNumericPad();
            this.setGuiHomeProperties();
            this.btnsHome = new Button[] { this.home.btnHomeGuestOrder, this.home.btnHomeAllInclusive, this.home.btnHomeBill, this.home.btnHomeEntitle, this.home.btnHomeBillSplit, this.home.btnHomeReports, this.home.btnHomePrinter, this.home.btnHomePrinterToItems, this.home.btnHomeItemRemarks, this.home.btnHomeSettings };
        }
        //sql
        private int posGetUnsettledBillCount(String[] data) {
            this.qry = "SELECT count(OrdM_No) FROM FOOrderMaster where OrdM_Status = 'UB' and CCent_Code = '" + data[0] + "' ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (Convert.ToInt16(this.dt[0,0])>0)
            {
                return Convert.ToInt16(this.dt[0, 0]);
            }
            return 0;
        }
        
        public void posSetNumericKeyPadObject(posClsNumericPad numPadObject)
        {
            this.numPad = numPadObject;
        }
        public void posSetFullKeyBoardObject(posClsFullKeyBoard fullKeyBoardObject)
        {
            this.fullKeyBoard = fullKeyBoardObject;
        }
        //#########################
        private void setGuiHomeProperties() {
            this.home.Load += this.posSetFormLoad;
            this.home.btnHomeGuestOrder.Click += this.posSetBtnGuestOrderClick;
            this.home.btnHomeBill.Click += this.posSetBtnBillClick;
            this.home.btnHomeSettings.Click += this.posSetBtnSettingClick;
            this.home.btnHomeStewardLogout.Click += this.posSetBtnStewardLogoutClick;
            this.home.btnHomeSwitchOff.Click += this.posSetBtnSwitchOffClick;
            this.home.btnHomeAllInclusive.Click += this.posSetBtnAllInclusiveClick;
            this.home.btnHomeEntitle.Click += this.posSetBtnEntitleClick;
            this.home.btnHomeBillSplit.Click += this.posSetBtnBillSplitClick;
            this.home.posDgvHome.Click += this.posSetDgvCellContentClick;
            this.home.btnHomeBillReprint.Click += this.posSetBtnBillReprint;
            this.home.btnHomeOpenItem.Click += this.posSetBtnOpenItem;
        }        
        private void posSetCostCenter() {
            try
            {
                posStaticLoadOnce.setCostCenterCode(0, this.home.posDgvHome.SelectedCells[1].Value.ToString());
                posStaticLoadOnce.setCostCenterCode(1, this.home.posDgvHome.SelectedCells[0].Value.ToString());
            }
            catch (Exception){ }            
        }
        //Event
        private void posSetBtnOpenItem(object sender, EventArgs e) {
            this.posSetCostCenter();
            // Varification
            this.admin.posShowGuiAdmin();
            this.admin.posSetHomeObject(this);
            this.admin.posSetOpenItemStatus(true);
            this.fullKeyBoard.posShowFullKeyBoardPad();
            this.admin.posGetGuiAdminObject().posTxtLoginAdminUsername.Focus();
        }
        private void posSetBtnBillReprint(object sender, EventArgs e)
        {
            this.posSetCostCenter();
            // Varification
            this.admin.posShowGuiAdmin();
            this.admin.posSetHomeObject(this);
            this.admin.posSetBillReprintStatus(true);
            this.fullKeyBoard.posShowFullKeyBoardPad();
            this.admin.posGetGuiAdminObject().posTxtLoginAdminUsername.Focus();
        }
        private void posSetBtnStewardLogout(object sender, EventArgs e) {
            try { this.guestOrder.posHideGuestOrderPanel(); }
            catch (Exception ex) { }
            try { this.bill.posBillPanelHide(); }
            catch (Exception ex) { }
            try { this.setting.posSettingPanelDispose(); }
            catch (Exception ex) { }
            try { this.posHomePanelDispose(); }
            catch (Exception ex) { }
            try {  }
            catch (Exception ex) { }    
        }
        private void posSetBtnGuestOrderClick(object sender, EventArgs e) {
            this.posSetCostCenter();            
            this.guestOrder = new posClsGuestOrder(this.home,this.admin);
            this.guestOrder.posSetFullKeyBoardObject(this.fullKeyBoard);
            this.guestOrder.posSetNumericKeyPadObject(this.numPad);            
            this.guestOrder.posShowGuestOrderPanel();
            this.posHideGuiHome();
            //this.guestOrder.posSetFullKeyBoardObject(this.fullKeyBoard);
            //this.guestOrder.posSetNumericKeyPadObject(this.numPad);
        }
        private void posSetBtnBillClick(object sender, EventArgs e)
        {
           
            this.posSetCostCenter();
            if (this.bill == null) {
                this.bill = new posClsBill();
                this.bill.posSetHomeObject(this);
                this.bill.posSetLoginAdmin(this.admin);
            }            
            this.bill.posSetFullKeyBoardObject(this.fullKeyBoard);
            this.bill.posSetNumericKeyPadObject(this.numPad);
            this.bill.posClearText();
            this.bill.posBillPanelShow();
            posGuiBill billGui = this.bill.posGetBillGuiObject();
            this.numPad.posSetBillObject(billGui);
            this.posHideGuiHome();
        }
        private void posSetBtnAllInclusiveClick(object sender, EventArgs e)
        {
            this.posSetCostCenter();
            this.guestOrder = new posClsGuestOrder(this.home, this.admin);
            this.guestOrder.posSetFullKeyBoardObject(this.fullKeyBoard);
            this.guestOrder.posSetNumericKeyPadObject(this.numPad);
            this.guestOrder.posSetGuestType("AI");
            this.guestOrder.posShowGuestOrderPanel();
            this.posHideGuiHome();
        }
        private void posSetBtnEntitleClick(object sender, EventArgs e)
        {
            this.posSetCostCenter();
            this.guestOrder = new posClsGuestOrder(this.home, this.admin);
            this.guestOrder.posSetFullKeyBoardObject(this.fullKeyBoard);
            this.guestOrder.posSetNumericKeyPadObject(this.numPad);
            this.guestOrder.posSetGuestType("ENT");
            this.guestOrder.posShowGuestOrderPanel();
            this.posHideGuiHome();
        }
        private void posSetBtnStewardLogoutClick(object sender, EventArgs e) {
            this.posHideGuiHome();
            this.steward.posShowGuiSteward();
        }
        private void posSetBtnSwitchOffClick(object sender, EventArgs e)
        {
            int counter= 0;
            String ccentCode = "";
            for(int i=0;i<this.home.posDgvHome.Rows.Count;i++){
                ccentCode = this.home.posDgvHome.Rows[i].Cells[1].Value.ToString();
                if(this.posGetUnsettledBillCount(new String[]{ccentCode}) > 0){
                    counter++;
                }
            }
            
            if (counter ==0)
            {
                this.posHideGuiHome();
                this.admin.posSetApplicationCloseState(true,this);
                this.admin.posShowGuiAdmin();
            }
            else {
                DialogResult dr;
                dr= MessageBox.Show("You have Unbilled Orders.Please Bill those orders");
            }
            
        }
        private void posSetDgvCellContentClick(object sender, EventArgs e)
        {
            this.posSetCostCenter();    
            this.posBtnDisable();
            if (this.home.posDgvHome.SelectedRows.Count > 0)
            {

                this.userRight = posStaticLoadOnce.PosUserRightPermission();
                for (int i = 0; i < 10; i++)
                {
                    this.btnsHome[i].Enabled = userRight[i];
                }
            }
            else {
                this.posBtnDisable();
            }
             
            
        }
        private void posBtnDisable() { 
            for(int i=0;i<10;i++){
                this.btnsHome[i].Enabled = false;
            }
        }
        private void posSetBtnSettingClick(object sender, EventArgs e) {
            this.posSetCostCenter();
            this.setting = new posClsSettings(this);
            this.setting.posSettingPanelShow();
            this.posHideGuiHome();
        }
        private void posSetBtnBillSplitClick(object sender, EventArgs e)
        {
            this.posSetCostCenter();
            // Varification
            this.admin.posShowGuiAdmin();
            this.admin.posSetHomeObject(this);
            this.admin.posSetBillSplitState(true);
            this.fullKeyBoard.posShowFullKeyBoardPad();
            this.admin.posGetGuiAdminObject().posTxtLoginAdminUsername.Focus();
        }
        private void posSetDgvHomeProperties()
        {            
            this.home.posDgvHome.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.home.posDgvHome.MultiSelect = false;
            this.home.posDgvHome.RowHeadersVisible = false;
            try
            {
                this.home.posDgvHome.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                this.home.posDgvHome.Columns[1].Visible = false;
            } catch (Exception) { }            
            this.home.posDgvHome.ColumnHeadersVisible = false;
            this.home.posDgvHome.DefaultCellStyle.ForeColor = Color.Blue;
            this.home.posDgvHome.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);            
            this.home.posDgvHome.ReadOnly = true;
            this.home.posDgvHome.AllowUserToResizeRows = false;
           
        }
        private void posSetFormLoad(object sender, EventArgs e)
        {
            this.posSetHomeCostCenters();
            
            try { this.home.posDgvHome.Rows[0].Selected = true; } catch (Exception) { }
            this.posSetCostCenter();
            this.posBtnDisable();

           // this.posClrDgvHomeSelection();
        }
       // other
        public void posExeBillSplit() {
            if (this.billSeperate == null)
            {
                this.billSeperate = new posClsBillSeperate();
                this.billSeperate.posSetHomeObject(this);
                this.billSeperate.posSetLoginAdminObject(this.admin);
            }
            this.billSeperate.posSetFullKeyBoardObject(this.fullKeyBoard);
            this.billSeperate.posSetNumericKeyPadObject(this.numPad);
            this.billSeperate.posShowBillSeperatePanel();
        }
        public void posExeOPenItem() {
            this.posSetCostCenter();
            this.setting = null;
            if (this.setting == null) {
                this.setting = new posClsSettings(this);
                
            }
            try {
                
                this.setting.posGetGuiSettingObject().posTabSettings.TabPages.RemoveAt(2);
                this.setting.posGetGuiSettingObject().posTabSettings.TabPages.RemoveAt(1);
                this.setting.posGetGuiSettingObject().posTabSettings.TabPages.RemoveAt(0);

                //this.setting.posGetGuiSettingObject().posTabSettings
            }
            catch (Exception) { }
            this.setting.posSettingPanelShow();
            this.posHideGuiHome();
        }
        public void posBillReprint() {
            
        }
        public posGuiHome posGetGuiHomeObject() {
            return this.home;
        }
        private void posClrDgvHomeSelection() {
            try { this.home.posDgvHome.ClearSelection(); }
            catch (Exception) { }
        }
        public void posSetHomeCostCenters() {
            this.home.posDgvHome.RowTemplate.Height = 50;
            this.home.posDgvHome.DataSource = this.posGetCostCenter("1");
            this.posSetDgvHomeProperties();
        }
        public DataTable posGetCostCenter(String selector) {
            this.qry = "SELECT  CCent_Name,CCent_Code FROM FOCostCenter where CCent_POS = '" + selector + "' ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) { return posStaticLoadOnce.PosDataTable; }
            return null;
        }
        private void posSetHomeButtonED() { 
            
        }
        public void posShowGuiHome()
        {
            this.home.Show();      
            //this.home.posDgvHome.CurrentRow.DefaultCellStyle.BackColor = Color.Red; 
            this.posClrDgvHomeSelection();
            this.posBtnDisable();
        }
        public void posHideGuiHome() {
            this.home.Hide();
        }
        public void posHomePanelDispose() {
            this.home.Dispose();
        }
    }
}
