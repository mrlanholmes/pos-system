﻿namespace POSSystemReDeveloping
{
    partial class posGuiBillReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.posTxtBillReportHiddenCenter = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.posCmbReportBillCcenter = new System.Windows.Forms.ComboBox();
            this.posBtnBillReportPrint = new System.Windows.Forms.Button();
            this.posBtnReportBillDetail = new System.Windows.Forms.Button();
            this.posBtnBillReportSearch = new System.Windows.Forms.Button();
            this.posDtpBillReportToDate = new System.Windows.Forms.DateTimePicker();
            this.posDtpReportBillFromDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.posTxtReportBillFromDate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.posTxtReportBillToDate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.posTxtReportBillStewardNo = new System.Windows.Forms.TextBox();
            this.posTxtReportBillStaffNo = new System.Windows.Forms.TextBox();
            this.posTxtReportBillRoomNo = new System.Windows.Forms.TextBox();
            this.posTxtReportBillSerialNo = new System.Windows.Forms.TextBox();
            this.posTxtReportBillNo = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.posDgvReportBill = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvReportBill)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.posTxtBillReportHiddenCenter);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.posCmbReportBillCcenter);
            this.groupBox1.Controls.Add(this.posBtnBillReportPrint);
            this.groupBox1.Controls.Add(this.posBtnReportBillDetail);
            this.groupBox1.Controls.Add(this.posBtnBillReportSearch);
            this.groupBox1.Controls.Add(this.posDtpBillReportToDate);
            this.groupBox1.Controls.Add(this.posDtpReportBillFromDate);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.posTxtReportBillFromDate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.posTxtReportBillToDate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.posTxtReportBillStewardNo);
            this.groupBox1.Controls.Add(this.posTxtReportBillStaffNo);
            this.groupBox1.Controls.Add(this.posTxtReportBillRoomNo);
            this.groupBox1.Controls.Add(this.posTxtReportBillSerialNo);
            this.groupBox1.Controls.Add(this.posTxtReportBillNo);
            this.groupBox1.Location = new System.Drawing.Point(4, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1004, 120);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // posTxtBillReportHiddenCenter
            // 
            this.posTxtBillReportHiddenCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillReportHiddenCenter.Location = new System.Drawing.Point(72, 12);
            this.posTxtBillReportHiddenCenter.Name = "posTxtBillReportHiddenCenter";
            this.posTxtBillReportHiddenCenter.Size = new System.Drawing.Size(18, 15);
            this.posTxtBillReportHiddenCenter.TabIndex = 14;
            this.posTxtBillReportHiddenCenter.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Cost Center";
            // 
            // posCmbReportBillCcenter
            // 
            this.posCmbReportBillCcenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posCmbReportBillCcenter.FormattingEnabled = true;
            this.posCmbReportBillCcenter.Location = new System.Drawing.Point(6, 30);
            this.posCmbReportBillCcenter.Name = "posCmbReportBillCcenter";
            this.posCmbReportBillCcenter.Size = new System.Drawing.Size(161, 24);
            this.posCmbReportBillCcenter.TabIndex = 12;
            this.posCmbReportBillCcenter.SelectedIndexChanged += new System.EventHandler(this.posCmbReportBillCcenter_SelectedIndexChanged);
            // 
            // posBtnBillReportPrint
            // 
            this.posBtnBillReportPrint.Location = new System.Drawing.Point(852, 81);
            this.posBtnBillReportPrint.Name = "posBtnBillReportPrint";
            this.posBtnBillReportPrint.Size = new System.Drawing.Size(75, 23);
            this.posBtnBillReportPrint.TabIndex = 11;
            this.posBtnBillReportPrint.Text = "Print";
            this.posBtnBillReportPrint.UseVisualStyleBackColor = true;
            this.posBtnBillReportPrint.Click += new System.EventHandler(this.posBtnBillReportPrint_Click);
            // 
            // posBtnReportBillDetail
            // 
            this.posBtnReportBillDetail.Location = new System.Drawing.Point(771, 81);
            this.posBtnReportBillDetail.Name = "posBtnReportBillDetail";
            this.posBtnReportBillDetail.Size = new System.Drawing.Size(75, 23);
            this.posBtnReportBillDetail.TabIndex = 10;
            this.posBtnReportBillDetail.Text = "Detail";
            this.posBtnReportBillDetail.UseVisualStyleBackColor = true;
            this.posBtnReportBillDetail.Click += new System.EventHandler(this.posBtnReportBillDetail_Click);
            // 
            // posBtnBillReportSearch
            // 
            this.posBtnBillReportSearch.Location = new System.Drawing.Point(690, 81);
            this.posBtnBillReportSearch.Name = "posBtnBillReportSearch";
            this.posBtnBillReportSearch.Size = new System.Drawing.Size(75, 23);
            this.posBtnBillReportSearch.TabIndex = 9;
            this.posBtnBillReportSearch.Text = "Search";
            this.posBtnBillReportSearch.UseVisualStyleBackColor = true;
            this.posBtnBillReportSearch.Click += new System.EventHandler(this.posBtnBillReportSearch_Click);
            // 
            // posDtpBillReportToDate
            // 
            this.posDtpBillReportToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posDtpBillReportToDate.Location = new System.Drawing.Point(653, 81);
            this.posDtpBillReportToDate.Name = "posDtpBillReportToDate";
            this.posDtpBillReportToDate.Size = new System.Drawing.Size(18, 24);
            this.posDtpBillReportToDate.TabIndex = 8;
            this.posDtpBillReportToDate.ValueChanged += new System.EventHandler(this.posDtpBillReportToDate_ValueChanged);
            // 
            // posDtpReportBillFromDate
            // 
            this.posDtpReportBillFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posDtpReportBillFromDate.Location = new System.Drawing.Point(310, 81);
            this.posDtpReportBillFromDate.Name = "posDtpReportBillFromDate";
            this.posDtpReportBillFromDate.Size = new System.Drawing.Size(18, 24);
            this.posDtpReportBillFromDate.TabIndex = 0;
            this.posDtpReportBillFromDate.ValueChanged += new System.EventHandler(this.posDtpReportBillFromDate_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(335, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "To Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(506, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Room No";
            // 
            // posTxtReportBillFromDate
            // 
            this.posTxtReportBillFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillFromDate.Location = new System.Drawing.Point(6, 81);
            this.posTxtReportBillFromDate.Name = "posTxtReportBillFromDate";
            this.posTxtReportBillFromDate.Size = new System.Drawing.Size(304, 24);
            this.posTxtReportBillFromDate.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "From Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(340, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Serial No";
            // 
            // posTxtReportBillToDate
            // 
            this.posTxtReportBillToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillToDate.Location = new System.Drawing.Point(338, 81);
            this.posTxtReportBillToDate.Name = "posTxtReportBillToDate";
            this.posTxtReportBillToDate.Size = new System.Drawing.Size(315, 24);
            this.posTxtReportBillToDate.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(838, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Steward No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(174, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bill No";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(672, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Staff No";
            // 
            // posTxtReportBillStewardNo
            // 
            this.posTxtReportBillStewardNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillStewardNo.Location = new System.Drawing.Point(841, 30);
            this.posTxtReportBillStewardNo.Name = "posTxtReportBillStewardNo";
            this.posTxtReportBillStewardNo.Size = new System.Drawing.Size(160, 24);
            this.posTxtReportBillStewardNo.TabIndex = 4;
            // 
            // posTxtReportBillStaffNo
            // 
            this.posTxtReportBillStaffNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillStaffNo.Location = new System.Drawing.Point(675, 30);
            this.posTxtReportBillStaffNo.Name = "posTxtReportBillStaffNo";
            this.posTxtReportBillStaffNo.Size = new System.Drawing.Size(160, 24);
            this.posTxtReportBillStaffNo.TabIndex = 3;
            // 
            // posTxtReportBillRoomNo
            // 
            this.posTxtReportBillRoomNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillRoomNo.Location = new System.Drawing.Point(509, 30);
            this.posTxtReportBillRoomNo.Name = "posTxtReportBillRoomNo";
            this.posTxtReportBillRoomNo.Size = new System.Drawing.Size(160, 24);
            this.posTxtReportBillRoomNo.TabIndex = 2;
            // 
            // posTxtReportBillSerialNo
            // 
            this.posTxtReportBillSerialNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillSerialNo.Location = new System.Drawing.Point(343, 30);
            this.posTxtReportBillSerialNo.Name = "posTxtReportBillSerialNo";
            this.posTxtReportBillSerialNo.Size = new System.Drawing.Size(160, 24);
            this.posTxtReportBillSerialNo.TabIndex = 1;
            // 
            // posTxtReportBillNo
            // 
            this.posTxtReportBillNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillNo.Location = new System.Drawing.Point(177, 30);
            this.posTxtReportBillNo.Name = "posTxtReportBillNo";
            this.posTxtReportBillNo.Size = new System.Drawing.Size(160, 24);
            this.posTxtReportBillNo.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.posDgvReportBill);
            this.groupBox2.Location = new System.Drawing.Point(4, 121);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1004, 610);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // posDgvReportBill
            // 
            this.posDgvReportBill.AllowUserToAddRows = false;
            this.posDgvReportBill.AllowUserToDeleteRows = false;
            this.posDgvReportBill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvReportBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.posDgvReportBill.Location = new System.Drawing.Point(3, 16);
            this.posDgvReportBill.Name = "posDgvReportBill";
            this.posDgvReportBill.Size = new System.Drawing.Size(998, 591);
            this.posDgvReportBill.TabIndex = 0;
            // 
            // posGuiBillReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 733);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "posGuiBillReport";
            this.Text = "posGuiBillReport";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.posDgvReportBill)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox posCmbReportBillCcenter;
        private System.Windows.Forms.Button posBtnBillReportPrint;
        private System.Windows.Forms.Button posBtnReportBillDetail;
        private System.Windows.Forms.Button posBtnBillReportSearch;
        private System.Windows.Forms.DateTimePicker posDtpBillReportToDate;
        private System.Windows.Forms.DateTimePicker posDtpReportBillFromDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox posTxtReportBillFromDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox posTxtReportBillToDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox posTxtReportBillStewardNo;
        private System.Windows.Forms.TextBox posTxtReportBillStaffNo;
        private System.Windows.Forms.TextBox posTxtReportBillRoomNo;
        private System.Windows.Forms.TextBox posTxtReportBillSerialNo;
        private System.Windows.Forms.TextBox posTxtReportBillNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView posDgvReportBill;
        private System.Windows.Forms.TextBox posTxtBillReportHiddenCenter;
    }
}