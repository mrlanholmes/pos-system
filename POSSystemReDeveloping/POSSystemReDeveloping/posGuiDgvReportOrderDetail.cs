﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public partial class posGuiDgvReportOrderDetail : Form
    {
        String orderNo;
        posDsReportOrderDetail ds1;
        posCrvReportOrderDetail CrvDetail;
        public posGuiDgvReportOrderDetail()
        {
            InitializeComponent();
           
            orderNo = "";
            this.ds1 = new posDsReportOrderDetail();
        }

        public void posSetOrderNo(String[] Data)
        {
            this.orderNo = Data[0];

        }
            

      

        private void posGuiDgvReportOrderDetail_Load(object sender, EventArgs e)
        {
            posStaticLoadOnce.getConnection();
            String sql = null;

            int counter = 0;
            sql = "Select FOOrderDetail.OrdM_No,FOMItems.FOIM_ItemDes,FOOrderDetail.OrdD_Qty,FOOrderDetail.OrdD_Price "
                   + "from FOOrderDetail, FOMItems where FOMItems.FOIM_ItemNo = FOOrderDetail.FOIM_ItemNo AND FOOrderDetail.OrdM_No ='" + this.orderNo + "'";

            SqlDataAdapter dscmd = new SqlDataAdapter(sql, posStaticLoadOnce.getConnection());

            dscmd.Fill(ds1, "orderDetail");
            posDgvReportOrderDetail.DataSource = ds1.Tables[1];
        }

        private void posBtnReportsOrderDetailPrint_Click(object sender, EventArgs e)
        {
            this.CrvDetail = null;
            if (this.CrvDetail == null) {
                this.CrvDetail = new posCrvReportOrderDetail();
            }
            


            this.CrvDetail.Show();
            ReportDocument cryRpt = new ReportDocument();
            posCrystalReportOrderDetail crOrderDetail = new posCrystalReportOrderDetail();
            crOrderDetail.SetDataSource(ds1.Tables[1]);
            this.CrvDetail.posGetOrderReporDetailtViewer().ReportSource = crOrderDetail;
            this.CrvDetail.posGetOrderReporDetailtViewer().Refresh();
        }

        private void btnReportOrderDetailBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
