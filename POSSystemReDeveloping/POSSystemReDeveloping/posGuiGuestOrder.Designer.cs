﻿namespace POSSystemReDeveloping
{
    partial class posGuiGuestOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(posGuiGuestOrder));
            this.btnGuestOrderBeverage = new System.Windows.Forms.Button();
            this.btnGuestOrderFood = new System.Windows.Forms.Button();
            this.btnGuestOrderHome = new System.Windows.Forms.Button();
            this.posDgvGuestOrderOrder = new System.Windows.Forms.DataGridView();
            this.posTxtGuestOrderCostCenter = new System.Windows.Forms.TextBox();
            this.posTxtGuestOrderHOD = new System.Windows.Forms.TextBox();
            this.posTxtHomeCostCenterName = new System.Windows.Forms.TextBox();
            this.posTxtGuestOrderKOTBOT = new System.Windows.Forms.TextBox();
            this.posTxtHomeGuestName = new System.Windows.Forms.TextBox();
            this.posTxtGuestOrderStewardName = new System.Windows.Forms.TextBox();
            this.posTxtGuestOrderRemarks = new System.Windows.Forms.TextBox();
            this.posTxtGuestOrderTableNo = new System.Windows.Forms.TextBox();
            this.posTxtGuestOrderSaleType = new System.Windows.Forms.TextBox();
            this.posLblGuestOrderCostCenter = new System.Windows.Forms.Label();
            this.posLblGuestOrderHOD = new System.Windows.Forms.Label();
            this.posLblGuestOrderKOTBOT = new System.Windows.Forms.Label();
            this.posLblGuestOrderRoom = new System.Windows.Forms.Label();
            this.posLblGuestOrderRemarks = new System.Windows.Forms.Label();
            this.posLblGuestOrderTable = new System.Windows.Forms.Label();
            this.posLblGuestOrderSaleType = new System.Windows.Forms.Label();
            this.posBtnGuestOrderRoom = new System.Windows.Forms.Button();
            this.posBtnGuestOrderSaleType = new System.Windows.Forms.Button();
            this.posRdbGuestOrderGuest = new System.Windows.Forms.RadioButton();
            this.posRdbGuestOrderOthers = new System.Windows.Forms.RadioButton();
            this.posBtnGuestOrderSave = new System.Windows.Forms.Button();
            this.posTxtGuestOrderRoom = new System.Windows.Forms.TextBox();
            this.posGuestOrderErrMsg = new System.Windows.Forms.TextBox();
            this.posGuestOrderGbxMenuPanel = new System.Windows.Forms.GroupBox();
            this.posTxtGuestOrderPax = new System.Windows.Forms.TextBox();
            this.posLblGuestOrderPax = new System.Windows.Forms.Label();
            this.posTxtGuestOrderTourName = new System.Windows.Forms.TextBox();
            this.btnOrderCancelClose = new System.Windows.Forms.Button();
            this.btnGuestOrderCigarette = new System.Windows.Forms.Button();
            this.btnGuestOrderOthers = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.posBtnGuestOrderNew = new System.Windows.Forms.Button();
            this.posBtnGuestOrderVoid = new System.Windows.Forms.Button();
            this.posBtnGuestOrderOpenTable = new System.Windows.Forms.Button();
            this.posBtnGuestOrderBill = new System.Windows.Forms.Button();
            this.posBtnGuestOrderDelete = new System.Windows.Forms.Button();
            this.posGuestOrderGbxRightPanel = new System.Windows.Forms.GroupBox();
            this.posPnlGuestOrderRightPanel = new System.Windows.Forms.Panel();
            this.posBtnGuestOrderKeyboard = new System.Windows.Forms.Button();
            this.posGuestOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posGuestOrderItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posGuestOrderItemPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posGuestOrderItemQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posGuestOrderItemTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posGuestOrderItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posGuestOrderItemCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posGuestOrderRemark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posRemarkUnchangeble = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posRemarkChangeble = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvGuestOrderOrder)).BeginInit();
            this.posGuestOrderGbxMenuPanel.SuspendLayout();
            this.posGuestOrderGbxRightPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGuestOrderBeverage
            // 
            this.btnGuestOrderBeverage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnGuestOrderBeverage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuestOrderBeverage.Location = new System.Drawing.Point(166, 187);
            this.btnGuestOrderBeverage.Name = "btnGuestOrderBeverage";
            this.btnGuestOrderBeverage.Size = new System.Drawing.Size(75, 75);
            this.btnGuestOrderBeverage.TabIndex = 15;
            this.btnGuestOrderBeverage.Text = "Bever- age";
            this.btnGuestOrderBeverage.UseVisualStyleBackColor = false;
            this.btnGuestOrderBeverage.Click += new System.EventHandler(this.btnGuestOrderBeverage_Click);
            // 
            // btnGuestOrderFood
            // 
            this.btnGuestOrderFood.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnGuestOrderFood.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuestOrderFood.Location = new System.Drawing.Point(85, 187);
            this.btnGuestOrderFood.Name = "btnGuestOrderFood";
            this.btnGuestOrderFood.Size = new System.Drawing.Size(75, 75);
            this.btnGuestOrderFood.TabIndex = 14;
            this.btnGuestOrderFood.Text = "Food";
            this.btnGuestOrderFood.UseVisualStyleBackColor = false;
            this.btnGuestOrderFood.Click += new System.EventHandler(this.btnHomeFood_Click);
            // 
            // btnGuestOrderHome
            // 
            this.btnGuestOrderHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuestOrderHome.Location = new System.Drawing.Point(6, 19);
            this.btnGuestOrderHome.Name = "btnGuestOrderHome";
            this.btnGuestOrderHome.Size = new System.Drawing.Size(75, 75);
            this.btnGuestOrderHome.TabIndex = 16;
            this.btnGuestOrderHome.Text = "Home";
            this.btnGuestOrderHome.UseVisualStyleBackColor = true;
            this.btnGuestOrderHome.Click += new System.EventHandler(this.btnGuestOrderHome_Click);
            // 
            // posDgvGuestOrderOrder
            // 
            this.posDgvGuestOrderOrder.AllowUserToAddRows = false;
            this.posDgvGuestOrderOrder.AllowUserToDeleteRows = false;
            this.posDgvGuestOrderOrder.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.posDgvGuestOrderOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.posDgvGuestOrderOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvGuestOrderOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.posGuestOrderNo,
            this.posGuestOrderItemName,
            this.posGuestOrderItemPrice,
            this.posGuestOrderItemQty,
            this.posGuestOrderItemTotal,
            this.posGuestOrderItemCode,
            this.posGuestOrderItemCost,
            this.posGuestOrderRemark,
            this.posRemarkUnchangeble,
            this.posRemarkChangeble});
            this.posDgvGuestOrderOrder.Location = new System.Drawing.Point(6, 349);
            this.posDgvGuestOrderOrder.Name = "posDgvGuestOrderOrder";
            this.posDgvGuestOrderOrder.ReadOnly = true;
            this.posDgvGuestOrderOrder.RowHeadersVisible = false;
            this.posDgvGuestOrderOrder.RowTemplate.Height = 35;
            this.posDgvGuestOrderOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.posDgvGuestOrderOrder.Size = new System.Drawing.Size(659, 368);
            this.posDgvGuestOrderOrder.TabIndex = 17;
            this.posDgvGuestOrderOrder.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.posDgvGuestOrderOrder_CellContentClick);
            this.posDgvGuestOrderOrder.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.posDgvGuestOrderOrder_CellEndEdit);
            this.posDgvGuestOrderOrder.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.posDgvGuestOrderOrder_CellValueChanged);
            // 
            // posTxtGuestOrderCostCenter
            // 
            this.posTxtGuestOrderCostCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtGuestOrderCostCenter.Location = new System.Drawing.Point(247, 11);
            this.posTxtGuestOrderCostCenter.Multiline = true;
            this.posTxtGuestOrderCostCenter.Name = "posTxtGuestOrderCostCenter";
            this.posTxtGuestOrderCostCenter.ReadOnly = true;
            this.posTxtGuestOrderCostCenter.Size = new System.Drawing.Size(116, 31);
            this.posTxtGuestOrderCostCenter.TabIndex = 18;
            // 
            // posTxtGuestOrderHOD
            // 
            this.posTxtGuestOrderHOD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtGuestOrderHOD.Location = new System.Drawing.Point(247, 46);
            this.posTxtGuestOrderHOD.Multiline = true;
            this.posTxtGuestOrderHOD.Name = "posTxtGuestOrderHOD";
            this.posTxtGuestOrderHOD.ReadOnly = true;
            this.posTxtGuestOrderHOD.Size = new System.Drawing.Size(116, 31);
            this.posTxtGuestOrderHOD.TabIndex = 19;
            // 
            // posTxtHomeCostCenterName
            // 
            this.posTxtHomeCostCenterName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtHomeCostCenterName.Location = new System.Drawing.Point(366, 11);
            this.posTxtHomeCostCenterName.Multiline = true;
            this.posTxtHomeCostCenterName.Name = "posTxtHomeCostCenterName";
            this.posTxtHomeCostCenterName.ReadOnly = true;
            this.posTxtHomeCostCenterName.Size = new System.Drawing.Size(175, 31);
            this.posTxtHomeCostCenterName.TabIndex = 20;
            // 
            // posTxtGuestOrderKOTBOT
            // 
            this.posTxtGuestOrderKOTBOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtGuestOrderKOTBOT.Location = new System.Drawing.Point(247, 81);
            this.posTxtGuestOrderKOTBOT.Multiline = true;
            this.posTxtGuestOrderKOTBOT.Name = "posTxtGuestOrderKOTBOT";
            this.posTxtGuestOrderKOTBOT.ReadOnly = true;
            this.posTxtGuestOrderKOTBOT.Size = new System.Drawing.Size(116, 31);
            this.posTxtGuestOrderKOTBOT.TabIndex = 21;
            // 
            // posTxtHomeGuestName
            // 
            this.posTxtHomeGuestName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtHomeGuestName.Location = new System.Drawing.Point(366, 81);
            this.posTxtHomeGuestName.Multiline = true;
            this.posTxtHomeGuestName.Name = "posTxtHomeGuestName";
            this.posTxtHomeGuestName.ReadOnly = true;
            this.posTxtHomeGuestName.Size = new System.Drawing.Size(299, 31);
            this.posTxtHomeGuestName.TabIndex = 23;
            // 
            // posTxtGuestOrderStewardName
            // 
            this.posTxtGuestOrderStewardName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtGuestOrderStewardName.Location = new System.Drawing.Point(420, 115);
            this.posTxtGuestOrderStewardName.Name = "posTxtGuestOrderStewardName";
            this.posTxtGuestOrderStewardName.ReadOnly = true;
            this.posTxtGuestOrderStewardName.Size = new System.Drawing.Size(103, 26);
            this.posTxtGuestOrderStewardName.TabIndex = 24;
            // 
            // posTxtGuestOrderRemarks
            // 
            this.posTxtGuestOrderRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtGuestOrderRemarks.Location = new System.Drawing.Point(529, 154);
            this.posTxtGuestOrderRemarks.Name = "posTxtGuestOrderRemarks";
            this.posTxtGuestOrderRemarks.Size = new System.Drawing.Size(136, 29);
            this.posTxtGuestOrderRemarks.TabIndex = 25;
            this.posTxtGuestOrderRemarks.WordWrap = false;
            this.posTxtGuestOrderRemarks.Click += new System.EventHandler(this.posTxtGuestOrderRemarks_Click);
            this.posTxtGuestOrderRemarks.Enter += new System.EventHandler(this.posTxtGuestOrderRemarks_Enter);
            // 
            // posTxtGuestOrderTableNo
            // 
            this.posTxtGuestOrderTableNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtGuestOrderTableNo.Location = new System.Drawing.Point(623, 12);
            this.posTxtGuestOrderTableNo.MaxLength = 3;
            this.posTxtGuestOrderTableNo.Name = "posTxtGuestOrderTableNo";
            this.posTxtGuestOrderTableNo.Size = new System.Drawing.Size(42, 31);
            this.posTxtGuestOrderTableNo.TabIndex = 26;
            this.posTxtGuestOrderTableNo.WordWrap = false;
            this.posTxtGuestOrderTableNo.Click += new System.EventHandler(this.posTxtGuestOrderTableNo_Click);
            // 
            // posTxtGuestOrderSaleType
            // 
            this.posTxtGuestOrderSaleType.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtGuestOrderSaleType.Location = new System.Drawing.Point(247, 150);
            this.posTxtGuestOrderSaleType.Multiline = true;
            this.posTxtGuestOrderSaleType.Name = "posTxtGuestOrderSaleType";
            this.posTxtGuestOrderSaleType.ReadOnly = true;
            this.posTxtGuestOrderSaleType.Size = new System.Drawing.Size(116, 31);
            this.posTxtGuestOrderSaleType.TabIndex = 27;
            // 
            // posLblGuestOrderCostCenter
            // 
            this.posLblGuestOrderCostCenter.AutoSize = true;
            this.posLblGuestOrderCostCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblGuestOrderCostCenter.Location = new System.Drawing.Point(116, 13);
            this.posLblGuestOrderCostCenter.Name = "posLblGuestOrderCostCenter";
            this.posLblGuestOrderCostCenter.Size = new System.Drawing.Size(131, 24);
            this.posLblGuestOrderCostCenter.TabIndex = 28;
            this.posLblGuestOrderCostCenter.Text = "Cost Center :";
            // 
            // posLblGuestOrderHOD
            // 
            this.posLblGuestOrderHOD.AutoSize = true;
            this.posLblGuestOrderHOD.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblGuestOrderHOD.Location = new System.Drawing.Point(180, 48);
            this.posLblGuestOrderHOD.Name = "posLblGuestOrderHOD";
            this.posLblGuestOrderHOD.Size = new System.Drawing.Size(67, 24);
            this.posLblGuestOrderHOD.TabIndex = 29;
            this.posLblGuestOrderHOD.Text = "HOD :";
            // 
            // posLblGuestOrderKOTBOT
            // 
            this.posLblGuestOrderKOTBOT.AutoSize = true;
            this.posLblGuestOrderKOTBOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblGuestOrderKOTBOT.Location = new System.Drawing.Point(135, 81);
            this.posLblGuestOrderKOTBOT.Name = "posLblGuestOrderKOTBOT";
            this.posLblGuestOrderKOTBOT.Size = new System.Drawing.Size(112, 24);
            this.posLblGuestOrderKOTBOT.TabIndex = 30;
            this.posLblGuestOrderKOTBOT.Text = "KOT/BOT :";
            // 
            // posLblGuestOrderRoom
            // 
            this.posLblGuestOrderRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblGuestOrderRoom.Location = new System.Drawing.Point(93, 118);
            this.posLblGuestOrderRoom.Name = "posLblGuestOrderRoom";
            this.posLblGuestOrderRoom.Size = new System.Drawing.Size(154, 24);
            this.posLblGuestOrderRoom.TabIndex = 31;
            this.posLblGuestOrderRoom.Text = "Room :";
            this.posLblGuestOrderRoom.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // posLblGuestOrderRemarks
            // 
            this.posLblGuestOrderRemarks.AutoSize = true;
            this.posLblGuestOrderRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblGuestOrderRemarks.Location = new System.Drawing.Point(420, 157);
            this.posLblGuestOrderRemarks.Name = "posLblGuestOrderRemarks";
            this.posLblGuestOrderRemarks.Size = new System.Drawing.Size(103, 24);
            this.posLblGuestOrderRemarks.TabIndex = 34;
            this.posLblGuestOrderRemarks.Text = "Remarks :";
            // 
            // posLblGuestOrderTable
            // 
            this.posLblGuestOrderTable.AutoSize = true;
            this.posLblGuestOrderTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblGuestOrderTable.Location = new System.Drawing.Point(541, 14);
            this.posLblGuestOrderTable.Name = "posLblGuestOrderTable";
            this.posLblGuestOrderTable.Size = new System.Drawing.Size(75, 24);
            this.posLblGuestOrderTable.TabIndex = 36;
            this.posLblGuestOrderTable.Text = "Table :";
            // 
            // posLblGuestOrderSaleType
            // 
            this.posLblGuestOrderSaleType.AutoSize = true;
            this.posLblGuestOrderSaleType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblGuestOrderSaleType.Location = new System.Drawing.Point(131, 155);
            this.posLblGuestOrderSaleType.Name = "posLblGuestOrderSaleType";
            this.posLblGuestOrderSaleType.Size = new System.Drawing.Size(116, 24);
            this.posLblGuestOrderSaleType.TabIndex = 37;
            this.posLblGuestOrderSaleType.Text = "Sale Type :";
            // 
            // posBtnGuestOrderRoom
            // 
            this.posBtnGuestOrderRoom.Location = new System.Drawing.Point(366, 115);
            this.posBtnGuestOrderRoom.Name = "posBtnGuestOrderRoom";
            this.posBtnGuestOrderRoom.Size = new System.Drawing.Size(47, 31);
            this.posBtnGuestOrderRoom.TabIndex = 39;
            this.posBtnGuestOrderRoom.Text = ">";
            this.posBtnGuestOrderRoom.UseVisualStyleBackColor = true;
            this.posBtnGuestOrderRoom.Click += new System.EventHandler(this.posBtnHomeRoom_Click);
            // 
            // posBtnGuestOrderSaleType
            // 
            this.posBtnGuestOrderSaleType.Location = new System.Drawing.Point(366, 150);
            this.posBtnGuestOrderSaleType.Name = "posBtnGuestOrderSaleType";
            this.posBtnGuestOrderSaleType.Size = new System.Drawing.Size(47, 31);
            this.posBtnGuestOrderSaleType.TabIndex = 40;
            this.posBtnGuestOrderSaleType.Text = ">";
            this.posBtnGuestOrderSaleType.UseVisualStyleBackColor = true;
            this.posBtnGuestOrderSaleType.Click += new System.EventHandler(this.posBtnHomeSaleType_Click);
            // 
            // posRdbGuestOrderGuest
            // 
            this.posRdbGuestOrderGuest.AutoSize = true;
            this.posRdbGuestOrderGuest.Checked = true;
            this.posRdbGuestOrderGuest.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posRdbGuestOrderGuest.Location = new System.Drawing.Point(366, 49);
            this.posRdbGuestOrderGuest.Name = "posRdbGuestOrderGuest";
            this.posRdbGuestOrderGuest.Size = new System.Drawing.Size(82, 28);
            this.posRdbGuestOrderGuest.TabIndex = 41;
            this.posRdbGuestOrderGuest.TabStop = true;
            this.posRdbGuestOrderGuest.Text = "Guest";
            this.posRdbGuestOrderGuest.UseVisualStyleBackColor = true;
            this.posRdbGuestOrderGuest.CheckedChanged += new System.EventHandler(this.posRdbHomeGuest_CheckedChanged);
            // 
            // posRdbGuestOrderOthers
            // 
            this.posRdbGuestOrderOthers.AutoSize = true;
            this.posRdbGuestOrderOthers.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posRdbGuestOrderOthers.Location = new System.Drawing.Point(450, 48);
            this.posRdbGuestOrderOthers.Name = "posRdbGuestOrderOthers";
            this.posRdbGuestOrderOthers.Size = new System.Drawing.Size(90, 28);
            this.posRdbGuestOrderOthers.TabIndex = 42;
            this.posRdbGuestOrderOthers.TabStop = true;
            this.posRdbGuestOrderOthers.Text = "Others";
            this.posRdbGuestOrderOthers.UseVisualStyleBackColor = true;
            // 
            // posBtnGuestOrderSave
            // 
            this.posBtnGuestOrderSave.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.posBtnGuestOrderSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnGuestOrderSave.Location = new System.Drawing.Point(6, 187);
            this.posBtnGuestOrderSave.Name = "posBtnGuestOrderSave";
            this.posBtnGuestOrderSave.Size = new System.Drawing.Size(75, 75);
            this.posBtnGuestOrderSave.TabIndex = 43;
            this.posBtnGuestOrderSave.Text = "Save";
            this.posBtnGuestOrderSave.UseVisualStyleBackColor = false;
            this.posBtnGuestOrderSave.Click += new System.EventHandler(this.posBtnGuestOrderSave_Click);
            // 
            // posTxtGuestOrderRoom
            // 
            this.posTxtGuestOrderRoom.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.posTxtGuestOrderRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtGuestOrderRoom.Location = new System.Drawing.Point(247, 116);
            this.posTxtGuestOrderRoom.Multiline = true;
            this.posTxtGuestOrderRoom.Name = "posTxtGuestOrderRoom";
            this.posTxtGuestOrderRoom.ReadOnly = true;
            this.posTxtGuestOrderRoom.Size = new System.Drawing.Size(116, 31);
            this.posTxtGuestOrderRoom.TabIndex = 44;
            // 
            // posGuestOrderErrMsg
            // 
            this.posGuestOrderErrMsg.BackColor = System.Drawing.Color.WhiteSmoke;
            this.posGuestOrderErrMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posGuestOrderErrMsg.ForeColor = System.Drawing.Color.Red;
            this.posGuestOrderErrMsg.Location = new System.Drawing.Point(408, 187);
            this.posGuestOrderErrMsg.Multiline = true;
            this.posGuestOrderErrMsg.Name = "posGuestOrderErrMsg";
            this.posGuestOrderErrMsg.ReadOnly = true;
            this.posGuestOrderErrMsg.Size = new System.Drawing.Size(257, 75);
            this.posGuestOrderErrMsg.TabIndex = 45;
            // 
            // posGuestOrderGbxMenuPanel
            // 
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtGuestOrderPax);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posLblGuestOrderPax);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtGuestOrderTourName);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.btnOrderCancelClose);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.btnGuestOrderCigarette);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.btnGuestOrderOthers);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.button3);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.button2);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posBtnGuestOrderNew);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posBtnGuestOrderVoid);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posBtnGuestOrderKeyboard);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posBtnGuestOrderOpenTable);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posBtnGuestOrderBill);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtHomeCostCenterName);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtGuestOrderTableNo);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posBtnGuestOrderSave);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtHomeGuestName);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posBtnGuestOrderDelete);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtGuestOrderCostCenter);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.btnGuestOrderHome);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.btnGuestOrderBeverage);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.btnGuestOrderFood);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posGuestOrderErrMsg);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtGuestOrderRoom);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posLblGuestOrderRemarks);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posRdbGuestOrderOthers);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtGuestOrderRemarks);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posRdbGuestOrderGuest);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posLblGuestOrderTable);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posBtnGuestOrderRoom);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posLblGuestOrderRoom);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtGuestOrderSaleType);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posLblGuestOrderKOTBOT);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posBtnGuestOrderSaleType);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posLblGuestOrderHOD);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posLblGuestOrderCostCenter);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posLblGuestOrderSaleType);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtGuestOrderStewardName);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtGuestOrderKOTBOT);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posDgvGuestOrderOrder);
            this.posGuestOrderGbxMenuPanel.Controls.Add(this.posTxtGuestOrderHOD);
            this.posGuestOrderGbxMenuPanel.Location = new System.Drawing.Point(1, -5);
            this.posGuestOrderGbxMenuPanel.Name = "posGuestOrderGbxMenuPanel";
            this.posGuestOrderGbxMenuPanel.Size = new System.Drawing.Size(670, 723);
            this.posGuestOrderGbxMenuPanel.TabIndex = 46;
            this.posGuestOrderGbxMenuPanel.TabStop = false;
            // 
            // posTxtGuestOrderPax
            // 
            this.posTxtGuestOrderPax.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtGuestOrderPax.Location = new System.Drawing.Point(623, 47);
            this.posTxtGuestOrderPax.MaxLength = 3;
            this.posTxtGuestOrderPax.Name = "posTxtGuestOrderPax";
            this.posTxtGuestOrderPax.Size = new System.Drawing.Size(42, 31);
            this.posTxtGuestOrderPax.TabIndex = 57;
            this.posTxtGuestOrderPax.WordWrap = false;
            // 
            // posLblGuestOrderPax
            // 
            this.posLblGuestOrderPax.AutoSize = true;
            this.posLblGuestOrderPax.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblGuestOrderPax.Location = new System.Drawing.Point(559, 51);
            this.posLblGuestOrderPax.Name = "posLblGuestOrderPax";
            this.posLblGuestOrderPax.Size = new System.Drawing.Size(57, 24);
            this.posLblGuestOrderPax.TabIndex = 58;
            this.posLblGuestOrderPax.Text = "Pax :";
            // 
            // posTxtGuestOrderTourName
            // 
            this.posTxtGuestOrderTourName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtGuestOrderTourName.Location = new System.Drawing.Point(532, 115);
            this.posTxtGuestOrderTourName.Name = "posTxtGuestOrderTourName";
            this.posTxtGuestOrderTourName.ReadOnly = true;
            this.posTxtGuestOrderTourName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.posTxtGuestOrderTourName.Size = new System.Drawing.Size(133, 31);
            this.posTxtGuestOrderTourName.TabIndex = 56;
            // 
            // btnOrderCancelClose
            // 
            this.btnOrderCancelClose.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnOrderCancelClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrderCancelClose.Location = new System.Drawing.Point(248, 268);
            this.btnOrderCancelClose.Name = "btnOrderCancelClose";
            this.btnOrderCancelClose.Size = new System.Drawing.Size(75, 75);
            this.btnOrderCancelClose.TabIndex = 55;
            this.btnOrderCancelClose.Text = "Close";
            this.btnOrderCancelClose.UseVisualStyleBackColor = false;
            // 
            // btnGuestOrderCigarette
            // 
            this.btnGuestOrderCigarette.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnGuestOrderCigarette.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuestOrderCigarette.Location = new System.Drawing.Point(327, 187);
            this.btnGuestOrderCigarette.Name = "btnGuestOrderCigarette";
            this.btnGuestOrderCigarette.Size = new System.Drawing.Size(76, 75);
            this.btnGuestOrderCigarette.TabIndex = 54;
            this.btnGuestOrderCigarette.Text = "Cigar- ette";
            this.btnGuestOrderCigarette.UseVisualStyleBackColor = false;
            this.btnGuestOrderCigarette.Click += new System.EventHandler(this.btnGuestOrderCigarette_Click);
            // 
            // btnGuestOrderOthers
            // 
            this.btnGuestOrderOthers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnGuestOrderOthers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuestOrderOthers.Location = new System.Drawing.Point(248, 187);
            this.btnGuestOrderOthers.Name = "btnGuestOrderOthers";
            this.btnGuestOrderOthers.Size = new System.Drawing.Size(75, 75);
            this.btnGuestOrderOthers.TabIndex = 53;
            this.btnGuestOrderOthers.Text = "Others";
            this.btnGuestOrderOthers.UseVisualStyleBackColor = false;
            this.btnGuestOrderOthers.Click += new System.EventHandler(this.btnGuestOrderOthers_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(327, 187);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 75);
            this.button3.TabIndex = 54;
            this.button3.Text = "Cigarette";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(247, 187);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 75);
            this.button2.TabIndex = 53;
            this.button2.Text = "Others";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // posBtnGuestOrderNew
            // 
            this.posBtnGuestOrderNew.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.posBtnGuestOrderNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnGuestOrderNew.Location = new System.Drawing.Point(6, 103);
            this.posBtnGuestOrderNew.Name = "posBtnGuestOrderNew";
            this.posBtnGuestOrderNew.Size = new System.Drawing.Size(75, 75);
            this.posBtnGuestOrderNew.TabIndex = 51;
            this.posBtnGuestOrderNew.Text = "New";
            this.posBtnGuestOrderNew.UseVisualStyleBackColor = false;
            this.posBtnGuestOrderNew.Click += new System.EventHandler(this.posBtnGuestOrderNew_Click);
            // 
            // posBtnGuestOrderVoid
            // 
            this.posBtnGuestOrderVoid.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.posBtnGuestOrderVoid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnGuestOrderVoid.Location = new System.Drawing.Point(166, 268);
            this.posBtnGuestOrderVoid.Name = "posBtnGuestOrderVoid";
            this.posBtnGuestOrderVoid.Size = new System.Drawing.Size(75, 75);
            this.posBtnGuestOrderVoid.TabIndex = 50;
            this.posBtnGuestOrderVoid.Text = "Void";
            this.posBtnGuestOrderVoid.UseVisualStyleBackColor = false;
            this.posBtnGuestOrderVoid.Click += new System.EventHandler(this.posBtnGuestOrderVoid_Click);
            // 
            // posBtnGuestOrderOpenTable
            // 
            this.posBtnGuestOrderOpenTable.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.posBtnGuestOrderOpenTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnGuestOrderOpenTable.Location = new System.Drawing.Point(327, 268);
            this.posBtnGuestOrderOpenTable.Name = "posBtnGuestOrderOpenTable";
            this.posBtnGuestOrderOpenTable.Size = new System.Drawing.Size(254, 75);
            this.posBtnGuestOrderOpenTable.TabIndex = 48;
            this.posBtnGuestOrderOpenTable.Text = "Open Tables";
            this.posBtnGuestOrderOpenTable.UseVisualStyleBackColor = false;
            this.posBtnGuestOrderOpenTable.Click += new System.EventHandler(this.posBtnGuestOrderOpenTable_Click);
            // 
            // posBtnGuestOrderBill
            // 
            this.posBtnGuestOrderBill.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.posBtnGuestOrderBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnGuestOrderBill.Location = new System.Drawing.Point(85, 268);
            this.posBtnGuestOrderBill.Name = "posBtnGuestOrderBill";
            this.posBtnGuestOrderBill.Size = new System.Drawing.Size(75, 75);
            this.posBtnGuestOrderBill.TabIndex = 47;
            this.posBtnGuestOrderBill.Text = "Bill";
            this.posBtnGuestOrderBill.UseVisualStyleBackColor = false;
            this.posBtnGuestOrderBill.Click += new System.EventHandler(this.posBtnGuestOrderBill_Click);
            // 
            // posBtnGuestOrderDelete
            // 
            this.posBtnGuestOrderDelete.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.posBtnGuestOrderDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnGuestOrderDelete.Location = new System.Drawing.Point(6, 268);
            this.posBtnGuestOrderDelete.Name = "posBtnGuestOrderDelete";
            this.posBtnGuestOrderDelete.Size = new System.Drawing.Size(75, 75);
            this.posBtnGuestOrderDelete.TabIndex = 46;
            this.posBtnGuestOrderDelete.Text = "Delete";
            this.posBtnGuestOrderDelete.UseVisualStyleBackColor = false;
            this.posBtnGuestOrderDelete.Click += new System.EventHandler(this.posBtnGuestOrderDelete_Click);
            // 
            // posGuestOrderGbxRightPanel
            // 
            this.posGuestOrderGbxRightPanel.Controls.Add(this.posPnlGuestOrderRightPanel);
            this.posGuestOrderGbxRightPanel.Location = new System.Drawing.Point(677, -5);
            this.posGuestOrderGbxRightPanel.Name = "posGuestOrderGbxRightPanel";
            this.posGuestOrderGbxRightPanel.Size = new System.Drawing.Size(327, 723);
            this.posGuestOrderGbxRightPanel.TabIndex = 47;
            this.posGuestOrderGbxRightPanel.TabStop = false;
            // 
            // posPnlGuestOrderRightPanel
            // 
            this.posPnlGuestOrderRightPanel.AutoScroll = true;
            this.posPnlGuestOrderRightPanel.Location = new System.Drawing.Point(6, 13);
            this.posPnlGuestOrderRightPanel.Name = "posPnlGuestOrderRightPanel";
            this.posPnlGuestOrderRightPanel.Size = new System.Drawing.Size(312, 703);
            this.posPnlGuestOrderRightPanel.TabIndex = 0;
            // 
            // posBtnGuestOrderKeyboard
            // 
            this.posBtnGuestOrderKeyboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnGuestOrderKeyboard.Image = ((System.Drawing.Image)(resources.GetObject("posBtnGuestOrderKeyboard.Image")));
            this.posBtnGuestOrderKeyboard.Location = new System.Drawing.Point(587, 268);
            this.posBtnGuestOrderKeyboard.Name = "posBtnGuestOrderKeyboard";
            this.posBtnGuestOrderKeyboard.Size = new System.Drawing.Size(78, 75);
            this.posBtnGuestOrderKeyboard.TabIndex = 49;
            this.posBtnGuestOrderKeyboard.UseVisualStyleBackColor = true;
            this.posBtnGuestOrderKeyboard.Click += new System.EventHandler(this.posBtnGuestOrderKeyboard_Click);
            // 
            // posGuestOrderNo
            // 
            this.posGuestOrderNo.HeaderText = "No";
            this.posGuestOrderNo.Name = "posGuestOrderNo";
            this.posGuestOrderNo.ReadOnly = true;
            this.posGuestOrderNo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.posGuestOrderNo.Width = 30;
            // 
            // posGuestOrderItemName
            // 
            this.posGuestOrderItemName.HeaderText = "Item Name";
            this.posGuestOrderItemName.Name = "posGuestOrderItemName";
            this.posGuestOrderItemName.ReadOnly = true;
            this.posGuestOrderItemName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.posGuestOrderItemName.Width = 210;
            // 
            // posGuestOrderItemPrice
            // 
            this.posGuestOrderItemPrice.HeaderText = "Item Price";
            this.posGuestOrderItemPrice.Name = "posGuestOrderItemPrice";
            this.posGuestOrderItemPrice.ReadOnly = true;
            this.posGuestOrderItemPrice.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.posGuestOrderItemPrice.Width = 90;
            // 
            // posGuestOrderItemQty
            // 
            this.posGuestOrderItemQty.HeaderText = "Qty";
            this.posGuestOrderItemQty.Name = "posGuestOrderItemQty";
            this.posGuestOrderItemQty.ReadOnly = true;
            this.posGuestOrderItemQty.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.posGuestOrderItemQty.Width = 50;
            // 
            // posGuestOrderItemTotal
            // 
            this.posGuestOrderItemTotal.HeaderText = "Total";
            this.posGuestOrderItemTotal.Name = "posGuestOrderItemTotal";
            this.posGuestOrderItemTotal.ReadOnly = true;
            // 
            // posGuestOrderItemCode
            // 
            this.posGuestOrderItemCode.HeaderText = "ItemCode";
            this.posGuestOrderItemCode.Name = "posGuestOrderItemCode";
            this.posGuestOrderItemCode.ReadOnly = true;
            this.posGuestOrderItemCode.Visible = false;
            // 
            // posGuestOrderItemCost
            // 
            this.posGuestOrderItemCost.HeaderText = "ItemCost";
            this.posGuestOrderItemCost.Name = "posGuestOrderItemCost";
            this.posGuestOrderItemCost.ReadOnly = true;
            this.posGuestOrderItemCost.Visible = false;
            // 
            // posGuestOrderRemark
            // 
            this.posGuestOrderRemark.HeaderText = "Remark";
            this.posGuestOrderRemark.Name = "posGuestOrderRemark";
            this.posGuestOrderRemark.ReadOnly = true;
            this.posGuestOrderRemark.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.posGuestOrderRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.posGuestOrderRemark.ToolTipText = "Test";
            this.posGuestOrderRemark.Width = 170;
            // 
            // posRemarkUnchangeble
            // 
            this.posRemarkUnchangeble.HeaderText = "Unchangeble Remark";
            this.posRemarkUnchangeble.MaxInputLength = 100;
            this.posRemarkUnchangeble.Name = "posRemarkUnchangeble";
            this.posRemarkUnchangeble.ReadOnly = true;
            this.posRemarkUnchangeble.Visible = false;
            // 
            // posRemarkChangeble
            // 
            this.posRemarkChangeble.HeaderText = "Changeble Remark";
            this.posRemarkChangeble.MaxInputLength = 100;
            this.posRemarkChangeble.Name = "posRemarkChangeble";
            this.posRemarkChangeble.ReadOnly = true;
            this.posRemarkChangeble.Visible = false;
            // 
            // posGuiGuestOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.ControlBox = false;
            this.Controls.Add(this.posGuestOrderGbxRightPanel);
            this.Controls.Add(this.posGuestOrderGbxMenuPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "posGuiGuestOrder";
            this.ShowInTaskbar = false;
            this.Text = "GUEST ORDER";
            this.Load += new System.EventHandler(this.posGuiGuestOrder_Load);
            this.Click += new System.EventHandler(this.posGuiGuestOrder_Click);
            this.Move += new System.EventHandler(this.posGuiGuestOrder_Move);
            ((System.ComponentModel.ISupportInitialize)(this.posDgvGuestOrderOrder)).EndInit();
            this.posGuestOrderGbxMenuPanel.ResumeLayout(false);
            this.posGuestOrderGbxMenuPanel.PerformLayout();
            this.posGuestOrderGbxRightPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.DataGridView posDgvGuestOrderOrder;
        internal System.Windows.Forms.TextBox posTxtHomeGuestName;
        internal System.Windows.Forms.TextBox posTxtGuestOrderSaleType;
        internal System.Windows.Forms.TextBox posTxtGuestOrderKOTBOT;
        internal System.Windows.Forms.TextBox posTxtGuestOrderStewardName;
        internal System.Windows.Forms.TextBox posTxtGuestOrderHOD;
        internal System.Windows.Forms.TextBox posTxtGuestOrderRemarks;
        internal System.Windows.Forms.TextBox posTxtGuestOrderTableNo;
        internal System.Windows.Forms.RadioButton posRdbGuestOrderGuest;
        internal System.Windows.Forms.RadioButton posRdbGuestOrderOthers;
        internal System.Windows.Forms.TextBox posTxtGuestOrderRoom;
        internal System.Windows.Forms.TextBox posGuestOrderErrMsg;
        internal System.Windows.Forms.Button posBtnGuestOrderSave;
        internal System.Windows.Forms.Button posBtnGuestOrderDelete;
        internal System.Windows.Forms.GroupBox posGuestOrderGbxRightPanel;
        internal System.Windows.Forms.GroupBox posGuestOrderGbxMenuPanel;
        internal System.Windows.Forms.Panel posPnlGuestOrderRightPanel;
        internal System.Windows.Forms.Button posBtnGuestOrderOpenTable;
        internal System.Windows.Forms.Button posBtnGuestOrderBill;
        internal System.Windows.Forms.Button posBtnGuestOrderKeyboard;
        internal System.Windows.Forms.Button posBtnGuestOrderVoid;
        internal System.Windows.Forms.Button posBtnGuestOrderNew;
        internal System.Windows.Forms.Button btnOrderCancelClose;
        internal System.Windows.Forms.TextBox posTxtGuestOrderCostCenter;
        internal System.Windows.Forms.TextBox posTxtHomeCostCenterName;
        internal System.Windows.Forms.Label posLblGuestOrderRoom;
        internal System.Windows.Forms.Button btnGuestOrderBeverage;
        internal System.Windows.Forms.Button btnGuestOrderFood;
        internal System.Windows.Forms.Button btnGuestOrderHome;
        internal System.Windows.Forms.Label posLblGuestOrderCostCenter;
        internal System.Windows.Forms.Label posLblGuestOrderHOD;
        internal System.Windows.Forms.Label posLblGuestOrderKOTBOT;
        internal System.Windows.Forms.Label posLblGuestOrderRemarks;
        internal System.Windows.Forms.Label posLblGuestOrderTable;
        internal System.Windows.Forms.Label posLblGuestOrderSaleType;
        internal System.Windows.Forms.Button posBtnGuestOrderRoom;
        internal System.Windows.Forms.Button posBtnGuestOrderSaleType;
        internal System.Windows.Forms.Button button3;
        internal System.Windows.Forms.Button button2;
        internal System.Windows.Forms.Button btnGuestOrderCigarette;
        internal System.Windows.Forms.Button btnGuestOrderOthers;
        internal System.Windows.Forms.TextBox posTxtGuestOrderPax;
        internal System.Windows.Forms.Label posLblGuestOrderPax;
        internal System.Windows.Forms.TextBox posTxtGuestOrderTourName;
        private System.Windows.Forms.DataGridViewTextBoxColumn posGuestOrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn posGuestOrderItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn posGuestOrderItemPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn posGuestOrderItemQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn posGuestOrderItemTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn posGuestOrderItemCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn posGuestOrderItemCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn posGuestOrderRemark;
        private System.Windows.Forms.DataGridViewTextBoxColumn posRemarkUnchangeble;
        private System.Windows.Forms.DataGridViewTextBoxColumn posRemarkChangeble;
    }
}