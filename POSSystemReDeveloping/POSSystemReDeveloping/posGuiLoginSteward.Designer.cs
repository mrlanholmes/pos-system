﻿namespace POSSystemReDeveloping
{
    partial class posGuiLoginSteward
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(posGuiLoginSteward));
            this.posLblLoginStewardPassword = new System.Windows.Forms.Label();
            this.posTxtLoginStewardPassword = new System.Windows.Forms.TextBox();
            this.posBtnLoginSteward = new System.Windows.Forms.Button();
            this.posBtnStewardLoginKeyboard = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // posLblLoginStewardPassword
            // 
            this.posLblLoginStewardPassword.AutoSize = true;
            this.posLblLoginStewardPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblLoginStewardPassword.Location = new System.Drawing.Point(3, 6);
            this.posLblLoginStewardPassword.Name = "posLblLoginStewardPassword";
            this.posLblLoginStewardPassword.Size = new System.Drawing.Size(168, 24);
            this.posLblLoginStewardPassword.TabIndex = 8;
            this.posLblLoginStewardPassword.Text = "Password Code :";
            // 
            // posTxtLoginStewardPassword
            // 
            this.posTxtLoginStewardPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtLoginStewardPassword.ForeColor = System.Drawing.Color.Maroon;
            this.posTxtLoginStewardPassword.Location = new System.Drawing.Point(172, 3);
            this.posTxtLoginStewardPassword.Name = "posTxtLoginStewardPassword";
            this.posTxtLoginStewardPassword.PasswordChar = '*';
            this.posTxtLoginStewardPassword.Size = new System.Drawing.Size(234, 35);
            this.posTxtLoginStewardPassword.TabIndex = 0;
            this.posTxtLoginStewardPassword.WordWrap = false;
            // 
            // posBtnLoginSteward
            // 
            this.posBtnLoginSteward.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnLoginSteward.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.posBtnLoginSteward.Location = new System.Drawing.Point(290, 40);
            this.posBtnLoginSteward.Name = "posBtnLoginSteward";
            this.posBtnLoginSteward.Size = new System.Drawing.Size(116, 44);
            this.posBtnLoginSteward.TabIndex = 1;
            this.posBtnLoginSteward.Text = "Login";
            this.posBtnLoginSteward.UseVisualStyleBackColor = true;
            this.posBtnLoginSteward.Click += new System.EventHandler(this.btnLoginSteward_Click);
            // 
            // posBtnStewardLoginKeyboard
            // 
            this.posBtnStewardLoginKeyboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnStewardLoginKeyboard.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.posBtnStewardLoginKeyboard.Image = ((System.Drawing.Image)(resources.GetObject("posBtnStewardLoginKeyboard.Image")));
            this.posBtnStewardLoginKeyboard.Location = new System.Drawing.Point(172, 40);
            this.posBtnStewardLoginKeyboard.Name = "posBtnStewardLoginKeyboard";
            this.posBtnStewardLoginKeyboard.Size = new System.Drawing.Size(116, 44);
            this.posBtnStewardLoginKeyboard.TabIndex = 2;
            this.posBtnStewardLoginKeyboard.UseVisualStyleBackColor = true;
            // 
            // posGuiLoginSteward
            // 
            this.AcceptButton = this.posBtnLoginSteward;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 85);
            this.ControlBox = false;
            this.Controls.Add(this.posBtnStewardLoginKeyboard);
            this.Controls.Add(this.posLblLoginStewardPassword);
            this.Controls.Add(this.posTxtLoginStewardPassword);
            this.Controls.Add(this.posBtnLoginSteward);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "posGuiLoginSteward";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "STEWARD LOGIN";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label posLblLoginStewardPassword;
        internal System.Windows.Forms.TextBox posTxtLoginStewardPassword;
        internal System.Windows.Forms.Button posBtnLoginSteward;
        internal System.Windows.Forms.Button posBtnStewardLoginKeyboard;
    }
}