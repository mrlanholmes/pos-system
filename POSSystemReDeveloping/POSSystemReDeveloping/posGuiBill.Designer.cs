﻿namespace POSSystemReDeveloping
{
    partial class posGuiBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.posDgvBillTableDetails = new System.Windows.Forms.DataGridView();
            this.posBtnBillHome = new System.Windows.Forms.Button();
            this.posTxtBillOrderNo = new System.Windows.Forms.TextBox();
            this.posLblBillBillNo = new System.Windows.Forms.Label();
            this.posDgvBillTableItemDetails = new System.Windows.Forms.DataGridView();
            this.posLblBillTotal = new System.Windows.Forms.Label();
            this.posTxtBillTotal = new System.Windows.Forms.TextBox();
            this.posLblBillTableNo = new System.Windows.Forms.Label();
            this.posTxtBillTableNo = new System.Windows.Forms.TextBox();
            this.posLblBillSaleType = new System.Windows.Forms.Label();
            this.posLblBillSteward = new System.Windows.Forms.Label();
            this.posLblBillRemarks = new System.Windows.Forms.Label();
            this.posLblBillSerial = new System.Windows.Forms.Label();
            this.posLblBillCostCenter = new System.Windows.Forms.Label();
            this.posLblBillRoom = new System.Windows.Forms.Label();
            this.posLblBillStatus = new System.Windows.Forms.Label();
            this.posLblBillCashier = new System.Windows.Forms.Label();
            this.posTxtBillSteward = new System.Windows.Forms.TextBox();
            this.posTxtBillRemarks = new System.Windows.Forms.TextBox();
            this.posTxtBillSerialNo = new System.Windows.Forms.TextBox();
            this.posTxtBillSaleType = new System.Windows.Forms.TextBox();
            this.posTxtBillCostCenter = new System.Windows.Forms.TextBox();
            this.posTxtBillRoom = new System.Windows.Forms.TextBox();
            this.posTxtBillStatus = new System.Windows.Forms.TextBox();
            this.posTxtBillCashier = new System.Windows.Forms.TextBox();
            this.posTxtBillDate = new System.Windows.Forms.TextBox();
            this.posLblBillCess = new System.Windows.Forms.Label();
            this.posLblBillVatAmt = new System.Windows.Forms.Label();
            this.posLblBillActDis = new System.Windows.Forms.Label();
            this.posLblBillVat = new System.Windows.Forms.Label();
            this.posLblBillSubTotal = new System.Windows.Forms.Label();
            this.posLblBillScAmt = new System.Windows.Forms.Label();
            this.posLblBillSc = new System.Windows.Forms.Label();
            this.posLblBillDiscAmt = new System.Windows.Forms.Label();
            this.posLblBillDiscount = new System.Windows.Forms.Label();
            this.posLblBillCessAmt = new System.Windows.Forms.Label();
            this.posTxtBillCess = new System.Windows.Forms.TextBox();
            this.posTxtBillCessAmt = new System.Windows.Forms.TextBox();
            this.posTxtBillDiscount = new System.Windows.Forms.TextBox();
            this.posTxtBillDiscAmt = new System.Windows.Forms.TextBox();
            this.posTxtBillSc = new System.Windows.Forms.TextBox();
            this.posTxtBillScAmt = new System.Windows.Forms.TextBox();
            this.posTxtBillSubTotal = new System.Windows.Forms.TextBox();
            this.posTxtBillVat = new System.Windows.Forms.TextBox();
            this.posTxtBillVatAmt = new System.Windows.Forms.TextBox();
            this.posTxtBillActDis = new System.Windows.Forms.TextBox();
            this.posBtnBillDiscount = new System.Windows.Forms.Button();
            this.posBtnBillCalculateDiscount = new System.Windows.Forms.Button();
            this.posBtnBillBillSeperate = new System.Windows.Forms.Button();
            this.posBtnBillSave = new System.Windows.Forms.Button();
            this.posTxtBillCustomerType = new System.Windows.Forms.TextBox();
            this.posLblBillCustomerType = new System.Windows.Forms.Label();
            this.posGbxBillTotalAmout = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.posGbxBillDetails = new System.Windows.Forms.GroupBox();
            this.posTxtBillDgvItemTotal = new System.Windows.Forms.TextBox();
            this.posTxtBillTableDetailsKOTBOT = new System.Windows.Forms.TextBox();
            this.posDgvBillTableNumber = new System.Windows.Forms.DataGridView();
            this.posBtnBillKeyBoard = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillTableDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillTableItemDetails)).BeginInit();
            this.posGbxBillTotalAmout.SuspendLayout();
            this.posGbxBillDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillTableNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // posDgvBillTableDetails
            // 
            this.posDgvBillTableDetails.AllowUserToAddRows = false;
            this.posDgvBillTableDetails.BackgroundColor = System.Drawing.SystemColors.Control;
            this.posDgvBillTableDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvBillTableDetails.Location = new System.Drawing.Point(98, 460);
            this.posDgvBillTableDetails.Name = "posDgvBillTableDetails";
            this.posDgvBillTableDetails.Size = new System.Drawing.Size(463, 223);
            this.posDgvBillTableDetails.TabIndex = 0;
            // 
            // posBtnBillHome
            // 
            this.posBtnBillHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnBillHome.Location = new System.Drawing.Point(1, 8);
            this.posBtnBillHome.Name = "posBtnBillHome";
            this.posBtnBillHome.Size = new System.Drawing.Size(91, 76);
            this.posBtnBillHome.TabIndex = 2;
            this.posBtnBillHome.Text = "Home";
            this.posBtnBillHome.UseVisualStyleBackColor = true;
            this.posBtnBillHome.Click += new System.EventHandler(this.posBtnBillHome_Click);
            // 
            // posTxtBillOrderNo
            // 
            this.posTxtBillOrderNo.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillOrderNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillOrderNo.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillOrderNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillOrderNo.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillOrderNo.Location = new System.Drawing.Point(123, 20);
            this.posTxtBillOrderNo.Multiline = true;
            this.posTxtBillOrderNo.Name = "posTxtBillOrderNo";
            this.posTxtBillOrderNo.ReadOnly = true;
            this.posTxtBillOrderNo.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillOrderNo.TabIndex = 3;
            // 
            // posLblBillBillNo
            // 
            this.posLblBillBillNo.AutoSize = true;
            this.posLblBillBillNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillBillNo.Location = new System.Drawing.Point(36, 26);
            this.posLblBillBillNo.Name = "posLblBillBillNo";
            this.posLblBillBillNo.Size = new System.Drawing.Size(83, 24);
            this.posLblBillBillNo.TabIndex = 4;
            this.posLblBillBillNo.Text = "Bill No :";
            // 
            // posDgvBillTableItemDetails
            // 
            this.posDgvBillTableItemDetails.AllowUserToAddRows = false;
            this.posDgvBillTableItemDetails.BackgroundColor = System.Drawing.SystemColors.Control;
            this.posDgvBillTableItemDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvBillTableItemDetails.Location = new System.Drawing.Point(567, 460);
            this.posDgvBillTableItemDetails.Name = "posDgvBillTableItemDetails";
            this.posDgvBillTableItemDetails.Size = new System.Drawing.Size(429, 223);
            this.posDgvBillTableItemDetails.TabIndex = 5;
            // 
            // posLblBillTotal
            // 
            this.posLblBillTotal.AutoSize = true;
            this.posLblBillTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillTotal.Location = new System.Drawing.Point(614, 115);
            this.posLblBillTotal.Name = "posLblBillTotal";
            this.posLblBillTotal.Size = new System.Drawing.Size(68, 24);
            this.posLblBillTotal.TabIndex = 6;
            this.posLblBillTotal.Text = "Total :";
            // 
            // posTxtBillTotal
            // 
            this.posTxtBillTotal.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillTotal.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillTotal.ForeColor = System.Drawing.Color.IndianRed;
            this.posTxtBillTotal.Location = new System.Drawing.Point(688, 108);
            this.posTxtBillTotal.Multiline = true;
            this.posTxtBillTotal.Name = "posTxtBillTotal";
            this.posTxtBillTotal.ReadOnly = true;
            this.posTxtBillTotal.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillTotal.TabIndex = 7;
            // 
            // posLblBillTableNo
            // 
            this.posLblBillTableNo.AutoSize = true;
            this.posLblBillTableNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillTableNo.Location = new System.Drawing.Point(11, 69);
            this.posLblBillTableNo.Name = "posLblBillTableNo";
            this.posLblBillTableNo.Size = new System.Drawing.Size(108, 24);
            this.posLblBillTableNo.TabIndex = 8;
            this.posLblBillTableNo.Text = "Table No :";
            // 
            // posTxtBillTableNo
            // 
            this.posTxtBillTableNo.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillTableNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillTableNo.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillTableNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillTableNo.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillTableNo.Location = new System.Drawing.Point(123, 63);
            this.posTxtBillTableNo.Multiline = true;
            this.posTxtBillTableNo.Name = "posTxtBillTableNo";
            this.posTxtBillTableNo.ReadOnly = true;
            this.posTxtBillTableNo.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillTableNo.TabIndex = 9;
            // 
            // posLblBillSaleType
            // 
            this.posLblBillSaleType.AutoSize = true;
            this.posLblBillSaleType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillSaleType.Location = new System.Drawing.Point(4, 155);
            this.posLblBillSaleType.Name = "posLblBillSaleType";
            this.posLblBillSaleType.Size = new System.Drawing.Size(116, 24);
            this.posLblBillSaleType.TabIndex = 10;
            this.posLblBillSaleType.Text = "Sale Type :";
            // 
            // posLblBillSteward
            // 
            this.posLblBillSteward.AutoSize = true;
            this.posLblBillSteward.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillSteward.Location = new System.Drawing.Point(287, 108);
            this.posLblBillSteward.Name = "posLblBillSteward";
            this.posLblBillSteward.Size = new System.Drawing.Size(97, 24);
            this.posLblBillSteward.TabIndex = 11;
            this.posLblBillSteward.Text = "Steward :";
            // 
            // posLblBillRemarks
            // 
            this.posLblBillRemarks.AutoSize = true;
            this.posLblBillRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillRemarks.Location = new System.Drawing.Point(280, 159);
            this.posLblBillRemarks.Name = "posLblBillRemarks";
            this.posLblBillRemarks.Size = new System.Drawing.Size(103, 24);
            this.posLblBillRemarks.TabIndex = 12;
            this.posLblBillRemarks.Text = "Remarks :";
            // 
            // posLblBillSerial
            // 
            this.posLblBillSerial.AutoSize = true;
            this.posLblBillSerial.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillSerial.Location = new System.Drawing.Point(573, 159);
            this.posLblBillSerial.Name = "posLblBillSerial";
            this.posLblBillSerial.Size = new System.Drawing.Size(108, 24);
            this.posLblBillSerial.TabIndex = 13;
            this.posLblBillSerial.Text = "Serial No :";
            // 
            // posLblBillCostCenter
            // 
            this.posLblBillCostCenter.AutoSize = true;
            this.posLblBillCostCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillCostCenter.Location = new System.Drawing.Point(253, 66);
            this.posLblBillCostCenter.Name = "posLblBillCostCenter";
            this.posLblBillCostCenter.Size = new System.Drawing.Size(131, 24);
            this.posLblBillCostCenter.TabIndex = 14;
            this.posLblBillCostCenter.Text = "Cost Center :";
            // 
            // posLblBillRoom
            // 
            this.posLblBillRoom.AutoSize = true;
            this.posLblBillRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillRoom.Location = new System.Drawing.Point(6, 112);
            this.posLblBillRoom.Name = "posLblBillRoom";
            this.posLblBillRoom.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.posLblBillRoom.Size = new System.Drawing.Size(113, 24);
            this.posLblBillRoom.TabIndex = 15;
            this.posLblBillRoom.Text = "      Room :";
            // 
            // posLblBillStatus
            // 
            this.posLblBillStatus.AutoSize = true;
            this.posLblBillStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillStatus.Location = new System.Drawing.Point(307, 23);
            this.posLblBillStatus.Name = "posLblBillStatus";
            this.posLblBillStatus.Size = new System.Drawing.Size(78, 24);
            this.posLblBillStatus.TabIndex = 17;
            this.posLblBillStatus.Text = "Status :";
            // 
            // posLblBillCashier
            // 
            this.posLblBillCashier.AutoSize = true;
            this.posLblBillCashier.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillCashier.Location = new System.Drawing.Point(589, 66);
            this.posLblBillCashier.Name = "posLblBillCashier";
            this.posLblBillCashier.Size = new System.Drawing.Size(93, 24);
            this.posLblBillCashier.TabIndex = 18;
            this.posLblBillCashier.Text = "Cashier :";
            // 
            // posTxtBillSteward
            // 
            this.posTxtBillSteward.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillSteward.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillSteward.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillSteward.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillSteward.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillSteward.Location = new System.Drawing.Point(389, 103);
            this.posTxtBillSteward.Multiline = true;
            this.posTxtBillSteward.Name = "posTxtBillSteward";
            this.posTxtBillSteward.ReadOnly = true;
            this.posTxtBillSteward.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillSteward.TabIndex = 20;
            // 
            // posTxtBillRemarks
            // 
            this.posTxtBillRemarks.BackColor = System.Drawing.Color.White;
            this.posTxtBillRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillRemarks.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillRemarks.Location = new System.Drawing.Point(389, 153);
            this.posTxtBillRemarks.MaxLength = 200;
            this.posTxtBillRemarks.Name = "posTxtBillRemarks";
            this.posTxtBillRemarks.Size = new System.Drawing.Size(129, 38);
            this.posTxtBillRemarks.TabIndex = 21;
            this.posTxtBillRemarks.WordWrap = false;
            // 
            // posTxtBillSerialNo
            // 
            this.posTxtBillSerialNo.BackColor = System.Drawing.Color.White;
            this.posTxtBillSerialNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillSerialNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillSerialNo.ForeColor = System.Drawing.Color.Black;
            this.posTxtBillSerialNo.Location = new System.Drawing.Point(686, 153);
            this.posTxtBillSerialNo.MaxLength = 10;
            this.posTxtBillSerialNo.Name = "posTxtBillSerialNo";
            this.posTxtBillSerialNo.Size = new System.Drawing.Size(129, 38);
            this.posTxtBillSerialNo.TabIndex = 22;
            this.posTxtBillSerialNo.WordWrap = false;
            // 
            // posTxtBillSaleType
            // 
            this.posTxtBillSaleType.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillSaleType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillSaleType.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillSaleType.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillSaleType.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillSaleType.Location = new System.Drawing.Point(123, 149);
            this.posTxtBillSaleType.Multiline = true;
            this.posTxtBillSaleType.Name = "posTxtBillSaleType";
            this.posTxtBillSaleType.ReadOnly = true;
            this.posTxtBillSaleType.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillSaleType.TabIndex = 23;
            // 
            // posTxtBillCostCenter
            // 
            this.posTxtBillCostCenter.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillCostCenter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillCostCenter.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillCostCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillCostCenter.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillCostCenter.Location = new System.Drawing.Point(389, 60);
            this.posTxtBillCostCenter.Multiline = true;
            this.posTxtBillCostCenter.Name = "posTxtBillCostCenter";
            this.posTxtBillCostCenter.ReadOnly = true;
            this.posTxtBillCostCenter.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillCostCenter.TabIndex = 24;
            // 
            // posTxtBillRoom
            // 
            this.posTxtBillRoom.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillRoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillRoom.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillRoom.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillRoom.Location = new System.Drawing.Point(123, 106);
            this.posTxtBillRoom.Multiline = true;
            this.posTxtBillRoom.Name = "posTxtBillRoom";
            this.posTxtBillRoom.ReadOnly = true;
            this.posTxtBillRoom.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillRoom.TabIndex = 25;
            // 
            // posTxtBillStatus
            // 
            this.posTxtBillStatus.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillStatus.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillStatus.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillStatus.Location = new System.Drawing.Point(389, 17);
            this.posTxtBillStatus.Multiline = true;
            this.posTxtBillStatus.Name = "posTxtBillStatus";
            this.posTxtBillStatus.ReadOnly = true;
            this.posTxtBillStatus.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillStatus.TabIndex = 27;
            // 
            // posTxtBillCashier
            // 
            this.posTxtBillCashier.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillCashier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillCashier.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillCashier.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillCashier.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillCashier.Location = new System.Drawing.Point(686, 60);
            this.posTxtBillCashier.Multiline = true;
            this.posTxtBillCashier.Name = "posTxtBillCashier";
            this.posTxtBillCashier.ReadOnly = true;
            this.posTxtBillCashier.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillCashier.TabIndex = 28;
            // 
            // posTxtBillDate
            // 
            this.posTxtBillDate.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillDate.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillDate.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillDate.Location = new System.Drawing.Point(524, 103);
            this.posTxtBillDate.Multiline = true;
            this.posTxtBillDate.Name = "posTxtBillDate";
            this.posTxtBillDate.ReadOnly = true;
            this.posTxtBillDate.Size = new System.Drawing.Size(291, 37);
            this.posTxtBillDate.TabIndex = 29;
            this.posTxtBillDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.posTxtBillDate.WordWrap = false;
            // 
            // posLblBillCess
            // 
            this.posLblBillCess.AutoSize = true;
            this.posLblBillCess.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillCess.Location = new System.Drawing.Point(53, 26);
            this.posLblBillCess.Name = "posLblBillCess";
            this.posLblBillCess.Size = new System.Drawing.Size(68, 24);
            this.posLblBillCess.TabIndex = 30;
            this.posLblBillCess.Text = "Cess :";
            // 
            // posLblBillVatAmt
            // 
            this.posLblBillVatAmt.AutoSize = true;
            this.posLblBillVatAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillVatAmt.Location = new System.Drawing.Point(290, 154);
            this.posLblBillVatAmt.Name = "posLblBillVatAmt";
            this.posLblBillVatAmt.Size = new System.Drawing.Size(94, 24);
            this.posLblBillVatAmt.TabIndex = 31;
            this.posLblBillVatAmt.Text = "Vat Amt :";
            // 
            // posLblBillActDis
            // 
            this.posLblBillActDis.AutoSize = true;
            this.posLblBillActDis.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillActDis.Location = new System.Drawing.Point(595, 25);
            this.posLblBillActDis.Name = "posLblBillActDis";
            this.posLblBillActDis.Size = new System.Drawing.Size(87, 24);
            this.posLblBillActDis.TabIndex = 32;
            this.posLblBillActDis.Text = "Act Dis :";
            // 
            // posLblBillVat
            // 
            this.posLblBillVat.AutoSize = true;
            this.posLblBillVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillVat.Location = new System.Drawing.Point(310, 110);
            this.posLblBillVat.Name = "posLblBillVat";
            this.posLblBillVat.Size = new System.Drawing.Size(74, 24);
            this.posLblBillVat.TabIndex = 33;
            this.posLblBillVat.Text = "Vat % :";
            // 
            // posLblBillSubTotal
            // 
            this.posLblBillSubTotal.AutoSize = true;
            this.posLblBillSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillSubTotal.Location = new System.Drawing.Point(571, 72);
            this.posLblBillSubTotal.Name = "posLblBillSubTotal";
            this.posLblBillSubTotal.Size = new System.Drawing.Size(111, 24);
            this.posLblBillSubTotal.TabIndex = 34;
            this.posLblBillSubTotal.Text = "Sub Total :";
            // 
            // posLblBillScAmt
            // 
            this.posLblBillScAmt.AutoSize = true;
            this.posLblBillScAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillScAmt.Location = new System.Drawing.Point(294, 68);
            this.posLblBillScAmt.Name = "posLblBillScAmt";
            this.posLblBillScAmt.Size = new System.Drawing.Size(91, 24);
            this.posLblBillScAmt.TabIndex = 35;
            this.posLblBillScAmt.Text = "SC Amt :";
            // 
            // posLblBillSc
            // 
            this.posLblBillSc.AutoSize = true;
            this.posLblBillSc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillSc.Location = new System.Drawing.Point(314, 26);
            this.posLblBillSc.Name = "posLblBillSc";
            this.posLblBillSc.Size = new System.Drawing.Size(71, 24);
            this.posLblBillSc.TabIndex = 36;
            this.posLblBillSc.Text = "SC % :";
            // 
            // posLblBillDiscAmt
            // 
            this.posLblBillDiscAmt.AutoSize = true;
            this.posLblBillDiscAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillDiscAmt.Location = new System.Drawing.Point(15, 154);
            this.posLblBillDiscAmt.Name = "posLblBillDiscAmt";
            this.posLblBillDiscAmt.Size = new System.Drawing.Size(104, 24);
            this.posLblBillDiscAmt.TabIndex = 37;
            this.posLblBillDiscAmt.Text = "Disc Amt :";
            // 
            // posLblBillDiscount
            // 
            this.posLblBillDiscount.AutoSize = true;
            this.posLblBillDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillDiscount.Location = new System.Drawing.Point(-4, 112);
            this.posLblBillDiscount.Name = "posLblBillDiscount";
            this.posLblBillDiscount.Size = new System.Drawing.Size(125, 24);
            this.posLblBillDiscount.TabIndex = 38;
            this.posLblBillDiscount.Text = "Discount  %:";
            this.posLblBillDiscount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // posLblBillCessAmt
            // 
            this.posLblBillCessAmt.AutoSize = true;
            this.posLblBillCessAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillCessAmt.Location = new System.Drawing.Point(11, 69);
            this.posLblBillCessAmt.Name = "posLblBillCessAmt";
            this.posLblBillCessAmt.Size = new System.Drawing.Size(110, 24);
            this.posLblBillCessAmt.TabIndex = 39;
            this.posLblBillCessAmt.Text = "Cess Amt :";
            // 
            // posTxtBillCess
            // 
            this.posTxtBillCess.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillCess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillCess.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillCess.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillCess.ForeColor = System.Drawing.Color.IndianRed;
            this.posTxtBillCess.Location = new System.Drawing.Point(125, 18);
            this.posTxtBillCess.Multiline = true;
            this.posTxtBillCess.Name = "posTxtBillCess";
            this.posTxtBillCess.ReadOnly = true;
            this.posTxtBillCess.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillCess.TabIndex = 40;
            // 
            // posTxtBillCessAmt
            // 
            this.posTxtBillCessAmt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillCessAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillCessAmt.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillCessAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillCessAmt.ForeColor = System.Drawing.Color.IndianRed;
            this.posTxtBillCessAmt.Location = new System.Drawing.Point(125, 62);
            this.posTxtBillCessAmt.Multiline = true;
            this.posTxtBillCessAmt.Name = "posTxtBillCessAmt";
            this.posTxtBillCessAmt.ReadOnly = true;
            this.posTxtBillCessAmt.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillCessAmt.TabIndex = 41;
            // 
            // posTxtBillDiscount
            // 
            this.posTxtBillDiscount.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillDiscount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillDiscount.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillDiscount.Enabled = false;
            this.posTxtBillDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillDiscount.ForeColor = System.Drawing.Color.IndianRed;
            this.posTxtBillDiscount.Location = new System.Drawing.Point(125, 103);
            this.posTxtBillDiscount.MaxLength = 2;
            this.posTxtBillDiscount.Name = "posTxtBillDiscount";
            this.posTxtBillDiscount.Size = new System.Drawing.Size(129, 38);
            this.posTxtBillDiscount.TabIndex = 42;
            // 
            // posTxtBillDiscAmt
            // 
            this.posTxtBillDiscAmt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillDiscAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillDiscAmt.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillDiscAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillDiscAmt.ForeColor = System.Drawing.Color.IndianRed;
            this.posTxtBillDiscAmt.Location = new System.Drawing.Point(125, 147);
            this.posTxtBillDiscAmt.Multiline = true;
            this.posTxtBillDiscAmt.Name = "posTxtBillDiscAmt";
            this.posTxtBillDiscAmt.ReadOnly = true;
            this.posTxtBillDiscAmt.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillDiscAmt.TabIndex = 43;
            // 
            // posTxtBillSc
            // 
            this.posTxtBillSc.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillSc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillSc.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillSc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillSc.ForeColor = System.Drawing.Color.IndianRed;
            this.posTxtBillSc.Location = new System.Drawing.Point(390, 19);
            this.posTxtBillSc.Multiline = true;
            this.posTxtBillSc.Name = "posTxtBillSc";
            this.posTxtBillSc.ReadOnly = true;
            this.posTxtBillSc.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillSc.TabIndex = 44;
            // 
            // posTxtBillScAmt
            // 
            this.posTxtBillScAmt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillScAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillScAmt.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillScAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillScAmt.ForeColor = System.Drawing.Color.IndianRed;
            this.posTxtBillScAmt.Location = new System.Drawing.Point(390, 62);
            this.posTxtBillScAmt.Multiline = true;
            this.posTxtBillScAmt.Name = "posTxtBillScAmt";
            this.posTxtBillScAmt.ReadOnly = true;
            this.posTxtBillScAmt.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillScAmt.TabIndex = 45;
            // 
            // posTxtBillSubTotal
            // 
            this.posTxtBillSubTotal.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillSubTotal.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillSubTotal.ForeColor = System.Drawing.Color.IndianRed;
            this.posTxtBillSubTotal.Location = new System.Drawing.Point(688, 65);
            this.posTxtBillSubTotal.Multiline = true;
            this.posTxtBillSubTotal.Name = "posTxtBillSubTotal";
            this.posTxtBillSubTotal.ReadOnly = true;
            this.posTxtBillSubTotal.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillSubTotal.TabIndex = 46;
            // 
            // posTxtBillVat
            // 
            this.posTxtBillVat.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillVat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillVat.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillVat.ForeColor = System.Drawing.Color.IndianRed;
            this.posTxtBillVat.Location = new System.Drawing.Point(390, 105);
            this.posTxtBillVat.Multiline = true;
            this.posTxtBillVat.Name = "posTxtBillVat";
            this.posTxtBillVat.ReadOnly = true;
            this.posTxtBillVat.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillVat.TabIndex = 47;
            // 
            // posTxtBillVatAmt
            // 
            this.posTxtBillVatAmt.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillVatAmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillVatAmt.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillVatAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillVatAmt.ForeColor = System.Drawing.Color.IndianRed;
            this.posTxtBillVatAmt.Location = new System.Drawing.Point(390, 148);
            this.posTxtBillVatAmt.Multiline = true;
            this.posTxtBillVatAmt.Name = "posTxtBillVatAmt";
            this.posTxtBillVatAmt.ReadOnly = true;
            this.posTxtBillVatAmt.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillVatAmt.TabIndex = 48;
            // 
            // posTxtBillActDis
            // 
            this.posTxtBillActDis.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillActDis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillActDis.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillActDis.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillActDis.ForeColor = System.Drawing.Color.IndianRed;
            this.posTxtBillActDis.Location = new System.Drawing.Point(688, 18);
            this.posTxtBillActDis.Multiline = true;
            this.posTxtBillActDis.Name = "posTxtBillActDis";
            this.posTxtBillActDis.ReadOnly = true;
            this.posTxtBillActDis.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillActDis.TabIndex = 49;
            // 
            // posBtnBillDiscount
            // 
            this.posBtnBillDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnBillDiscount.Location = new System.Drawing.Point(96, 411);
            this.posBtnBillDiscount.Name = "posBtnBillDiscount";
            this.posBtnBillDiscount.Size = new System.Drawing.Size(309, 42);
            this.posBtnBillDiscount.TabIndex = 50;
            this.posBtnBillDiscount.Text = "Discount";
            this.posBtnBillDiscount.UseVisualStyleBackColor = true;
            // 
            // posBtnBillCalculateDiscount
            // 
            this.posBtnBillCalculateDiscount.Enabled = false;
            this.posBtnBillCalculateDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnBillCalculateDiscount.Location = new System.Drawing.Point(409, 411);
            this.posBtnBillCalculateDiscount.Name = "posBtnBillCalculateDiscount";
            this.posBtnBillCalculateDiscount.Size = new System.Drawing.Size(304, 42);
            this.posBtnBillCalculateDiscount.TabIndex = 51;
            this.posBtnBillCalculateDiscount.Text = "Calculate Discount";
            this.posBtnBillCalculateDiscount.UseVisualStyleBackColor = true;
            this.posBtnBillCalculateDiscount.Click += new System.EventHandler(this.posBtnBillCalculateDiscount_Click);
            // 
            // posBtnBillBillSeperate
            // 
            this.posBtnBillBillSeperate.Enabled = false;
            this.posBtnBillBillSeperate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnBillBillSeperate.Location = new System.Drawing.Point(718, 411);
            this.posBtnBillBillSeperate.Name = "posBtnBillBillSeperate";
            this.posBtnBillBillSeperate.Size = new System.Drawing.Size(279, 42);
            this.posBtnBillBillSeperate.TabIndex = 52;
            this.posBtnBillBillSeperate.Text = "Bill Seperate";
            this.posBtnBillBillSeperate.UseVisualStyleBackColor = true;
            this.posBtnBillBillSeperate.Click += new System.EventHandler(this.posBtnBillBillSeperate_Click);
            // 
            // posBtnBillSave
            // 
            this.posBtnBillSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnBillSave.Location = new System.Drawing.Point(1, 91);
            this.posBtnBillSave.Name = "posBtnBillSave";
            this.posBtnBillSave.Size = new System.Drawing.Size(91, 77);
            this.posBtnBillSave.TabIndex = 53;
            this.posBtnBillSave.Text = "Save";
            this.posBtnBillSave.UseVisualStyleBackColor = true;
            this.posBtnBillSave.Click += new System.EventHandler(this.posBtnBillSave_Click);
            // 
            // posTxtBillCustomerType
            // 
            this.posTxtBillCustomerType.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillCustomerType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillCustomerType.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillCustomerType.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillCustomerType.Location = new System.Drawing.Point(686, 17);
            this.posTxtBillCustomerType.Multiline = true;
            this.posTxtBillCustomerType.Name = "posTxtBillCustomerType";
            this.posTxtBillCustomerType.Size = new System.Drawing.Size(129, 37);
            this.posTxtBillCustomerType.TabIndex = 55;
            // 
            // posLblBillCustomerType
            // 
            this.posLblBillCustomerType.AutoSize = true;
            this.posLblBillCustomerType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblBillCustomerType.Location = new System.Drawing.Point(518, 23);
            this.posLblBillCustomerType.Name = "posLblBillCustomerType";
            this.posLblBillCustomerType.Size = new System.Drawing.Size(164, 24);
            this.posLblBillCustomerType.TabIndex = 54;
            this.posLblBillCustomerType.Text = "Customor Type :";
            // 
            // posGbxBillTotalAmout
            // 
            this.posGbxBillTotalAmout.Controls.Add(this.panel1);
            this.posGbxBillTotalAmout.Controls.Add(this.posTxtBillSc);
            this.posGbxBillTotalAmout.Controls.Add(this.posLblBillTotal);
            this.posGbxBillTotalAmout.Controls.Add(this.posTxtBillTotal);
            this.posGbxBillTotalAmout.Controls.Add(this.posLblBillCess);
            this.posGbxBillTotalAmout.Controls.Add(this.posLblBillActDis);
            this.posGbxBillTotalAmout.Controls.Add(this.posLblBillVat);
            this.posGbxBillTotalAmout.Controls.Add(this.posLblBillSubTotal);
            this.posGbxBillTotalAmout.Controls.Add(this.posTxtBillVatAmt);
            this.posGbxBillTotalAmout.Controls.Add(this.posTxtBillDiscAmt);
            this.posGbxBillTotalAmout.Controls.Add(this.posTxtBillActDis);
            this.posGbxBillTotalAmout.Controls.Add(this.posLblBillDiscAmt);
            this.posGbxBillTotalAmout.Controls.Add(this.posLblBillScAmt);
            this.posGbxBillTotalAmout.Controls.Add(this.posLblBillVatAmt);
            this.posGbxBillTotalAmout.Controls.Add(this.posLblBillSc);
            this.posGbxBillTotalAmout.Controls.Add(this.posTxtBillVat);
            this.posGbxBillTotalAmout.Controls.Add(this.posTxtBillSubTotal);
            this.posGbxBillTotalAmout.Controls.Add(this.posLblBillCessAmt);
            this.posGbxBillTotalAmout.Controls.Add(this.posTxtBillScAmt);
            this.posGbxBillTotalAmout.Controls.Add(this.posTxtBillCess);
            this.posGbxBillTotalAmout.Controls.Add(this.posTxtBillCessAmt);
            this.posGbxBillTotalAmout.Controls.Add(this.posTxtBillDiscount);
            this.posGbxBillTotalAmout.Controls.Add(this.posLblBillDiscount);
            this.posGbxBillTotalAmout.Location = new System.Drawing.Point(97, 203);
            this.posGbxBillTotalAmout.Name = "posGbxBillTotalAmout";
            this.posGbxBillTotalAmout.Size = new System.Drawing.Size(900, 201);
            this.posGbxBillTotalAmout.TabIndex = 56;
            this.posGbxBillTotalAmout.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(830, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(59, 148);
            this.panel1.TabIndex = 50;
            // 
            // posGbxBillDetails
            // 
            this.posGbxBillDetails.Controls.Add(this.posLblBillBillNo);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillOrderNo);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillCustomerType);
            this.posGbxBillDetails.Controls.Add(this.posLblBillTableNo);
            this.posGbxBillDetails.Controls.Add(this.posLblBillCustomerType);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillTableNo);
            this.posGbxBillDetails.Controls.Add(this.posLblBillSaleType);
            this.posGbxBillDetails.Controls.Add(this.posLblBillSteward);
            this.posGbxBillDetails.Controls.Add(this.posLblBillRemarks);
            this.posGbxBillDetails.Controls.Add(this.posLblBillSerial);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillDate);
            this.posGbxBillDetails.Controls.Add(this.posLblBillCostCenter);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillCashier);
            this.posGbxBillDetails.Controls.Add(this.posLblBillRoom);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillStatus);
            this.posGbxBillDetails.Controls.Add(this.posLblBillStatus);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillRoom);
            this.posGbxBillDetails.Controls.Add(this.posLblBillCashier);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillCostCenter);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillSaleType);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillSteward);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillSerialNo);
            this.posGbxBillDetails.Controls.Add(this.posTxtBillRemarks);
            this.posGbxBillDetails.Location = new System.Drawing.Point(98, 8);
            this.posGbxBillDetails.Name = "posGbxBillDetails";
            this.posGbxBillDetails.Size = new System.Drawing.Size(898, 195);
            this.posGbxBillDetails.TabIndex = 57;
            this.posGbxBillDetails.TabStop = false;
            this.posGbxBillDetails.Text = "BILL DETAILS";
            // 
            // posTxtBillDgvItemTotal
            // 
            this.posTxtBillDgvItemTotal.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillDgvItemTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillDgvItemTotal.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillDgvItemTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillDgvItemTotal.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillDgvItemTotal.Location = new System.Drawing.Point(567, 689);
            this.posTxtBillDgvItemTotal.Multiline = true;
            this.posTxtBillDgvItemTotal.Name = "posTxtBillDgvItemTotal";
            this.posTxtBillDgvItemTotal.ReadOnly = true;
            this.posTxtBillDgvItemTotal.Size = new System.Drawing.Size(429, 37);
            this.posTxtBillDgvItemTotal.TabIndex = 58;
            // 
            // posTxtBillTableDetailsKOTBOT
            // 
            this.posTxtBillTableDetailsKOTBOT.BackColor = System.Drawing.SystemColors.ControlLight;
            this.posTxtBillTableDetailsKOTBOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posTxtBillTableDetailsKOTBOT.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.posTxtBillTableDetailsKOTBOT.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillTableDetailsKOTBOT.ForeColor = System.Drawing.Color.Blue;
            this.posTxtBillTableDetailsKOTBOT.Location = new System.Drawing.Point(98, 689);
            this.posTxtBillTableDetailsKOTBOT.Multiline = true;
            this.posTxtBillTableDetailsKOTBOT.Name = "posTxtBillTableDetailsKOTBOT";
            this.posTxtBillTableDetailsKOTBOT.ReadOnly = true;
            this.posTxtBillTableDetailsKOTBOT.Size = new System.Drawing.Size(463, 37);
            this.posTxtBillTableDetailsKOTBOT.TabIndex = 59;
            // 
            // posDgvBillTableNumber
            // 
            this.posDgvBillTableNumber.AllowUserToAddRows = false;
            this.posDgvBillTableNumber.BackgroundColor = System.Drawing.SystemColors.Control;
            this.posDgvBillTableNumber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvBillTableNumber.Location = new System.Drawing.Point(1, 460);
            this.posDgvBillTableNumber.Name = "posDgvBillTableNumber";
            this.posDgvBillTableNumber.Size = new System.Drawing.Size(91, 266);
            this.posDgvBillTableNumber.TabIndex = 50;
            // 
            // posBtnBillKeyBoard
            // 
            this.posBtnBillKeyBoard.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnBillKeyBoard.Image = global::POSSystemReDeveloping.Properties.Resources.keyboard12;
            this.posBtnBillKeyBoard.Location = new System.Drawing.Point(1, 174);
            this.posBtnBillKeyBoard.Name = "posBtnBillKeyBoard";
            this.posBtnBillKeyBoard.Size = new System.Drawing.Size(91, 76);
            this.posBtnBillKeyBoard.TabIndex = 60;
            this.posBtnBillKeyBoard.UseVisualStyleBackColor = true;
            // 
            // posGuiBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.ControlBox = false;
            this.Controls.Add(this.posBtnBillKeyBoard);
            this.Controls.Add(this.posDgvBillTableNumber);
            this.Controls.Add(this.posTxtBillTableDetailsKOTBOT);
            this.Controls.Add(this.posTxtBillDgvItemTotal);
            this.Controls.Add(this.posGbxBillTotalAmout);
            this.Controls.Add(this.posGbxBillDetails);
            this.Controls.Add(this.posBtnBillSave);
            this.Controls.Add(this.posBtnBillBillSeperate);
            this.Controls.Add(this.posBtnBillCalculateDiscount);
            this.Controls.Add(this.posBtnBillDiscount);
            this.Controls.Add(this.posDgvBillTableItemDetails);
            this.Controls.Add(this.posBtnBillHome);
            this.Controls.Add(this.posDgvBillTableDetails);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "posGuiBill";
            this.ShowInTaskbar = false;
            this.Text = "BILL";
            this.Load += new System.EventHandler(this.posGuiBill_Load);
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillTableDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillTableItemDetails)).EndInit();
            this.posGbxBillTotalAmout.ResumeLayout(false);
            this.posGbxBillTotalAmout.PerformLayout();
            this.posGbxBillDetails.ResumeLayout(false);
            this.posGbxBillDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillTableNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.DataGridView posDgvBillTableDetails;
        internal System.Windows.Forms.Button posBtnBillHome;
        private System.Windows.Forms.Label posLblBillBillNo;
        internal System.Windows.Forms.TextBox posTxtBillOrderNo;
        internal System.Windows.Forms.DataGridView posDgvBillTableItemDetails;
        private System.Windows.Forms.Label posLblBillTotal;
        internal System.Windows.Forms.TextBox posTxtBillTotal;
        private System.Windows.Forms.Label posLblBillTableNo;
        internal System.Windows.Forms.TextBox posTxtBillTableNo;
        private System.Windows.Forms.Label posLblBillSaleType;
        private System.Windows.Forms.Label posLblBillSteward;
        private System.Windows.Forms.Label posLblBillRemarks;
        private System.Windows.Forms.Label posLblBillSerial;
        private System.Windows.Forms.Label posLblBillCostCenter;
        private System.Windows.Forms.Label posLblBillStatus;
        private System.Windows.Forms.Label posLblBillCashier;
        internal System.Windows.Forms.TextBox posTxtBillSteward;
        internal System.Windows.Forms.TextBox posTxtBillRemarks;
        internal System.Windows.Forms.TextBox posTxtBillSerialNo;
        internal System.Windows.Forms.TextBox posTxtBillSaleType;
        internal System.Windows.Forms.TextBox posTxtBillCostCenter;
        internal System.Windows.Forms.TextBox posTxtBillRoom;
        internal System.Windows.Forms.TextBox posTxtBillStatus;
        internal System.Windows.Forms.TextBox posTxtBillCashier;
        internal System.Windows.Forms.TextBox posTxtBillDate;
        private System.Windows.Forms.Label posLblBillCess;
        private System.Windows.Forms.Label posLblBillVatAmt;
        private System.Windows.Forms.Label posLblBillActDis;
        private System.Windows.Forms.Label posLblBillVat;
        private System.Windows.Forms.Label posLblBillSubTotal;
        private System.Windows.Forms.Label posLblBillScAmt;
        private System.Windows.Forms.Label posLblBillSc;
        private System.Windows.Forms.Label posLblBillDiscAmt;
        private System.Windows.Forms.Label posLblBillDiscount;
        private System.Windows.Forms.Label posLblBillCessAmt;
        internal System.Windows.Forms.TextBox posTxtBillCess;
        internal System.Windows.Forms.TextBox posTxtBillCessAmt;
        internal System.Windows.Forms.TextBox posTxtBillDiscount;
        internal System.Windows.Forms.TextBox posTxtBillDiscAmt;
        internal System.Windows.Forms.TextBox posTxtBillSc;
        internal System.Windows.Forms.TextBox posTxtBillScAmt;
        internal System.Windows.Forms.TextBox posTxtBillSubTotal;
        internal System.Windows.Forms.TextBox posTxtBillVat;
        internal System.Windows.Forms.TextBox posTxtBillVatAmt;
        internal System.Windows.Forms.TextBox posTxtBillActDis;
        internal System.Windows.Forms.Label posLblBillRoom;
        internal System.Windows.Forms.Button posBtnBillDiscount;
        internal System.Windows.Forms.Button posBtnBillCalculateDiscount;
        internal System.Windows.Forms.Button posBtnBillBillSeperate;
        internal System.Windows.Forms.Button posBtnBillSave;
        internal System.Windows.Forms.TextBox posTxtBillCustomerType;
        private System.Windows.Forms.Label posLblBillCustomerType;
        private System.Windows.Forms.GroupBox posGbxBillTotalAmout;
        private System.Windows.Forms.GroupBox posGbxBillDetails;
        internal System.Windows.Forms.TextBox posTxtBillDgvItemTotal;
        internal System.Windows.Forms.TextBox posTxtBillTableDetailsKOTBOT;
        internal System.Windows.Forms.DataGridView posDgvBillTableNumber;
        internal System.Windows.Forms.Button posBtnBillKeyBoard;
        private System.Windows.Forms.Panel panel1;

    }
}