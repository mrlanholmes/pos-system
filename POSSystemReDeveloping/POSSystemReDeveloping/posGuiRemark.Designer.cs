﻿namespace POSSystemReDeveloping
{
    partial class posGuiRemark
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.posCmbRemark = new System.Windows.Forms.ComboBox();
            this.posLblPnlRemark = new System.Windows.Forms.Label();
            this.posLblPnlYourRemark = new System.Windows.Forms.Label();
            this.posRtxtYourRemark = new System.Windows.Forms.RichTextBox();
            this.posBtnBack = new System.Windows.Forms.Button();
            this.posBtnUpdate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // posCmbRemark
            // 
            this.posCmbRemark.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.posCmbRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posCmbRemark.FormattingEnabled = true;
            this.posCmbRemark.Location = new System.Drawing.Point(137, 25);
            this.posCmbRemark.Name = "posCmbRemark";
            this.posCmbRemark.Size = new System.Drawing.Size(400, 33);
            this.posCmbRemark.TabIndex = 0;
            // 
            // posLblPnlRemark
            // 
            this.posLblPnlRemark.AutoSize = true;
            this.posLblPnlRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblPnlRemark.Location = new System.Drawing.Point(50, 32);
            this.posLblPnlRemark.Name = "posLblPnlRemark";
            this.posLblPnlRemark.Size = new System.Drawing.Size(81, 20);
            this.posLblPnlRemark.TabIndex = 1;
            this.posLblPnlRemark.Text = "Remark :";
            // 
            // posLblPnlYourRemark
            // 
            this.posLblPnlYourRemark.AutoSize = true;
            this.posLblPnlYourRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblPnlYourRemark.Location = new System.Drawing.Point(7, 70);
            this.posLblPnlYourRemark.Name = "posLblPnlYourRemark";
            this.posLblPnlYourRemark.Size = new System.Drawing.Size(124, 20);
            this.posLblPnlYourRemark.TabIndex = 2;
            this.posLblPnlYourRemark.Text = "Your Remark :";
            // 
            // posRtxtYourRemark
            // 
            this.posRtxtYourRemark.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posRtxtYourRemark.Location = new System.Drawing.Point(137, 64);
            this.posRtxtYourRemark.MaxLength = 200;
            this.posRtxtYourRemark.Name = "posRtxtYourRemark";
            this.posRtxtYourRemark.Size = new System.Drawing.Size(400, 100);
            this.posRtxtYourRemark.TabIndex = 3;
            this.posRtxtYourRemark.Text = "";
            // 
            // posBtnBack
            // 
            this.posBtnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnBack.Location = new System.Drawing.Point(137, 170);
            this.posBtnBack.Name = "posBtnBack";
            this.posBtnBack.Size = new System.Drawing.Size(197, 61);
            this.posBtnBack.TabIndex = 4;
            this.posBtnBack.Text = "Back";
            this.posBtnBack.UseVisualStyleBackColor = true;
            // 
            // posBtnUpdate
            // 
            this.posBtnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnUpdate.Location = new System.Drawing.Point(340, 170);
            this.posBtnUpdate.Name = "posBtnUpdate";
            this.posBtnUpdate.Size = new System.Drawing.Size(197, 61);
            this.posBtnUpdate.TabIndex = 5;
            this.posBtnUpdate.Text = "Update";
            this.posBtnUpdate.UseVisualStyleBackColor = true;
            // 
            // posGuiRemark
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 232);
            this.ControlBox = false;
            this.Controls.Add(this.posBtnUpdate);
            this.Controls.Add(this.posBtnBack);
            this.Controls.Add(this.posRtxtYourRemark);
            this.Controls.Add(this.posLblPnlYourRemark);
            this.Controls.Add(this.posLblPnlRemark);
            this.Controls.Add(this.posCmbRemark);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "posGuiRemark";
            this.ShowInTaskbar = false;
            this.Text = "REMARK";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ComboBox posCmbRemark;
        private System.Windows.Forms.Label posLblPnlRemark;
        private System.Windows.Forms.Label posLblPnlYourRemark;
        internal System.Windows.Forms.RichTextBox posRtxtYourRemark;
        internal System.Windows.Forms.Button posBtnBack;
        internal System.Windows.Forms.Button posBtnUpdate;

    }
}