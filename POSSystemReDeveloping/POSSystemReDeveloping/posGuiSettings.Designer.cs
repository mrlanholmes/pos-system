﻿namespace POSSystemReDeveloping
{
    partial class posGuiSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.posTabSettings = new System.Windows.Forms.TabControl();
            this.posTabSettingsUsers = new System.Windows.Forms.TabPage();
            this.posDgvSettingUser = new System.Windows.Forms.DataGridView();
            this.posBtnUserSave = new System.Windows.Forms.Button();
            this.posTxtUserName = new System.Windows.Forms.TextBox();
            this.posTxtUserUserId = new System.Windows.Forms.TextBox();
            this.posTxtUserPassword = new System.Windows.Forms.TextBox();
            this.posDtpUserExpireDate = new System.Windows.Forms.DateTimePicker();
            this.posCmbUserUserType = new System.Windows.Forms.ComboBox();
            this.posCmbUserAllowPwChange = new System.Windows.Forms.ComboBox();
            this.posLblSettingsUserAllowPwChange = new System.Windows.Forms.Label();
            this.posLblSettingsUserExpireDate = new System.Windows.Forms.Label();
            this.posLblSettingsUserUserType = new System.Windows.Forms.Label();
            this.posLblSettingsUserPassword = new System.Windows.Forms.Label();
            this.posLblSettingsUserUserID = new System.Windows.Forms.Label();
            this.posLblSettingsUserUserName = new System.Windows.Forms.Label();
            this.posTabSettingsUserRight = new System.Windows.Forms.TabPage();
            this.posBtnUserRightChkbxClearAll = new System.Windows.Forms.Button();
            this.posBtnUserRightChkBxSelectAll = new System.Windows.Forms.Button();
            this.posBtnUserRightSave = new System.Windows.Forms.Button();
            this.posCmbUserRightCostCenter = new System.Windows.Forms.ComboBox();
            this.posCmbUserRightUserId = new System.Windows.Forms.ComboBox();
            this.posTxtUserRightCostCenter = new System.Windows.Forms.TextBox();
            this.posTxtUserRightEmployeeName = new System.Windows.Forms.TextBox();
            this.posLblUserRightTopic = new System.Windows.Forms.Label();
            this.posChkUserRightAddPrinter = new System.Windows.Forms.CheckBox();
            this.posChkUserRightAddItemRemarks = new System.Windows.Forms.CheckBox();
            this.posChkUserRightPrinterToItems = new System.Windows.Forms.CheckBox();
            this.posChkUserRightReports = new System.Windows.Forms.CheckBox();
            this.posChkUserRightSettings = new System.Windows.Forms.CheckBox();
            this.posChkUserRightSettlement = new System.Windows.Forms.CheckBox();
            this.posChkUserRightEntitle = new System.Windows.Forms.CheckBox();
            this.posChkUserRightAI = new System.Windows.Forms.CheckBox();
            this.posChkUserRightBill = new System.Windows.Forms.CheckBox();
            this.posChkUserRightKotBot = new System.Windows.Forms.CheckBox();
            this.posLblUserRightAddPrinters = new System.Windows.Forms.Label();
            this.posLblUserRightAddItemRemarks = new System.Windows.Forms.Label();
            this.posLblUserRightPrinterToItems = new System.Windows.Forms.Label();
            this.posLblUserRightReports = new System.Windows.Forms.Label();
            this.posLblUserRightSecurity = new System.Windows.Forms.Label();
            this.posLblUserRightSettlement = new System.Windows.Forms.Label();
            this.posLblUserRightEntittle = new System.Windows.Forms.Label();
            this.posLblUserRightAI = new System.Windows.Forms.Label();
            this.posLblUserRightBill = new System.Windows.Forms.Label();
            this.posLblUserRightKotBot = new System.Windows.Forms.Label();
            this.posCmbUserRightCmbCostCenter = new System.Windows.Forms.Label();
            this.posLblUserRightUserId = new System.Windows.Forms.Label();
            this.posLblUserRightCostCenter = new System.Windows.Forms.Label();
            this.posLblUserRightEmployeeName = new System.Windows.Forms.Label();
            this.posTabSettingsAddPrinterToItem = new System.Windows.Forms.TabPage();
            this.posCmbSettingsAPTIPrinter = new System.Windows.Forms.ComboBox();
            this.posTxtSettingsAPTICCenter = new System.Windows.Forms.TextBox();
            this.posTxtSettingsAPTIName = new System.Windows.Forms.TextBox();
            this.posTxtSettingsAPTIItemNo = new System.Windows.Forms.TextBox();
            this.posTabSettingsAPTILblPrinter = new System.Windows.Forms.Label();
            this.posTabSettingsAPTILblCCenter = new System.Windows.Forms.Label();
            this.posTabSettingsAPTILblName = new System.Windows.Forms.Label();
            this.posTabSettingsAPTILblItemNo = new System.Windows.Forms.Label();
            this.posTabSettingsAPTIBtnHome = new System.Windows.Forms.Button();
            this.posTabSettingsAPTIBtnSave = new System.Windows.Forms.Button();
            this.posTabSettingsAPTIDgvPrinterItem = new System.Windows.Forms.DataGridView();
            this.posSettingsTabOpenItem = new System.Windows.Forms.TabPage();
            this.posBtnSettingsOpenItemSave = new System.Windows.Forms.Button();
            this.posCmbSettingsOpenItemSaleType = new System.Windows.Forms.ComboBox();
            this.posLblSettingsOpenItemSaleType = new System.Windows.Forms.Label();
            this.posDgvSettingsOpenItem = new System.Windows.Forms.DataGridView();
            this.posCmbSettingsOpenItemCostCenter = new System.Windows.Forms.ComboBox();
            this.posLblSettingsOpenItem = new System.Windows.Forms.Label();
            this.posBtnUserRightHome = new System.Windows.Forms.Button();
            this.posTabSettings.SuspendLayout();
            this.posTabSettingsUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvSettingUser)).BeginInit();
            this.posTabSettingsUserRight.SuspendLayout();
            this.posTabSettingsAddPrinterToItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posTabSettingsAPTIDgvPrinterItem)).BeginInit();
            this.posSettingsTabOpenItem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvSettingsOpenItem)).BeginInit();
            this.SuspendLayout();
            // 
            // posTabSettings
            // 
            this.posTabSettings.Controls.Add(this.posTabSettingsUsers);
            this.posTabSettings.Controls.Add(this.posTabSettingsUserRight);
            this.posTabSettings.Controls.Add(this.posTabSettingsAddPrinterToItem);
            this.posTabSettings.Controls.Add(this.posSettingsTabOpenItem);
            this.posTabSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTabSettings.Location = new System.Drawing.Point(102, 3);
            this.posTabSettings.Name = "posTabSettings";
            this.posTabSettings.SelectedIndex = 0;
            this.posTabSettings.Size = new System.Drawing.Size(908, 728);
            this.posTabSettings.TabIndex = 0;
            // 
            // posTabSettingsUsers
            // 
            this.posTabSettingsUsers.BackColor = System.Drawing.SystemColors.Control;
            this.posTabSettingsUsers.Controls.Add(this.posDgvSettingUser);
            this.posTabSettingsUsers.Controls.Add(this.posBtnUserSave);
            this.posTabSettingsUsers.Controls.Add(this.posTxtUserName);
            this.posTabSettingsUsers.Controls.Add(this.posTxtUserUserId);
            this.posTabSettingsUsers.Controls.Add(this.posTxtUserPassword);
            this.posTabSettingsUsers.Controls.Add(this.posDtpUserExpireDate);
            this.posTabSettingsUsers.Controls.Add(this.posCmbUserUserType);
            this.posTabSettingsUsers.Controls.Add(this.posCmbUserAllowPwChange);
            this.posTabSettingsUsers.Controls.Add(this.posLblSettingsUserAllowPwChange);
            this.posTabSettingsUsers.Controls.Add(this.posLblSettingsUserExpireDate);
            this.posTabSettingsUsers.Controls.Add(this.posLblSettingsUserUserType);
            this.posTabSettingsUsers.Controls.Add(this.posLblSettingsUserPassword);
            this.posTabSettingsUsers.Controls.Add(this.posLblSettingsUserUserID);
            this.posTabSettingsUsers.Controls.Add(this.posLblSettingsUserUserName);
            this.posTabSettingsUsers.Location = new System.Drawing.Point(4, 33);
            this.posTabSettingsUsers.Name = "posTabSettingsUsers";
            this.posTabSettingsUsers.Padding = new System.Windows.Forms.Padding(3);
            this.posTabSettingsUsers.Size = new System.Drawing.Size(900, 691);
            this.posTabSettingsUsers.TabIndex = 0;
            this.posTabSettingsUsers.Text = "User";
            // 
            // posDgvSettingUser
            // 
            this.posDgvSettingUser.AllowUserToAddRows = false;
            this.posDgvSettingUser.AllowUserToDeleteRows = false;
            this.posDgvSettingUser.BackgroundColor = System.Drawing.SystemColors.Control;
            this.posDgvSettingUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvSettingUser.Location = new System.Drawing.Point(6, 137);
            this.posDgvSettingUser.Name = "posDgvSettingUser";
            this.posDgvSettingUser.ReadOnly = true;
            this.posDgvSettingUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.posDgvSettingUser.Size = new System.Drawing.Size(891, 548);
            this.posDgvSettingUser.TabIndex = 9;
            // 
            // posBtnUserSave
            // 
            this.posBtnUserSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnUserSave.Location = new System.Drawing.Point(615, 85);
            this.posBtnUserSave.Name = "posBtnUserSave";
            this.posBtnUserSave.Size = new System.Drawing.Size(275, 50);
            this.posBtnUserSave.TabIndex = 8;
            this.posBtnUserSave.Text = "Save";
            this.posBtnUserSave.UseVisualStyleBackColor = true;
            this.posBtnUserSave.Click += new System.EventHandler(this.posBtnUserSave_Click);
            // 
            // posTxtUserName
            // 
            this.posTxtUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtUserName.Location = new System.Drawing.Point(115, 6);
            this.posTxtUserName.Name = "posTxtUserName";
            this.posTxtUserName.Size = new System.Drawing.Size(145, 26);
            this.posTxtUserName.TabIndex = 0;
            // 
            // posTxtUserUserId
            // 
            this.posTxtUserUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtUserUserId.Location = new System.Drawing.Point(371, 5);
            this.posTxtUserUserId.Name = "posTxtUserUserId";
            this.posTxtUserUserId.Size = new System.Drawing.Size(125, 26);
            this.posTxtUserUserId.TabIndex = 1;
            // 
            // posTxtUserPassword
            // 
            this.posTxtUserPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtUserPassword.Location = new System.Drawing.Point(115, 47);
            this.posTxtUserPassword.Name = "posTxtUserPassword";
            this.posTxtUserPassword.Size = new System.Drawing.Size(145, 26);
            this.posTxtUserPassword.TabIndex = 2;
            // 
            // posDtpUserExpireDate
            // 
            this.posDtpUserExpireDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posDtpUserExpireDate.Location = new System.Drawing.Point(615, 53);
            this.posDtpUserExpireDate.Name = "posDtpUserExpireDate";
            this.posDtpUserExpireDate.Size = new System.Drawing.Size(275, 26);
            this.posDtpUserExpireDate.TabIndex = 3;
            // 
            // posCmbUserUserType
            // 
            this.posCmbUserUserType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.posCmbUserUserType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posCmbUserUserType.FormattingEnabled = true;
            this.posCmbUserUserType.ItemHeight = 20;
            this.posCmbUserUserType.Items.AddRange(new object[] {
            "EXECUTIVE",
            "STEWARD"});
            this.posCmbUserUserType.Location = new System.Drawing.Point(371, 51);
            this.posCmbUserUserType.Name = "posCmbUserUserType";
            this.posCmbUserUserType.Size = new System.Drawing.Size(125, 28);
            this.posCmbUserUserType.TabIndex = 5;
            // 
            // posCmbUserAllowPwChange
            // 
            this.posCmbUserAllowPwChange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.posCmbUserAllowPwChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posCmbUserAllowPwChange.FormattingEnabled = true;
            this.posCmbUserAllowPwChange.Items.AddRange(new object[] {
            "True",
            "False"});
            this.posCmbUserAllowPwChange.Location = new System.Drawing.Point(615, 6);
            this.posCmbUserAllowPwChange.Name = "posCmbUserAllowPwChange";
            this.posCmbUserAllowPwChange.Size = new System.Drawing.Size(275, 28);
            this.posCmbUserAllowPwChange.TabIndex = 7;
            // 
            // posLblSettingsUserAllowPwChange
            // 
            this.posLblSettingsUserAllowPwChange.AutoSize = true;
            this.posLblSettingsUserAllowPwChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblSettingsUserAllowPwChange.Location = new System.Drawing.Point(502, 12);
            this.posLblSettingsUserAllowPwChange.Name = "posLblSettingsUserAllowPwChange";
            this.posLblSettingsUserAllowPwChange.Size = new System.Drawing.Size(94, 18);
            this.posLblSettingsUserAllowPwChange.TabIndex = 7;
            this.posLblSettingsUserAllowPwChange.Text = "User Status :";
            // 
            // posLblSettingsUserExpireDate
            // 
            this.posLblSettingsUserExpireDate.AutoSize = true;
            this.posLblSettingsUserExpireDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblSettingsUserExpireDate.Location = new System.Drawing.Point(504, 58);
            this.posLblSettingsUserExpireDate.Name = "posLblSettingsUserExpireDate";
            this.posLblSettingsUserExpireDate.Size = new System.Drawing.Size(92, 18);
            this.posLblSettingsUserExpireDate.TabIndex = 5;
            this.posLblSettingsUserExpireDate.Text = "Expire Date :";
            // 
            // posLblSettingsUserUserType
            // 
            this.posLblSettingsUserUserType.AutoSize = true;
            this.posLblSettingsUserUserType.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblSettingsUserUserType.Location = new System.Drawing.Point(270, 52);
            this.posLblSettingsUserUserType.Name = "posLblSettingsUserUserType";
            this.posLblSettingsUserUserType.Size = new System.Drawing.Size(84, 18);
            this.posLblSettingsUserUserType.TabIndex = 4;
            this.posLblSettingsUserUserType.Text = "User Type :";
            // 
            // posLblSettingsUserPassword
            // 
            this.posLblSettingsUserPassword.AutoSize = true;
            this.posLblSettingsUserPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblSettingsUserPassword.Location = new System.Drawing.Point(16, 52);
            this.posLblSettingsUserPassword.Name = "posLblSettingsUserPassword";
            this.posLblSettingsUserPassword.Size = new System.Drawing.Size(83, 18);
            this.posLblSettingsUserPassword.TabIndex = 2;
            this.posLblSettingsUserPassword.Text = "Password :";
            // 
            // posLblSettingsUserUserID
            // 
            this.posLblSettingsUserUserID.AutoSize = true;
            this.posLblSettingsUserUserID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblSettingsUserUserID.Location = new System.Drawing.Point(290, 13);
            this.posLblSettingsUserUserID.Name = "posLblSettingsUserUserID";
            this.posLblSettingsUserUserID.Size = new System.Drawing.Size(66, 18);
            this.posLblSettingsUserUserID.TabIndex = 1;
            this.posLblSettingsUserUserID.Text = "User ID :";
            // 
            // posLblSettingsUserUserName
            // 
            this.posLblSettingsUserUserName.AutoSize = true;
            this.posLblSettingsUserUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblSettingsUserUserName.Location = new System.Drawing.Point(6, 12);
            this.posLblSettingsUserUserName.Name = "posLblSettingsUserUserName";
            this.posLblSettingsUserUserName.Size = new System.Drawing.Size(92, 18);
            this.posLblSettingsUserUserName.TabIndex = 0;
            this.posLblSettingsUserUserName.Text = "User Name :";
            // 
            // posTabSettingsUserRight
            // 
            this.posTabSettingsUserRight.BackColor = System.Drawing.SystemColors.Control;
            this.posTabSettingsUserRight.Controls.Add(this.posBtnUserRightChkbxClearAll);
            this.posTabSettingsUserRight.Controls.Add(this.posBtnUserRightChkBxSelectAll);
            this.posTabSettingsUserRight.Controls.Add(this.posBtnUserRightSave);
            this.posTabSettingsUserRight.Controls.Add(this.posCmbUserRightCostCenter);
            this.posTabSettingsUserRight.Controls.Add(this.posCmbUserRightUserId);
            this.posTabSettingsUserRight.Controls.Add(this.posTxtUserRightCostCenter);
            this.posTabSettingsUserRight.Controls.Add(this.posTxtUserRightEmployeeName);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightTopic);
            this.posTabSettingsUserRight.Controls.Add(this.posChkUserRightAddPrinter);
            this.posTabSettingsUserRight.Controls.Add(this.posChkUserRightAddItemRemarks);
            this.posTabSettingsUserRight.Controls.Add(this.posChkUserRightPrinterToItems);
            this.posTabSettingsUserRight.Controls.Add(this.posChkUserRightReports);
            this.posTabSettingsUserRight.Controls.Add(this.posChkUserRightSettings);
            this.posTabSettingsUserRight.Controls.Add(this.posChkUserRightSettlement);
            this.posTabSettingsUserRight.Controls.Add(this.posChkUserRightEntitle);
            this.posTabSettingsUserRight.Controls.Add(this.posChkUserRightAI);
            this.posTabSettingsUserRight.Controls.Add(this.posChkUserRightBill);
            this.posTabSettingsUserRight.Controls.Add(this.posChkUserRightKotBot);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightAddPrinters);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightAddItemRemarks);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightPrinterToItems);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightReports);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightSecurity);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightSettlement);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightEntittle);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightAI);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightBill);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightKotBot);
            this.posTabSettingsUserRight.Controls.Add(this.posCmbUserRightCmbCostCenter);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightUserId);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightCostCenter);
            this.posTabSettingsUserRight.Controls.Add(this.posLblUserRightEmployeeName);
            this.posTabSettingsUserRight.Location = new System.Drawing.Point(4, 33);
            this.posTabSettingsUserRight.Name = "posTabSettingsUserRight";
            this.posTabSettingsUserRight.Padding = new System.Windows.Forms.Padding(3);
            this.posTabSettingsUserRight.Size = new System.Drawing.Size(900, 691);
            this.posTabSettingsUserRight.TabIndex = 1;
            this.posTabSettingsUserRight.Text = "User R⁬ights";
            // 
            // posBtnUserRightChkbxClearAll
            // 
            this.posBtnUserRightChkbxClearAll.Location = new System.Drawing.Point(323, 316);
            this.posBtnUserRightChkbxClearAll.Name = "posBtnUserRightChkbxClearAll";
            this.posBtnUserRightChkbxClearAll.Size = new System.Drawing.Size(252, 62);
            this.posBtnUserRightChkbxClearAll.TabIndex = 31;
            this.posBtnUserRightChkbxClearAll.Text = "Clear All";
            this.posBtnUserRightChkbxClearAll.UseVisualStyleBackColor = true;
            // 
            // posBtnUserRightChkBxSelectAll
            // 
            this.posBtnUserRightChkBxSelectAll.Location = new System.Drawing.Point(323, 236);
            this.posBtnUserRightChkBxSelectAll.Name = "posBtnUserRightChkBxSelectAll";
            this.posBtnUserRightChkBxSelectAll.Size = new System.Drawing.Size(252, 64);
            this.posBtnUserRightChkBxSelectAll.TabIndex = 30;
            this.posBtnUserRightChkBxSelectAll.Text = "Select All";
            this.posBtnUserRightChkBxSelectAll.UseVisualStyleBackColor = true;
            // 
            // posBtnUserRightSave
            // 
            this.posBtnUserRightSave.Location = new System.Drawing.Point(323, 165);
            this.posBtnUserRightSave.Name = "posBtnUserRightSave";
            this.posBtnUserRightSave.Size = new System.Drawing.Size(252, 63);
            this.posBtnUserRightSave.TabIndex = 29;
            this.posBtnUserRightSave.Text = "Save";
            this.posBtnUserRightSave.UseVisualStyleBackColor = true;
            // 
            // posCmbUserRightCostCenter
            // 
            this.posCmbUserRightCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.posCmbUserRightCostCenter.FormattingEnabled = true;
            this.posCmbUserRightCostCenter.Location = new System.Drawing.Point(439, 91);
            this.posCmbUserRightCostCenter.Name = "posCmbUserRightCostCenter";
            this.posCmbUserRightCostCenter.Size = new System.Drawing.Size(224, 32);
            this.posCmbUserRightCostCenter.TabIndex = 28;
            // 
            // posCmbUserRightUserId
            // 
            this.posCmbUserRightUserId.DropDownHeight = 200;
            this.posCmbUserRightUserId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.posCmbUserRightUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posCmbUserRightUserId.IntegralHeight = false;
            this.posCmbUserRightUserId.Location = new System.Drawing.Point(439, 42);
            this.posCmbUserRightUserId.Name = "posCmbUserRightUserId";
            this.posCmbUserRightUserId.Size = new System.Drawing.Size(224, 33);
            this.posCmbUserRightUserId.TabIndex = 27;
            this.posCmbUserRightUserId.SelectedIndexChanged += new System.EventHandler(this.posCmbUserRightUserId_SelectedIndexChanged);
            // 
            // posTxtUserRightCostCenter
            // 
            this.posTxtUserRightCostCenter.Location = new System.Drawing.Point(186, 88);
            this.posTxtUserRightCostCenter.Name = "posTxtUserRightCostCenter";
            this.posTxtUserRightCostCenter.ReadOnly = true;
            this.posTxtUserRightCostCenter.Size = new System.Drawing.Size(100, 29);
            this.posTxtUserRightCostCenter.TabIndex = 26;
            // 
            // posTxtUserRightEmployeeName
            // 
            this.posTxtUserRightEmployeeName.Location = new System.Drawing.Point(186, 48);
            this.posTxtUserRightEmployeeName.Name = "posTxtUserRightEmployeeName";
            this.posTxtUserRightEmployeeName.ReadOnly = true;
            this.posTxtUserRightEmployeeName.Size = new System.Drawing.Size(148, 29);
            this.posTxtUserRightEmployeeName.TabIndex = 25;
            // 
            // posLblUserRightTopic
            // 
            this.posLblUserRightTopic.AutoSize = true;
            this.posLblUserRightTopic.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posLblUserRightTopic.Location = new System.Drawing.Point(239, 3);
            this.posLblUserRightTopic.Name = "posLblUserRightTopic";
            this.posLblUserRightTopic.Size = new System.Drawing.Size(247, 29);
            this.posLblUserRightTopic.TabIndex = 24;
            this.posLblUserRightTopic.Text = "POS USER RIGHTS";
            // 
            // posChkUserRightAddPrinter
            // 
            this.posChkUserRightAddPrinter.AutoSize = true;
            this.posChkUserRightAddPrinter.Location = new System.Drawing.Point(219, 415);
            this.posChkUserRightAddPrinter.Name = "posChkUserRightAddPrinter";
            this.posChkUserRightAddPrinter.Size = new System.Drawing.Size(15, 14);
            this.posChkUserRightAddPrinter.TabIndex = 23;
            this.posChkUserRightAddPrinter.UseVisualStyleBackColor = true;
            // 
            // posChkUserRightAddItemRemarks
            // 
            this.posChkUserRightAddItemRemarks.AutoSize = true;
            this.posChkUserRightAddItemRemarks.Location = new System.Drawing.Point(219, 389);
            this.posChkUserRightAddItemRemarks.Name = "posChkUserRightAddItemRemarks";
            this.posChkUserRightAddItemRemarks.Size = new System.Drawing.Size(15, 14);
            this.posChkUserRightAddItemRemarks.TabIndex = 22;
            this.posChkUserRightAddItemRemarks.UseVisualStyleBackColor = true;
            // 
            // posChkUserRightPrinterToItems
            // 
            this.posChkUserRightPrinterToItems.AutoSize = true;
            this.posChkUserRightPrinterToItems.Location = new System.Drawing.Point(219, 363);
            this.posChkUserRightPrinterToItems.Name = "posChkUserRightPrinterToItems";
            this.posChkUserRightPrinterToItems.Size = new System.Drawing.Size(15, 14);
            this.posChkUserRightPrinterToItems.TabIndex = 21;
            this.posChkUserRightPrinterToItems.UseVisualStyleBackColor = true;
            // 
            // posChkUserRightReports
            // 
            this.posChkUserRightReports.AutoSize = true;
            this.posChkUserRightReports.Location = new System.Drawing.Point(219, 333);
            this.posChkUserRightReports.Name = "posChkUserRightReports";
            this.posChkUserRightReports.Size = new System.Drawing.Size(15, 14);
            this.posChkUserRightReports.TabIndex = 20;
            this.posChkUserRightReports.UseVisualStyleBackColor = true;
            // 
            // posChkUserRightSettings
            // 
            this.posChkUserRightSettings.AutoSize = true;
            this.posChkUserRightSettings.Location = new System.Drawing.Point(219, 307);
            this.posChkUserRightSettings.Name = "posChkUserRightSettings";
            this.posChkUserRightSettings.Size = new System.Drawing.Size(15, 14);
            this.posChkUserRightSettings.TabIndex = 19;
            this.posChkUserRightSettings.UseVisualStyleBackColor = true;
            // 
            // posChkUserRightSettlement
            // 
            this.posChkUserRightSettlement.AutoSize = true;
            this.posChkUserRightSettlement.Location = new System.Drawing.Point(219, 280);
            this.posChkUserRightSettlement.Name = "posChkUserRightSettlement";
            this.posChkUserRightSettlement.Size = new System.Drawing.Size(15, 14);
            this.posChkUserRightSettlement.TabIndex = 18;
            this.posChkUserRightSettlement.UseVisualStyleBackColor = true;
            // 
            // posChkUserRightEntitle
            // 
            this.posChkUserRightEntitle.AutoSize = true;
            this.posChkUserRightEntitle.Location = new System.Drawing.Point(219, 249);
            this.posChkUserRightEntitle.Name = "posChkUserRightEntitle";
            this.posChkUserRightEntitle.Size = new System.Drawing.Size(15, 14);
            this.posChkUserRightEntitle.TabIndex = 17;
            this.posChkUserRightEntitle.UseVisualStyleBackColor = true;
            // 
            // posChkUserRightAI
            // 
            this.posChkUserRightAI.AutoSize = true;
            this.posChkUserRightAI.Location = new System.Drawing.Point(219, 221);
            this.posChkUserRightAI.Name = "posChkUserRightAI";
            this.posChkUserRightAI.Size = new System.Drawing.Size(15, 14);
            this.posChkUserRightAI.TabIndex = 16;
            this.posChkUserRightAI.UseVisualStyleBackColor = true;
            // 
            // posChkUserRightBill
            // 
            this.posChkUserRightBill.AutoSize = true;
            this.posChkUserRightBill.Location = new System.Drawing.Point(219, 193);
            this.posChkUserRightBill.Name = "posChkUserRightBill";
            this.posChkUserRightBill.Size = new System.Drawing.Size(15, 14);
            this.posChkUserRightBill.TabIndex = 15;
            this.posChkUserRightBill.UseVisualStyleBackColor = true;
            // 
            // posChkUserRightKotBot
            // 
            this.posChkUserRightKotBot.AutoSize = true;
            this.posChkUserRightKotBot.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posChkUserRightKotBot.Location = new System.Drawing.Point(219, 165);
            this.posChkUserRightKotBot.Name = "posChkUserRightKotBot";
            this.posChkUserRightKotBot.Size = new System.Drawing.Size(15, 14);
            this.posChkUserRightKotBot.TabIndex = 14;
            this.posChkUserRightKotBot.UseVisualStyleBackColor = true;
            // 
            // posLblUserRightAddPrinters
            // 
            this.posLblUserRightAddPrinters.AutoSize = true;
            this.posLblUserRightAddPrinters.Location = new System.Drawing.Point(70, 407);
            this.posLblUserRightAddPrinters.Name = "posLblUserRightAddPrinters";
            this.posLblUserRightAddPrinters.Size = new System.Drawing.Size(127, 24);
            this.posLblUserRightAddPrinters.TabIndex = 13;
            this.posLblUserRightAddPrinters.Text = "Add Printer :";
            // 
            // posLblUserRightAddItemRemarks
            // 
            this.posLblUserRightAddItemRemarks.AutoSize = true;
            this.posLblUserRightAddItemRemarks.Location = new System.Drawing.Point(5, 381);
            this.posLblUserRightAddItemRemarks.Name = "posLblUserRightAddItemRemarks";
            this.posLblUserRightAddItemRemarks.Size = new System.Drawing.Size(192, 24);
            this.posLblUserRightAddItemRemarks.TabIndex = 12;
            this.posLblUserRightAddItemRemarks.Text = "Add Item Remarks :";
            // 
            // posLblUserRightPrinterToItems
            // 
            this.posLblUserRightPrinterToItems.AutoSize = true;
            this.posLblUserRightPrinterToItems.Location = new System.Drawing.Point(28, 355);
            this.posLblUserRightPrinterToItems.Name = "posLblUserRightPrinterToItems";
            this.posLblUserRightPrinterToItems.Size = new System.Drawing.Size(169, 24);
            this.posLblUserRightPrinterToItems.TabIndex = 11;
            this.posLblUserRightPrinterToItems.Text = "Printer To Items :";
            // 
            // posLblUserRightReports
            // 
            this.posLblUserRightReports.AutoSize = true;
            this.posLblUserRightReports.Location = new System.Drawing.Point(103, 327);
            this.posLblUserRightReports.Name = "posLblUserRightReports";
            this.posLblUserRightReports.Size = new System.Drawing.Size(94, 24);
            this.posLblUserRightReports.TabIndex = 10;
            this.posLblUserRightReports.Text = "Reports :";
            // 
            // posLblUserRightSecurity
            // 
            this.posLblUserRightSecurity.AutoSize = true;
            this.posLblUserRightSecurity.Location = new System.Drawing.Point(100, 301);
            this.posLblUserRightSecurity.Name = "posLblUserRightSecurity";
            this.posLblUserRightSecurity.Size = new System.Drawing.Size(96, 24);
            this.posLblUserRightSecurity.TabIndex = 9;
            this.posLblUserRightSecurity.Text = "Settings :";
            // 
            // posLblUserRightSettlement
            // 
            this.posLblUserRightSettlement.AutoSize = true;
            this.posLblUserRightSettlement.Location = new System.Drawing.Point(77, 274);
            this.posLblUserRightSettlement.Name = "posLblUserRightSettlement";
            this.posLblUserRightSettlement.Size = new System.Drawing.Size(120, 24);
            this.posLblUserRightSettlement.TabIndex = 8;
            this.posLblUserRightSettlement.Text = "Settlement :";
            // 
            // posLblUserRightEntittle
            // 
            this.posLblUserRightEntittle.AutoSize = true;
            this.posLblUserRightEntittle.Location = new System.Drawing.Point(112, 243);
            this.posLblUserRightEntittle.Name = "posLblUserRightEntittle";
            this.posLblUserRightEntittle.Size = new System.Drawing.Size(85, 24);
            this.posLblUserRightEntittle.TabIndex = 7;
            this.posLblUserRightEntittle.Text = "Entittle :";
            // 
            // posLblUserRightAI
            // 
            this.posLblUserRightAI.AutoSize = true;
            this.posLblUserRightAI.Location = new System.Drawing.Point(156, 215);
            this.posLblUserRightAI.Name = "posLblUserRightAI";
            this.posLblUserRightAI.Size = new System.Drawing.Size(41, 24);
            this.posLblUserRightAI.TabIndex = 6;
            this.posLblUserRightAI.Text = "AI :";
            // 
            // posLblUserRightBill
            // 
            this.posLblUserRightBill.AutoSize = true;
            this.posLblUserRightBill.Location = new System.Drawing.Point(147, 187);
            this.posLblUserRightBill.Name = "posLblUserRightBill";
            this.posLblUserRightBill.Size = new System.Drawing.Size(50, 24);
            this.posLblUserRightBill.TabIndex = 5;
            this.posLblUserRightBill.Text = "Bill :";
            // 
            // posLblUserRightKotBot
            // 
            this.posLblUserRightKotBot.AutoSize = true;
            this.posLblUserRightKotBot.Location = new System.Drawing.Point(85, 160);
            this.posLblUserRightKotBot.Name = "posLblUserRightKotBot";
            this.posLblUserRightKotBot.Size = new System.Drawing.Size(112, 24);
            this.posLblUserRightKotBot.TabIndex = 4;
            this.posLblUserRightKotBot.Text = "KOT/BOT :";
            // 
            // posCmbUserRightCmbCostCenter
            // 
            this.posCmbUserRightCmbCostCenter.AutoSize = true;
            this.posCmbUserRightCmbCostCenter.Location = new System.Drawing.Point(302, 95);
            this.posCmbUserRightCmbCostCenter.Name = "posCmbUserRightCmbCostCenter";
            this.posCmbUserRightCmbCostCenter.Size = new System.Drawing.Size(131, 24);
            this.posCmbUserRightCmbCostCenter.TabIndex = 3;
            this.posCmbUserRightCmbCostCenter.Text = "Cost Center :";
            // 
            // posLblUserRightUserId
            // 
            this.posLblUserRightUserId.AutoSize = true;
            this.posLblUserRightUserId.Location = new System.Drawing.Point(343, 46);
            this.posLblUserRightUserId.Name = "posLblUserRightUserId";
            this.posLblUserRightUserId.Size = new System.Drawing.Size(90, 24);
            this.posLblUserRightUserId.TabIndex = 2;
            this.posLblUserRightUserId.Text = "User ID :";
            // 
            // posLblUserRightCostCenter
            // 
            this.posLblUserRightCostCenter.AutoSize = true;
            this.posLblUserRightCostCenter.Location = new System.Drawing.Point(55, 91);
            this.posLblUserRightCostCenter.Name = "posLblUserRightCostCenter";
            this.posLblUserRightCostCenter.Size = new System.Drawing.Size(125, 24);
            this.posLblUserRightCostCenter.TabIndex = 1;
            this.posLblUserRightCostCenter.Text = "CostCenter :";
            this.posLblUserRightCostCenter.Click += new System.EventHandler(this.label1_Click);
            // 
            // posLblUserRightEmployeeName
            // 
            this.posLblUserRightEmployeeName.AutoSize = true;
            this.posLblUserRightEmployeeName.Location = new System.Drawing.Point(3, 51);
            this.posLblUserRightEmployeeName.Name = "posLblUserRightEmployeeName";
            this.posLblUserRightEmployeeName.Size = new System.Drawing.Size(177, 24);
            this.posLblUserRightEmployeeName.TabIndex = 0;
            this.posLblUserRightEmployeeName.Text = "Employee Name :";
            // 
            // posTabSettingsAddPrinterToItem
            // 
            this.posTabSettingsAddPrinterToItem.BackColor = System.Drawing.SystemColors.Control;
            this.posTabSettingsAddPrinterToItem.Controls.Add(this.posCmbSettingsAPTIPrinter);
            this.posTabSettingsAddPrinterToItem.Controls.Add(this.posTxtSettingsAPTICCenter);
            this.posTabSettingsAddPrinterToItem.Controls.Add(this.posTxtSettingsAPTIName);
            this.posTabSettingsAddPrinterToItem.Controls.Add(this.posTxtSettingsAPTIItemNo);
            this.posTabSettingsAddPrinterToItem.Controls.Add(this.posTabSettingsAPTILblPrinter);
            this.posTabSettingsAddPrinterToItem.Controls.Add(this.posTabSettingsAPTILblCCenter);
            this.posTabSettingsAddPrinterToItem.Controls.Add(this.posTabSettingsAPTILblName);
            this.posTabSettingsAddPrinterToItem.Controls.Add(this.posTabSettingsAPTILblItemNo);
            this.posTabSettingsAddPrinterToItem.Controls.Add(this.posTabSettingsAPTIBtnHome);
            this.posTabSettingsAddPrinterToItem.Controls.Add(this.posTabSettingsAPTIBtnSave);
            this.posTabSettingsAddPrinterToItem.Controls.Add(this.posTabSettingsAPTIDgvPrinterItem);
            this.posTabSettingsAddPrinterToItem.Location = new System.Drawing.Point(4, 33);
            this.posTabSettingsAddPrinterToItem.Name = "posTabSettingsAddPrinterToItem";
            this.posTabSettingsAddPrinterToItem.Padding = new System.Windows.Forms.Padding(3);
            this.posTabSettingsAddPrinterToItem.Size = new System.Drawing.Size(900, 691);
            this.posTabSettingsAddPrinterToItem.TabIndex = 2;
            this.posTabSettingsAddPrinterToItem.Text = "Add Printer To Item";
            // 
            // posCmbSettingsAPTIPrinter
            // 
            this.posCmbSettingsAPTIPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.posCmbSettingsAPTIPrinter.FormattingEnabled = true;
            this.posCmbSettingsAPTIPrinter.Location = new System.Drawing.Point(579, 147);
            this.posCmbSettingsAPTIPrinter.Name = "posCmbSettingsAPTIPrinter";
            this.posCmbSettingsAPTIPrinter.Size = new System.Drawing.Size(142, 32);
            this.posCmbSettingsAPTIPrinter.TabIndex = 10;
            // 
            // posTxtSettingsAPTICCenter
            // 
            this.posTxtSettingsAPTICCenter.Location = new System.Drawing.Point(579, 74);
            this.posTxtSettingsAPTICCenter.Name = "posTxtSettingsAPTICCenter";
            this.posTxtSettingsAPTICCenter.ReadOnly = true;
            this.posTxtSettingsAPTICCenter.Size = new System.Drawing.Size(142, 29);
            this.posTxtSettingsAPTICCenter.TabIndex = 9;
            // 
            // posTxtSettingsAPTIName
            // 
            this.posTxtSettingsAPTIName.Location = new System.Drawing.Point(579, 39);
            this.posTxtSettingsAPTIName.Name = "posTxtSettingsAPTIName";
            this.posTxtSettingsAPTIName.ReadOnly = true;
            this.posTxtSettingsAPTIName.Size = new System.Drawing.Size(142, 29);
            this.posTxtSettingsAPTIName.TabIndex = 8;
            // 
            // posTxtSettingsAPTIItemNo
            // 
            this.posTxtSettingsAPTIItemNo.Location = new System.Drawing.Point(579, 6);
            this.posTxtSettingsAPTIItemNo.Name = "posTxtSettingsAPTIItemNo";
            this.posTxtSettingsAPTIItemNo.ReadOnly = true;
            this.posTxtSettingsAPTIItemNo.Size = new System.Drawing.Size(142, 29);
            this.posTxtSettingsAPTIItemNo.TabIndex = 7;
            // 
            // posTabSettingsAPTILblPrinter
            // 
            this.posTabSettingsAPTILblPrinter.AutoSize = true;
            this.posTabSettingsAPTILblPrinter.Location = new System.Drawing.Point(490, 150);
            this.posTabSettingsAPTILblPrinter.Name = "posTabSettingsAPTILblPrinter";
            this.posTabSettingsAPTILblPrinter.Size = new System.Drawing.Size(83, 24);
            this.posTabSettingsAPTILblPrinter.TabIndex = 6;
            this.posTabSettingsAPTILblPrinter.Text = "Printer :";
            // 
            // posTabSettingsAPTILblCCenter
            // 
            this.posTabSettingsAPTILblCCenter.AutoSize = true;
            this.posTabSettingsAPTILblCCenter.Location = new System.Drawing.Point(469, 77);
            this.posTabSettingsAPTILblCCenter.Name = "posTabSettingsAPTILblCCenter";
            this.posTabSettingsAPTILblCCenter.Size = new System.Drawing.Size(104, 24);
            this.posTabSettingsAPTILblCCenter.TabIndex = 5;
            this.posTabSettingsAPTILblCCenter.Text = "C.Center :";
            // 
            // posTabSettingsAPTILblName
            // 
            this.posTabSettingsAPTILblName.AutoSize = true;
            this.posTabSettingsAPTILblName.Location = new System.Drawing.Point(496, 42);
            this.posTabSettingsAPTILblName.Name = "posTabSettingsAPTILblName";
            this.posTabSettingsAPTILblName.Size = new System.Drawing.Size(77, 24);
            this.posTabSettingsAPTILblName.TabIndex = 4;
            this.posTabSettingsAPTILblName.Text = "Name :";
            // 
            // posTabSettingsAPTILblItemNo
            // 
            this.posTabSettingsAPTILblItemNo.AutoSize = true;
            this.posTabSettingsAPTILblItemNo.Location = new System.Drawing.Point(479, 9);
            this.posTabSettingsAPTILblItemNo.Name = "posTabSettingsAPTILblItemNo";
            this.posTabSettingsAPTILblItemNo.Size = new System.Drawing.Size(94, 24);
            this.posTabSettingsAPTILblItemNo.TabIndex = 3;
            this.posTabSettingsAPTILblItemNo.Text = "Item No :";
            // 
            // posTabSettingsAPTIBtnHome
            // 
            this.posTabSettingsAPTIBtnHome.Location = new System.Drawing.Point(455, 259);
            this.posTabSettingsAPTIBtnHome.Name = "posTabSettingsAPTIBtnHome";
            this.posTabSettingsAPTIBtnHome.Size = new System.Drawing.Size(266, 57);
            this.posTabSettingsAPTIBtnHome.TabIndex = 2;
            this.posTabSettingsAPTIBtnHome.Text = "Home";
            this.posTabSettingsAPTIBtnHome.UseVisualStyleBackColor = true;
            // 
            // posTabSettingsAPTIBtnSave
            // 
            this.posTabSettingsAPTIBtnSave.Location = new System.Drawing.Point(455, 196);
            this.posTabSettingsAPTIBtnSave.Name = "posTabSettingsAPTIBtnSave";
            this.posTabSettingsAPTIBtnSave.Size = new System.Drawing.Size(266, 57);
            this.posTabSettingsAPTIBtnSave.TabIndex = 1;
            this.posTabSettingsAPTIBtnSave.Text = "Save";
            this.posTabSettingsAPTIBtnSave.UseVisualStyleBackColor = true;
            // 
            // posTabSettingsAPTIDgvPrinterItem
            // 
            this.posTabSettingsAPTIDgvPrinterItem.AllowUserToAddRows = false;
            this.posTabSettingsAPTIDgvPrinterItem.AllowUserToDeleteRows = false;
            this.posTabSettingsAPTIDgvPrinterItem.BackgroundColor = System.Drawing.SystemColors.Control;
            this.posTabSettingsAPTIDgvPrinterItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posTabSettingsAPTIDgvPrinterItem.GridColor = System.Drawing.SystemColors.Control;
            this.posTabSettingsAPTIDgvPrinterItem.Location = new System.Drawing.Point(0, 3);
            this.posTabSettingsAPTIDgvPrinterItem.MultiSelect = false;
            this.posTabSettingsAPTIDgvPrinterItem.Name = "posTabSettingsAPTIDgvPrinterItem";
            this.posTabSettingsAPTIDgvPrinterItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.posTabSettingsAPTIDgvPrinterItem.Size = new System.Drawing.Size(450, 443);
            this.posTabSettingsAPTIDgvPrinterItem.TabIndex = 0;
            // 
            // posSettingsTabOpenItem
            // 
            this.posSettingsTabOpenItem.BackColor = System.Drawing.SystemColors.Control;
            this.posSettingsTabOpenItem.Controls.Add(this.posBtnSettingsOpenItemSave);
            this.posSettingsTabOpenItem.Controls.Add(this.posCmbSettingsOpenItemSaleType);
            this.posSettingsTabOpenItem.Controls.Add(this.posLblSettingsOpenItemSaleType);
            this.posSettingsTabOpenItem.Controls.Add(this.posDgvSettingsOpenItem);
            this.posSettingsTabOpenItem.Controls.Add(this.posCmbSettingsOpenItemCostCenter);
            this.posSettingsTabOpenItem.Controls.Add(this.posLblSettingsOpenItem);
            this.posSettingsTabOpenItem.Location = new System.Drawing.Point(4, 33);
            this.posSettingsTabOpenItem.Name = "posSettingsTabOpenItem";
            this.posSettingsTabOpenItem.Padding = new System.Windows.Forms.Padding(3);
            this.posSettingsTabOpenItem.Size = new System.Drawing.Size(900, 691);
            this.posSettingsTabOpenItem.TabIndex = 3;
            this.posSettingsTabOpenItem.Text = "Open Item";
            // 
            // posBtnSettingsOpenItemSave
            // 
            this.posBtnSettingsOpenItemSave.Location = new System.Drawing.Point(747, 49);
            this.posBtnSettingsOpenItemSave.Name = "posBtnSettingsOpenItemSave";
            this.posBtnSettingsOpenItemSave.Size = new System.Drawing.Size(141, 57);
            this.posBtnSettingsOpenItemSave.TabIndex = 5;
            this.posBtnSettingsOpenItemSave.Text = "Update";
            this.posBtnSettingsOpenItemSave.UseVisualStyleBackColor = true;
            // 
            // posCmbSettingsOpenItemSaleType
            // 
            this.posCmbSettingsOpenItemSaleType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.posCmbSettingsOpenItemSaleType.FormattingEnabled = true;
            this.posCmbSettingsOpenItemSaleType.Location = new System.Drawing.Point(532, 11);
            this.posCmbSettingsOpenItemSaleType.Name = "posCmbSettingsOpenItemSaleType";
            this.posCmbSettingsOpenItemSaleType.Size = new System.Drawing.Size(209, 32);
            this.posCmbSettingsOpenItemSaleType.TabIndex = 4;
            // 
            // posLblSettingsOpenItemSaleType
            // 
            this.posLblSettingsOpenItemSaleType.AutoSize = true;
            this.posLblSettingsOpenItemSaleType.Location = new System.Drawing.Point(395, 14);
            this.posLblSettingsOpenItemSaleType.Name = "posLblSettingsOpenItemSaleType";
            this.posLblSettingsOpenItemSaleType.Size = new System.Drawing.Size(131, 24);
            this.posLblSettingsOpenItemSaleType.TabIndex = 3;
            this.posLblSettingsOpenItemSaleType.Text = "Cost Center :";
            // 
            // posDgvSettingsOpenItem
            // 
            this.posDgvSettingsOpenItem.AllowUserToAddRows = false;
            this.posDgvSettingsOpenItem.AllowUserToDeleteRows = false;
            this.posDgvSettingsOpenItem.BackgroundColor = System.Drawing.SystemColors.Control;
            this.posDgvSettingsOpenItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvSettingsOpenItem.Location = new System.Drawing.Point(6, 49);
            this.posDgvSettingsOpenItem.Name = "posDgvSettingsOpenItem";
            this.posDgvSettingsOpenItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.posDgvSettingsOpenItem.Size = new System.Drawing.Size(735, 636);
            this.posDgvSettingsOpenItem.TabIndex = 2;
            // 
            // posCmbSettingsOpenItemCostCenter
            // 
            this.posCmbSettingsOpenItemCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.posCmbSettingsOpenItemCostCenter.FormattingEnabled = true;
            this.posCmbSettingsOpenItemCostCenter.Location = new System.Drawing.Point(156, 11);
            this.posCmbSettingsOpenItemCostCenter.Name = "posCmbSettingsOpenItemCostCenter";
            this.posCmbSettingsOpenItemCostCenter.Size = new System.Drawing.Size(209, 32);
            this.posCmbSettingsOpenItemCostCenter.TabIndex = 1;
            this.posCmbSettingsOpenItemCostCenter.SelectedIndexChanged += new System.EventHandler(this.posCmbSettingsOpenItemCostCenter_SelectedIndexChanged);
            // 
            // posLblSettingsOpenItem
            // 
            this.posLblSettingsOpenItem.AutoSize = true;
            this.posLblSettingsOpenItem.Location = new System.Drawing.Point(19, 14);
            this.posLblSettingsOpenItem.Name = "posLblSettingsOpenItem";
            this.posLblSettingsOpenItem.Size = new System.Drawing.Size(131, 24);
            this.posLblSettingsOpenItem.TabIndex = 0;
            this.posLblSettingsOpenItem.Text = "Cost Center :";
            // 
            // posBtnUserRightHome
            // 
            this.posBtnUserRightHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnUserRightHome.Location = new System.Drawing.Point(2, 3);
            this.posBtnUserRightHome.Name = "posBtnUserRightHome";
            this.posBtnUserRightHome.Size = new System.Drawing.Size(94, 83);
            this.posBtnUserRightHome.TabIndex = 32;
            this.posBtnUserRightHome.Text = "Home";
            this.posBtnUserRightHome.UseVisualStyleBackColor = true;
            // 
            // posGuiSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.ControlBox = false;
            this.Controls.Add(this.posBtnUserRightHome);
            this.Controls.Add(this.posTabSettings);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "posGuiSettings";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SETTINGS";
            this.posTabSettings.ResumeLayout(false);
            this.posTabSettingsUsers.ResumeLayout(false);
            this.posTabSettingsUsers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvSettingUser)).EndInit();
            this.posTabSettingsUserRight.ResumeLayout(false);
            this.posTabSettingsUserRight.PerformLayout();
            this.posTabSettingsAddPrinterToItem.ResumeLayout(false);
            this.posTabSettingsAddPrinterToItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posTabSettingsAPTIDgvPrinterItem)).EndInit();
            this.posSettingsTabOpenItem.ResumeLayout(false);
            this.posSettingsTabOpenItem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvSettingsOpenItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ComboBox posCmbUserAllowPwChange;
        internal System.Windows.Forms.TextBox posTxtUserName;
        internal System.Windows.Forms.TextBox posTxtUserUserId;
        internal System.Windows.Forms.TextBox posTxtUserPassword;
        internal System.Windows.Forms.DateTimePicker posDtpUserExpireDate;
        internal System.Windows.Forms.ComboBox posCmbUserUserType;
        internal System.Windows.Forms.ComboBox posCmbUserRightCostCenter;
        internal System.Windows.Forms.ComboBox posCmbUserRightUserId;
        internal System.Windows.Forms.TextBox posTxtUserRightCostCenter;
        internal System.Windows.Forms.TextBox posTxtUserRightEmployeeName;
        internal System.Windows.Forms.CheckBox posChkUserRightAddPrinter;
        internal System.Windows.Forms.CheckBox posChkUserRightAddItemRemarks;
        internal System.Windows.Forms.CheckBox posChkUserRightPrinterToItems;
        internal System.Windows.Forms.CheckBox posChkUserRightReports;
        internal System.Windows.Forms.CheckBox posChkUserRightSettings;
        internal System.Windows.Forms.CheckBox posChkUserRightSettlement;
        internal System.Windows.Forms.CheckBox posChkUserRightEntitle;
        internal System.Windows.Forms.CheckBox posChkUserRightAI;
        internal System.Windows.Forms.CheckBox posChkUserRightBill;
        internal System.Windows.Forms.CheckBox posChkUserRightKotBot;
        internal System.Windows.Forms.Button posBtnUserRightHome;
        internal System.Windows.Forms.Button posBtnUserRightChkbxClearAll;
        internal System.Windows.Forms.Button posBtnUserRightSave;
        internal System.Windows.Forms.Button posBtnUserSave;
        internal System.Windows.Forms.ComboBox posCmbSettingsAPTIPrinter;
        internal System.Windows.Forms.TextBox posTxtSettingsAPTICCenter;
        internal System.Windows.Forms.TextBox posTxtSettingsAPTIName;
        internal System.Windows.Forms.TextBox posTxtSettingsAPTIItemNo;
        internal System.Windows.Forms.DataGridView posTabSettingsAPTIDgvPrinterItem;
        internal System.Windows.Forms.DataGridView posDgvSettingUser;
        internal System.Windows.Forms.ComboBox posCmbSettingsOpenItemCostCenter;
        internal System.Windows.Forms.ComboBox posCmbSettingsOpenItemSaleType;
        internal System.Windows.Forms.DataGridView posDgvSettingsOpenItem;
        internal System.Windows.Forms.TabControl posTabSettings;
        internal System.Windows.Forms.TabPage posTabSettingsUsers;
        internal System.Windows.Forms.TabPage posTabSettingsUserRight;
        internal System.Windows.Forms.Label posLblSettingsUserAllowPwChange;
        internal System.Windows.Forms.Label posLblSettingsUserExpireDate;
        internal System.Windows.Forms.Label posLblSettingsUserUserType;
        internal System.Windows.Forms.Label posLblSettingsUserPassword;
        internal System.Windows.Forms.Label posLblSettingsUserUserID;
        internal System.Windows.Forms.Label posLblSettingsUserUserName;
        internal System.Windows.Forms.Label posLblUserRightCostCenter;
        internal System.Windows.Forms.Label posLblUserRightEmployeeName;
        internal System.Windows.Forms.Label posLblUserRightUserId;
        internal System.Windows.Forms.Label posLblUserRightKotBot;
        internal System.Windows.Forms.Label posCmbUserRightCmbCostCenter;
        internal System.Windows.Forms.Label posLblUserRightTopic;
        internal System.Windows.Forms.Label posLblUserRightAddPrinters;
        internal System.Windows.Forms.Label posLblUserRightAddItemRemarks;
        internal System.Windows.Forms.Label posLblUserRightPrinterToItems;
        internal System.Windows.Forms.Label posLblUserRightReports;
        internal System.Windows.Forms.Label posLblUserRightSecurity;
        internal System.Windows.Forms.Label posLblUserRightSettlement;
        internal System.Windows.Forms.Label posLblUserRightEntittle;
        internal System.Windows.Forms.Label posLblUserRightAI;
        internal System.Windows.Forms.Label posLblUserRightBill;
        internal System.Windows.Forms.Button posBtnUserRightChkBxSelectAll;
        internal System.Windows.Forms.TabPage posTabSettingsAddPrinterToItem;
        internal System.Windows.Forms.Label posTabSettingsAPTILblPrinter;
        internal System.Windows.Forms.Label posTabSettingsAPTILblCCenter;
        internal System.Windows.Forms.Label posTabSettingsAPTILblName;
        internal System.Windows.Forms.Label posTabSettingsAPTILblItemNo;
        internal System.Windows.Forms.Button posTabSettingsAPTIBtnHome;
        internal System.Windows.Forms.Button posTabSettingsAPTIBtnSave;
        internal System.Windows.Forms.TabPage posSettingsTabOpenItem;
        internal System.Windows.Forms.Label posLblSettingsOpenItem;
        internal System.Windows.Forms.Label posLblSettingsOpenItemSaleType;
        internal System.Windows.Forms.Button posBtnSettingsOpenItemSave;
    }
}