﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public partial class posGuiOrderReport : Form
    {
        posGuiDgvReportOrderDetail orderDetail;
        posDsReportOrder ds;
        posGuiCrvOrderReport Crv;
        public posGuiOrderReport()
        {

            InitializeComponent();
            this.orderDetail = new posGuiDgvReportOrderDetail();
            this.ds = new posDsReportOrder();
        }

        

        private void posTxtReportOrderFromDate_TextChanged(object sender, EventArgs e)
        {

        }

        private void posDtpReportOrderFromDate_ValueChanged(object sender, EventArgs e)
        {
            posTxtReportOrderFromDate.Text = posDtpReportOrderFromDate.Value.Date.ToString("yyyy-MM-dd");
        }

        private void posDtpReportOrderToDate_ValueChanged(object sender, EventArgs e)
        {
            posTxtReportOrderToDate.Text = posDtpReportOrderToDate.Value.Date.ToString("yyyy-MM-dd");
        }

        private void posBtnReportOrderSearch_Click(object sender, EventArgs e)
        {
            if (posTxtReportOrderNo.Text != "" || posTxtReportOrderRoomNo.Text != "" || posTxtReportOrderBillNo.Text != "" || posTxtReportOrderTableNo.Text != "" || posTxtReportOrderStaffNo.Text != "" || posTxtReportOrderStewardNo.Text != "" || posTxtReportOrderFromDate.Text != "" || posTxtReportOrderToDate.Text != "" && DateTime.Parse(posTxtReportOrderFromDate.Text) >= DateTime.Parse(posTxtReportOrderToDate.Text)) 
            {

                ReportDocument cryRpt = new ReportDocument();
                TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
                TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
                ConnectionInfo crConnectionInfo = new ConnectionInfo();
                Tables CrTables;

                crConnectionInfo.ServerName = "TBHSYSSERVER";
                crConnectionInfo.DatabaseName = "SMARTTBHPOS";
                crConnectionInfo.UserID = "sa";
                crConnectionInfo.Password = "";

                posStaticLoadOnce.getConnection();
                String sql = null;
                int counter = 0;
                sql = "select FOOrderMaster.OrdM_No,FOOrderMaster.OrdM_Date,FOOrderMaster.OrdM_Stw,FOOrderMaster.OrdM_Table,"
                        + "FOOrderMaster.OrdM_CusType,FOOrderMaster.OrdM_BillNo,FOOrderMaster.OrdM_Room,FOOrderMaster.OrdM_Staff,"
                        + "FOOrderMaster.SaTy_Code,OrdM_KOTBOT from FOOrderMaster "
                        + "WHERE OrdM_KOTBOT LIKE 'POS%' AND ";
                if (!posTxtReportOrderNo.Text.Equals(""))
                {
                    sql += "OrdM_No='" + posTxtReportOrderNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportOrderTableNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sql += " and ";
                        counter = 0;
                    }
                    sql += "OrdM_Table='" + posTxtReportOrderTableNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportOrderBillNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sql += " and ";
                        counter = 0;
                    }
                    sql += "OrdM_BillNo='" + posTxtReportOrderBillNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportOrderRoomNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sql += " and ";
                        counter = 0;
                    }
                    sql += "OrdM_Room ='" + posTxtReportOrderRoomNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportOrderStewardNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sql += " and ";
                        counter = 0;
                    }
                    sql += "OrdM_Stw ='" + posTxtReportOrderStewardNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportOrderStaffNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sql += "and ";
                        counter = 0;
                    }
                    sql += "OrdM_Staff='" + posTxtReportOrderStaffNo.Text + "'";
                    counter++;
                }

                if ((!posTxtReportOrderFromDate.Text.Equals("")) && (!posTxtReportOrderToDate.Text.Equals("")))
                {
                    if (counter == 1)
                    {
                        sql += " and ";
                        counter = 0;
                    }
                    sql += "OrdM_Date BETWEEN '" + posTxtReportOrderFromDate.Text.ToString() + "' and '" + posTxtReportOrderToDate.Text + "'";
                }

      //          sql += "Group By FOOrderMaster.OrdM_No,FOOrderMaster.OrdM_Table,"
      // + "FOOrderMaster.OrdM_CusType,FOOrderMaster.OrdM_BillNo,FOOrderMaster.OrdM_Room,FOOrderMaster.OrdM_Staff,"
      //+ "FOOrderMaster.SaTy_Code,FOOrderMaster.OrdM_Stw ";
                SqlDataAdapter dscmd = new SqlDataAdapter(sql, posStaticLoadOnce.getConnection());

                //dscmd.Fill(this.ds, "order");

                //posStaticLoadOnce.getConnection().Close();
                dscmd.Fill(this.ds, "order");
                posDgvReportOrder.DataSource = this.ds.Tables[1];


            }
        }

        private void posBtnReportOrderDetailShow_Click(object sender, EventArgs e)
        {
            this.orderDetail = null;
            if (orderDetail == null)
            {
                this.orderDetail = new posGuiDgvReportOrderDetail();

            }
            try
            {
                String orderId = posDgvReportOrder.CurrentRow.Cells[0].Value.ToString();
                this.orderDetail.posSetOrderNo(new String[] { orderId });
                this.orderDetail.Show();
            }
            catch (Exception) { }
            

        }

        private void posBtnReportOrderPrint_Click(object sender, EventArgs e)
        {
            this.Crv = null;
            if (this.Crv == null) {
                this.Crv = new posGuiCrvOrderReport();
            }
             
            Crv.Show();
            ReportDocument cryRpt = new ReportDocument();
            posCrystalReportOrder objRpt = new posCrystalReportOrder();
            objRpt.SetDataSource(ds.Tables[1]);
            Crv.posGetOrderReportViewer().ReportSource = objRpt;
            Crv.posGetOrderReportViewer().Refresh();
        }

        private void btnOderReportBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void posGuiOrderReport_Load(object sender, EventArgs e)
        {

        }
    }
}
