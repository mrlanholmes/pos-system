﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public partial class posGuiHDDValidation : Form
    {
        private posClsLoginAdmin admin;
        public posGuiHDDValidation()
        {
            InitializeComponent();
        }
        public void posSetAdminObject(posClsLoginAdmin adminObject) {
            this.admin = adminObject;
        }
        private void btnHddNo_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnHddYes_Click(object sender, EventArgs e)
        {
            posClsHDD hdd = new posClsHDD();            
            if (hdd.setHDDSerial()) {
                this.Hide();
                this.admin.posShowGuiAdmin();
                this.admin.posClearTextFields();
            }
            else
            {
                MessageBox.Show("Cann't store HDD serial number..");
            }
        }
    }
}
