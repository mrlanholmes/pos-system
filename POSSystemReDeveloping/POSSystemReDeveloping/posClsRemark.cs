﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    class posClsRemark : pos
    {
        private posGuiRemark remark;
        private posClsGuestOrder guestOrder;
        private posClsFullKeyBoard fullKeyBoard;
        private posClsNumericPad numPad;
        public posClsRemark() {
            this.remark = new posGuiRemark();
            this.remark.posBtnBack.Click += this.posBtnCancelClick;
            this.remark.posBtnUpdate.Click += this.posBtnUpdateClick;
            this.remark.Load += this.posFrmLoad;
            this.remark.posCmbRemark.TextChanged += this.posCmbRemarkTextChange;
            this.remark.posRtxtYourRemark.Click += this.posRtxtYourRemarkClick;
        }
        // function

        // sql
        private String[,] posGetItemRemarks()
        {
            this.qry = "SELECT Item_Remarks_ID,Item_Remarks_Description FROM Item_Remarks order by Item_Remarks_Description ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount != 0) { return this.dt; }
            return null;
        }
        // Event
        private void posRtxtYourRemarkClick(object sender, EventArgs e)
        {
            this.fullKeyBoard.posShowFullKeyBoardPad();
        }
        private void posBtnCancelClick(object sender, EventArgs e)
        {
            this.guestOrder.posGetGuiGuestOrderObject().TopMost = true;
            this.guestOrder.posGetGuiGuestOrderObject().Focus();
            this.posHideRemarkPanel();
            this.fullKeyBoard.posHideFullKeyBoardPad();
            this.numPad.posHideNumericPad();
        }
        private void posBtnUpdateClick(object sender, EventArgs e)
        { 
            int currentIndex = this.guestOrder.posGetGuiGuestOrderObject().posDgvGuestOrderOrder.CurrentRow.Index;
            String unchangebleValue = "";
            try {  unchangebleValue = this.remark.posCmbRemark.SelectedItem.ToString(); }
            catch (Exception) { unchangebleValue = ""; }
            String changebleValue = "";
            try {  changebleValue = this.remark.posRtxtYourRemark.Text; }
            catch (Exception) { changebleValue = ""; }
            try {
                this.guestOrder.posGetGuiGuestOrderObject().posDgvGuestOrderOrder.Rows[currentIndex].Cells[8].Value = unchangebleValue.Trim();
                this.guestOrder.posGetGuiGuestOrderObject().posDgvGuestOrderOrder.Rows[currentIndex].Cells[9].Value = changebleValue.Trim();
                unchangebleValue += changebleValue.Equals("") ? "" :  " , ";
                this.guestOrder.posGetGuiGuestOrderObject().posDgvGuestOrderOrder.Rows[currentIndex].Cells[7].Value = unchangebleValue.Trim() + changebleValue.Trim();  
                this.guestOrder.posGetGuiGuestOrderObject().posDgvGuestOrderOrder.Rows[currentIndex].Cells[7].ToolTipText = unchangebleValue.Trim()  + changebleValue.Trim();
            }
            catch (Exception) { }
            this.fullKeyBoard.posHideFullKeyBoardPad();
            this.posHideRemarkPanel();
        }
        private void posCmbRemarkTextChange(object sender, EventArgs e)
        {
           // this.remark.posCmbRemark.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            //this.remark.posCmbRemark.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
           // this.remark.posCmbRemark.AutoCompleteSource = AutoCompleteSource.ListItems;
           // this.remark.posCmbRemark.DroppedDown = true;
        }
        private void posFrmLoad(object sender, EventArgs e)
        {
            String[,] itemRemark = this.posGetItemRemarks();
            int itemRemarkCount = posStaticLoadOnce.PosRowCount;
            for (int i = 0; i < itemRemarkCount; i++) {
                this.remark.posCmbRemark.Items.Add("" + itemRemark[i,1]);
            }
            
            
        }
        // public
        public void posSetRemarkPanelLocation(String[] data) {
            int x = 0;
            int y = 0;
            try {
                 x= Convert.ToInt32(data[0]);
                 y= Convert.ToInt32(data[1]);
                 this.remark.Location = new Point(x, y);
            }
            catch (Exception) { }
            
            
        }
        public void posSetGuestOrderObject(posClsGuestOrder guestOrderObject)
        {
            this.guestOrder = guestOrderObject;
        }
        public void posSetFullKeyBoard(posClsFullKeyBoard fullKeyBoardObject) {
            this.fullKeyBoard = fullKeyBoardObject;
            this.fullKeyBoard.posSetRemarkObject(this.remark);
        }
        public void posSetNumPad(posClsNumericPad numPadObject) {
            this.numPad = numPadObject;
        }
        public void posShowRemarkPanel(){
            this.numPad.posHideNumericPad();
            this.remark.Show();
            this.guestOrder.posGetGuiGuestOrderObject().TopMost = false;
            this.remark.TopMost = true;            
            this.remark.Focus();
            try { this.remark.posCmbRemark.SelectedIndex = -1; }
            catch (Exception) { }
            try { this.remark.posRtxtYourRemark.Text = ""; }
            catch (Exception) { }
            try
            {
                int currentIndex = this.guestOrder.posGetGuiGuestOrderObject().posDgvGuestOrderOrder.CurrentRow.Index;
                this.remark.posCmbRemark.SelectedItem = this.guestOrder.posGetGuiGuestOrderObject().posDgvGuestOrderOrder.Rows[currentIndex].Cells[8].Value.ToString().Trim();
                this.remark.posRtxtYourRemark.Text = this.guestOrder.posGetGuiGuestOrderObject().posDgvGuestOrderOrder.Rows[currentIndex].Cells[9].Value.ToString().Trim();
            }
            catch (Exception) { }
            this.posSetRemarkPanelLocation(new String[] {"100","100" });
        }
        public void posHideRemarkPanel() {
            this.remark.Hide();
        }
        public void posDisposePanel() {
            this.remark.Dispose();
        }
    }
}
