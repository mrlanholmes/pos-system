﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace POSSystemReDeveloping
{
    class fileRead
    {
        static String[] fileDataAry;
        String filePath;
        public fileRead() {
            fileDataAry = new String[4];
            this.filePath = "";
        }
        public String FilePath { 
            set{
                this.filePath = value;
            }
        }
        public String readLineAccordingLineNo(int LineNo) {
            String line = null;
            StreamReader file = new StreamReader(this.filePath);
            for(int i=0; (line = file.ReadLine()) != null;i++){
                if (LineNo == i) {
                    return line;
                }
            }
            file.Close();
            return null;
        }
        public void writeFileAccordingStringArray(String[] ary) {
            String line = null;
            StreamWriter file = new StreamWriter(this.filePath);
            for (int i = 0; i < fileDataAry.Length; i++) {
                line = line + fileDataAry[i] + "\n";
            }
            file.WriteLine(line);
            file.Close();
        }
        
    }
}
