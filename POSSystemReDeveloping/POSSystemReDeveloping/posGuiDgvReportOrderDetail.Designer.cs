﻿namespace POSSystemReDeveloping
{
    partial class posGuiDgvReportOrderDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.posBtnReportsOrderDetailPrint = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.posDgvReportOrderDetail = new System.Windows.Forms.DataGridView();
            this.btnReportOrderDetailBack = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvReportOrderDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.posBtnReportsOrderDetailPrint);
            this.groupBox1.Location = new System.Drawing.Point(121, -2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(886, 102);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // posBtnReportsOrderDetailPrint
            // 
            this.posBtnReportsOrderDetailPrint.Location = new System.Drawing.Point(379, 35);
            this.posBtnReportsOrderDetailPrint.Name = "posBtnReportsOrderDetailPrint";
            this.posBtnReportsOrderDetailPrint.Size = new System.Drawing.Size(256, 39);
            this.posBtnReportsOrderDetailPrint.TabIndex = 0;
            this.posBtnReportsOrderDetailPrint.Text = "Print";
            this.posBtnReportsOrderDetailPrint.UseVisualStyleBackColor = true;
            this.posBtnReportsOrderDetailPrint.Click += new System.EventHandler(this.posBtnReportsOrderDetailPrint_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.posDgvReportOrderDetail);
            this.groupBox2.Location = new System.Drawing.Point(121, 100);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(886, 621);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // posDgvReportOrderDetail
            // 
            this.posDgvReportOrderDetail.AllowUserToAddRows = false;
            this.posDgvReportOrderDetail.AllowUserToDeleteRows = false;
            this.posDgvReportOrderDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvReportOrderDetail.Location = new System.Drawing.Point(3, 16);
            this.posDgvReportOrderDetail.Name = "posDgvReportOrderDetail";
            this.posDgvReportOrderDetail.ReadOnly = true;
            this.posDgvReportOrderDetail.Size = new System.Drawing.Size(997, 602);
            this.posDgvReportOrderDetail.TabIndex = 0;
            // 
            // btnReportOrderDetailBack
            // 
            this.btnReportOrderDetailBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportOrderDetailBack.Location = new System.Drawing.Point(3, 1);
            this.btnReportOrderDetailBack.Name = "btnReportOrderDetailBack";
            this.btnReportOrderDetailBack.Size = new System.Drawing.Size(112, 99);
            this.btnReportOrderDetailBack.TabIndex = 1;
            this.btnReportOrderDetailBack.Text = "Back";
            this.btnReportOrderDetailBack.UseVisualStyleBackColor = true;
            this.btnReportOrderDetailBack.Click += new System.EventHandler(this.btnReportOrderDetailBack_Click);
            // 
            // posGuiDgvReportOrderDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 733);
            this.ControlBox = false;
            this.Controls.Add(this.btnReportOrderDetailBack);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "posGuiDgvReportOrderDetail";
            this.Text = "Report Details";
            this.Load += new System.EventHandler(this.posGuiDgvReportOrderDetail_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.posDgvReportOrderDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button posBtnReportsOrderDetailPrint;
        private System.Windows.Forms.DataGridView posDgvReportOrderDetail;
        private System.Windows.Forms.Button btnReportOrderDetailBack;
    }
}