﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
namespace POSSystemReDeveloping
{
    public class posClsGuestOrder : pos
    {
        
        private DataGridView dgvGuest;
        private posGuiGuestOrder guiGuest;
        private DataGridView dgvSaleType;
        private DataGridView dgvExecutive;
        private DataGridView dgvOpenTable;
        private DataGridView dgvOrderCancel;
        private DataGridView dgvOrderCancelItem;
        private DataGridView dgvOpenTableItem;
        private DataGridView dgvMealItem;
        private TextBox txtOrderCancelDate;
        private TextBox txtOrderCancelItemQty;
        private TextBox txtOrderCancelItemTotal;
        private Button btnOderCancel;
        private Button btnItemCancel;
        private Button btnGuestAdd;        
        private String checkNo;
        private posGuiHome home;
        private posClsPrinter printer;
        private Panel pnlMeal;
        private Panel pnlMealSecondCategory;
        private Button[] btnMeal;
        private String[,] dbMeal;
        private int mealRowCount;
        private String[] mealType;
        private Button[] btnMealItem;
        private int dbMealItemRowCount;
        private String[,] mealItem;
        private Button posBtnMealItemAdd;
        String[] data;
        private posClsLoginAdmin admin;
        private String[,] posItemRemark;
        private posClsNumericPad numPad;
        private posClsFullKeyBoard fullKeyBoard;
        private String posGuestStatus ;
        private String posClickingButton;
        private int mealCode;
        private TextBox txtSearch;
        private TextBox txtMealItemSearch;
        private String secondryCategoryCode;
        private posClsRemark remark;

        public posClsGuestOrder( posGuiHome homeObject, posClsLoginAdmin adminObject)
        {
            this.guiGuest = new posGuiGuestOrder();
            
            this.guiGuest.TopMost = true;
            this.home = homeObject;           
            this.dgvGuest = new DataGridView();
            this.dgvSaleType = new DataGridView();
            this.dgvExecutive = new DataGridView();
            this.dgvOpenTable = new DataGridView();
            this.dgvOrderCancel = new DataGridView();
            this.dgvOrderCancelItem = new DataGridView();
            this.dgvOpenTableItem = new DataGridView();
            this.dgvMealItem = new DataGridView();
            this.txtOrderCancelDate = new TextBox();
            this.txtOrderCancelItemQty = new TextBox();
            this.txtOrderCancelItemTotal = new TextBox();
            this.txtMealItemSearch = new TextBox();
            this.txtSearch = new TextBox();
            this.btnOderCancel = new Button();
            this.btnItemCancel = new Button();
            
            this.posBtnMealItemAdd = new Button();
            this.posSetFormProperties();
            //this.guiGuest.Load += this.posGuestOrderFormLoad;
            this.btnGuestAdd = new Button();
            this.data = new String[20];
            this.checkNo = "0";
            this.printer = new posClsPrinter();
            this.admin = adminObject;
            this.pnlMeal = new Panel();
            this.pnlMealSecondCategory = new Panel();
            this.posSetGuestOrderEvent();
            this.posGuestStatus = "";
            this.posClickingButton = "";
            this.btnOderCancel.Click += this.posBtnOrderCancelClick;
            this.btnItemCancel.Click += this.posBtnOrderCancelItemClick;
            this.posBtnMealItemAdd.Click += posSetMealItemAddClick;
            this.dgvMealItem.CellDoubleClick += posSetMealItemAddClick;
            this.txtSearch.TextChanged += this.posSetGuestOrderSearchBoxTextChanged;
            this.txtMealItemSearch.TextChanged += this.posSetGuestOrderMealItemSearchBoxTextChanged;
            this.txtSearch.Click += this.posSetGuestOrderSearchBoxClick;
            this.txtMealItemSearch.Click += this.posSetGuestOrderMealItemSearchBoxClick;
            this.remark = new posClsRemark();
        }
        //Event 
        public void posSetNumericKeyPadObject(posClsNumericPad numPadObject)
        {
            this.numPad = numPadObject;
            this.numPad.posSetGuestOrderObject(this.guiGuest);
        }
        public void posSetFullKeyBoardObject(posClsFullKeyBoard fullKeyBoardObject)
        {
            this.fullKeyBoard = fullKeyBoardObject;
        }
        public void posSetGuestType(String str) {
            this.posGuestStatus = str;
            this.posClickingButton = str;
        }
        private void posSetGuestOrderBtnDelete(object sender, EventArgs e)
        {
            try {
                if (this.guiGuest.posDgvGuestOrderOrder.SelectedRows.Count == 0) { this.posSetGuestOrderErrMsg("Please select a row to Delete..."); }
                else { this.guiGuest.posDgvGuestOrderOrder.Rows.RemoveAt(this.guiGuest.posDgvGuestOrderOrder.CurrentCell.RowIndex); }
                
            }
            catch (Exception ex) {  } 
            try
            {
                int itemRowCount = this.guiGuest.posDgvGuestOrderOrder.RowCount;
                for (int i = 0; i < itemRowCount; i++)
                {
                    this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[0].Value = i + 1;
                }
            }
            catch (Exception ex) { }
        }
        private void posSetGuestOrderSearchBoxTextChanged(object sender, EventArgs e)
        {
            String str = this.txtSearch.Text;
            if (!str.Equals("")) {
                this.dgvGuest.DataSource = null;
                if (this.guiGuest.posRdbGuestOrderGuest.Checked)
                {
                    this.dgvGuest.DataSource = this.posGetGuestCheckingDataSearch(new String[] { str.Trim() });
                    try { this.posSetdgvGuestGuestProperties(); }
                    catch (Exception) { }
                    for (int i = 0; i < this.dgvGuest.Rows.Count; i++)
                    {
                        this.dgvGuest.Rows[i].Height = 40;
                    }
                }
                else {
                    this.dgvExecutive.DataSource = null;
                    this.dgvExecutive.DataSource=this.posGetExecutiveDataSearch(new String[] {str.Trim()});
                    this.posSetdgvGuestExecutiveProperties();
                    for (int i = 0; i < this.dgvExecutive.Rows.Count; i++)
                    {
                        this.dgvExecutive.Rows[i].Height = 40;
                    }
                }      
            }
        }
        private void posSetGuestOrderMealItemSearchBoxTextChanged(object sender, EventArgs e)
        {
            String str = this.txtMealItemSearch.Text;
            if (!str.Equals(""))
            {
                this.posShowMealItemPanel(new String[] { this.mealType[0], this.guiGuest.posTxtGuestOrderSaleType.Text, posStaticLoadOnce.PosCostCenterCode[0], "" +this.secondryCategoryCode,str.Trim() });             
            }
        }
        private void posSetGuestOrderSearchBoxClick(object sender, EventArgs e) {
             
            this.numPad.posShowNumericPad();
            int x = this.guiGuest.Location.X;
            int y = this.guiGuest.Location.Y;
            this.numPad.posSetViewLocationNumericPad(new int[]{x + 688,y + 400});
            this.txtSearch.Focus();
        }
        private void posSetGuestOrderMealItemSearchBoxClick(object sender, EventArgs e)
        {
            this.numPad.posHideNumericPad();
            this.fullKeyBoard.posShowFullKeyBoardPad();
            this.fullKeyBoard.posSetViewLocationFullKeyBoard(new int[] { 0, 0 });
            this.txtMealItemSearch.Focus();
        }
        //Sql
        private DataTable posGetGuestCheckingDataSearch(String[] data)
        {
            this.qry = "SELECT  FOCheckinMaster.Room_Code As 'Room No', FOCheckinMaster.Bas_Code as 'B.Code'," +
                        " FOCheckinMaster.MChk_Number, FOGuest.Tit_Code + ' ' + RTRIM(FOGuest.Guest_FirstName) + ' ' + RTRIM(FOGuest.Guest_LastName) AS 'Name', FOCheckinMaster.MChk_StopCR as KK, " +
                        " FOCheckinMaster.MChk_NoOfAdults + FOCheckinMaster.MChk_NoOfKids AS PAX ,(select TouOpe_Name from FOTourOperator where TouOpe_Code = FOCheckinMaster.TouOpe_Code) as 'Tour_Name'" +
                        ",FOCheckinMaster.MChk_DepartureDate as 'Departure Date' " +
                        " FROM  FOCheckinDetail INNER JOIN " +
                        " FOCheckinMaster ON FOCheckinDetail.MChk_Number = FOCheckinMaster.MChk_Number INNER JOIN " +
                        " FOGuest ON FOCheckinDetail.Guest_Number = FOGuest.Guest_Number " +
                        " and  (FOCheckinDetail.DChk_Main = 1) AND (FOCheckinMaster.MChk_Status = 'I')   and FOCheckinMaster.Room_Code like '%"+data[0]+"%'";
            if (this.posGuestStatus.Equals("AI"))
            {
                this.qry += " and FOCheckinMaster.Bas_Code='AI' ";
            }
            if (this.posGuestStatus.Equals("ENT"))
            {
                this.qry += " and FOCheckinMaster.Bas_Code <> 'AI' ";
            }
            this.qry += "  order by FOCheckinMaster.Room_Code";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return posStaticLoadOnce.PosDataTable;
            }
            return null;
        }
        private DataTable posGetGuestCheckingData() {
            this.qry = "SELECT  FOCheckinMaster.Room_Code As 'Room No', FOCheckinMaster.Bas_Code as 'B.Code'," +
                        " FOCheckinMaster.MChk_Number, FOGuest.Tit_Code + ' ' + RTRIM(FOGuest.Guest_FirstName) + ' ' + RTRIM(FOGuest.Guest_LastName) AS 'Name', FOCheckinMaster.MChk_StopCR as KK, " +
                        " FOCheckinMaster.MChk_NoOfAdults + FOCheckinMaster.MChk_NoOfKids AS PAX ,(select TouOpe_Name from FOTourOperator where TouOpe_Code = FOCheckinMaster.TouOpe_Code) as 'Tour_Name'" +
                        ",FOCheckinMaster.MChk_DepartureDate as 'Departure Date' " +
                        " FROM  FOCheckinDetail INNER JOIN "+
                        " FOCheckinMaster ON FOCheckinDetail.MChk_Number = FOCheckinMaster.MChk_Number INNER JOIN "+
                        " FOGuest ON FOCheckinDetail.Guest_Number = FOGuest.Guest_Number "+
                        " and  (FOCheckinDetail.DChk_Main = 1) AND (FOCheckinMaster.MChk_Status = 'I') ";
            if (this.posGuestStatus.Equals("AI")) {
                this.qry += " and FOCheckinMaster.Bas_Code='AI' ";
            }
            if (this.posGuestStatus.Equals("ENT"))
            {
                this.qry += " and FOCheckinMaster.Bas_Code <> 'AI' ";
            }
            this.qry += "  order by FOCheckinMaster.Room_Code";
            this.dt= posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) {
                return posStaticLoadOnce.PosDataTable;
            }
            return null;
        }
        private DataTable posGetSaleTypeData()
        {
            this.qry = "SELECT SaTy_Code as 'Code', SaTy_Description as 'Name' FROM dbo.FOSalesType";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return posStaticLoadOnce.PosDataTable;
            }
            return null;
        }
        private DataTable posGetExecutiveDataSearch(String[] data)
        {
            this.qry = "SELECT Stf_Code as 'Code', Stf_Name as 'Name' FROM dbo.FOExecutive where Stf_Code like '%"+data[0]+"%'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return posStaticLoadOnce.PosDataTable;
            }
            return null;
        }   
        private DataTable posGetExecutiveData() {
            this.qry = "SELECT Stf_Code as 'Code', Stf_Name as 'Name' FROM dbo.FOExecutive";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return posStaticLoadOnce.PosDataTable;
            }
            return null;
        }        
        private String posGenerateKOTBOT()
        {
            this.qry = " SELECT IDENT_CURRENT('POSKOTBOT') + IDENT_INCR('POSKOTBOT') as NextIncrementId";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt[0, 0] == null) { this.dt[0, 0] = "1"; }
            return this.dt[0, 0].ToString().Trim().Replace(" ", "").PadLeft(5, '0').ToString().Insert(0, "POS");     
        }
        private String posGenerateOrderNo(String[] costCenterCode) {
            this.qry = "select CCent_Prefix from FOCostCenter where CCent_Code='"+ costCenterCode[0] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            String costCenterPrefix = this.dt[0, 0].Replace(" ","").Trim();
            this.qry = "select max(FOOrderMaster.OrdM_No) from FOOrderMaster where  Left(FOOrderMaster.OrdM_No,2)='"+costCenterPrefix+"'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry,"EQ");
            if (this.dt[0, 0] == null) { this.dt[0, 0] = "1"; }
            this.dt[0,0] = ( Convert.ToInt32(this.dt[0, 0].ToString().Trim().Replace(" ", "").Replace(""+costCenterPrefix+"","")) + 1 ).ToString();
            return this.dt[0,0].PadLeft(6, '0').ToString().Insert(0, costCenterPrefix);
        }
        private String posGetProcessDate(String code) {
            this.qry = "select Com_ProcessDate from Companies where Com_Code='" + code + "' ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            return this.dt[0, 0];
        }
        private String posGetAuditDate() {
            String processDate = (this.posGetProcessDate("TBH").Substring(0,10).Trim() + " " + this.getTime()).ToString();
            return Convert.ToDateTime(processDate).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
        }
        private DataTable posGetUBOrders() {
            this.qry = "SELECT OrdM_Table as 'Table',OrdM_No as 'Ord No',OrdM_KOTBOT as 'KOT/BOT',OrdM_Date as 'Date' FROM FOOrderMaster where (OrdM_Status = 'UB' or (OrdM_Status = 'AI' and Pos_AIStatus = 'AI' )) and CCent_Code = '" + posStaticLoadOnce.PosCostCenterCode[0] + "' order by OrdM_Table asc ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return posStaticLoadOnce.PosDataTable;
            }
            return null;
        }
        private String[,] posGetOrderItemDetails(String[] data)
        {
            this.qry = "  select FOMItems.FOIM_ItemDes as 'Name' ,"
            + " FOOrderDetail.OrdD_Qty as 'Qty' ,FOOrderDetail.OrdD_Price as 'Price',FOMItems.FOIM_ItemNo "
            + " "
            + " from FOOrderMaster,FOOrderDetail,FOMItems where FOOrderDetail.OrdM_No = FOOrderMaster.OrdM_No and "
            + "FOOrderDetail.FOIM_ItemNo = FOMItems.FOIM_ItemNo and FOOrderMaster.OrdM_Table ='" + data[0] + "' "
            + "and (FOOrderMaster.OrdM_Status = 'UB' or (FOOrderMaster.OrdM_Status = 'AI' and FOOrderMaster.Pos_AIStatus = 'AI' )) and FOOrderMaster.OrdM_No = '" + data[1] + "' and FOOrderDetail.Item_Status ='AC'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return this.dt;
            }
            return null;
        }
        
        private bool posUpdateCancelOrder(String[] data) {
            this.qry = "update FOOrderMaster set OrdM_Status='CN' where OrdM_No='"+data[0]+"'";
            if (Convert.ToInt16(posStaticLoadOnce.dbExecute(this.qry, "ENQ")[0, 0]) == 1)
            {
                this.qry = "update FOOrderDetail set Item_Status ='CN' where OrdM_No='" + data[0] + "'";
                if (Convert.ToInt16(posStaticLoadOnce.dbExecute(this.qry, "ENQ")[0, 0]) > 0) { return true; }
                
            } return false;
        }
        private bool posUpdateCancelOrderItem(String[] data) {
            this.qry = "update FOOrderDetail set Item_Status ='CN' where OrdM_No='" + data[0] + "' and FOIM_ItemNo='"+data[1]+"'";
            if (Convert.ToInt16(posStaticLoadOnce.dbExecute(this.qry, "ENQ")[0, 0]) > 0) { return true; }
            return false;
        }
        private String[,] posGetOrderDetails(String[] data)
        {
            this.qry = "SELECT  OrdM_Stw, OrdM_Table,OrdM_CusType ,OrdM_Room,OrdM_Staff,OrdM_Status ,SaTy_Code,OrdM_KOTBOT FROM FOOrderMaster where OrdM_No = '" + data[0] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return this.dt;
            }
            return null;
        }
        private bool posInsertGuestOrder(String[] data)
        {
            String AiStatus = "";
            if(data[12].Equals("AI")){
                AiStatus = "AI";
            }
            String CusType = "";
            if(data[8].Equals("OT")){CusType = "OrdM_Staff";}else{CusType="OrdM_Room";}
            this.qry = "insert into FOOrderMaster(OrdM_No,Cur_Code,CCent_Code,OrdM_Date,OrdM_Stw,OrdM_Table,OrdM_KOTBOT,OrdM_Type,OrdM_CusType,OrdM_Checkin," + CusType + ",OrdM_CurrRate,OrdM_Status,OrdM_CreatedBy,OrdM_CreatedOn,OrdM_ModifiedBy,OrdM_ModifiedOn,SaTy_Code,OrdM_Remark,Pos_AIStatus,POS_PAX) "
            +"values('"+data[0]+"','"
            +data[1]+"','"
            +data[2]+"','"
            +data[3]+"','"
            +data[4]+"','"
            +data[5]+"','"
            +data[6]+"','"
            +data[7]+"','"
            +data[8]+"',"
            +data[9]+",'"
            +data[10]+"',"
            +data[11]+",'"
            +data[12]+"','"
            +data[13]+"','"
            +data[14]+"','"
            +data[15]+"','"
            +data[16]+"','"
            +data[17]+"','"
            +data[18]+"','"
            +AiStatus+"',"
            +data[19]+")";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            
            if(Convert.ToInt32(this.dt[0,0])==1){
                return true;
            }
            return false;
        }
        private bool posInsertGuestOrderKOTBOT(String kotbotNo) {
            this.qry = "insert into POSKOTBOT(POSKOTBOT.POSKOTBOT_NO) values('"+kotbotNo+"')";
            if (Convert.ToInt16(posStaticLoadOnce.dbExecute(this.qry, "ENQ")[0,0]) == 1) { return true; } 
            return false;
        }
        // insert into ordercancel table data in ordercancelItem
        private bool posInsertOrderCancel(String[] data)
        {
            this.qry = " insert into [OrdCancel_Temp] ([OrdCancel_OrdM_No],[OrdCancel_Item_No],[OrdCancel_Qty],"
                + "[OrdCancel_Status],[OrdCancel_Printed_IP]) values('" + data[0] + "','" + data[1] + "',"
                +"(select FOOrderDetail.OrdD_Qty from FOOrderDetail where FOOrderDetail.OrdM_No = '"+data[0]+"' and "
            + " FOOrderDetail.FOIM_ItemNo='" + data[1] + "'),'AC',(select FOOrderDetail.Printed_IP from FOOrderDetail "
            +"where FOOrderDetail.OrdM_No = '" + data[0] + "' and  FOOrderDetail.FOIM_ItemNo='" + data[1] + "')) ";
            if (Convert.ToInt16(posStaticLoadOnce.dbExecute(this.qry, "ENQ")[0, 0]) == 1) { return true; }
            return false;
        }
        private bool posInsertGuestOrderItemsTmp(String[] data)
        {
            //this.qry = "insert into SMTPOSOrder(SMTPOSOrderItemNO,SMTPOSOrderItemDesc,SMTPOSOrderItemQty) values('"+data[0]+"','"+data[1]+"','"+data[2]+"')";
            //if(Convert.ToInt16(posStaticLoadOnce.dbExecute(this.qry,"ENQ")[0,0]) == 1){
            //    return true;
            //} return false;         
            return true;
        }
        private bool posInsertGuestItems(String[] data){
            this.qry = "insert into FOOrderDetail(OrdM_No,FOIM_ItemNo,OrdD_Cost,OrdD_Price,OrdD_Qty,Item_Status,OrdD_Remark) values('" + data[0] + "','" + data[1] + "'," + data[2] + "," + data[3] + "," + data[4] + ",'" + data[5] + "','" + data[6] + "')";
            
            if (Convert.ToInt16(posStaticLoadOnce.dbExecute(this.qry, "ENQ")[0, 0]) == 1)
            {
                return true;
            } return false; 
        }
        
        private void posSetFormProperties()
        {
            
            this.guiGuest.Click += this.posGuestOrderFormClick;
        }
        private Point posGetStartPosition()
        {
            int x = this.guiGuest.Location.X;
            int y = this.guiGuest.Location.Y;
            int guestWidth =this.guiGuest.Width;
            x = x + guestWidth;
            Point p = new Point(); p.X = x; p.Y = y;
            return p;
        }
        private void posGuestOrderFormClick(object sender, EventArgs e)
        {
           // this.guiGuest.posPnlGuestOrderRightPanel.Hide();
        }
        private void posSetdgvGuestGuestProperties() { 
            //this.dgvGuest.Dock = DockStyle.Fill;
            this.dgvGuest.Height = this.guiGuest.posPnlGuestOrderRightPanel.Height - 110;
            this.dgvGuest.Width = this.guiGuest.posPnlGuestOrderRightPanel.Width  ;
            int x = this.guiGuest.posPnlGuestOrderRightPanel.Location.X;
            int y = this.guiGuest.posPnlGuestOrderRightPanel.Location.Y;
            this.dgvGuest.Location = new Point(x-6,y+30);
            this.dgvGuest.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvGuest.MultiSelect = false;
            this.dgvGuest.RowHeadersVisible = false;
            this.dgvGuest.Columns[4].Visible = false;
            this.dgvGuest.Columns[2].Visible = false;
            this.dgvGuest.Columns[0].Width = 60;
            this.dgvGuest.Columns[1].Width = 60;
            this.dgvGuest.Columns[3].Width = 280;
            this.dgvGuest.Columns[5].Width = 40;
            this.dgvGuest.Columns[7].Width = 220;
            this.dgvGuest.Columns[6].Visible = false;
            this.dgvGuest.RowTemplate.Height = 50;
            this.dgvGuest.ColumnHeadersHeight = 40;
            this.dgvGuest.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 15F, GraphicsUnit.Pixel);
            this.dgvGuest.DefaultCellStyle.ForeColor = Color.Blue;
            this.dgvGuest.BackgroundColor = SystemColors.Control;
            this.dgvGuest.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            this.dgvGuest.AllowUserToResizeRows = false;
            this.dgvGuest.AllowUserToAddRows = false;
            this.dgvGuest.ReadOnly = true;
            this.dgvGuest.DoubleClick += this.posDgvGuestDoubleClick;
        }

        //###################################################
        //Meal Item Panel

        private void posSetDgvMealItemProperties() {
            //this.dgvOpenTable.Dock = DockStyle.Fill;
            this.dgvMealItem.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvMealItem.MultiSelect = false;
            this.dgvMealItem.RowHeadersVisible = false;
            this.dgvMealItem.Columns[0].Width = 200;
            this.dgvMealItem.Columns[1].Width = 60;
            this.dgvMealItem.Columns[2].Visible = false;
            this.dgvMealItem.Columns[3].Visible = false;
            this.dgvMealItem.Columns[4].Visible = false;
            this.dgvMealItem.Columns[5].Width = 30;
            this.dgvMealItem.Columns[6].Visible = false;
            this.dgvMealItem.RowTemplate.Height = 40;
            this.dgvMealItem.ColumnHeadersHeight = 40;
            this.dgvMealItem.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 15F, GraphicsUnit.Pixel);
            this.dgvMealItem.DefaultCellStyle.ForeColor = Color.Blue;
            this.dgvMealItem.BackgroundColor = SystemColors.Control;
            this.dgvMealItem.DefaultCellStyle.Font = new Font("Arial", 16F, GraphicsUnit.Pixel);
            this.dgvMealItem.AllowUserToResizeRows = false;
           // this.dgvMealItem.ReadOnly = true;
            this.dgvMealItem.AllowUserToAddRows = false;
            
            int width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            int height = this.guiGuest.posPnlGuestOrderRightPanel.Height ;
            this.dgvMealItem.Width = width;
            this.dgvMealItem.Height = ((height / 2 )-100)  ;
            this.dgvMealItem.Location = new Point(0, ((height/2)+20) +40);
        }        
        private void posSetMealItemAddClick(object sender, EventArgs e)
        {
            
            int errSpellCheck = 0;
            int errSpellCheckIndex = 0;
            for (int i = 0; i < this.dgvMealItem.Rows.Count; i++) {
                if (!this.dgvMealItem.Rows[i].Cells[5].Value.ToString().Equals("")) {
                    try { 
                        Convert.ToInt16(this.dgvMealItem.Rows[i].Cells[5].Value.ToString());
                        this.dgvMealItem.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                    catch (Exception) {
                        errSpellCheck++; 
                        errSpellCheckIndex = i;
                        this.dgvMealItem.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                    }
                }
                this.dgvMealItem.Rows[i].Selected = false;
            }
            if (errSpellCheck > 0)
            {
                //this.dgvMealItem.Rows[errSpellCheckIndex].Cells[5].Value = "";
                
                MessageBox.Show("Please Check Quantity value which you entered...");
            }
            else {
                //for (int i = 0; i < this.dgvMealItem.Rows.Count; i++)
                //{
                    
                //    this.dgvMealItem.Rows[i].DefaultCellStyle.BackColor = Color.White;
                   
                //}
                
                this.guiGuest.posBtnGuestOrderSaleType.Enabled = false;
                this.numPad.posHideNumericPad();
                int mealItemCount = this.dgvMealItem.Rows.Count;

                int rowCounter = 0;
                int tmpIndex = 0;
                int TotalQty = 0;
                bool selectionCheckBox = false;
                //String[,] itemRemark = this.posGetItemRemarks();
                //int itemRemarkCount = posStaticLoadOnce.PosRowCount;
                //this.posItemRemark = new String[itemRemarkCount, 3];
                for (int i = 0; i < mealItemCount; i++)
                {
                    selectionCheckBox = Convert.ToBoolean(this.dgvMealItem.Rows[i].Cells[2].Value);
                    //  if (!this.posGetMealItemCellData(i,5).Equals("0")) { chkEmptyCell = true; } else { chkEmptyCell = false; }
                    if (selectionCheckBox == true)
                    {
                        rowCounter = this.guiGuest.posDgvGuestOrderOrder.Rows.Count + 1;
                        this.guiGuest.posDgvGuestOrderOrder.Rows.Add();
                        this.guiGuest.posDgvGuestOrderOrder.Rows[rowCounter - 1].Cells[0].Value = rowCounter;
                        this.guiGuest.posDgvGuestOrderOrder.Rows[rowCounter - 1].Cells[1].Value = this.posGetMealItemCellData(i, 0);
                        this.guiGuest.posDgvGuestOrderOrder.Rows[rowCounter - 1].Cells[2].Value = this.posGetMealItemCellData(i, 1);
                        this.guiGuest.posDgvGuestOrderOrder.Rows[rowCounter - 1].Cells[3].Value = this.posGetMealItemCellData(i, 5);
                        this.guiGuest.posDgvGuestOrderOrder.Rows[rowCounter - 1].Cells[6].Value = this.posGetMealItemCellData(i, 6);
                        //DataGridViewComboBoxCell cb = (DataGridViewComboBoxCell)this.guiGuest.posDgvGuestOrderOrder.Rows[rowCounter - 1].Cells[7];
                        
                        //for (int j = 0; j < itemRemarkCount; j++)
                        //{
                        //    if (j == 0)
                        //    {
                        //        cb.Items.Add("None");
                        //        this.posItemRemark[j, 0] = "-1";
                        //        this.posItemRemark[j, 1] = "-1";
                        //        this.posItemRemark[j, 2] = "None";
                        //    }
                        //    else
                        //    {
                        //        cb.Items.Add(itemRemark[j - 1, 1].ToUpper());
                        //        this.posItemRemark[j, 0] = (j - 1).ToString();
                        //        this.posItemRemark[j, 1] = itemRemark[(j - 1), 0];
                        //        this.posItemRemark[j, 2] = itemRemark[(j - 1), 1];

                        //    }

                        //}
                        //cb.DataSource = new String[] { "1", "2", "3" };

                        //tmpIndex = this.posIsMealItemExistGuestOrder(this.posGetMealItemCellData(i, 4));
                        //if (tmpIndex >= 0)
                        //{
                        //    this.guiGuest.posDgvGuestOrderOrder.Rows.RemoveAt(this.guiGuest.posDgvGuestOrderOrder.Rows.Count - 1);
                        //    TotalQty = Convert.ToInt32(this.posGetMealItemCellData(i, 5)) + Convert.ToInt32(this.guiGuest.posDgvGuestOrderOrder.Rows[tmpIndex].Cells[3].Value);
                        //    this.guiGuest.posDgvGuestOrderOrder.Rows[tmpIndex].Cells[4].Value = Convert.ToDecimal(this.posGetMealItemCellData(i, 1)) * Convert.ToDecimal(TotalQty);
                        //    this.guiGuest.posDgvGuestOrderOrder.Rows[tmpIndex].Cells[3].Value = TotalQty;
                        //    TotalQty = 0;
                        //}
                        //else
                        //{
                            this.guiGuest.posDgvGuestOrderOrder.Rows[rowCounter - 1].Cells[4].Value = Convert.ToDecimal(this.posGetMealItemCellData(i, 1)) * Convert.ToDecimal(this.posGetMealItemCellData(i, 5));
                            this.guiGuest.posDgvGuestOrderOrder.Rows[rowCounter - 1].Cells[5].Value = this.posGetMealItemCellData(i, 4);
                        //}

                    }
                    this.dgvMealItem.Rows[i].Cells[2].Value = false;
                    try { this.dgvMealItem.Rows[i].Cells[5].Value = ""; }
                    catch (Exception) { }
                }
            }

            this.numPad.posHideNumericPad();
        }
        
        private void posSetBtnMealItemAdd() {
            int width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            int height = this.guiGuest.posPnlGuestOrderRightPanel.Height;
            int positionHeight = height / 2 + (height / 2 - 100) + 20 +40;
            this.posBtnMealItemAdd.Width = width;
            this.posBtnMealItemAdd.Height = 40;
            this.posBtnMealItemAdd.Font = new Font("Arial", 25F, GraphicsUnit.Pixel);
            this.posBtnMealItemAdd.Location = new Point(0, positionHeight);
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.posBtnMealItemAdd);
            this.posBtnMealItemAdd.Text = "Add Item";
            
        }
        private String posGetMealItemCellData(int rowIndex, int cellIndex)
        {
            String dgvData = Convert.ToString(this.dgvMealItem.Rows[rowIndex].Cells[cellIndex].Value);
            if (dgvData.Equals(""))
            {
                // Set 1 for meal item which are not exist qty.
                if (cellIndex == 5) { return "1"; }
                return "0";
            }
            return dgvData;
        }
        private int posIsMealItemExistGuestOrder(String itemCode)
        {
            int guestOrderItemCount = this.guiGuest.posDgvGuestOrderOrder.Rows.Count;
            for (int i = 0; i < guestOrderItemCount; i++)
            {
                String guestOrderItemCode = Convert.ToString(this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[5].Value);
                if (guestOrderItemCode.Equals(itemCode)) { return i; }
            }
            return -1;
        }
        private void posSetDgvGuestOrderOrderProperties()
        {
            this.guiGuest.posDgvGuestOrderOrder.Columns[1].Width = 260;
            this.guiGuest.posDgvGuestOrderOrder.Columns[0].Visible = false;
            this.guiGuest.posDgvGuestOrderOrder.Columns[1].ReadOnly = true;
            this.guiGuest.posDgvGuestOrderOrder.Columns[2].ReadOnly = true;
            this.guiGuest.posDgvGuestOrderOrder.Columns[3].ReadOnly = false;
            this.guiGuest.posDgvGuestOrderOrder.Columns[4].ReadOnly = true;
            this.guiGuest.posDgvGuestOrderOrder.Columns[5].ReadOnly = true;
            this.guiGuest.posDgvGuestOrderOrder.Columns[6].ReadOnly = true;
            this.guiGuest.posDgvGuestOrderOrder.Columns[7].ReadOnly = true;
            this.guiGuest.posDgvGuestOrderOrder.RowTemplate.Height = 35;
            this.guiGuest.posDgvGuestOrderOrder.Columns[2].Width = 70;
            this.guiGuest.posDgvGuestOrderOrder.DefaultCellStyle.Font = new Font("VERDANA", 16F, GraphicsUnit.Pixel);
        }
        private void posSetDgvGuestOrderOrderCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int errSpellCheck = 0;
                int errSpellCheckIndex = 0;
                Decimal itemPrice = 0;
                Decimal itemQty = 0;
                for (int i = 0; i < this.guiGuest.posDgvGuestOrderOrder.Rows.Count; i++)
                {
                    if (!this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[3].Value.ToString().Equals(""))
                    {
                        try
                        {
                            Convert.ToInt16(this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[3].Value.ToString());
                            this.guiGuest.posDgvGuestOrderOrder.Rows[i].DefaultCellStyle.BackColor = Color.White;
                           
                        }
                        catch (Exception)
                        {
                            errSpellCheck++;
                            errSpellCheckIndex = i;
                            this.guiGuest.posDgvGuestOrderOrder.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                        }
                    }
                    this.guiGuest.posDgvGuestOrderOrder.Rows[i].Selected = false;
                }
                if (errSpellCheck > 0)
                {
                    //this.dgvMealItem.Rows[errSpellCheckIndex].Cells[5].Value = "";
                    MessageBox.Show("Please Check Quantity value which you entered...");
                }
                else {
                    itemPrice = Convert.ToDecimal(this.guiGuest.posDgvGuestOrderOrder.Rows[e.RowIndex].Cells[2].Value);
                    itemQty = Convert.ToDecimal(this.guiGuest.posDgvGuestOrderOrder.Rows[e.RowIndex].Cells[3].Value);
                    this.guiGuest.posDgvGuestOrderOrder.Rows[e.RowIndex].Cells[4].Value = (itemPrice * itemQty).ToString();
                }              
                
                //this.guiGuest.posDgvGuestOrderOrder.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
            }
            catch (Exception) { }
        }
        private void posSetDgvMealItemCellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                this.fullKeyBoard.posHideFullKeyBoardPad();
                this.numPad.posShowNumericPad();
                this.dgvMealItem.Rows[e.RowIndex].Cells[2].Value = true;
                this.dgvMealItem.CurrentCell = this.dgvMealItem.Rows[e.RowIndex].Cells[5];
                this.dgvMealItem.BeginEdit(true);
            }
            catch (Exception) { }
        }
        public void posShowMealItemPanel(String[] mealData)
        {
           
            this.dgvMealItem.Rows.Clear();
            this.guiGuest.posPnlGuestOrderRightPanel.Show();            
            this.posSetFormProperties();
            this.dgvMealItem.RowTemplate.Height = 40;           
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.dgvMealItem);
            this.posSetMealItem(mealData);
            
            //this.posSetDgvMealItemProperties();
            this.guiGuest.posDgvGuestOrderOrder.ReadOnly  = false;
            this.guiGuest.posDgvGuestOrderOrder.CellClick += this.posSetDgvGuestOrderOrderCellClick;
            this.guiGuest.posDgvGuestOrderOrder.CellEndEdit += this.posSetDgvGuestOrderOrderCellEndEdit;
            this.posSetDgvMealItemProperties();
            this.posSetDgvGuestOrderOrderProperties();
            //this.posBtnMealItemAdd.Click += posSetMealItemAddClick;
            
            this.dgvMealItem.CellClick += this.posSetDgvMealItemCellClick;
            this.guiGuest.posPnlGuestOrderRightPanel.Refresh();
            this.dgvMealItem.Refresh();            
            this.posSetBtnPanelClose();
            this.posSetBtnMealItemAdd();
            this.posSetMealItemSearchPanelProperties();
        }
        private void posSetDgvGuestOrderOrderCellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.guiGuest.posDgvGuestOrderOrder.CurrentCell.ColumnIndex.Equals(3)) {
                this.numPad.posShowNumericPad();
            }
            if (this.guiGuest.posDgvGuestOrderOrder.CurrentCell.ColumnIndex.Equals(7))
            {
                this.remark.posSetGuestOrderObject(this);
                this.remark.posSetFullKeyBoard(this.fullKeyBoard);
                this.remark.posSetNumPad(this.numPad);
                this.remark.posShowRemarkPanel();  
 
            }
           
            //try
            //{
            //    this.guiGuest.posDgvGuestOrderOrder.CurrentCell = this.guiGuest.posDgvGuestOrderOrder.Rows[e.RowIndex].Cells[3];
            //    this.guiGuest.posDgvGuestOrderOrder.BeginEdit(true);
            //}
            //catch (Exception) { }
        }
        public posGuiGuestOrder posGetGuiGuestOrderObject() {
            return this.guiGuest;
        }
        public void posSetMealItem(String[] mealData)
        {
            try { this.dgvMealItem.Columns.Clear(); }
            catch (Exception) { }
            this.posGetDbMealItem(mealData);
            this.btnMealItem = new Button[this.dbMealItemRowCount];          

            int[] data = new int[3];
            DataGridViewCheckBoxColumn chkBoxColumn = new DataGridViewCheckBoxColumn();
            DataGridViewTextBoxColumn txtBoxColumn = new DataGridViewTextBoxColumn();
            this.dgvMealItem.Columns.Add("posMealItemName", "Name");
            this.dgvMealItem.Columns.Add("posMealItemCccpval", "Price");
            this.dgvMealItem.Columns.Add(chkBoxColumn);
            this.dgvMealItem.Columns.Add("posMealItemPrinter", "Printer");
            this.dgvMealItem.Columns.Add("posMealItemItemNo", "ItemNo");
            this.dgvMealItem.Columns.Add("posMealItemQty","Qty");
            this.dgvMealItem.Columns.Add("posMealItemCost", "Cost");
            for (int i = 0; i < this.dbMealItemRowCount; i++)
            {
                this.dgvMealItem.RowTemplate.Height = 40;
                try
                {
                    this.dgvMealItem.Rows.Add();
                    this.dgvMealItem.Rows[i].Cells[0].Value = this.mealItem[i, 1];
                    this.dgvMealItem.Rows[i].Cells[1].Value = this.mealItem[i, 2];
                    this.dgvMealItem.Rows[i].Cells[2] = new DataGridViewCheckBoxCell();
                    this.dgvMealItem.Rows[i].Cells[2].Value = false;
                    this.dgvMealItem.Rows[i].Cells[3].Value = this.mealItem[i, 3];
                    this.dgvMealItem.Rows[i].Cells[4].Value = this.mealItem[i, 0];
                    //this.dgvMealItem.Rows[i].Cells[5].Value = new DataGridViewTextBoxCell();
                    this.dgvMealItem.Rows[i].Cells[5].Value = "";
                    this.dgvMealItem.Rows[i].Cells[6].Value = this.mealItem[i, 4];
                    //this.dgvMealItem.Rows[i].Cells[5].ReadOnly = false;
                    //this.dgvMealItem.Rows[i].Cells[2].ReadOnly = false;
                    
                }
                catch (Exception)
                { }
            }
        }
        private String[,] posGetDbMealItem(String[] data)
        {
            this.qry = "select FOMItems.FOIM_ItemNo ,FOMItems.FOIM_ItemDes ,";
            if(data[1].Equals("EXE")){
                this.qry += "FOMItems.FOIM_Cost";
            }else{
                this.qry += "FOCostCenterItemPrice.CCP_Value";
            }

            this.qry += "  ,FOCostCenterItemPrice.Printer_Name ,FOMItems.FOIM_Cost "
                        + "from FOMItems,FOCostCenterItemPrice "
                        + "where FOCostCenterItemPrice.FOIM_ItemNo = FOMItems.FOIM_ItemNo "
                        + " and FOMItems.FOIM_Type1 = '" + data[0] + "'"
                        + " and FOCostCenterItemPrice.SaTy_Code = '" + data[1] + "' and FOMItems.FOIM_Active = 1 and "
                        + " FOCostCenterItemPrice.CCent_Code = '" + data[2] + "' and FOMItems.FOIM_Type3 = '"+data[3]+"'";
            if (!data[4].Equals("")) {
                this.qry += " and FOMItems.FOIM_ItemDes like '%"+data[4]+"%'";
            }
            this.qry += " order by FOMItems.FOIM_ItemDes";
            this.mealItem = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            this.dbMealItemRowCount = posStaticLoadOnce.PosRowCount;
            return mealItem;
        }               

        //###################################################
        // Meal panel
        public void posShowMealPanel(String[] data)
        {
            this.posHideGuestOrderKeyBoard();
            this.guiGuest.posPnlGuestOrderRightPanel.Show();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Clear(); 
           // this.posSetFormProperties();
            this.posSetGuiMealContent(data);
            this.guiGuest.posPnlGuestOrderRightPanel.Refresh();
            this.dgvMealItem.Refresh();
            this.posSetBtnPanelClose();
        }
        private void posSetPnlMealProperties()
        {
            this.pnlMeal.Width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            this.pnlMeal.Height = (this.guiGuest.posPnlGuestOrderRightPanel.Height / 4);
            this.pnlMeal.AutoScroll = true;
        }
        public void posSetGuiMealContent(String[] data)
        {
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Clear();
            this.pnlMeal.Controls.Clear();
            this.posSetPnlMealProperties();
            int height = 0;
            int width = 0;
            int[] passData = new int[3];
            this.mealType = data;
            this.dbMeal = this.posGetDbMeal(this.mealType);
            this.btnMeal = new Button[this.mealRowCount];
            for (int i = 0; i < this.mealRowCount; i++)
            {
                if ((i % 3) == 0)
                {
                    if (i < 3) { height = 10; } else { height += 70; }
                }
                if ((i % 3) != 0) { width += 85; } else { width = 10; }
                passData[0] = i;
                passData[1] = width;
                passData[2] = height;
                this.btnMeal[i] = this.posSetBtnContent(this.btnMeal[i], passData);
                this.pnlMeal.Controls.Add(this.btnMeal[i]);

            }
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.pnlMeal);
        }
        private String[,] posGetDbMeal(String[] data)
        {
            this.qry = " select InventoryNodes.Icr_Mul_NodeCode  from InventoryNodes,FOMItems "
            + "where Convert(VARCHAR,FOMItems.FOIM_Type2) = InventoryNodes.Icr_Mul_NodeCode and FOMItems.FOIM_Type1 = '" + data[0] + "' " +
            "and InventoryNodes.POS_Status= 'Y' Group by InventoryNodes.Icr_Mul_NodeCode";
            String[,] Inv_NodeCode = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            this.mealRowCount = posStaticLoadOnce.PosRowCount;
            int row = posStaticLoadOnce.PosRowCount;
            String[,] meal = new String[row, 2];
            for (int i = 0; i < row; i++)
            {
                this.qry = "select Icr_Mul_Description from InventoryNodes where Icr_Mul_NodeCode = '" + Inv_NodeCode[i, 0] + "'";
                meal[i, 0] = Inv_NodeCode[i, 0];
                meal[i, 1] = posStaticLoadOnce.dbExecute(this.qry, "EQ")[0, 0];
            }
            return meal;
        }
        private Button posSetBtnContent(Button btn, int[] data)
        {
            btn = new Button();
            btn.Name = "btn" + dbMeal[data[0], 0];
            btn.Location = new Point(data[1], data[2]);
            btn.Size = new Size(75, 58);
            btn.TabIndex = data[0];
            btn.Text = this.dbMeal[data[0], 1];
            btn.ForeColor = Color.Blue;
            btn.UseVisualStyleBackColor = true;
            btn.Click += this.posSetMealClick;
            btn.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            return btn;
        }
        private void posSetMealClick(object sender, EventArgs e)
        {
            try {
               // this.mealCode = this.posExtractMealCode((Button)sender);
                this.posShowMealSecondCategoryPanel(new String[] { this.mealType[0], "" + this.posExtractMealCode((Button)sender) });
            }
            catch (Exception) { }
           // this.posShowMealItemPanel(new String[] { this.mealType[0], "" + this.posExtractMealCode((Button)sender), this.guiGuest.posTxtGuestOrderSaleType.Text, posStaticLoadOnce.PosCostCenterCode[0] });
        }
        
        //###################################################
        public void posShowMealSecondCategoryPanel(String[] data)
        {
            this.posHideGuestOrderKeyBoard();
            this.guiGuest.posPnlGuestOrderRightPanel.Show();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Clear();
           // this.posSetFormProperties();
            this.posSetGuiMealSecondCategoryContent(data);
            this.guiGuest.posPnlGuestOrderRightPanel.Refresh();
            this.dgvMealItem.Refresh();
            this.posSetBtnPanelClose();
        }
        private void posSetPnlMealSecondCategoryProperties() {
            this.pnlMealSecondCategory.Width = this.guiGuest.posPnlGuestOrderRightPanel.Width-20;
            int pnlHeight = (this.guiGuest.posPnlGuestOrderRightPanel.Height / 2) ;
            this.pnlMealSecondCategory.Height = pnlHeight;
            int x = this.guiGuest.posPnlGuestOrderRightPanel.Location.X;
            int y = this.guiGuest.posPnlGuestOrderRightPanel.Location.Y;
           // y += pnlHeight;
            this.pnlMealSecondCategory.Location = new Point(x,y);
            this.pnlMealSecondCategory.AutoScroll = true;
        }
        private String[,] posGetDbMealSecondCategory(String[] data)
        {
            this.qry = "  SELECT POS_Type3.POS_Type3Code,POS_Type3.POS_Type3Description "
                        + " FROM FOMItems join POS_Type3 "
                        + " ON POS_Type3.POS_Type3Code = FOMItems.FOIM_Type3 "
                        + " join FOCostCenterItemPrice on "
                        + " FOCostCenterItemPrice.FOIM_ItemNo = FOMItems.FOIM_ItemNo and 	FOCostCenterItemPrice.CCent_Code = '"+posStaticLoadOnce.PosCostCenterCode[0]+"' "
                        + " AND FOMItems.FOIM_Type1 = '" + data[0] + "'  AND (FOMItems.POS_Status IS NULL) "
                        + " GROUP BY POS_Type3.POS_Type3Code,POS_Type3.POS_Type3Description order by POS_Type3.POS_Type3Description ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry,"EQ");
            if (this.dt != null) {
                return this.dt;
            }
            return null;
        }
        private Button posSetBtnSecondCategoryContent(Button btn, int[] data)
        {
            btn = new Button();
            btn.Name = "btn" + dbMeal[data[0], 0];
            btn.Location = new Point(data[1], data[2]);
            btn.Size = new Size(75, 58);
            btn.TabIndex = data[0];
            btn.Text = this.dbMeal[data[0], 1];
            btn.ForeColor = Color.Blue;
            btn.UseVisualStyleBackColor = true;
            btn.Click += this.posSetMealSecondCategoryClick;
            btn.Font = new Font("VERDANA", 14F, GraphicsUnit.Pixel);
            return btn;
        }
        public void posSetGuiMealSecondCategoryContent(String[] data)
        {
            //this.guiGuest.posPnlGuestOrderRightPanel.Controls.Clear();
            this.pnlMealSecondCategory.Controls.Clear();
            this.posSetPnlMealSecondCategoryProperties();
            int height = 0;
            int width = 0;
            int[] passData = new int[3];
            this.mealType = data;
            this.dbMeal = this.posGetDbMealSecondCategory(this.mealType);/////
            int rowCount = 0;
            rowCount = posStaticLoadOnce.PosRowCount;
            this.btnMeal = new Button[rowCount];///
            for (int i = 0; i < rowCount; i++)
            {
                if ((i % 3) == 0)
                {
                    if (i < 3) { height = 10; } else { height += 70; }
                }
                if ((i % 3) != 0) { width += 85; } else { width = 10; }
                passData[0] = i;
                passData[1] = width;
                passData[2] = height;
                this.btnMeal[i] = this.posSetBtnSecondCategoryContent(this.btnMeal[i], passData);
                this.pnlMealSecondCategory.Controls.Add(this.btnMeal[i]);

            }
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.pnlMealSecondCategory);
        }
        private void posSetMealSecondCategoryClick(object sender, EventArgs e)
        {
            this.txtMealItemSearch.Text = "";
            this.secondryCategoryCode = this.posExtractMealSecondCategoryCode((Button)sender);
            this.posShowMealItemPanel(new String[] { this.mealType[0], this.guiGuest.posTxtGuestOrderSaleType.Text, posStaticLoadOnce.PosCostCenterCode[0], "" + this.secondryCategoryCode, "" });
        }
        private String posExtractMealSecondCategoryCode(Button btn)
        {
            String btnName = btn.Name;
            int btnNameLength = btnName.Length;
            return btnName.Substring(3, (btnNameLength - 3));
        }
        private int posExtractMealCode(Button btn)
        {
            String btnName = btn.Name;
            int btnNameLength = btnName.Length;
            return Convert.ToInt32(btnName.Substring(3, (btnNameLength - 3)));
        }
        //###################################################
        private void posSetGuestOrderErrMsg(String msg) { this.guiGuest.posGuestOrderErrMsg.Text = msg; }
        private void posDgvGuestDoubleClick(object sender, EventArgs e) {
            this.posSetGuestOrderErrMsg("");
            try
            {
                if (Convert.ToBoolean(this.dgvGuest.SelectedCells[4].Value))
                {
                    this.posSetGuestOrderErrMsg("Credits has been stoped to room...  ");
                    this.guiGuest.posTxtGuestOrderRoom.Text = "";
                    this.guiGuest.posTxtHomeGuestName.Text = "";
                }
                
                
                    this.guiGuest.posTxtGuestOrderRoom.Text = this.dgvGuest.SelectedCells[0].Value.ToString();
                    this.guiGuest.posTxtHomeGuestName.Text = this.dgvGuest.SelectedCells[3].Value.ToString();
                    this.guiGuest.posTxtGuestOrderTourName.Text = this.dgvGuest.SelectedCells[6].Value.ToString();   
                    this.checkNo = this.dgvGuest.SelectedCells[2].Value.ToString();
                    this.posGuestPanelHide();
                    
                
            }
            catch (Exception)
            {
                this.posSetGuestOrderErrMsg("Plase Select A Guest.");
            }
           
            
        }
        private void posSetdgvGuestSaleTypeProperties() {
            this.dgvSaleType.Dock = DockStyle.Fill;
            this.dgvSaleType.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvSaleType.MultiSelect = false;
            this.dgvSaleType.RowHeadersVisible = false;
            this.dgvSaleType.Columns[0].Width = 80;
            this.dgvSaleType.RowTemplate.Height = 50;
            this.dgvSaleType.ColumnHeadersHeight = 40;
            this.dgvSaleType.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 15F, GraphicsUnit.Pixel);
            this.dgvSaleType.DefaultCellStyle.ForeColor = Color.Blue;
            this.dgvSaleType.BackgroundColor = SystemColors.Control;
            this.dgvSaleType.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            this.dgvSaleType.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dgvSaleType.AllowUserToResizeRows = false;
            this.dgvSaleType.ReadOnly = true;
            this.dgvSaleType.AllowUserToAddRows = false;
            this.dgvSaleType.DoubleClick += this.posDgvSaleTypeDoubleClick;
        }
        private void posDgvSaleTypeDoubleClick(object sender, EventArgs e)
        {
            
            this.guiGuest.posTxtGuestOrderSaleType.Text = this.dgvSaleType.SelectedCells[0].Value.ToString().ToUpper();
            this.posGuestPanelHide();
        }        
        private void posSetdgvGuestExecutiveProperties()
        {
            //this.dgvExecutive.Dock = DockStyle.Fill;
            this.dgvExecutive.Height = this.guiGuest.posPnlGuestOrderRightPanel.Height - 110;
            this.dgvExecutive.Width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            int x = this.guiGuest.posPnlGuestOrderRightPanel.Location.X;
            int y = this.guiGuest.posPnlGuestOrderRightPanel.Location.Y;
            this.dgvExecutive.Location = new Point(x - 6, y + 30);
            this.dgvExecutive.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvExecutive.MultiSelect = false;
            this.dgvExecutive.RowHeadersVisible = false;
            this.dgvExecutive.Columns[0].Width = 80;
            this.dgvExecutive.RowTemplate.Height = 50;
            this.dgvExecutive.ColumnHeadersHeight = 40;
            this.dgvExecutive.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 15F, GraphicsUnit.Pixel);
            this.dgvExecutive.DefaultCellStyle.ForeColor = Color.Blue;
            this.dgvExecutive.BackgroundColor = SystemColors.Control;
            this.dgvExecutive.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            this.dgvExecutive.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.dgvExecutive.AllowUserToResizeRows = false;
            this.dgvExecutive.ReadOnly = true;
            this.dgvExecutive.AllowUserToAddRows = false;
            this.dgvExecutive.DoubleClick += this.posDgvExecutiveDoubleClick;
        }
        private void posSetdgvGuestOpenTableProperties()
        {
            //this.dgvOpenTable.Dock = DockStyle.Fill;
            this.dgvOpenTable.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvOpenTable.MultiSelect = false;
            this.dgvOpenTable.RowHeadersVisible = false;
            this.dgvOpenTable.Columns[0].Width = 50;
            this.dgvOpenTable.Columns[1].Width = 110;
            this.dgvOpenTable.Columns[2].Width = 110;
            this.dgvOpenTable.Columns[3].Width = 210;
            //this.dgvOpenTable.RowTemplate.Height = 40;
            this.dgvOpenTable.ColumnHeadersHeight = 40;
            this.dgvOpenTable.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 15F, GraphicsUnit.Pixel);
            this.dgvOpenTable.DefaultCellStyle.ForeColor = Color.Blue;
            this.dgvOpenTable.BackgroundColor = SystemColors.Control;
            this.dgvOpenTable.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            this.dgvOpenTable.AllowUserToResizeRows = false;
            this.dgvOpenTable.ReadOnly = true;
            this.dgvOpenTable.AllowUserToAddRows = false;
            this.dgvOpenTable.Location = new Point(0,0);
            int width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            int height = this.guiGuest.posPnlGuestOrderRightPanel.Height-50;
            this.dgvOpenTable.Width = width;
            this.dgvOpenTable.Height = height / 2;
        }
        private void posSetdgvGuestOrderCancelProperties()
        {
            //this.dgvExecutive.Dock = DockStyle.Fill;
            this.dgvOrderCancel.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvOrderCancel.MultiSelect = false;
            this.dgvOrderCancel.RowHeadersVisible = false;
            this.dgvOrderCancel.Columns[0].Width = 40;
            this.dgvOrderCancel.Columns[1].Width = 110;
            this.dgvOrderCancel.Columns[2].Width = 110;
            this.dgvOrderCancel.Columns[3].Width = 210;
            this.dgvOrderCancel.RowTemplate.Height = 50;
            this.dgvOrderCancel.ColumnHeadersHeight = 40;
            this.dgvOrderCancel.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 15F, GraphicsUnit.Pixel);
            this.dgvOrderCancel.DefaultCellStyle.ForeColor = Color.Blue;
            this.dgvOrderCancel.BackgroundColor = SystemColors.Control;
            this.dgvOrderCancel.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            this.dgvOrderCancel.AllowUserToResizeRows = false;
            this.dgvOrderCancel.ReadOnly = true;
            this.dgvOrderCancel.AllowUserToAddRows = false;
            this.dgvOrderCancel.Location = new Point(0,0);
            this.dgvOrderCancel.Height = Convert.ToInt16(this.guiGuest.posPnlGuestOrderRightPanel.Height / 2);
            this.dgvOrderCancel.Width = Convert.ToInt16(this.guiGuest.posPnlGuestOrderRightPanel.Width);
            this.posSetOrderCancelDate();
        }
        private void posSetdgvGuestOrderCancelItemProperties()
        {
            //this.dgvExecutive.Dock = DockStyle.Fill;
            this.dgvOrderCancelItem.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvOrderCancelItem.MultiSelect = false;
            this.dgvOrderCancelItem.RowHeadersVisible = false;
            this.dgvOrderCancelItem.Columns[0].Width = 40;
            this.dgvOrderCancelItem.Columns[1].Width =180;
            this.dgvOrderCancelItem.Columns[2].Width = 50;
            this.dgvOrderCancelItem.Columns[3].Width = 80;
            this.dgvOrderCancelItem.Columns[4].Width = 80;
            this.dgvOrderCancelItem.Columns[1].ReadOnly = true;
            this.dgvOrderCancelItem.Columns[2].ReadOnly = true;
            this.dgvOrderCancelItem.Columns[3].ReadOnly = true;
            this.dgvOrderCancelItem.Columns[4].ReadOnly = true;
            this.dgvOrderCancelItem.RowTemplate.Height = 50;
            this.dgvOrderCancelItem.ColumnHeadersHeight = 40;
            this.dgvOrderCancelItem.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 15F, GraphicsUnit.Pixel);
            this.dgvOrderCancelItem.DefaultCellStyle.ForeColor = Color.Blue;
            this.dgvOrderCancelItem.BackgroundColor = SystemColors.Control;
            this.dgvOrderCancelItem.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            this.dgvOrderCancelItem.AllowUserToResizeRows = false;
            //this.dgvOrderCancelItem.ReadOnly = true;
            this.dgvOrderCancelItem.AllowUserToAddRows = false;
            int width = Convert.ToInt16(this.guiGuest.posPnlGuestOrderRightPanel.Width);
            int height = Convert.ToInt16(this.guiGuest.posPnlGuestOrderRightPanel.Height);
            this.dgvOrderCancelItem.Height = Convert.ToInt16(height / 4);
            this.dgvOrderCancelItem.Width = width;
            
            
            int y = this.guiGuest.posPnlGuestOrderRightPanel.Location.Y;
            int x = this.guiGuest.posPnlGuestOrderRightPanel.Location.X;
            y = y + (this.guiGuest.posPnlGuestOrderRightPanel.Height/2)+30;
            this.dgvOrderCancelItem.Location = new Point(0, y);
        }
        private void posSetDgvOpenTableItemProperties()
        {
            //this.dgvExecutive.Dock = DockStyle.Fill;
            this.dgvOpenTableItem.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgvOpenTableItem.MultiSelect = false;
            this.dgvOpenTableItem.RowHeadersVisible = false;
           
            this.dgvOpenTableItem.Columns[0].Width = 180;
            this.dgvOpenTableItem.Columns[1].Width = 50;
            this.dgvOpenTableItem.Columns[2].Width = 80;
            this.dgvOpenTableItem.Columns[3].Width = 80;
            this.dgvOpenTableItem.Columns[0].ReadOnly = true;
            this.dgvOpenTableItem.Columns[1].ReadOnly = true;
            this.dgvOpenTableItem.Columns[2].ReadOnly = true;
            this.dgvOpenTableItem.Columns[3].ReadOnly = true;
            this.dgvOpenTableItem.RowTemplate.Height = 50;
            this.dgvOpenTableItem.ColumnHeadersHeight = 40;
            this.dgvOpenTableItem.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 15F, GraphicsUnit.Pixel);
            this.dgvOpenTableItem.DefaultCellStyle.ForeColor = Color.Blue;
            this.dgvOpenTableItem.BackgroundColor = SystemColors.Control;
            this.dgvOpenTableItem.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            this.dgvOpenTableItem.AllowUserToResizeRows = false;
            //this.dgvOrderCancelItem.ReadOnly = true;
            this.dgvOpenTableItem.AllowUserToAddRows = false;
            int width = Convert.ToInt16(this.guiGuest.posPnlGuestOrderRightPanel.Width);
            int height = Convert.ToInt16(this.guiGuest.posPnlGuestOrderRightPanel.Height);
            this.dgvOpenTableItem.Height = Convert.ToInt16((height / 4)+0) ;
            this.dgvOpenTableItem.Width = width;


            int y = this.guiGuest.posPnlGuestOrderRightPanel.Location.Y;
            int x = this.guiGuest.posPnlGuestOrderRightPanel.Location.X;
            y = y + (this.guiGuest.posPnlGuestOrderRightPanel.Height / 2) + 30;
            this.dgvOpenTableItem.Location = new Point(0, y);
        }
        private void posDgvExecutiveDoubleClick(object sender, EventArgs e)
        {
            this.posSetGuestOrderErrMsg("");
            try
            {
                if (this.dgvExecutive.Rows.Count != 0) { 
                    this.guiGuest.posTxtGuestOrderRoom.Text = this.dgvExecutive.SelectedCells[0].Value.ToString();
                    this.guiGuest.posTxtHomeGuestName.Text = this.dgvExecutive.SelectedCells[1].Value.ToString();
                    this.posGuestPanelHide();
                }
                
            }
            catch (Exception)
            {
                this.posSetGuestOrderErrMsg("Please Select An Executive");
                
            }
           
            
            
        }
        private void posDgvOpenTableDoubleClick(object sender, EventArgs e) {
            this.guiGuest.posDgvGuestOrderOrder.Rows.Clear();
            String orderNo = this.dgvOpenTable.Rows[this.dgvOpenTable.CurrentCell.RowIndex].Cells[1].Value.ToString();
            String[,] orderDetails = this.posGetOrderDetails(new String[] {orderNo});
            this.guiGuest.posTxtGuestOrderTableNo.Text = orderDetails[0, 1];
            if (orderDetails[0, 2].Equals("OT"))
            { this.guiGuest.posRdbGuestOrderOthers.Checked = true; this.guiGuest.posTxtGuestOrderRoom.Text = orderDetails[0,4]; }
            else { this.guiGuest.posRdbGuestOrderGuest.Checked = true; this.guiGuest.posTxtGuestOrderRoom.Text = orderDetails[0, 3];}
            this.guiGuest.posTxtGuestOrderSaleType.Text = orderDetails[0, 6];
            //this.guiGuest.posTxtGuestOrderKOTBOT.Text = orderDetails[0, 7];
            //String[,] orderItemDetails = this.posGetOrderItemDetails(new String[] { orderDetails[0, 1], orderNo });
            //int orderItemCount = posStaticLoadOnce.PosRowCount;
            //String[,] itemRemark = this.posGetItemRemarks();            
            //int itemRemarkCount = posStaticLoadOnce.PosRowCount;
            //this.posItemRemark = new String[itemRemarkCount, 3];
            //for(int i = 0;i<orderItemCount;i++){
            //    this.guiGuest.posDgvGuestOrderOrder.Rows.Add();
                
            //    this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[0].Value = i+1;
            //    this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[1].Value = orderItemDetails[i, 0].ToString();
            //    this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[2].Value = orderItemDetails[i, 3].ToString();
            //    this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[3].Value = orderItemDetails[i, 1].ToString();
            //    this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[4].Value = Convert.ToDecimal(orderItemDetails[i, 3]) * Convert.ToDecimal(orderItemDetails[i, 1]);
            //    this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[5].Value = orderItemDetails[i, 2].ToString();
            //    DataGridViewComboBoxCell cb = (DataGridViewComboBoxCell)this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[7];
            //    for (int j = 0; j < itemRemarkCount; j++)
            //    {
            //        if (j == 0)
            //        {
            //            cb.Items.Add("None");
            //            this.posItemRemark[j, 0] = "-1";
            //            this.posItemRemark[j, 1] = "-1";
            //            this.posItemRemark[j, 2] = "None";
            //        }
            //        else
            //        {
            //            cb.Items.Add(itemRemark[j - 1, 1]);
            //            this.posItemRemark[j, 0] = (j - 1).ToString();
            //            this.posItemRemark[j, 1] = itemRemark[(j - 1), 0];
            //            this.posItemRemark[j, 2] = itemRemark[(j - 1), 1];

            //        }

            //    }
            //}
            
            this.posGuestPanelHide();
        }
        private void posDgvOrderCancelCellClick(object sender, EventArgs e)
        {
            this.posSetGuestOrderErrMsg("");
            
            try
            {
                if (this.dgvOrderCancel.Rows.Count != 0)
                {
                    String tableNo = this.dgvOrderCancel.SelectedCells[0].Value.ToString();
                    String orderNo = this.dgvOrderCancel.SelectedCells[1].Value.ToString();
                    String orderDate = this.dgvOrderCancel.SelectedCells[3].Value.ToString();
                    this.txtOrderCancelDate.Text = orderDate;
                    this.posShowOrderCancelItemPanel(new String[] {tableNo,orderNo});
                    this.txtOrderCancelItemQty.Text = "";
                    this.txtOrderCancelItemTotal.Text = "";              
                }
            }
            catch (Exception){ }
            
        }
        private void posBtnOrderCancelCloseClick(object sender, EventArgs e) {
            this.guiGuest.posPnlGuestOrderRightPanel.Hide();
        }
        private void posDgvOpenTableClick(object sender, EventArgs e)
        {
            try {
                this.posHideGuestOrderKeyBoard();
                String table = this.dgvOpenTable.Rows[this.dgvOpenTable.CurrentRow.Index].Cells[0].Value.ToString();
                String orderNo = this.dgvOpenTable.Rows[this.dgvOpenTable.CurrentRow.Index].Cells[1].Value.ToString();
                this.posShowOpenTableItemPanel(new String[] { table, orderNo });
            }
            catch(Exception) { }
            
        }
        private void posDgvOrderCancelItemClick(object sender, EventArgs e)
        {
            //try
            //{
            //    if (Convert.ToBoolean(this.dgvOrderCancelItem.Rows[this.dgvOrderCancelItem.CurrentCell.RowIndex].Cells[0].Value))
            //    { this.dgvOrderCancelItem.Rows[this.dgvOrderCancelItem.CurrentCell.RowIndex].Cells[0].Value = false; }
            //    else { this.dgvOrderCancelItem.Rows[this.dgvOrderCancelItem.CurrentCell.RowIndex].Cells[0].Value = true; }
            //}
            //catch (Exception) { }
            this.posHideGuestOrderKeyBoard();
            this.posSetTxtOrderCancelItemQty();
            this.posSetTxtOrderCancelItemTotal();
            try
            {
                String qty = this.dgvOrderCancelItem.CurrentRow.Cells[3].Value.ToString();
                String total = this.dgvOrderCancelItem.CurrentRow.Cells[4].Value.ToString();
                this.txtOrderCancelItemQty.Text = qty;
                this.txtOrderCancelItemTotal.Text = total;

            }
            catch (Exception) { }
        }
        private void posDgvOpenTableItemClick(object sender, EventArgs e)
        {
            

            this.posSetTxtOrderCancelItemQty();
            this.posSetTxtOrderCancelItemTotal();
            try
            {
                String qty = this.dgvOpenTableItem.CurrentRow.Cells[2].Value.ToString();
                String total = this.dgvOpenTableItem.CurrentRow.Cells[3].Value.ToString();
                this.txtOrderCancelItemQty.Text = qty;
                this.txtOrderCancelItemTotal.Text = total;

            }
            catch (Exception) { }
        }
        private void posBtnOrderCancelClick(object sender, EventArgs e) {
            
            if (this.dgvOrderCancel.SelectedRows.Count != 0) {
                    
                    String orderNo = this.dgvOrderCancel.Rows[this.dgvOrderCancel.CurrentRow.Index].Cells[1].Value.ToString();
                    DialogResult dr = MessageBox.Show("Are you sure want to cancel  order no [" + orderNo + "]?", "Cancel", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        try {
                            this.printer.posPrintOrderCancel(new String[] { orderNo });
                        }
                        catch (Exception) { }
                        if (this.posUpdateCancelOrder(new String[] { orderNo })) { this.posShowOrderCancelPanel(); this.posSetGuestOrderErrMsg("Order is Cancelled"); }
                        else { this.posSetGuestOrderErrMsg("Order is not cancelled"); }
                    }
                }  
        }     
        private void posBtnOrderCancelItemClick(object sender, EventArgs e) {
           
                int rowCount = this.dgvOrderCancelItem.Rows.Count;
                if (rowCount != 0)
                {
                    int counter = 0;
                    int sucessCounter= 0;
                    String[] cancelItem = new String[rowCount];
                    for(int i =0;i<rowCount;i++){                        
                        if (Convert.ToBoolean(this.dgvOrderCancelItem.Rows[i].Cells[0].Value) == true) {
                            cancelItem[counter] = this.dgvOrderCancelItem.Rows[i].Cells[4].Value.ToString();
                            counter++;
                        }
                    }
                    if (counter > 0) {
                        String orderNo = this.dgvOrderCancel.Rows[this.dgvOrderCancel.CurrentRow.Index].Cells[1].Value.ToString();
                        DialogResult dr = MessageBox.Show("Are you sure want to cancel  these items?", "Cancel", MessageBoxButtons.YesNo);
                        if (dr == DialogResult.Yes)
                        {
                            for (int i = 0; i < counter; i++) {
                                this.posInsertOrderCancel(new String[] { orderNo, cancelItem[i] });
                                if(this.posUpdateCancelOrderItem(new String[] { orderNo, cancelItem[i] })){sucessCounter++;}
                                
                            }
                            
                                this.printer.posPrintOrderItemCancel(new String[] { orderNo }, cancelItem, counter);
                            
                                                     
                                if (sucessCounter == counter) { this.posShowOrderCancelPanel(); this.posSetGuestOrderErrMsg("Order items are Cancelled"); }
                                else { this.posSetGuestOrderErrMsg("Order items are not cancelled"); }
                        }
                    }
                    else { this.posSetGuestOrderErrMsg("There is no item to cancel"); }
                    
                }
            
        }
        
        private void posSetdgvGuestGuestCreditStopColumn() {
            int rowCount = this.dgvGuest.Rows.Count;            
            for(int i=0;i < rowCount;i++){
                this.dgvGuest.Rows[i].Height = 40;
                if(Convert.ToBoolean(this.dgvGuest.Rows[i].Cells[4].Value)){
                    this.dgvGuest.Rows[i].DefaultCellStyle.BackColor = Color.Red;                    
                }                           
            }
        }
        private void posSetDgvGuestSaleTypeHeight() {
            int rowCount = this.dgvSaleType.Rows.Count;
            for (int i = 0; i < rowCount; i++)
            {
                this.dgvSaleType.Rows[i].Height = 40;
            }
        }
        private void posSetDgvExecutiveHeight()
        {
            int rowCount = this.dgvExecutive.Rows.Count;
            for (int i = 0; i < rowCount; i++)
            {
                this.dgvExecutive.Rows[i].Height = 40;
            }
        }
        private void posSetBtnGuestAddProperties() {
            this.btnGuestAdd.Dock = DockStyle.Bottom;
            this.btnGuestAdd.Height = 60;
            this.btnGuestAdd.Text = "ADD";
            this.btnGuestAdd.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
        }
        public void posShowGuestChekingPanel()
        {
            this.posHideGuestOrderKeyBoard();
            this.guiGuest.posPnlGuestOrderRightPanel.Show();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Clear();
            this.dgvGuest.Columns.Clear();
            this.posSetFormProperties();
            this.dgvGuest.DataSource = this.posGetGuestCheckingData();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.dgvGuest);
            try { this.posSetdgvGuestGuestProperties(); }
            catch (Exception){}
            
            this.posSetdgvGuestGuestCreditStopColumn();
            this.guiGuest.posPnlGuestOrderRightPanel.Refresh();
            this.dgvGuest.Refresh();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.btnGuestAdd);
            this.posSetBtnGuestAddProperties();
            this.btnGuestAdd.Click += this.posDgvGuestDoubleClick;
            if (this.dgvGuest.Rows.Count != 0) { this.dgvGuest.Rows[0].Cells[0].Selected = true; }
            this.posSetSearchPanelProperties();
            // text clear in this.txtSearch.Text
            this.txtSearch.Text = "";
        }
        public void posShowGuestSaleTypePanel() {
            this.posHideGuestOrderKeyBoard();
            this.guiGuest.posPnlGuestOrderRightPanel.Show();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Clear();
            this.dgvSaleType.Columns.Clear();
            this.posSetFormProperties();
            this.dgvSaleType.DataSource = this.posGetSaleTypeData();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.dgvSaleType);
            this.posSetdgvGuestSaleTypeProperties();
            this.posSetDgvGuestSaleTypeHeight();
            this.guiGuest.posPnlGuestOrderRightPanel.Refresh();
            this.dgvSaleType.Refresh();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.btnGuestAdd);
            this.posSetBtnGuestAddProperties();
            this.btnGuestAdd.Click += this.posDgvSaleTypeDoubleClick;
            if (this.dgvSaleType.Rows.Count != 0) { this.dgvSaleType.Rows[0].Cells[0].Selected = true; }
        }
        public void posShowExecutiveChekingPanel()
        {
            this.posHideGuestOrderKeyBoard();
            this.guiGuest.posPnlGuestOrderRightPanel.Show();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Clear();
            this.dgvExecutive.Columns.Clear();
            this.posSetFormProperties();
            this.dgvExecutive.DataSource = this.posGetExecutiveData();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.dgvExecutive);
            this.posSetdgvGuestExecutiveProperties();
            this.posSetDgvExecutiveHeight();
            this.guiGuest.posPnlGuestOrderRightPanel.Refresh();
            this.dgvExecutive.Refresh();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.btnGuestAdd);
            this.posSetBtnGuestAddProperties();
            this.btnGuestAdd.Click += posDgvExecutiveDoubleClick;
            if (this.dgvExecutive.Rows.Count != 0) { this.dgvExecutive.Rows[0].Cells[0].Selected = true; }
            this.posSetSearchPanelProperties();
            // text clear in this.txtSearch.Text
            this.txtSearch.Text = "";
        }
        public void posShowOpenTablePanel()
        {
            this.guiGuest.posPnlGuestOrderRightPanel.Show();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Clear();
            this.dgvOpenTable.Columns.Clear();
            this.posSetFormProperties();
            this.dgvOpenTable.RowTemplate.Height = 40;
            this.dgvOpenTable.DataSource = this.posGetUBOrders();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.dgvOpenTable);
            this.posSetdgvGuestOpenTableProperties();
            this.guiGuest.posPnlGuestOrderRightPanel.Refresh();
            this.dgvOpenTable.Refresh();
            this.dgvOpenTable.DoubleClick += this.posDgvOpenTableDoubleClick;
            this.dgvOpenTable.Click += this.posDgvOpenTableClick;
            this.dgvOpenTableItem.Click += this.posDgvOpenTableItemClick;
            this.posSetBtnPanelClose();
        }
        public void posShowOrderCancelPanel()
        {
            this.posHideGuestOrderKeyBoard();
            this.guiGuest.posPnlGuestOrderRightPanel.Show();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Clear();
            this.dgvOrderCancel.Columns.Clear();
            this.posSetFormProperties();
            this.dgvOrderCancel.RowTemplate.Height = 40;
            this.dgvOrderCancel.DataSource = this.posGetUBOrders();
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.dgvOrderCancel);           
            this.posSetdgvGuestOrderCancelProperties();
            this.posSetDgvExecutiveHeight();
            this.dgvOrderCancel.CellClick += this.posDgvOrderCancelCellClick;
            this.guiGuest.posPnlGuestOrderRightPanel.Refresh();
            this.dgvOrderCancel.Refresh();
            this.posSetBtnOrderCancelOrderCancel();
            this.posSetBtnOrderCancelOrderItemCancel();
            this.posSetBtnPanelClose();
        }
        public void posShowGuestOrderKeyBoard() {
            this.numPad.posShowNumericPad();
        }
        public void posHideGuestOrderKeyBoard() {
            this.numPad.posHideNumericPad();
            this.fullKeyBoard.posHideFullKeyBoardPad();
        }
        public void posShowOpenTableItemPanel(String[] data)
        {
            this.guiGuest.posPnlGuestOrderRightPanel.Show();
            //this.guiGuest.posPnlGuestOrderRightPanel.Controls.Clear();
            this.dgvOpenTableItem.Columns.Clear();
            this.posSetFormProperties();
            this.dgvOpenTableItem.RowTemplate.Height = 40;
            String[,] dataOrderItem = this.posGetOrderItemDetails(data);
            int rowCount = posStaticLoadOnce.PosRowCount;
            // Create other columns in data grid view
            this.dgvOpenTableItem.Columns.Add("posOrderCancelItemName", "Name");
            this.dgvOpenTableItem.Columns.Add("posOrderCancelItemQty", "Qty");
            this.dgvOpenTableItem.Columns.Add("posOrderCancelItemTotal", "Price");
            this.dgvOpenTableItem.Columns.Add("posOrderCancelItemCode", "Code");
            // add rows into data grid view
            for (int i = 0; i < rowCount; i++)
            {
                this.dgvOpenTableItem.Rows.Add();
                this.dgvOpenTableItem.Rows[i].Cells[0].Value = dataOrderItem[i, 0];
                this.dgvOpenTableItem.Rows[i].Cells[1].Value = dataOrderItem[i, 1];
                this.dgvOpenTableItem.Rows[i].Cells[2].Value = dataOrderItem[i, 2];
                this.dgvOpenTableItem.Rows[i].Cells[3].Value = dataOrderItem[i, 3];
            }

            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.dgvOpenTableItem);
            this.posSetDgvOpenTableItemProperties();
            this.guiGuest.posPnlGuestOrderRightPanel.Refresh();
            this.dgvOpenTableItem.Refresh();
            this.posSetTxtOpenTableItemQty();
            this.posSetTxtOpenTableItemTotal();

        }
        public void posShowOrderCancelItemPanel(String[] data)
        {
            this.guiGuest.posPnlGuestOrderRightPanel.Show();
            //this.guiGuest.posPnlGuestOrderRightPanel.Controls.Clear();
            this.dgvOrderCancelItem.Columns.Clear();
            this.posSetFormProperties();
            this.dgvOrderCancelItem.RowTemplate.Height = 40;
            String[,] dataOrderItem =  this.posGetOrderItemDetails(data);
            int rowCount = posStaticLoadOnce.PosRowCount;
            // Create a check box column and add into the data grid view
            DataGridViewCheckBoxColumn chkBoxColumn = new DataGridViewCheckBoxColumn();
            this.dgvOrderCancelItem.Columns.Add(chkBoxColumn);
            // Create other columns in data grid view
            this.dgvOrderCancelItem.Columns.Add("posOrderCancelItemName", "Name");
            this.dgvOrderCancelItem.Columns.Add("posOrderCancelItemQty", "Qty");
            this.dgvOrderCancelItem.Columns.Add("posOrderCancelItemTotal", "Total");
            this.dgvOrderCancelItem.Columns.Add("posOrderCancelItemCode", "Code");
            // add rows into data grid view
            for (int i = 0; i < rowCount; i++)
            {
                this.dgvOrderCancelItem.Rows.Add();
                this.dgvOrderCancelItem.Rows[i].Cells[0] = new DataGridViewCheckBoxCell();
                this.dgvOrderCancelItem.Rows[i].Cells[1].Value = dataOrderItem[i, 0];
                this.dgvOrderCancelItem.Rows[i].Cells[2].Value = dataOrderItem[i, 1];
                this.dgvOrderCancelItem.Rows[i].Cells[3].Value = dataOrderItem[i, 2];
                this.dgvOrderCancelItem.Rows[i].Cells[4].Value = dataOrderItem[i, 3];               
            }

            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.dgvOrderCancelItem);            
            this.posSetdgvGuestOrderCancelItemProperties();          
            this.guiGuest.posPnlGuestOrderRightPanel.Refresh();
            this.dgvOrderCancelItem.Refresh();
            this.posSetTxtOrderCancelItemQty();
            this.posSetTxtOrderCancelItemTotal();
                       
            this.dgvOrderCancelItem.Click += this.posDgvOrderCancelItemClick;
            
        }
        //other      
        private void posSetMealItemSearchPanelProperties() {
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.txtMealItemSearch);
            int x = this.guiGuest.posPnlGuestOrderRightPanel.Location.X;
            int y = this.guiGuest.posPnlGuestOrderRightPanel.Location.Y;
            int rightPanelHeight = this.guiGuest.posPnlGuestOrderRightPanel.Height;
            this.txtMealItemSearch.Location = new Point(x - 6, (y - 13) + ((rightPanelHeight / 2) + 20));
            this.txtMealItemSearch.Height = 40;
            this.txtMealItemSearch.Width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            this.txtMealItemSearch.Font = new Font("Arial", 25F, GraphicsUnit.Pixel);
        }
        private void posSetSearchPanelProperties() {
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.txtSearch);
            int x = this.guiGuest.posPnlGuestOrderRightPanel.Location.X;
            int y = this.guiGuest.posPnlGuestOrderRightPanel.Location.Y;
            this.txtSearch.Location = new Point(x-6,y-13);
            this.txtSearch.Height = 40;
            this.txtSearch.Width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            this.txtSearch.Font = new Font("Arial", 25F, GraphicsUnit.Pixel);
        }
        private void posSetOrderCancelDate() {
            int width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            int height = this.guiGuest.posPnlGuestOrderRightPanel.Height/2 + 2;

            this.txtOrderCancelDate.Width = width;
            this.txtOrderCancelDate.Multiline = true;
            this.txtOrderCancelDate.Height = 40;
            this.txtOrderCancelDate.Font = new Font("Arial", 25F, GraphicsUnit.Pixel);
            this.txtOrderCancelDate.Location = new Point(0, height);
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.txtOrderCancelDate);
            this.txtOrderCancelDate.ReadOnly = true;
        }
        private void posSetTxtOrderCancelItemQty()
        {
            int width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            int height = this.guiGuest.posPnlGuestOrderRightPanel.Height / 2 + this.guiGuest.posPnlGuestOrderRightPanel.Height / 4 + 48;
            this.txtOrderCancelItemQty.Width = 150;
            this.txtOrderCancelItemQty.Multiline = true;
            this.txtOrderCancelItemQty.Height = 40;
            this.txtOrderCancelItemQty.Font = new Font("Arial", 25F, GraphicsUnit.Pixel);
            this.txtOrderCancelItemQty.Location = new Point(0, height);
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.txtOrderCancelItemQty);
            this.txtOrderCancelItemQty.ReadOnly = true;            
        }
        private void posSetTxtOrderCancelItemTotal()
        {
            int width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            int height = this.guiGuest.posPnlGuestOrderRightPanel.Height / 2 + this.guiGuest.posPnlGuestOrderRightPanel.Height / 4 + 48;

            this.txtOrderCancelItemTotal.Width = 150;
            this.txtOrderCancelItemTotal.Multiline = true;
            this.txtOrderCancelItemTotal.Height = 40;
            this.txtOrderCancelItemTotal.Font = new Font("Arial", 25F, GraphicsUnit.Pixel);
            this.txtOrderCancelItemTotal.Location = new Point(150, height);
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.txtOrderCancelItemTotal);
            this.txtOrderCancelItemTotal.ReadOnly = true;            
        }
        private void posSetTxtOpenTableItemQty()
        {
            int width = this.guiGuest.posPnlGuestOrderRightPanel.Width;

            int height = this.guiGuest.posPnlGuestOrderRightPanel.Height / 2 + this.guiGuest.posPnlGuestOrderRightPanel.Height / 4 + 48;
            this.txtOrderCancelItemQty.Width = 150;
            this.txtOrderCancelItemQty.Multiline = true;
            this.txtOrderCancelItemQty.Height = 40;
            this.txtOrderCancelItemQty.Font = new Font("Arial", 25F, GraphicsUnit.Pixel);
            this.txtOrderCancelItemQty.Location = new Point(0, height);
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.txtOrderCancelItemQty);
            this.txtOrderCancelItemQty.ReadOnly = true;
        }
        private void posSetTxtOpenTableItemTotal()
        {
            int width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            int height = this.guiGuest.posPnlGuestOrderRightPanel.Height / 2 + this.guiGuest.posPnlGuestOrderRightPanel.Height / 4 + 48;

            this.txtOrderCancelItemTotal.Width = 150;
            this.txtOrderCancelItemTotal.Multiline = true;
            this.txtOrderCancelItemTotal.Height = 40;
            this.txtOrderCancelItemTotal.Font = new Font("Arial", 25F, GraphicsUnit.Pixel);
            this.txtOrderCancelItemTotal.Location = new Point(150, height);
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.txtOrderCancelItemTotal);
            this.txtOrderCancelItemTotal.ReadOnly = true;
        }
        private void posSetBtnOrderCancelOrderCancel()
        {
            int width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            int height = this.guiGuest.posPnlGuestOrderRightPanel.Height / 2 + this.guiGuest.posPnlGuestOrderRightPanel.Height / 4 + 92;
            this.btnOderCancel.Width = width;
            this.btnOderCancel.Height = 40;
            this.btnOderCancel.Font = new Font("Arial", 25F, GraphicsUnit.Pixel);
            this.btnOderCancel.Location = new Point(0, height);
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.btnOderCancel);
            this.btnOderCancel.Text = "Order Cancel";
                     
        }
        private void posSetBtnOrderCancelOrderItemCancel()
        {
            int width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            int height = this.guiGuest.posPnlGuestOrderRightPanel.Height / 2 + this.guiGuest.posPnlGuestOrderRightPanel.Height / 4 + 132;
            this.btnItemCancel.Width = width;
            this.btnItemCancel.Height = 40;
            this.btnItemCancel.Font = new Font("Arial", 25F, GraphicsUnit.Pixel);
            this.btnItemCancel.Location = new Point(0, height);
            this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.btnItemCancel);
            this.btnItemCancel.Text = "Oder Item Cancel";
            
        }
        private void posSetBtnPanelClose() {
            //int width = this.guiGuest.posPnlGuestOrderRightPanel.Width;
            //int height =50;
            //this.btnOrderCancelClose.Width = width;
            //this.btnOrderCancelClose.Height = height;
            //this.btnOrderCancelClose.Font = new Font("Arial", 30F, GraphicsUnit.Pixel);
            //this.btnOrderCancelClose.Location = new Point(0, 0);
            //this.guiGuest.posPnlGuestOrderRightPanel.Controls.Add(this.btnOrderCancelClose);
            //this.btnOrderCancelClose.Text = "Order Cancel Close";
            this.guiGuest.btnOrderCancelClose.Click += this.posBtnOrderCancelCloseClick;
        }
        private String posGetCustomerType() {
            if (this.guiGuest.posRdbGuestOrderGuest.Checked) { return "GU"; }
            if (this.guiGuest.posRdbGuestOrderOthers.Checked) { return "OT"; }
            return null;
        }
        private String posGetOrderType() {
            return "K";
        }
        private String posGetCurrencyCode() {
            return "LKR";
        }
        private String posGetCurrencyRate() {
            return "1.00";
        }
        private String posGetOrderStatus()
        {
            String saleType = this.guiGuest.posTxtGuestOrderSaleType.Text;
            if (saleType.Equals("AI")) { return "AI"; }
            else if (saleType.Equals("ENT")) { return "BL"; }
            else { return "UB"; }
            
        }
        public void posGuestOrderSave() {
            
            String roomNo = this.guiGuest.posTxtGuestOrderRoom.Text;
            String tableNo = this.guiGuest.posTxtGuestOrderTableNo.Text;
            String saleType = this.guiGuest.posTxtGuestOrderSaleType.Text;
            
            try { this.guiGuest.posDgvGuestOrderOrder.ClearSelection(); }
            catch (Exception ) { }
            this.posGuestPanelHide();
            int rowCount = this.guiGuest.posDgvGuestOrderOrder.Rows.Count;
            String[,] printItem  = new String[rowCount,5];
            int emptyItemRowCount = 0;
            int wrongItemData = 0;
            for (int i = 0; i < rowCount; i++) {
                try { Convert.ToInt32(this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[3].Value);} catch (Exception) {
                    try { this.guiGuest.posDgvGuestOrderOrder.Rows[i].DefaultCellStyle.BackColor = Color.Red; }
                    catch (Exception) { }
                    wrongItemData++; break;}
                try
                {
                    if (Convert.ToDecimal(this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[3].Value) == 0)
                    { this.guiGuest.posDgvGuestOrderOrder.Rows[i].DefaultCellStyle.BackColor = Color.Red; emptyItemRowCount++; }
                }
                catch (Exception) {  }
                
            }
               
            if (roomNo.Equals("")) {
                this.posSetGuestOrderErrMsg("Please Select A Guest Or Excecutive...");
            }
            else if(tableNo.Equals(""))
            {
                this.posSetGuestOrderErrMsg("Please Enter A TableNo...");
            }
            else if (saleType.Equals(""))
            {
                this.posSetGuestOrderErrMsg("Please Select A Guest Type...");
            }
            else if (rowCount == 0) {
                
                this.posSetGuestOrderErrMsg("Please select order Items...");
            }
            else if (emptyItemRowCount != 0) { this.posSetGuestOrderErrMsg("Please Enter Quantity Value For Items..."); }
            else if(wrongItemData != 0){
                
                this.posSetGuestOrderErrMsg("Please Recheck the Quantity value...");}
            else
            {
                this.posSetGuestOrderErrMsg("");
                this.guiGuest.posPnlGuestOrderRightPanel.Visible = false;
                this.guiGuest.posBtnGuestOrderSave.Enabled = false;

                posStaticLoadOnce.posLogSystem(new String[] { "Guest Order Save" });
                this.posSetGuestOrderErrMsg("");
                data[0] = this.posGenerateOrderNo(new String[] { posStaticLoadOnce.PosCostCenterCode[0] });
                data[1] = this.posGetCurrencyCode();
                data[2] = posStaticLoadOnce.PosCostCenterCode[0];
                data[3] = this.posGetAuditDate();
                data[4] = posStaticLoadOnce.PosLoggedStewardUserId;
                data[5] = this.guiGuest.posTxtGuestOrderTableNo.Text;
                data[6] = this.posGenerateKOTBOT();
                data[7] = this.posGetOrderType();
                data[8] = this.posGetCustomerType();
                data[9] = this.checkNo;
                data[10] = this.guiGuest.posTxtGuestOrderRoom.Text;
                data[11] = this.posGetCurrencyRate();
                data[12] = this.posGetOrderStatus();
                data[13] = posStaticLoadOnce.PosLoggedUserId;
                data[14] = this.getDateTime();
                data[15] = posStaticLoadOnce.PosLoggedUserId;
                data[16] = this.getDateTime();
                data[17] = this.guiGuest.posTxtGuestOrderSaleType.Text;
                data[18] = this.guiGuest.posTxtGuestOrderRemarks.Text;
                data[19] =( this.guiGuest.posTxtGuestOrderPax.Text.Equals("")) ? "0" :  this.guiGuest.posTxtGuestOrderPax.Text; 
                 if (this.posInsertGuestOrder(data))
                {
                    if (this.posInsertGuestOrderKOTBOT(data[6]))
                    {

                        String itemCode = "";
                        String itemName = "";
                        String itemQty = "";
                        String itemCost = "";
                        String itemPrice = "";
                        String itemRemark = "";
                        int errorCount1 = 0;
                        int errorCount2 = 0;
                        
                        for (int i = 0; i < rowCount; i++)
                        {
                            try {
                                itemCode = this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[5].Value.ToString();
                                itemName = this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[1].Value.ToString();
                                try { itemQty = this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[3].Value.ToString(); }
                                catch (Exception ex) { }
                                itemPrice = this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[2].Value.ToString();
                                itemCost = this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[6].Value.ToString();
                                itemRemark = this.guiGuest.posDgvGuestOrderOrder.Rows[i].Cells[7].Value.ToString();
                            }
                            catch (Exception) { }
                            
                            // Get Item Details to array called printItem
                            printItem[i, 0] = itemCode; printItem[i, 1] = itemName; printItem[i, 2] = itemQty; printItem[i, 3] = itemPrice; printItem[i, 4] = itemRemark;

                            if (!this.posInsertGuestItems(new String[] { data[0], itemCode, itemCost, itemPrice, itemQty, "AC" ,itemRemark}))
                            {
                                errorCount1++;
                            }
                            if (!this.posInsertGuestOrderItemsTmp(new String[] { itemCode, itemName, itemQty }))
                            {
                                errorCount2++;
                            } 
                            itemCode = "";
                            itemName = "";
                            itemQty = "";
                            itemPrice = "";
                            itemCost = "";
                            itemRemark = "";
                        }
                        //Send data to print
                        this.printer.posPrintOrderItems( data, printItem,rowCount);
                        
                        if (errorCount2 == 0 && errorCount1 == 0)
                        {
                            this.guiGuest.posTxtGuestOrderKOTBOT.Text = data[6];
                            this.guiGuest.posTxtGuestOrderTableNo.Text = "";
                            this.guiGuest.posTxtHomeGuestName.Text = "";
                            this.guiGuest.posTxtGuestOrderRoom.Text = "";
                            this.guiGuest.posTxtGuestOrderSaleType.Text = "";
                            this.guiGuest.posDgvGuestOrderOrder.Rows.Clear();
                            MessageBox.Show("Order Saved");
                            this.guiGuest.posTxtGuestOrderKOTBOT.Text = "";
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Order is not Saved");
                }
                 this.posTextClear();
            }
            this.guiGuest.posBtnGuestOrderSave.Enabled = true;
            
            this.guiGuest.posPnlGuestOrderRightPanel.Hide();
            
        }
        public void posGuestPanelHide() {
            this.guiGuest.posPnlGuestOrderRightPanel.Hide();
        }
        public void posShowGuestOrderPanel() { this.guiGuest.Show(); }
        public void posHideGuestOrderPanel() { this.guiGuest.Hide(); }
        public void posDisposeGuestOrderPanel() { this.guiGuest.Dispose(); }
        public void posTextClear() {
            this.guiGuest.posDgvGuestOrderOrder.Rows.Clear();
            this.guiGuest.posTxtGuestOrderKOTBOT.Text = "";
            this.guiGuest.posTxtGuestOrderRemarks.Text = "";
            this.guiGuest.posTxtGuestOrderRoom.Text = "";
            
            this.guiGuest.posTxtGuestOrderTableNo.Text = "";
            this.guiGuest.posTxtHomeGuestName.Text = "";
            this.guiGuest.posTxtGuestOrderPax.Text = "";
            this.guiGuest.posTxtGuestOrderTourName.Text = "";
            this.guiGuest.posBtnGuestOrderSave.Enabled = true;
            if (this.posClickingButton.Equals("ENT") || this.posClickingButton.Equals("AI"))
            {  
                this.guiGuest.posBtnGuestOrderSaleType.Enabled = false;                
            }
            else { 
                this.guiGuest.posBtnGuestOrderSaleType.Enabled = true;
                this.guiGuest.posTxtGuestOrderSaleType.Text = "";
            }
            this.guiGuest.posPnlGuestOrderRightPanel.Hide();
        }
        
        //############################################################################################
        private void posSetGuestOrderEvent()
        {
            this.guiGuest.btnGuestOrderFood.Click += this.btnHomeFood_Click;
            this.guiGuest.btnGuestOrderHome.Click += this.btnGuestOrderHome_Click;
            this.guiGuest.btnGuestOrderBeverage.Click += this.btnGuestOrderBeverage_Click;
            this.guiGuest.posBtnGuestOrderRoom.Click += this.posBtnHomeRoom_Click;
            this.guiGuest.posBtnGuestOrderSaleType.Click += this.posBtnHomeSaleType_Click;
            this.guiGuest.posRdbGuestOrderGuest.CheckedChanged += this.posRdbHomeGuest_CheckedChanged;
            this.guiGuest.posBtnGuestOrderSave.Click += this.posBtnGuestOrderSave_Click;
            this.guiGuest.Load += this.posGuiGuestOrder_Load;
            this.guiGuest.posBtnGuestOrderDelete.Click += this.posBtnGuestOrderDelete_Click;
            this.guiGuest.posBtnGuestOrderOpenTable.Click += this.posBtnGuestOrderOpenTable_Click;
            this.guiGuest.posBtnGuestOrderVoid.Click += this.posBtnGuestOrderVoid_Click;
            this.guiGuest.posBtnGuestOrderNew.Click += this.posBtnGuestOrderNew_Click;
            this.guiGuest.posBtnGuestOrderKeyboard.Click += this.posBtnGuestOrderKeyboard_Click;
            this.guiGuest.posTxtGuestOrderTableNo.Click += this.posTxtGuestOrderTableNo_Click;
            this.guiGuest.Click += this.posGuiGuestOrder_Click;
            this.guiGuest.Move += this.posGuiGuestOrder_Move;
            this.guiGuest.btnGuestOrderOthers.Click += this.btnGuestOrderOthers_Click;
            this.guiGuest.btnGuestOrderCigarette.Click += this.btnGuestOrderCigarette_Click;
            this.guiGuest.posTxtGuestOrderPax.Click += this.posTxtGuestOrderPax_Click;
            this.guiGuest.posTxtGuestOrderRemarks.Click += this.posTxtGuestOrderRemark_Click;
        }
        private void btnHomeFood_Click(object sender, EventArgs e)
        {
            if (this.guiGuest.posTxtGuestOrderSaleType.Text.Equals(""))
            {
                MessageBox.Show("Please Select a Sale Type ");
            }
            else
            {

               // this.posShowMealPanel(new String[] { "F" });
                this.posShowMealSecondCategoryPanel(new String[] { "F" });
            }

            //this.clsGuest.posGuestPanelHide();

        }
        private void btnGuestOrderHome_Click(object sender, EventArgs e)
        {
            this.home.Show();

            this.posHideGuestOrderKeyBoard();
            this.posHideGuestOrderPanel();
            
        }
        private void btnGuestOrderBeverage_Click(object sender, EventArgs e)
        {
            if (this.guiGuest.posTxtGuestOrderSaleType.Text.Equals(""))
            {
                MessageBox.Show("Please Select a Sale Type ");
            }
            else
            {
                //this.posShowMealPanel(new String[] { "B" });
                this.posShowMealSecondCategoryPanel(new String[] { "B" });
            }
        }
        private void btnGuestOrderOthers_Click(object sender, EventArgs e)
        {
            if (this.guiGuest.posTxtGuestOrderSaleType.Text.Equals(""))
            {
                MessageBox.Show("Please Select a Sale Type ");
            }
            else
            {
                //this.posShowMealPanel(new String[] { "O" });
                this.posShowMealSecondCategoryPanel(new String[] { "O" });
            }

        }
        private void btnGuestOrderCigarette_Click(object sender, EventArgs e)
        {
            if (this.guiGuest.posTxtGuestOrderSaleType.Text.Equals(""))
            {
                MessageBox.Show("Please Select a Sale Type ");
            }
            else
            {
               // this.posShowMealPanel(new String[] { "C" });
                this.posShowMealSecondCategoryPanel(new String[] { "C" });
            }
        }
        private void posBtnHomeRoom_Click(object sender, EventArgs e)
        {
           
            if (this.guiGuest.posRdbGuestOrderGuest.Checked) { this.posShowGuestChekingPanel(); }
            else { this.posShowExecutiveChekingPanel(); }
        }
        private void posBtnHomeSaleType_Click(object sender, EventArgs e)
        {
           
            this.posShowGuestSaleTypePanel();
        }
        private void posRdbHomeGuest_CheckedChanged(object sender, EventArgs e)
        {
            this.posGuestPanelHide();
            this.guiGuest.posTxtGuestOrderRoom.Text = "";
            this.guiGuest.posTxtHomeGuestName.Text = "";
            if (this.guiGuest.posRdbGuestOrderGuest.Checked == true) { this.guiGuest.posLblGuestOrderRoom.Text = "Guest :"; }
            else { this.guiGuest.posLblGuestOrderRoom.Text = "Executive :"; }

        }
        private void posBtnGuestOrderSave_Click(object sender, EventArgs e)
        {
            this.posGuestOrderSave();
        }
        private void posGuiGuestOrder_Load(object sender, EventArgs e)
        {
            this.guiGuest.posTxtGuestOrderCostCenter.Text = posStaticLoadOnce.PosCostCenterCode[0];
            this.guiGuest.posTxtHomeCostCenterName.Text = posStaticLoadOnce.PosCostCenterCode[1];
            this.guiGuest.posTxtGuestOrderHOD.Text = posStaticLoadOnce.PosLoggedUserName;
            this.guiGuest.posTxtGuestOrderStewardName.Text = posStaticLoadOnce.PosLoggedStewardUserName;
           // this.guiGuest.WindowState = FormWindowState.Maximized;
            this.numPad.posSetGuestOrderObject(this.guiGuest);
            this.fullKeyBoard.posSetGuestOrderObject(this.guiGuest);
            if (this.posGuestStatus == "AI")
            {
                this.guiGuest.posBtnGuestOrderSaleType.Enabled = false;
                this.guiGuest.posTxtGuestOrderSaleType.Text = "AI";
            }
            else if (this.posGuestStatus == "ENT")
            {
                this.guiGuest.posBtnGuestOrderSaleType.Enabled = false;
                this.guiGuest.posTxtGuestOrderSaleType.Text = "ENT";
            }
            else {
                this.guiGuest.posBtnGuestOrderSaleType.Enabled = true;
            }
        }        
        private void posBtnGuestOrderDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.guiGuest.posDgvGuestOrderOrder.Rows.Count == 0) { MessageBox.Show("There is no rows in the table"); }
                else
                {
                    DialogResult dr = MessageBox.Show("Are you want to delete this item?", "Delete", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes) { this.guiGuest.posDgvGuestOrderOrder.Rows.RemoveAt(this.guiGuest.posDgvGuestOrderOrder.CurrentCell.RowIndex); }
                }

            }
            catch (Exception ex) { }
        }        
        private void posBtnGuestOrderOpenTable_Click(object sender, EventArgs e)
        {
            this.posHideGuestOrderKeyBoard();
            this.posShowOpenTablePanel();

        }
        private void posBtnGuestOrderVoid_Click(object sender, EventArgs e)
        {
            this.numPad.posHideNumericPad();
            this.admin.posSetGuestOrderVarificationState(true, this.guiGuest, this);
            this.fullKeyBoard.posShowFullKeyBoardPad();
            this.admin.posShowGuiAdmin();
            
        }
        private void posBtnGuestOrderNew_Click(object sender, EventArgs e)
        {
            this.posTextClear();   
        }
        private void posBtnGuestOrderKeyboard_Click(object sender, EventArgs e)
        {
            this.posShowGuestOrderKeyBoard();
        }
        private void posTxtGuestOrderTableNo_Click(object sender, EventArgs e)
        {
            this.fullKeyBoard.posHideFullKeyBoardPad();
            this.posShowGuestOrderKeyBoard();
            this.guiGuest.posTxtGuestOrderTableNo.Focus();
        }
        private void posTxtGuestOrderPax_Click(object sender, EventArgs e)
        {
            this.fullKeyBoard.posHideFullKeyBoardPad();
            this.posShowGuestOrderKeyBoard();            
            this.guiGuest.posTxtGuestOrderPax.Focus();
        }
        private void posTxtGuestOrderRemark_Click(object sender, EventArgs e)
        {
            this.numPad.posHideNumericPad();
            this.fullKeyBoard.posShowFullKeyBoardPad();
            this.guiGuest.posTxtGuestOrderRemarks.Focus();
        }  
        private void posGuiGuestOrder_Click(object sender, EventArgs e)
        {
            try
            {
               // this.posShowGuestOrderKeyBoard();
            }
            catch (Exception) { }
        }
        private void posGuiGuestOrder_Move(object sender, EventArgs e)
        {
            if (this.numPad.posIsNumericPadShow()) {
                this.posShowGuestOrderKeyBoard();
            }
                
        }        
    }
}

