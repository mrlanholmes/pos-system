﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public partial class posGuiReportBillDetail : Form
    {
        String billNo;
        posDsReportBillDetail dsBillDet;
        public posGuiReportBillDetail()
        {
            InitializeComponent();
            billNo = "";
            this.dsBillDet = new posDsReportBillDetail();
        }

        public void posSetBillNo(String[] Data)
        {
            this.billNo = Data[0];

        }
            

        private void posGuiReportBillDetail_Load(object sender, EventArgs e)
        {
            posStaticLoadOnce.getConnection();
            String sqlBillDet = null;
            sqlBillDet = "SELECT FODOutletBill.OblM_BillNo,FOMItems.FOIM_ItemDes,FODOutletBill.OblD_Qty,FODOutletBill.OblD_Price,FODOutletBill.OrdM_KOTBOT,"
                        + "FOMOutletBill.OblM_F_Tot,FOMOutletBill.OblM_B_Tot,FOMOutletBill.OblM_C_Tot,FOMOutletBill.OblM_O_Tot,FOMOutletBill.OblM_Stot"
                        + " FROM FODOutletBill INNER JOIN "
                        + "FOMOutletBill ON FODOutletBill.OblM_BillNo = FOMOutletBill.OblM_BillNo INNER JOIN "
                         + "FOMItems ON FODOutletBill.FOIM_ItemNo = FOMItems.FOIM_ItemNo WHERE FODOutletBill.OblM_BillNo ='" + this.billNo + "'";

            SqlDataAdapter AdpBillDet = new SqlDataAdapter(sqlBillDet, posStaticLoadOnce.getConnection());

            AdpBillDet.Fill(dsBillDet, "orderDetail");
            posDgvReportBillDetail.DataSource = dsBillDet.Tables[1];

        }

        private void posBtnReportBillDetailPrint_Click(object sender, EventArgs e)
        {
            posGuiCrvReportBillDetail CrvBillDet = new posGuiCrvReportBillDetail();


            CrvBillDet.Show();
            ReportDocument cryRpt = new ReportDocument();
            posCrystalReportBillDetail crBillDetail = new posCrystalReportBillDetail();
            crBillDetail.SetDataSource(dsBillDet.Tables[1]);
            CrvBillDet.posGetBillDetailReportViewer().ReportSource = crBillDetail;
            CrvBillDet.posGetBillDetailReportViewer().Refresh();
        }
    }
}
