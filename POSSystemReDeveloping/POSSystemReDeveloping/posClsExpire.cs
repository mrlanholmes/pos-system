﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POSSystemReDeveloping
{
    class posClsExpire : pos
    {
        public bool posExpireUserChecking() {
           
            if (DateTime.Compare(Convert.ToDateTime(posStaticLoadOnce.PosLoggedExpireDate) , Convert.ToDateTime(this.getDate("dd-MM-yyyy"))) < 0)
            {                
                return true;
            }
            return false; 
        }
        public bool posExpireUserStewardChecking() {
            
            if (DateTime.Compare(Convert.ToDateTime(posStaticLoadOnce.PosLoggedStewardExpireDate), Convert.ToDateTime(this.getDate("dd-MM-yyyy"))) < 0)
            {
                return true;
            }
            return false; 
        }
        public bool posExpireSystemChecking() {
            
            if (DateTime.Compare(Convert.ToDateTime(posStaticLoadOnce.PosSystemExpireDate), Convert.ToDateTime(this.getDate())) < 0) {
                return true;
            }  
            return false; 
        }
    }
}
