﻿namespace POSSystemReDeveloping
{
    partial class posGuiCrvReportBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.posCrvReportBill = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.posCrvReportBill);
            this.groupBox1.Location = new System.Drawing.Point(5, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1003, 727);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // posCrvReportBill
            // 
            this.posCrvReportBill.ActiveViewIndex = -1;
            this.posCrvReportBill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posCrvReportBill.Cursor = System.Windows.Forms.Cursors.Default;
            this.posCrvReportBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.posCrvReportBill.Location = new System.Drawing.Point(3, 16);
            this.posCrvReportBill.Name = "posCrvReportBill";
            this.posCrvReportBill.Size = new System.Drawing.Size(997, 708);
            this.posCrvReportBill.TabIndex = 0;
            this.posCrvReportBill.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // posGuiCrvReportBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 733);
            this.Controls.Add(this.groupBox1);
            this.Name = "posGuiCrvReportBill";
            this.Text = "posGuiCrvReportBill";
            this.Load += new System.EventHandler(this.posGuiCrvReportBill_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer posCrvReportBill;
    }
}