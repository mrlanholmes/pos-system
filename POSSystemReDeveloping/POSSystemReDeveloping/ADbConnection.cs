﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace POSSystemReDeveloping
{
    public  abstract class ADbConnection
    {
        protected  DataTable dt;
        protected Boolean ConState;
        protected int dtRowCount;
        protected int dtColumnCount;
        protected String dataSource;
        protected String qry;
        protected String serverName;
        protected String dbUserName;
        protected String dbPassword;
        protected String dbName;
        protected abstract Boolean createConnection();
        protected abstract Boolean openConnection();
        protected abstract Boolean closeConnection();
        public abstract String[,] dbExecute(String sqlQuery, String ExecutionType);
        public abstract int getRsRowCount();
        public abstract int getRsColumnCount();
        public abstract DataTable getDataTable();
        
    }
}
