﻿namespace POSSystemReDeveloping
{
    partial class posGuiBillSplitReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.posTxtBillSplitReportHiddenCenter = new System.Windows.Forms.TextBox();
            this.posCmbReportBillSplitCcenter = new System.Windows.Forms.ComboBox();
            this.posBtnBillSplitReportPrint = new System.Windows.Forms.Button();
            this.posBtnReportBillSplitDetail = new System.Windows.Forms.Button();
            this.posBtnBillSplitReportSearch = new System.Windows.Forms.Button();
            this.posDtpBillSplitReportToDate = new System.Windows.Forms.DateTimePicker();
            this.posDtpReportBillSplitFromDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.posTxtReportBillSplitFromDate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.posTxtReportBillSplitToDate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.posTxtReportBillSplitStewardNo = new System.Windows.Forms.TextBox();
            this.posTxtReportBillSplitRoomNo = new System.Windows.Forms.TextBox();
            this.posTxtReportBillSplitSerialNo = new System.Windows.Forms.TextBox();
            this.posTxtReportBillSplitBillNo = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.posTxtReportBillSplitStaffNo = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.posDgvBillSplitReport = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillSplitReport)).BeginInit();
            this.SuspendLayout();
            // 
            // posTxtBillSplitReportHiddenCenter
            // 
            this.posTxtBillSplitReportHiddenCenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillSplitReportHiddenCenter.Location = new System.Drawing.Point(72, 12);
            this.posTxtBillSplitReportHiddenCenter.Name = "posTxtBillSplitReportHiddenCenter";
            this.posTxtBillSplitReportHiddenCenter.Size = new System.Drawing.Size(18, 15);
            this.posTxtBillSplitReportHiddenCenter.TabIndex = 14;
            this.posTxtBillSplitReportHiddenCenter.Visible = false;
            // 
            // posCmbReportBillSplitCcenter
            // 
            this.posCmbReportBillSplitCcenter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posCmbReportBillSplitCcenter.FormattingEnabled = true;
            this.posCmbReportBillSplitCcenter.Location = new System.Drawing.Point(6, 30);
            this.posCmbReportBillSplitCcenter.Name = "posCmbReportBillSplitCcenter";
            this.posCmbReportBillSplitCcenter.Size = new System.Drawing.Size(161, 24);
            this.posCmbReportBillSplitCcenter.TabIndex = 12;
            this.posCmbReportBillSplitCcenter.SelectedIndexChanged += new System.EventHandler(this.posCmbReportBillSplitCcenter_SelectedIndexChanged);
            // 
            // posBtnBillSplitReportPrint
            // 
            this.posBtnBillSplitReportPrint.Location = new System.Drawing.Point(852, 81);
            this.posBtnBillSplitReportPrint.Name = "posBtnBillSplitReportPrint";
            this.posBtnBillSplitReportPrint.Size = new System.Drawing.Size(75, 23);
            this.posBtnBillSplitReportPrint.TabIndex = 11;
            this.posBtnBillSplitReportPrint.Text = "Print";
            this.posBtnBillSplitReportPrint.UseVisualStyleBackColor = true;
            this.posBtnBillSplitReportPrint.Click += new System.EventHandler(this.posBtnBillSplitReportPrint_Click);
            // 
            // posBtnReportBillSplitDetail
            // 
            this.posBtnReportBillSplitDetail.Location = new System.Drawing.Point(771, 81);
            this.posBtnReportBillSplitDetail.Name = "posBtnReportBillSplitDetail";
            this.posBtnReportBillSplitDetail.Size = new System.Drawing.Size(75, 23);
            this.posBtnReportBillSplitDetail.TabIndex = 10;
            this.posBtnReportBillSplitDetail.Text = "Detail";
            this.posBtnReportBillSplitDetail.UseVisualStyleBackColor = true;
            this.posBtnReportBillSplitDetail.Click += new System.EventHandler(this.posBtnReportBillSplitDetail_Click);
            // 
            // posBtnBillSplitReportSearch
            // 
            this.posBtnBillSplitReportSearch.Location = new System.Drawing.Point(690, 81);
            this.posBtnBillSplitReportSearch.Name = "posBtnBillSplitReportSearch";
            this.posBtnBillSplitReportSearch.Size = new System.Drawing.Size(75, 23);
            this.posBtnBillSplitReportSearch.TabIndex = 9;
            this.posBtnBillSplitReportSearch.Text = "Search";
            this.posBtnBillSplitReportSearch.UseVisualStyleBackColor = true;
            this.posBtnBillSplitReportSearch.Click += new System.EventHandler(this.posBtnBillSplitReportSearch_Click);
            // 
            // posDtpBillSplitReportToDate
            // 
            this.posDtpBillSplitReportToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posDtpBillSplitReportToDate.Location = new System.Drawing.Point(653, 81);
            this.posDtpBillSplitReportToDate.Name = "posDtpBillSplitReportToDate";
            this.posDtpBillSplitReportToDate.Size = new System.Drawing.Size(18, 24);
            this.posDtpBillSplitReportToDate.TabIndex = 8;
            this.posDtpBillSplitReportToDate.ValueChanged += new System.EventHandler(this.posDtpBillSplitReportToDate_ValueChanged);
            // 
            // posDtpReportBillSplitFromDate
            // 
            this.posDtpReportBillSplitFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posDtpReportBillSplitFromDate.Location = new System.Drawing.Point(310, 81);
            this.posDtpReportBillSplitFromDate.Name = "posDtpReportBillSplitFromDate";
            this.posDtpReportBillSplitFromDate.Size = new System.Drawing.Size(18, 24);
            this.posDtpReportBillSplitFromDate.TabIndex = 0;
            this.posDtpReportBillSplitFromDate.ValueChanged += new System.EventHandler(this.posDtpReportBillSplitFromDate_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(506, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Room No";
            // 
            // posTxtReportBillSplitFromDate
            // 
            this.posTxtReportBillSplitFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillSplitFromDate.Location = new System.Drawing.Point(6, 81);
            this.posTxtReportBillSplitFromDate.Name = "posTxtReportBillSplitFromDate";
            this.posTxtReportBillSplitFromDate.Size = new System.Drawing.Size(304, 24);
            this.posTxtReportBillSplitFromDate.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "From Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(340, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Serial No";
            // 
            // posTxtReportBillSplitToDate
            // 
            this.posTxtReportBillSplitToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillSplitToDate.Location = new System.Drawing.Point(338, 81);
            this.posTxtReportBillSplitToDate.Name = "posTxtReportBillSplitToDate";
            this.posTxtReportBillSplitToDate.Size = new System.Drawing.Size(315, 24);
            this.posTxtReportBillSplitToDate.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(838, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Steward No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(174, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bill No";
            // 
            // posTxtReportBillSplitStewardNo
            // 
            this.posTxtReportBillSplitStewardNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillSplitStewardNo.Location = new System.Drawing.Point(841, 30);
            this.posTxtReportBillSplitStewardNo.Name = "posTxtReportBillSplitStewardNo";
            this.posTxtReportBillSplitStewardNo.Size = new System.Drawing.Size(160, 24);
            this.posTxtReportBillSplitStewardNo.TabIndex = 4;
            // 
            // posTxtReportBillSplitRoomNo
            // 
            this.posTxtReportBillSplitRoomNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillSplitRoomNo.Location = new System.Drawing.Point(509, 30);
            this.posTxtReportBillSplitRoomNo.Name = "posTxtReportBillSplitRoomNo";
            this.posTxtReportBillSplitRoomNo.Size = new System.Drawing.Size(160, 24);
            this.posTxtReportBillSplitRoomNo.TabIndex = 2;
            // 
            // posTxtReportBillSplitSerialNo
            // 
            this.posTxtReportBillSplitSerialNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillSplitSerialNo.Location = new System.Drawing.Point(343, 30);
            this.posTxtReportBillSplitSerialNo.Name = "posTxtReportBillSplitSerialNo";
            this.posTxtReportBillSplitSerialNo.Size = new System.Drawing.Size(160, 24);
            this.posTxtReportBillSplitSerialNo.TabIndex = 1;
            // 
            // posTxtReportBillSplitBillNo
            // 
            this.posTxtReportBillSplitBillNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillSplitBillNo.Location = new System.Drawing.Point(177, 30);
            this.posTxtReportBillSplitBillNo.Name = "posTxtReportBillSplitBillNo";
            this.posTxtReportBillSplitBillNo.Size = new System.Drawing.Size(160, 24);
            this.posTxtReportBillSplitBillNo.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.posTxtBillSplitReportHiddenCenter);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.posCmbReportBillSplitCcenter);
            this.groupBox1.Controls.Add(this.posBtnBillSplitReportPrint);
            this.groupBox1.Controls.Add(this.posBtnReportBillSplitDetail);
            this.groupBox1.Controls.Add(this.posBtnBillSplitReportSearch);
            this.groupBox1.Controls.Add(this.posDtpBillSplitReportToDate);
            this.groupBox1.Controls.Add(this.posDtpReportBillSplitFromDate);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.posTxtReportBillSplitFromDate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.posTxtReportBillSplitToDate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.posTxtReportBillSplitStewardNo);
            this.groupBox1.Controls.Add(this.posTxtReportBillSplitStaffNo);
            this.groupBox1.Controls.Add(this.posTxtReportBillSplitRoomNo);
            this.groupBox1.Controls.Add(this.posTxtReportBillSplitSerialNo);
            this.groupBox1.Controls.Add(this.posTxtReportBillSplitBillNo);
            this.groupBox1.Location = new System.Drawing.Point(4, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1004, 120);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Cost Center";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(335, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "To Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(672, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Staff No";
            // 
            // posTxtReportBillSplitStaffNo
            // 
            this.posTxtReportBillSplitStaffNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportBillSplitStaffNo.Location = new System.Drawing.Point(675, 30);
            this.posTxtReportBillSplitStaffNo.Name = "posTxtReportBillSplitStaffNo";
            this.posTxtReportBillSplitStaffNo.Size = new System.Drawing.Size(160, 24);
            this.posTxtReportBillSplitStaffNo.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.posDgvBillSplitReport);
            this.groupBox2.Location = new System.Drawing.Point(4, 125);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1004, 604);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // posDgvBillSplitReport
            // 
            this.posDgvBillSplitReport.AllowUserToAddRows = false;
            this.posDgvBillSplitReport.AllowUserToDeleteRows = false;
            this.posDgvBillSplitReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvBillSplitReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.posDgvBillSplitReport.Location = new System.Drawing.Point(3, 16);
            this.posDgvBillSplitReport.Name = "posDgvBillSplitReport";
            this.posDgvBillSplitReport.ReadOnly = true;
            this.posDgvBillSplitReport.Size = new System.Drawing.Size(998, 585);
            this.posDgvBillSplitReport.TabIndex = 0;
            // 
            // posGuiBillSplitReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 733);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "posGuiBillSplitReport";
            this.Text = "posGuiBillSplitReport";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillSplitReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox posTxtBillSplitReportHiddenCenter;
        private System.Windows.Forms.ComboBox posCmbReportBillSplitCcenter;
        private System.Windows.Forms.Button posBtnBillSplitReportPrint;
        private System.Windows.Forms.Button posBtnReportBillSplitDetail;
        private System.Windows.Forms.Button posBtnBillSplitReportSearch;
        private System.Windows.Forms.DateTimePicker posDtpBillSplitReportToDate;
        private System.Windows.Forms.DateTimePicker posDtpReportBillSplitFromDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox posTxtReportBillSplitFromDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox posTxtReportBillSplitToDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox posTxtReportBillSplitStewardNo;
        private System.Windows.Forms.TextBox posTxtReportBillSplitRoomNo;
        private System.Windows.Forms.TextBox posTxtReportBillSplitSerialNo;
        private System.Windows.Forms.TextBox posTxtReportBillSplitBillNo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox posTxtReportBillSplitStaffNo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView posDgvBillSplitReport;

    }
}