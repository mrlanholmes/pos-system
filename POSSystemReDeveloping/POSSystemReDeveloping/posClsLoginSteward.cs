﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace POSSystemReDeveloping
{
   public class posClsLoginSteward :pos
    {
        private posGuiLoginSteward stewardGui;
        private posClsHome home;
        private posClsLoginAdmin admin;
        private posClsFullKeyBoard fullKeyBoard;
        private posClsNumericPad numPad;
        posClsExpire exp;
        public posClsLoginSteward() {
            this.stewardGui = new posGuiLoginSteward();          
            this.stewardGui.posBtnLoginSteward.Click += this.posLoginStewardLoginClick;
            this.stewardGui.posBtnStewardLoginKeyboard.Click += this.posLoginStewardFullKeyBoardClick;
            this.stewardGui.Load += this.posLoginStewardFormLoad;
            this.exp = new posClsExpire();
        }
        public void posSetNumericKeyPadObject(posClsNumericPad numPadObject) { 
            this.numPad = numPadObject;
            this.home = new posClsHome(this, this.admin);
        }
        public void posSetLoginAdminObject(posClsLoginAdmin adminObject)
        {           
            this.admin = adminObject;           
        }
        public void posSetFullKeyBoardObject(posClsFullKeyBoard fullKeyBoardObject) {
            this.fullKeyBoard = fullKeyBoardObject;
        }
        //Event
        private void posLoginStewardLoginClick(object sender, EventArgs e) {
            
                String uname = this.stewardGui.posTxtLoginStewardPassword.Text;
                if (uname.Equals(""))
                {
                    this.fullKeyBoard.posHideFullKeyBoardPad();
                    MessageBox.Show("Cann't Empty User Pass Code...");
                    this.fullKeyBoard.posShowFullKeyBoardPad();
                    this.posSetFocusSteward();
                }
                else
                {
                    if (this.posLoginAuthentication(new String[] { uname }))
                    {

                        if (this.exp.posExpireUserStewardChecking() )
                        {
                            this.fullKeyBoard.posHideFullKeyBoardPad();
                            MessageBox.Show("You are expired User. Please contact to your administrator.");
                            this.stewardGui.posTxtLoginStewardPassword.Text = "";
                            this.fullKeyBoard.posShowFullKeyBoardPad();
                            this.posSetFocusSteward();
                        }
                        else {
                            if (posStaticLoadOnce.posLogSystem(new String[] { "Logged in from Steward login." }))
                            {
                                this.home.posSetFullKeyBoardObject(this.fullKeyBoard);
                                this.home.posSetNumericKeyPadObject(this.numPad);
                                this.fullKeyBoard.posHideFullKeyBoardPad();
                                this.stewardGui.posTxtLoginStewardPassword.Text = ""; this.home.posShowGuiHome(); this.posHideGuiSteward();
                            }
                        }
            

                        
                        //else {
                        //    this.fullKeyBoard.posHideFullKeyBoardPad();
                        //    MessageBox.Show("Log writing was failed");
                        //    this.fullKeyBoard.posShowFullKeyBoardPad();
                        //    this.posSetFocusSteward();
                        //}
                    }
                    else
                    {
                        this.fullKeyBoard.posHideFullKeyBoardPad();
                        MessageBox.Show("Invalid Password");
                        this.fullKeyBoard.posShowFullKeyBoardPad();
                        this.posSetFocusSteward();
                        this.stewardGui.posTxtLoginStewardPassword.Text = "";
                    }
                }
            
            
        }
        private void posLoginStewardFullKeyBoardClick(object sender, EventArgs e) {
            this.fullKeyBoard.posSetLoginStewardObject(this.stewardGui);
            this.fullKeyBoard.posShowFullKeyBoardPad();
            this.posSetFocusSteward();
        }
        private void posLoginStewardFormLoad(object sender, EventArgs e) {
            this.posLoginStewardFullKeyBoardClick(sender, e);
            this.stewardGui.posTxtLoginStewardPassword.Focus();
            try
            {
                this.stewardGui.Location = new Point((946 - 426) / 2, 250);
                this.fullKeyBoard.posSetPositionFullKeyBoardPad();
            }
            catch (Exception){}
        }
        public void posSetFocusSteward() {

            this.stewardGui.Focus();
            try { this.stewardGui.posTxtLoginStewardPassword.Focus(); }
            catch (Exception) { }
        }
        public void posSetClsLoginAdmin(posClsLoginAdmin adminObject) {
            this.admin = adminObject;
        }
        //Other
        public  bool posLoginAuthentication(string[] data)
        {
            String qry = "SELECT count(*) FROM [POSUsers] WHERE POSUsers_UserPassword='" + data[0] + "'  and POSUsers_UserStatus = 'True';";
            String[,] dt = posStaticLoadOnce.dbExecute(qry, "EQ");
            if (Convert.ToInt32(dt[0, 0]) == 1)
            {
                if (this.posSetLoggedUserData(data))
                {
                    return true;
                }
                else
                {
                    return false;
                }    
            }
            return false;
        }
        private bool posSetLoggedUserData(String[] data)
        {
            this.qry = "SELECT POSUsers_UserID,POSUsers_UserName,POSUsers_UserType,POSUsers_PasswordExp FROM [POSUsers] WHERE  POSUsers_UserPassword='" + data[0] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount == 1)
            {
                try
                {
                    posStaticLoadOnce.PosLoggedStewardUserId = dt[0, 0].Trim();
                    posStaticLoadOnce.PosLoggedStewardUserName = dt[0, 1].Trim();
                    posStaticLoadOnce.PosLoggedStewardUserType = dt[0, 2].Trim();
                    posStaticLoadOnce.PosLoggedStewardExpireDate = this.getConvertedDate(dt[0, 3].Trim());
                    return true;
                }
                catch (Exception) { return false; }
            }
            return false;
        }
        public void posShowGuiSteward()
        {
            this.stewardGui.Show();
            this.fullKeyBoard.posShowFullKeyBoardPad();
            this.posSetFocusSteward();
        }
        public void posHideGuiSteward()
        {
            this.stewardGui.Hide();
        }
        public void posStewardPanelDispose()
        {
            this.stewardGui.Dispose();
        }
    }
}
