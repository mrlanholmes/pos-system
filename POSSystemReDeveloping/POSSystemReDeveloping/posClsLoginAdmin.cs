﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
 
namespace POSSystemReDeveloping
{
    public class posClsLoginAdmin : pos
    {
        private posGuiLoginAdmin adminGui;
        private posClsHome home;
        posClsExpire exp;
        posClsLoginSteward steward;
        private bool ApplicationClosingState;
        private bool billVarificationState;
        private bool AdminValidationState;
        private posGuiBill bill;
        private posGuiGuestOrder guestOrder;
        private bool guestOrderVarificationState;
        private posClsGuestOrder clsGuestOrder;
        private posClsNumericPad numPad;
        private posClsFullKeyBoard fullKeyBoard;
        private bool posBillSplitStatus;
        private bool posOpenItemStatus;
        private bool posBillReprintStatus;
        public posClsLoginAdmin() {           

            this.ApplicationClosingState = false;
            this.posBillSplitStatus = false;
            this.posOpenItemStatus = false;
            this.posBillReprintStatus = false;
            new posStaticLoadOnce();
            //this.home = homeObject;
            this.exp = new posClsExpire();
            this.adminGui = new posGuiLoginAdmin();
            this.adminGui.posBtnLoginAdminLogin.Click += this.posLoginAdminBtnLoginClick;
            this.adminGui.posBtnLoginAdminCancel.Click += this.posLoginAdminBtnCancelClick;
            this.adminGui.Load += this.posLoginAdminFormLoad;
            this.steward = new posClsLoginSteward();
            
            this.steward.posSetClsLoginAdmin(this);
            this.billVarificationState = false;
            this.AdminValidationState = false;
            this.fullKeyBoard = new posClsFullKeyBoard();
            this.numPad = new posClsNumericPad();
            this.numPad.posSetFullKeyBoard(this.fullKeyBoard);
            this.steward.posSetNumericKeyPadObject(this.numPad);
            this.steward.posSetFullKeyBoardObject(this.fullKeyBoard);
            
            this.adminGui.posBtnLoginAdminKeyBoard.Click += this.posLoginAdminBtnKeyBoardClick;
            this.steward.posSetLoginAdminObject(this);
        }
        //Event
        private void posLoginAdminFormLoad(object sender, EventArgs e)
        {
            
            if (this.exp.posExpireSystemChecking())
            {
                MessageBox.Show("System has been Expired. Please contact your administrator");
                this.posAdminPanelDispose();
            }

            this.posLoginAdminBtnKeyBoardClick(sender, e);

            try
            {
                //double primaryScreenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
                //double primaryScreenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
               // int x = Screen.PrimaryScreen.WorkingArea.Width;
                //int y = Screen.PrimaryScreen.WorkingArea.Height;
                //MessageBox.Show(primaryScreenWidth.ToString() +"-" + primaryScreenHeight + "-" + x + "-" +y);
                
                this.adminGui.Location = new Point((946-426)/2, 200);
                this.fullKeyBoard.posSetPositionFullKeyBoardPad();
                
            }
            catch (Exception){   }
            
        }
       
        private void posLoginAdminBtnCancelClick(object sender, EventArgs e){
            if (this.ApplicationClosingState == false && this.billVarificationState == false && this.guestOrderVarificationState == false && this.posBillSplitStatus == false && this.posBillReprintStatus == false && this.posOpenItemStatus == false)
            {
                Application.Exit();
            }
            else {
                try { this.bill.Enabled = true; }
                catch (Exception) { }
                try { this.guestOrder.Enabled = true; }
                catch (Exception) { }
                try { this.home.posGetGuiHomeObject().Enabled = true; }
                catch (Exception) { }
                this.posHideGuiAdmin(); }
            try { this.posClearTextFields(); }
            catch (Exception) { }
            try {
                this.adminGui.posBtnLoginAdminLogin.Text = "LOGIN";
                this.adminGui.Text = "LOGIN";
                this.home.posShowGuiHome(); }
            catch (Exception) { }
            try { this.guestOrder.TopMost = true; }
            catch (Exception) { }
            try { this.bill.TopMost = true; }
            catch (Exception) { }
            this.fullKeyBoard.posHideFullKeyBoardPad();
        }
        private void posLoginAdminBtnLoginClick(object sender, EventArgs e) {
            
            String uname = this.adminGui.posTxtLoginAdminUsername.Text;
            String pword = this.adminGui.posTxtLoginAdminPassword.Text;
            if (uname.Equals(""))
            {
                MessageBox.Show("Cann't empty Username");
            }
            else if (pword.Equals(""))
            {
                MessageBox.Show("Cann't empty Password");
            }
            else
            {
                if (this.posLoginAuthentication(new String[] { uname, pword }))
                {

                    
                    if (this.exp.posExpireUserChecking())
                    {
                        MessageBox.Show("You are expired User. Please contact to your administrator.");
                        this.posClearTextFields();
                    }
                    else
                    {
                        posClsHDD hdd = new posClsHDD();
                        if (hdd.checkHDDSerialNumberValidation())
                        {

                            if (posStaticLoadOnce.isPosLoggedUserExecutive() || posStaticLoadOnce.isPosLoggedUserAdmin() || posStaticLoadOnce.isPosLoggedUserOther())
                            {
                                if (posStaticLoadOnce.posLogExecutive(new String[] { "YES", "NO" })) {
                                    this.posClearTextFields();
                                    if (this.ApplicationClosingState == true) { this.ApplicationClosingState = false;Application.Exit(); }
                                    else if(this.billVarificationState == true){
                                        try
                                        {
                                            this.billVarificationState = false;
                                            this.AdminValidationState = true;
                                            this.posHideGuiAdmin();
                                            this.bill.TopMost = true;
                                            this.bill.Enabled = true;
                                            this.bill.posTxtBillDiscount.Enabled = true ;
                                            this.bill.posTxtBillDiscount.BackColor = Color.LightPink;                                            
                                            this.bill.posBtnBillCalculateDiscount.Enabled = true;
                                            this.fullKeyBoard.posHideFullKeyBoardPad();
                                        }
                                        catch (Exception) { }
                                        
                                    }
                                    else if(this.posBillSplitStatus == true){
                                        this.home.posExeBillSplit();
                                        this.home.posGetGuiHomeObject().Enabled = true;
                                        this.home.posHideGuiHome();
                                        this.posBillSplitStatus = false;
                                        this.fullKeyBoard.posHideFullKeyBoardPad();
                                        this.posHideGuiAdmin();
                                    }
                                    
                                    else if(this.guestOrderVarificationState == true){
                                        try
                                        {
                                            this.guestOrderVarificationState = false;
                                            this.AdminValidationState = true;
                                            this.posHideGuiAdmin();
                                            this.guestOrder.TopMost = true;
                                            this.guestOrder.Enabled = true;
                                            this.clsGuestOrder.posShowOrderCancelPanel();
                                            this.fullKeyBoard.posHideFullKeyBoardPad();
                                        }
                                        catch (Exception) { }
                                        
                                    }
                                    else if (this.posOpenItemStatus == true)
                                    {
                                        this.home.posExeOPenItem();
                                        this.home.posGetGuiHomeObject().Enabled = true;
                                        this.home.posHideGuiHome();
                                        this.posOpenItemStatus = false;
                                        this.fullKeyBoard.posHideFullKeyBoardPad();
                                        this.posHideGuiAdmin();
                                        this.posClearTextFields();
                                    }
                                    else if (this.posBillReprintStatus == true)
                                    {
                                        this.home.posBillReprint();
                                        this.home.posGetGuiHomeObject().Enabled = true;
                                        // this.home.posHideGuiHome();
                                        this.posOpenItemStatus = false;
                                        this.fullKeyBoard.posHideFullKeyBoardPad();
                                        this.posHideGuiAdmin();
                                        this.posClearTextFields();
                                    }
                                    else { this.steward.posShowGuiSteward(); this.posHideGuiAdmin(); this.steward.posSetFocusSteward(); }                                     
                                }
                                else { MessageBox.Show("Log writing was failed"); }
                            }
                            
                            
                        }
                        else
                        {
                            if (posStaticLoadOnce.PosLoggedUserId.ToLower().Equals("admin"))
                            {
                               posGuiHDDValidation hddValidation= new posGuiHDDValidation();
                                hddValidation.Show();
                                hddValidation.posSetAdminObject(this); this.posHideGuiAdmin();
                            }
                            else
                            {
                                MessageBox.Show("Please Enter Administrator Username And Passwrod \n for system validation");
                                this.posClearTextFields();
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Username or Password is incorrect..");
                    this.posClearTextFields();
                    this.billVarificationState = false;
                    this.AdminValidationState = false;
                    this.ApplicationClosingState = false;
                    this.posSetFocusAdmin();
                }
            }
        }
        private void posLoginAdminBtnKeyBoardClick(object sender, EventArgs e) {
            this.fullKeyBoard.posSetLoginAdminObject(this.adminGui);
            this.fullKeyBoard.posShowFullKeyBoardPad();
            this.posSetFocusAdmin();

        }
        //Other
        public posGuiLoginAdmin posGetGuiAdminObject() {
            return this.adminGui;
        }
        public void posSetHomeObject(posClsHome homeObject) {
            this.home = homeObject;
        }
        public void posSetFocusAdmin() {
            this.adminGui.Focus();
            try {
                this.adminGui.posTxtLoginAdminUsername.Focus();
            }
            catch (Exception) { }
            
        }
        public void posClearTextFields()
        {
            this.adminGui.posTxtLoginAdminPassword.Text = "";
            this.adminGui.posTxtLoginAdminUsername.Text = "";
        }
        public  bool posLoginAuthentication(String[] data)
        {
            if (this.posOpenItemStatus == true || this.posBillReprintStatus == true) {
                this.qry = "SELECT count(*) FROM [POSUsers] WHERE POSUsers_UserID = '" + data[0] + "' AND POSUsers_UserPassword='" + data[1] + "' AND (POSUsers_UserType = 'OTHER' OR POSUsers_UserType = 'ADMIN') and POSUsers_UserStatus = 'True';";
            } else {
                this.qry = "SELECT count(*) FROM [POSUsers] WHERE POSUsers_UserID = '" + data[0] + "' AND POSUsers_UserPassword='" + data[1] + "' AND (POSUsers_UserType = 'ADMIN' OR POSUsers_UserType = 'EXECUTIVE') and POSUsers_UserStatus = 'True';";
            }
            
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");

            if (Convert.ToInt32(dt[0, 0]) == 1)
            {
                if (this.posSetLoggedUserData(data))
                {
                    return true;
                }
                else {
                    return false;
                }               
            }
            return false;
        }
        public void posSetBillSplitState(bool state) {
            this.posBillSplitStatus = state;
            this.home.posGetGuiHomeObject().Enabled = false;
        }
        public void posSetOpenItemStatus(bool status) {
            this.posOpenItemStatus = status;
        }
        public void posSetBillReprintStatus(bool status) {
            this.posBillReprintStatus = status;
        }
        private bool posSetLoggedUserData(String[] data)
        {
            this.qry = "SELECT POSUsers_UserID,POSUsers_UserName,POSUsers_UserType,POSUsers_PasswordExp FROM [POSUsers] WHERE POSUsers_UserID = '" + data[0] + "' AND POSUsers_UserPassword='" + data[1] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount==1) {
                try
                {
                    posStaticLoadOnce.PosLoggedUserId = dt[0, 0].Trim();
                    posStaticLoadOnce.PosLoggedUserName = dt[0, 1].Trim();
                    posStaticLoadOnce.PosLoggedUserType = dt[0, 2].Trim();
                    posStaticLoadOnce.PosLoggedExpireDate = this.getConvertedDate( dt[0, 3].Trim());
                    return true;
                }catch (Exception){ return false; }               
            }
            return false;
        }
        public void posSetApplicationCloseState(bool state,posClsHome homeObject) {
            this.ApplicationClosingState = state;
            this.home = homeObject;
            this.adminGui.posBtnLoginAdminLogin.Text = "LOGOUT";
            this.adminGui.posBtnLoginAdminLogin.Font = new Font("Arial", 15F, GraphicsUnit.Pixel); 
            this.adminGui.Text = "LOGOUT";
        }
        public void posSetBillVarificationState(bool state,posGuiBill billObject) {
            this.billVarificationState = state;
            this.bill = billObject;
            this.bill.TopMost = false;
            this.bill.Enabled = false;
        }
        public void posSetGuestOrderVarificationState(bool state,posGuiGuestOrder guestOrderObject,posClsGuestOrder clsGuestObject) {
            this.guestOrder = guestOrderObject;
            this.guestOrderVarificationState = state;
            this.guestOrder.TopMost = false;
            this.guestOrder.Enabled = false;
            this.clsGuestOrder = clsGuestObject;
           
        }
        public bool isValid() {
            return this.AdminValidationState;
        }
        public void isValid(bool state) {
            this.AdminValidationState = state;
        }
        public posGuiLoginAdmin posShowGuiAdmin()
        {
            try {
                this.adminGui.Show();
                this.posSetFocusAdmin();        
            }catch(Exception){
                
            }
            
            return this.adminGui;
        }
        public void posHideGuiAdmin()
        {
            this.adminGui.Hide();
        }
        public void posAdminPanelDispose()
        {
            this.adminGui.Dispose();
        }       
    }
}
