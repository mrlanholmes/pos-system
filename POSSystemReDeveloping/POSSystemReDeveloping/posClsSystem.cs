﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POSSystemReDeveloping
{
    class posClsSystem :pos
    {
        public bool posSetSystemData() {
            this.qry = "select POS_NoofUsers,POS_ExpireDate from POSSISCONFIG";
            this.dt = posStaticLoadOnce.dbExecute(this.qry,"EQ");
            if(posStaticLoadOnce.PosRowCount == 1){
                posStaticLoadOnce.PosSystemNoOfUsers = Convert.ToInt16(this.dt[0, 0]);
                posStaticLoadOnce.PosSystemExpireDate = this.dt[0, 1];
                return true;
            }            
            return false;
        }
        public String[,] posGetCompanyName() {
            this.qry = "select Com_Name from [Companies]";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return this.dt; }
            return null;
        }
    }
}
