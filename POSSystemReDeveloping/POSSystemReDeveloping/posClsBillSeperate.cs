﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public class posClsBillSeperate : pos
    {
        private posClsNumericPad numPad;
        private posClsFullKeyBoard fullKeyBoard;
        private posClsLoginAdmin admin;
        private posClsBill bill;
        private posGuiBill billGui;
        private posGuiBillSeperate guiBillSeperate;
        private posClsHome home;
        private String posOldBillNo;
        
        
        String[] newBillData;
        private posClsPrinter printer;
        public posClsBillSeperate() {     
            
            this.guiBillSeperate = new posGuiBillSeperate();
            this.printer = new posClsPrinter();
            this.guiBillSeperate.posBtnBillSeperateBack.Click += this.posSetBtnBack;
            this.guiBillSeperate.Load += this.posSetFormLoad;
            this.guiBillSeperate.posBtnBillSeperateRightSide.Click += this.posSetBtnRightSideClick;
            this.guiBillSeperate.posBtnBillSeperateLeftSide.Click += this.posSetBtnLeftSideClick;
            this.guiBillSeperate.posBtnBillSeperateBill.Click += this.posSetBtnSplitBillClick;
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.Click += this.posSetDgvBillForSettleClick;
            this.guiBillSeperate.posTxtBillSeperateValue.Click += this.posSetTxtBillSeperateValueClick;
            
        }
        public void posSetNumericKeyPadObject(posClsNumericPad numPadObject)
        {
            this.numPad = numPadObject;
        }
        public void posSetFullKeyBoardObject(posClsFullKeyBoard fullKeyBoardObject)
        {
            this.fullKeyBoard = fullKeyBoardObject;
        }
        public void posSetLoginAdminObject(posClsLoginAdmin adminObject) {
            this.admin = adminObject;
        }
        
        private String[,] posGetOrderItemDetails(String[] data)
        {
            //,FOMItems.FOIM_ItemNo
            this.qry = "select (select [FOMItems].FOIM_ItemDes from [FOMItems] where [FOMItems].FOIM_ItemNo = FODOutletBill.FOIM_ItemNo) as 'Item Name'"
                        + " ,FODOutletBill.[OblD_Qty],FODOutletBill.[OblD_Price] as 'Price',FODOutletBill.[OblD_Qty]*FODOutletBill.[OblD_Price] as 'Total',FODOutletBill.[OrdM_KOTBOT],FODOutletBill.FOIM_ItemNo as 'Item Code'"
                        + " FROM [FODOutletBill] WHERE FODOutletBill.OblM_BillNo = '" + data[0] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return this.dt;
            }
            return null;
        }
        private void posSetDgvBillForSettleClick(object sender, EventArgs e)
        {
            try {
                this.guiBillSeperate.posDgvBillSeperateExistItem.Rows.Clear();
            }
            catch (Exception) { }
            try
            {
                this.guiBillSeperate.posDgvBillSeperateNewItem.Rows.Clear();
            }
            catch (Exception) { }           
            if (this.guiBillSeperate.posDgvBillSeperateBillForSettle.Rows.Count == 0)
            {
                //MessageBox.Show("Please select a Bill...");
            }
            else {
                try { this.guiBillSeperate.posDgvBillSeperateBillForSettle.Rows[this.guiBillSeperate.posDgvBillSeperateBillForSettle.CurrentRow.Index].Cells[0].Selected = true; }
                catch (Exception) { }
                //String tableNo = this.guiBillSeperate.posDgvBillSeperateBillForSettle.Rows[this.guiBillSeperate.posDgvBillSeperateBillForSettle.CurrentRow.Index].Cells[1].Value.ToString();
                try{
                    this.posOldBillNo =   this.guiBillSeperate.posDgvBillSeperateBillForSettle.Rows[this.guiBillSeperate.posDgvBillSeperateBillForSettle.CurrentRow.Index].Cells[1].Value.ToString();
                }catch(Exception){}
                
               // this.guiBillSeperate.posDgvBillSeperateExistItem.DataSource = 
                
                String[] row = null;
                String[,] data= null;
                data = this.posGetOrderItemDetails(new String[] { this.posOldBillNo });
                int rowCount = posStaticLoadOnce.PosRowCount;
                for (int i = 0; i < rowCount; i++) {
                     
                     row = new String[] { data[i,0],data[i,1].Replace(".00",""),data[i,2],data[i,3],data[i,4],data[i,5] };
                    this.guiBillSeperate.posDgvBillSeperateExistItem.Rows.Add(row);
                }
            }
            try { this.posSetDgvExistingItemProperties(); }
            catch (Exception) { }
            
        }
        private void posSetDgvOrderedTableItemDetailsProperties()
        {
            this.billGui.posDgvBillTableItemDetails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.billGui.posDgvBillTableItemDetails.MultiSelect = false;
            this.billGui.posDgvBillTableItemDetails.RowHeadersVisible = false;
            for (int i = 0; i < this.billGui.posDgvBillTableItemDetails.Rows.Count; i++)
            {
                this.billGui.posDgvBillTableItemDetails.Rows[i].Height = 50;
            }
            this.billGui.posDgvBillTableItemDetails.DefaultCellStyle.ForeColor = Color.Blue;
            this.billGui.posDgvBillTableItemDetails.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            this.billGui.posDgvBillTableItemDetails.AllowUserToResizeRows = false;
            this.billGui.posDgvBillTableItemDetails.AllowUserToResizeColumns = false;
            this.billGui.posDgvBillTableItemDetails.ReadOnly = true;          
            
            this.billGui.posDgvBillTableItemDetails.Columns[0].Width = 250;
            this.billGui.posDgvBillTableItemDetails.Columns[1].Width = 50;
            this.billGui.posDgvBillTableItemDetails.Columns[4].Width = 120;
            this.billGui.posDgvBillTableItemDetails.Columns[5].Width = 80;

        }
        private void posSetDgvBillForSettleProperties()
        {
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.MultiSelect = false;
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.RowHeadersVisible = false;
            for (int i = 0; i < this.guiBillSeperate.posDgvBillSeperateBillForSettle.Rows.Count; i++)
            {
                this.guiBillSeperate.posDgvBillSeperateBillForSettle.Rows[i].Height = 40;
            }
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.DefaultCellStyle.ForeColor = Color.Blue;
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.DefaultCellStyle.Font = new Font("Verdana", 15F, GraphicsUnit.Pixel);
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.AllowUserToResizeRows = false;
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.AllowUserToResizeColumns = false;
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.ReadOnly = true;
            

            this.guiBillSeperate.posDgvBillSeperateBillForSettle.Columns[0].Width = 50;
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.Columns[1].Width = 150;
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.Columns[2].Width = 120;
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.Columns[3].Width = 120;

        }
        private void posSetDgvExistingItemProperties()
        {
            this.guiBillSeperate.posDgvBillSeperateExistItem.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.guiBillSeperate.posDgvBillSeperateExistItem.MultiSelect = false;
            this.guiBillSeperate.posDgvBillSeperateExistItem.RowHeadersVisible = false;
            for (int i = 0; i < this.guiBillSeperate.posDgvBillSeperateExistItem.Rows.Count; i++)
            {
                this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[i].Height = 40;
            }
            this.guiBillSeperate.posDgvBillSeperateExistItem.DefaultCellStyle.ForeColor = Color.Blue;
            this.guiBillSeperate.posDgvBillSeperateExistItem.DefaultCellStyle.Font = new Font("Verdana", 15F, GraphicsUnit.Pixel);
            this.guiBillSeperate.posDgvBillSeperateExistItem.AllowUserToResizeRows = false;
            this.guiBillSeperate.posDgvBillSeperateExistItem.AllowUserToResizeColumns = false;
            this.guiBillSeperate.posDgvBillSeperateExistItem.ReadOnly = true;


            this.guiBillSeperate.posDgvBillSeperateExistItem.Columns[0].Width = 250;
            this.guiBillSeperate.posDgvBillSeperateExistItem.Columns[1].Width = 50;
            this.guiBillSeperate.posDgvBillSeperateExistItem.Columns[2].Width = 120;
            this.guiBillSeperate.posDgvBillSeperateExistItem.Columns[3].Width = 120;

        }
        private void posSetDgvNewItemProperties()
        {
            this.guiBillSeperate.posDgvBillSeperateNewItem.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.guiBillSeperate.posDgvBillSeperateNewItem.MultiSelect = false;
            this.guiBillSeperate.posDgvBillSeperateNewItem.RowHeadersVisible = false;
            for (int i = 0; i < this.guiBillSeperate.posDgvBillSeperateNewItem.Rows.Count; i++)
            {
                this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[i].Height = 40;
            }
            this.guiBillSeperate.posDgvBillSeperateNewItem.DefaultCellStyle.ForeColor = Color.Blue;
            this.guiBillSeperate.posDgvBillSeperateNewItem.DefaultCellStyle.Font = new Font("Verdana", 15F, GraphicsUnit.Pixel);
            this.guiBillSeperate.posDgvBillSeperateNewItem.AllowUserToResizeRows = false;
            this.guiBillSeperate.posDgvBillSeperateNewItem.AllowUserToResizeColumns = false;
            this.guiBillSeperate.posDgvBillSeperateNewItem.ReadOnly = true;


            this.guiBillSeperate.posDgvBillSeperateNewItem.Columns[0].Width = 250;
            this.guiBillSeperate.posDgvBillSeperateNewItem.Columns[1].Width = 50;
            this.guiBillSeperate.posDgvBillSeperateNewItem.Columns[2].Width = 120;
            this.guiBillSeperate.posDgvBillSeperateNewItem.Columns[3].Width = 120;

        }
        private String[,] posGetBilledOrderDetails() {
            //FOMOutletBill.OblM_EStaff as 'Staff No '
            this.qry = "select FOMOutletBill.OblM_Table as 'Table No',FOMOutletBill.OblM_BillNo as 'Bill No',FOMOutletBill.OblM_ERoom as 'Room No',"
                        + " FOMOutletBill.OblM_SerialNo as 'Serial No' "
                        +" from FOMOutletBill where FOMOutletBill.OblM_Status = 'PR' and FOMOutletBill.CCent_Code = '"+posStaticLoadOnce.PosCostCenterCode[0]+"' and "
                        + " FOMOutletBill.OblM_CreatedOn between '" + DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd") + "' and '" + DateTime.Today.AddDays(+1).ToString("yyyy-MM-dd") + "' "
                        + " and FOMOutletBill.POS_SplitStatus is NULL order by CONVERT(INT, FOMOutletBill.OblM_Table)  asc";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) {
                return this.dt;
            }
            return null;
        }
        private bool posCancelSplitedMainBill(String[] data) {
            this.qry = "update FOMOutletBill set OblM_Status = 'CN' where OblM_BillNo ='"+data[0]+"' ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt16(this.dt[0, 0]) == 1) {
                return true;
            }
            return false;
        }       
        
        private String posGetNewBillItemMealCategory(String itemCode) {
            this.qry = "  select FOIM_Type1 from FOMItems where FOIM_ItemNo = '"+itemCode+"'; ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) {
                return this.dt[0, 0];
            }
            return null;
        }
        private String posGenerateBillNo(String[] costCenterCode)
        {
            this.qry = "select CCent_Prefix from FOCostCenter where CCent_Code='" + costCenterCode[0] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            String costCenterPrefix = this.dt[0, 0].Replace(" ", "").Trim();
            this.qry = "select max(FOMOutletBill.OblM_BillNo) from FOMOutletBill where  Left(FOMOutletBill.OblM_BillNo,2)='" + costCenterPrefix + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt[0, 0] == null) { this.dt[0, 0] = "1"; }
            this.dt[0, 0] = (Convert.ToInt32(this.dt[0, 0].ToString().Trim().Replace(" ", "").Replace("" + costCenterPrefix + "", "")) + 1).ToString();
            return this.dt[0, 0].PadLeft(6, '0').ToString().Insert(0, costCenterPrefix);
        }
        private bool posInsertNewSplitBillData(String[] data)
        {
            String CusType = "";
            if (data[7].Equals("OT")) { CusType = "OblM_EStaff"; } else { CusType = "OblM_ERoom"; }
            this.qry = "insert into FOMOutletBill(OblM_BillNo,OblM_SerialNo,Cur_Code,CCent_Code," + CusType + ",OblM_Date,OblM_Stw,OblM_CusType,OblM_Table,OblM_SCusType,"
            + " OblM_Status,OblM_Remark,OblM_CurrRate,OblM_Stot,OblM_Ces,OblM_CesA,OblM_ScP,OblM_SpA,OblM_VatP,OblM_VatA,OblM_DisP,OblM_DisA,OblM_ActDisA,OblM_F_Tot,"
            + "OblM_B_Tot,OblM_C_Tot,OblM_O_Tot,OblM_Billed,OblM_CreatedBy,OblM_CreatedOn,OblM_ModifiedBy,OblM_ModifiedOn,Saty_Code,POS_SplitOldBillNo,POS_SplitStatus)"
            + "values('" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "','" + data[7] + "',"
            + "'" + data[8] + "','" + data[9] + "','" + data[10] + "','" + data[11] + "'," + data[12] + "," + data[13] + "," + data[14] + "," + data[15] + ","
            + "" + data[16] + "," + data[17] + "," + data[18] + "," + data[19] + "," + data[20] + "," + data[21] + "," + data[22] + "," + data[23] + "," + data[24] + ","
            + "" + data[25] + "," + data[26] + ",'" + data[27] + "','" + data[28] + "','" + data[29] + "','" + data[30] + "','" + data[31] + "','" + data[32] + "','"+data[33]+"','"+data[34]+"')";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt32(this.dt[0, 0]) == 1)
            {
                return true;
            }
            return false;
        }       
        private void posSetBtnRightSideClick(object sender, EventArgs e)
        {    
            if (this.guiBillSeperate.posTxtBillSeperateValue.Text.Equals(""))
            {
                MessageBox.Show("Please Enter Seperate Value");
            }else if(this.guiBillSeperate.posDgvBillSeperateExistItem.SelectedRows.Count==0){
                MessageBox.Show("Please Select an existing item...");
            }
            else {
                Int64 Qty = Convert.ToInt64(Convert.ToDecimal(this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[this.guiBillSeperate.posDgvBillSeperateExistItem.CurrentRow.Index].Cells[1].Value.ToString()));
                Int64 newQty = 0;
                try { newQty = Convert.ToInt64(this.guiBillSeperate.posTxtBillSeperateValue.Text); }
                catch (Exception) { }
                
                if(newQty> Qty || newQty == 0 || newQty < 0){
                MessageBox.Show("Please Check Seperate Value...");
                }
                else
                {
                    String itemName = this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[this.guiBillSeperate.posDgvBillSeperateExistItem.CurrentRow.Index].Cells[0].Value.ToString();
                    //String itemQty = this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[this.guiBillSeperate.posDgvBillSeperateExistItem.CurrentRow.Index].Cells[1].Value.ToString();
                    String itemPrice = this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[this.guiBillSeperate.posDgvBillSeperateExistItem.CurrentRow.Index].Cells[2].Value.ToString();
                    String itemTotal = this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[this.guiBillSeperate.posDgvBillSeperateExistItem.CurrentRow.Index].Cells[3].Value.ToString();
                    String itemKotBot = this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[this.guiBillSeperate.posDgvBillSeperateExistItem.CurrentRow.Index].Cells[4].Value.ToString();
                    String itemCode = this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[this.guiBillSeperate.posDgvBillSeperateExistItem.CurrentRow.Index].Cells[5].Value.ToString();
                    
                    // reduce existing item 
                    this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[this.guiBillSeperate.posDgvBillSeperateExistItem.CurrentRow.Index].Cells[1].Value = Qty - newQty;
                    this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[this.guiBillSeperate.posDgvBillSeperateExistItem.CurrentRow.Index].Cells[3].Value = Math.Round((Convert.ToDecimal(Qty - newQty) * Convert.ToDecimal(itemPrice)), 2);
                    if ((Qty - newQty) <= 0) {                         
                        this.guiBillSeperate.posDgvBillSeperateExistItem.Rows.RemoveAt(this.guiBillSeperate.posDgvBillSeperateExistItem.CurrentRow.Index);
                    }
                    String getItemCode = "";
                    int existItemCount = 0;
                    int existItemIndex = -1;
                    for (int i = 0; i < this.guiBillSeperate.posDgvBillSeperateNewItem.Rows.Count; i++) {
                        getItemCode = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[i].Cells[5].Value.ToString();
                        if (getItemCode.Equals(itemCode)) {
                            existItemCount++;
                            existItemIndex = i;
                        }
                    }
                    if (existItemCount == 1) {
                        int existQty = 0;
                        try { existQty = Convert.ToInt16(this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[existItemIndex].Cells[1].Value); }
                        catch (Exception) { }
                        this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[existItemIndex].Cells[1].Value = newQty+existQty;
                        this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[existItemIndex].Cells[3].Value = Math.Round((Convert.ToDecimal(newQty + existQty) * Convert.ToDecimal(itemPrice)), 2);
                    }
                    else
                    {
                        string[] row = new string[] { itemName, newQty.ToString(), itemPrice, Math.Round((Convert.ToDecimal(newQty) * Convert.ToDecimal(itemPrice)), 2).ToString(), itemKotBot, itemCode };
                        this.guiBillSeperate.posDgvBillSeperateNewItem.Rows.Add(row);
                    }
                    try { this.posSetDgvNewItemProperties(); }
                    catch (Exception) { }
                    this.guiBillSeperate.posTxtBillSeperateValue.Text = "";
                }   
            }
            
        }
        private void posSetBtnLeftSideClick(object sender, EventArgs e) {
            if (this.guiBillSeperate.posTxtBillSeperateValue.Text.Equals(""))
            {
                MessageBox.Show("Please Enter Seperate Value");
            }
            else if (this.guiBillSeperate.posDgvBillSeperateNewItem.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select an New item...");
            }
            else
            {
                Int64 Qty = Convert.ToInt64(Convert.ToDecimal(this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[this.guiBillSeperate.posDgvBillSeperateNewItem.CurrentRow.Index].Cells[1].Value.ToString()));
                Int64 newQty = Convert.ToInt64(this.guiBillSeperate.posTxtBillSeperateValue.Text);
                if (newQty > Qty || newQty == 0)
                {
                    MessageBox.Show("Please Check Seperate Value...");
                }
                else
                {
                    String itemKotBot = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[this.guiBillSeperate.posDgvBillSeperateNewItem.CurrentRow.Index].Cells[4].Value.ToString();
                    String itemName = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[this.guiBillSeperate.posDgvBillSeperateNewItem.CurrentRow.Index].Cells[0].Value.ToString();
                    //String itemQty = this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[this.guiBillSeperate.posDgvBillSeperateExistItem.CurrentRow.Index].Cells[1].Value.ToString();
                    String itemPrice = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[this.guiBillSeperate.posDgvBillSeperateNewItem.CurrentRow.Index].Cells[2].Value.ToString();
                    String itemTotal = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[this.guiBillSeperate.posDgvBillSeperateNewItem.CurrentRow.Index].Cells[3].Value.ToString();
                    String itemCode = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[this.guiBillSeperate.posDgvBillSeperateNewItem.CurrentRow.Index].Cells[5].Value.ToString();
                    // reduce existing item 
                    this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[this.guiBillSeperate.posDgvBillSeperateNewItem.CurrentRow.Index].Cells[1].Value = Qty - newQty;
                    this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[this.guiBillSeperate.posDgvBillSeperateNewItem.CurrentRow.Index].Cells[3].Value = Math.Round((Convert.ToDecimal(Qty - newQty) * Convert.ToDecimal(itemPrice)), 2); ;
                    if ((Qty - newQty) <= 0)
                    {                        
                        this.guiBillSeperate.posDgvBillSeperateNewItem.Rows.RemoveAt(this.guiBillSeperate.posDgvBillSeperateNewItem.CurrentRow.Index);
                    }
                   
                    int existItemCount = 0;
                    int existItemIndex = -1;
                    String getitemCode = "";
                    for (int i = 0; i < this.guiBillSeperate.posDgvBillSeperateExistItem.Rows.Count; i++)
                    {
                        getitemCode = this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[i].Cells[5].Value.ToString();
                        
                        if (getitemCode.Equals(itemCode))
                        {
                            existItemCount++;
                            existItemIndex = i;
                        }
                    }
                    if (existItemCount == 1)
                    {
                        int existQty = 0;
                        try { existQty = Convert.ToInt16(this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[existItemIndex].Cells[1].Value); }
                        catch (Exception) { }
                        
                        this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[existItemIndex].Cells[1].Value = (newQty + existQty).ToString();                        
                        this.guiBillSeperate.posDgvBillSeperateExistItem.Rows[existItemIndex].Cells[3].Value = Math.Round((Convert.ToDecimal(newQty + existQty) * Convert.ToDecimal(itemPrice)), 2);
                    }
                    else
                    {
                        string[] row = new string[] { itemName, newQty.ToString(), itemPrice, Math.Round((Convert.ToDecimal(newQty) * Convert.ToDecimal(itemPrice)), 2).ToString(), itemKotBot, itemCode };
                        this.guiBillSeperate.posDgvBillSeperateExistItem.Rows.Add(row);
                    }
                    try { this.posSetDgvExistingItemProperties(); }
                    catch (Exception) { }
                   
                    this.guiBillSeperate.posTxtBillSeperateValue.Text = "";
                }
            }
        }
        private void posSetBtnSplitBillClick(object sender, EventArgs e) {
            if(this.guiBillSeperate.posDgvBillSeperateBillForSettle.Rows.Count==0){
                MessageBox.Show("Please Select a bill...");
            }
            else if (this.guiBillSeperate.posDgvBillSeperateNewItem.Rows.Count == 0)
            {
                MessageBox.Show("Please Select items to split...");
            }             
            else {
                this.posCreateGuiBillData();
                this.posSetNewBillSplitData();                
                this.bill.posBillPanelShow();                
                try { this.billGui.posDgvBillTableNumber.DataSource = null; }
                catch (Exception) { }
                try { this.billGui.posDgvBillTableDetails.DataSource = null; }
                catch (Exception) { }
                this.billGui.posDgvBillTableNumber.Enabled = false;
                this.billGui.posDgvBillTableDetails.Enabled = false;
                this.billGui.posBtnBillDiscount.Click -= this.bill.posSetBtnDiscountClick;
                this.billGui.posBtnBillDiscount.Click += this.posSetBtnDiscountClick;
                this.billGui.posBtnBillCalculateDiscount.Click -= this.bill.posSetBtnDiscountCalculateClick;
                this.billGui.posBtnBillCalculateDiscount.Click += this.posSetBtnDiscountCalculateClick;
                this.billGui.posBtnBillHome.Text = "Back";
                this.billGui.posBtnBillHome.Click -= this.bill.posSetBtnHomeClick;
                this.billGui.posBtnBillHome.Click += this.posSetBtnHomeClick;
                this.billGui.posBtnBillSave.Click -= this.bill.posSetBtnBillSaveClick;
                this.billGui.posBtnBillSave.Click += this.posSetBtnSaveClick;
                this.posHideBillSeperatePanel();
            }            
        }
        private void posSetBtnHomeClick(object sender, EventArgs e)
        {
            this.posShowBillSeperatePanel();
            this.bill.posBillPanelHide();
            
        }
        private void posSetTxtBillSeperateValueClick(object sender, EventArgs e)
        {
            int x = this.guiBillSeperate.Location.X;
            int y = this.guiBillSeperate.Location.Y;
            this.numPad.posSetBillSeperateGuiObject(this);
            this.numPad.posShowNumericPad();
            this.numPad.posSetViewLocationNumericPad(new int[]{x+600,y+400});
            this.guiBillSeperate.posTxtBillSeperateValue.Focus();
        }
        public  void posSetBtnDiscountClick(object sender, EventArgs e)
        {
            this.billGui.posTxtBillDiscount.BackColor = SystemColors.ControlLight;
            this.admin.posSetBillVarificationState(true, this.billGui);
            this.admin.posShowGuiAdmin();
        }
        public void posSetBtnSaveClick(object sender, EventArgs e)
        {
            if (this.posSaveBillDetails()) {

                this.posClearText();
                try { this.billGui.posDgvBillTableItemDetails.Rows.Clear(); }
                catch (Exception) { }
                try { this.guiBillSeperate.posDgvBillSeperateNewItem.Rows.Clear(); }
                catch (Exception) { }
                try {
                    if (this.guiBillSeperate.posDgvBillSeperateExistItem.Rows.Count != 0) {
                        this.guiBillSeperate.posDgvBillSeperateBillForSettle.Enabled = false;
                        this.guiBillSeperate.posBtnBillSeperateBack.Enabled = false;
                    }
                    else
                    {
                        this.guiBillSeperate.posDgvBillSeperateBillForSettle.Enabled = true;
                        this.guiBillSeperate.posBtnBillSeperateBack.Enabled = true;
                        this.posCancelSplitedMainBill(new String[] { newBillData[0] });
                        this.posSetBillForSettleDetails();            
                    }                    
                }
                catch (Exception) { }
                try { }
                catch (Exception) { }
                this.bill.posBillPanelHide();
                this.posShowBillSeperatePanel();
            }
            
        }
        private void posClearText()
        {
            this.billGui.posTxtBillTotal.Text = "";
            this.billGui.posTxtBillTableNo.Text = "";
            this.billGui.posTxtBillSteward.Text = "";
            this.billGui.posTxtBillRoom.Text = "";
            this.billGui.posTxtBillRemarks.Text = "";
            this.billGui.posTxtBillActDis.Text = "";
            this.billGui.posTxtBillCess.Text = "";
            this.billGui.posTxtBillCessAmt.Text = "";
            this.billGui.posTxtBillDiscAmt.Text = "";
            this.billGui.posTxtBillSc.Text = "";
            this.billGui.posTxtBillScAmt.Text = "";
            this.billGui.posTxtBillStatus.Text = "";
            this.billGui.posTxtBillSubTotal.Text = "";
            this.billGui.posTxtBillVat.Text = "";
            this.billGui.posTxtBillVatAmt.Text = "";
            this.billGui.posTxtBillSaleType.Text = "";
            this.billGui.posTxtBillDiscount.Text = "";
            this.billGui.posTxtBillTableDetailsKOTBOT.Text = "";
            this.billGui.posTxtBillDgvItemTotal.Text = "";
        }
        private void posSetBtnDiscountCalculateClick(object sender, EventArgs e)
        {

            if (this.admin.isValid())
            {
                this.posSetNewBillSplitData();
                this.admin.isValid(false);

                this.billGui.posTxtBillDiscount.Enabled = false;

            }
            this.billGui.posBtnBillCalculateDiscount.Enabled = false;
        }
        private Decimal posGetActDis(String[] data)
        {
            Decimal discountValue = Convert.ToDecimal(data[3]);
            Decimal taxFactor1 = Convert.ToDecimal(data[0]);
            Decimal taxFactor2 = Convert.ToDecimal(data[1]);
            Decimal taxFactor3 = Convert.ToDecimal(data[2]);
            Decimal actDis = Math.Round(discountValue - (((taxFactor1 / 100 * discountValue)) + ((taxFactor2 / 100 * discountValue)) + ((taxFactor3 / 100 * discountValue))), 2);
            return actDis;
        } 
        private Decimal posGetDiscountvalue(Decimal total)
        {
            String billDiscount = this.billGui.posTxtBillDiscount.Text;
            if (billDiscount.Equals("") || billDiscount == null)
            {
                billDiscount = "0";
                this.billGui.posTxtBillDiscount.Text = "0";
            }
            Decimal discount = Convert.ToDecimal(billDiscount);
            return total - (total * discount) / 100;
        }
        private void posSetBillForSettleDetails() {
            try { this.guiBillSeperate.posDgvBillSeperateBillForSettle.Rows.Clear(); }
            catch (Exception) { }
            try { this.guiBillSeperate.posDgvBillSeperateBillForSettle.Columns.Clear(); }
            catch (Exception) { }
            String[,] data = this.posGetBilledOrderDetails();
            int rowCount = posStaticLoadOnce.PosRowCount;
            /// set Bill for split bill
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.Columns.Add("posBillForSettleDescription", "Table No");
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.Columns.Add("posBillForSettleQty", "Bill No");
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.Columns.Add("posBillForSettlePrice", "Room No");
            this.guiBillSeperate.posDgvBillSeperateBillForSettle.Columns.Add("posBillForSettleTotal", "Serial No");        
            
                      
            for (int r = 0; r < rowCount; r++)
            {
                String[] row = new String[] { data[r, 0], data[r, 1], data[r, 2], data[r, 3] };
                this.guiBillSeperate.posDgvBillSeperateBillForSettle.Rows.Add(row);
            }
            
        }
        private void posSetFormLoad(object sender, EventArgs e) {
            this.posSetBillForSettleDetails();
            this.posSetDgvBillForSettleProperties();
            try
            {
               // String tableNo = this.guiBillSeperate.posDgvBillSeperateBillForSettle.Rows[this.guiBillSeperate.posDgvBillSeperateBillForSettle.CurrentRow.Index].Cells[0].Value.ToString();
               // this.guiBillSeperate.posDgvBillSeperateExistItem.DataSource = this.posGetOrderItemDetails(new String[] { tableNo });
            }
            catch (Exception) { }           
        }
        private void posSetBtnBack(object sender, EventArgs e)
        {
            //try { this.bill.Enabled = true; }
            //catch (Exception) { }
            try { this.home.posShowGuiHome(); }
            catch (Exception) { }
            this.posHideBillSeperatePanel();            
        }
        private String posGetAuditDate()
        {
            String processDate = this.posGetProcessDate("TBH").Substring(0, 10).Trim() + " " + this.getTime();
            return Convert.ToDateTime(processDate).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
        }
        private String posGetProcessDate(String code)
        {
            this.qry = "select Com_ProcessDate from Companies where Com_Code='" + code + "' ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            return this.dt[0, 0];
        }
        private void posSetNewBillSplitData()
        {            
            try { this.billGui.posDgvBillTableItemDetails.Rows.Clear(); }
            catch (Exception) { }

            String splitBillNo = this.guiBillSeperate.posDgvBillSeperateBillForSettle.Rows[this.guiBillSeperate.posDgvBillSeperateBillForSettle.CurrentRow.Index].Cells[1].Value.ToString();

            String[] newBillData = this.posSetNewSplitBill(new String[] { splitBillNo });
            newBillData[13] = Math.Round(Convert.ToDecimal(newBillData[13]), 2).ToString();

            this.billGui.posTxtBillSubTotal.Text = newBillData[13];
            this.billGui.posTxtBillTableNo.Text = newBillData[8];
            this.billGui.posTxtBillRoom.Text = newBillData[4];
            this.billGui.posTxtBillSaleType.Text = newBillData[32];
            this.billGui.posTxtBillStatus.Text = newBillData[10];
            this.billGui.posTxtBillSteward.Text = newBillData[6];
            this.billGui.posTxtBillCustomerType.Text = newBillData[7];

            String[,] dataTax = this.posGetTaxProperties(new String[] { newBillData[32] });
            Decimal total = Convert.ToDecimal(newBillData[13]);
            Decimal discountTotal = this.posGetDiscountvalue(total);
            if (discountTotal == total) { this.billGui.posTxtBillDiscAmt.Text = "0"; }
            else { this.billGui.posTxtBillDiscAmt.Text = (total - discountTotal).ToString(); }

            this.billGui.posTxtBillCess.Text = dataTax[0, 0];
            this.billGui.posTxtBillSc.Text = dataTax[0, 1];
            this.billGui.posTxtBillVat.Text = dataTax[0, 2];
            this.billGui.posTxtBillCessAmt.Text = Math.Round(((Convert.ToDecimal(dataTax[0, 3]) * discountTotal) / 100), 2).ToString();
            this.billGui.posTxtBillScAmt.Text = Math.Round(((Convert.ToDecimal(dataTax[0, 4]) * discountTotal) / 100), 2).ToString();
            this.billGui.posTxtBillVatAmt.Text = Math.Round(((Convert.ToDecimal(dataTax[0, 5]) * discountTotal) / 100), 2).ToString();
            Decimal actDiscount = this.posGetActDis(new String[] { dataTax[0, 3].ToString(), dataTax[0, 4].ToString(), dataTax[0, 5].ToString(), (total - discountTotal).ToString() });
            if (discountTotal != total) { this.billGui.posTxtBillActDis.Text = actDiscount.ToString(); this.billGui.posTxtBillTotal.Text = discountTotal.ToString(); }
            else { this.billGui.posTxtBillActDis.Text = "0"; }
            this.billGui.posTxtBillTotal.Text = discountTotal.ToString();
            int rowCount = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows.Count;
            // Add column to new Split bill item
            this.billGui.posDgvBillTableItemDetails.Columns.Add("posBillSplitDescription", "Description");
            this.billGui.posDgvBillTableItemDetails.Columns.Add("posBillSplitQty", "Qty");
            this.billGui.posDgvBillTableItemDetails.Columns.Add("posBillSplitPrice", "Price");
            this.billGui.posDgvBillTableItemDetails.Columns.Add("posBillSplitTotal", "Total");
            this.billGui.posDgvBillTableItemDetails.Columns.Add("posBillSplitKotBotNo", "KotBotNo");
            this.billGui.posDgvBillTableItemDetails.Columns.Add("posBillSplitCode", "Code");
            String itemKotBot = "";
            String itemName = "";
            String itemPrice = "";
            String itemTotal = "";
            String itemCode = "";
            String itemQty = "";
            for (int r = 0; r < rowCount; r++)
            {
                itemName = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[r].Cells[0].Value.ToString();
                itemQty = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[r].Cells[1].Value.ToString();
                itemPrice = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[r].Cells[2].Value.ToString();
                itemTotal = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[r].Cells[3].Value.ToString();
                itemKotBot = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[r].Cells[4].Value.ToString();
                itemCode = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[r].Cells[5].Value.ToString();
                String[] row = new String[] { itemName, itemQty, itemPrice, itemTotal, itemKotBot, itemCode };
                this.billGui.posDgvBillTableItemDetails.Rows.Add(row);
            }
            this.posSetDgvOrderedTableItemDetailsProperties();
        }
        private String[] posSetNewSplitBill(String[] data)
        {
            newBillData = new String[33];
            this.qry = "select   OblM_BillNo,OblM_SerialNo,Cur_Code,CCent_Code,(case when OblM_CusType = 'OT' then  oblM_EStaff else OblM_ERoom end ) As 'No',OblM_Date,OblM_Stw,OblM_CusType,OblM_Table,OblM_SCusType,"
            + " OblM_Status,OblM_Remark,OblM_CurrRate,OblM_Stot,OblM_Ces,OblM_CesA,OblM_ScP,OblM_SpA,OblM_VatP,OblM_VatA,OblM_DisP,OblM_DisA,OblM_ActDisA,OblM_F_Tot,"
            + "OblM_B_Tot,OblM_C_Tot,OblM_O_Tot,OblM_Billed,OblM_CreatedBy,OblM_CreatedOn,OblM_ModifiedBy,OblM_ModifiedOn,Saty_Code from FOMOutletBill where OblM_BillNo ='" + data[0] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                for (int i = 0; i < 33; i++)
                {
                    newBillData[i] = this.dt[0, i];
                }
                Decimal[] count = new Decimal[4];
                String mealCategory = "";
                for (int i = 0; i < this.guiBillSeperate.posDgvBillSeperateNewItem.Rows.Count; i++)
                {
                    String itemCode = this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[i].Cells[5].Value.ToString();
                    mealCategory = this.posGetNewBillItemMealCategory(itemCode);
                    if (mealCategory.Trim().Equals("F"))
                    {
                        count[0] += Convert.ToDecimal(this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[i].Cells[3].Value.ToString());
                    }
                    else if (mealCategory.Trim().Equals("B"))
                    {
                        count[1] += Convert.ToDecimal(this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[i].Cells[3].Value.ToString());
                    }
                    else if (mealCategory.Trim().Equals("C"))
                    {
                        count[2] += Convert.ToDecimal(this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[i].Cells[3].Value.ToString());
                    }
                    else if (mealCategory.Trim().Equals("O"))
                    {
                        count[3] += Convert.ToDecimal(this.guiBillSeperate.posDgvBillSeperateNewItem.Rows[i].Cells[3].Value.ToString());
                    }
                }

                //newBillData[0] = 
                newBillData[23] = count[0].ToString();
                newBillData[24] = count[1].ToString();
                newBillData[25] = count[2].ToString();
                newBillData[26] = count[3].ToString();
                newBillData[13] = (count[0] + count[1] + count[2] + count[3]).ToString();
                newBillData[28] = posStaticLoadOnce.PosLoggedUserId;
                newBillData[29] = this.getDateTime();
                newBillData[30] = posStaticLoadOnce.PosLoggedUserId;
                newBillData[31] = this.getDateTime();
                return newBillData;
            }
            return null;
        }
        private bool posSaveBillDetails()
        {
            
            Decimal total = Convert.ToDecimal(this.billGui.posTxtBillTotal.Text);
            String[] data = new String[35];
            data[0] = this.posGenerateBillNo(new String[] { posStaticLoadOnce.PosCostCenterCode[0].ToString() });
            data[1] = this.billGui.posTxtBillSerialNo.Text;
            data[2] = "LKR";
            data[3] = posStaticLoadOnce.PosCostCenterCode[0].ToString();
            //if (this.orderDetails[0, 2].Equals("OT")) { data[4] = this.orderDetails[0, 4]; } else { data[4] = this.orderDetails[0, 3]; }
            data[5] = this.posGetAuditDate();
            data[6] = newBillData[6];
            data[7] = newBillData[7];
            data[8] = newBillData[8];
            data[9] = newBillData[9];
            data[10] = "PR";
            data[11] = this.billGui.posTxtBillRemarks.Text;
            data[12] = "1.00";
            data[13] = total.ToString();
            data[14] = this.billGui.posTxtBillCess.Text;
            data[15] = this.billGui.posTxtBillCessAmt.Text;
            data[16] = this.billGui.posTxtBillSc.Text;
            data[17] = this.billGui.posTxtBillScAmt.Text;
            data[18] = this.billGui.posTxtBillVat.Text;
            data[19] = this.billGui.posTxtBillVatAmt.Text;
            data[20] = this.billGui.posTxtBillDiscount.Text;
            data[21] = this.billGui.posTxtBillDiscAmt.Text;
            data[22] = this.billGui.posTxtBillActDis.Text;


            data[23] = newBillData[23];
            data[24] = newBillData[24];
            data[25] = newBillData[25];
            data[26] = newBillData[26];
            data[27] = "N";
            data[28] = posStaticLoadOnce.PosLoggedStewardUserId;
            data[29] = this.getDateTime();
            data[30] = posStaticLoadOnce.PosLoggedStewardUserId;
            data[31] = this.getDateTime();
            data[32] = this.billGui.posTxtBillSaleType.Text;
            data[33] = this.guiBillSeperate.posDgvBillSeperateBillForSettle.CurrentRow.Cells[1].Value.ToString();
            data[34] = "SP";
            
                if (this.posInsertNewSplitBillData(data))
                {
                    String[] dataGuestOrder = new String[5];
                    if (this.billGui.posTxtBillSaleType.Text.ToString().Trim().Equals("AI")) { dataGuestOrder[0] = "AI"; } else { dataGuestOrder[0] = "BL"; }
                    dataGuestOrder[1] = data[0];
                    dataGuestOrder[2] = data[8];
                    dataGuestOrder[3] = posStaticLoadOnce.PosCostCenterCode[0];
                    dataGuestOrder[4] = data[32];
                    //if (this.posUpdateGuestOrderData(dataGuestOrder))
                    //{
                    String tableNo = this.billGui.posTxtBillTableNo.Text;
                    String steward = posStaticLoadOnce.PosLoggedStewardUserId;
                    String billNo = data[0];
                    String serialNo = data[1];
                    String roomNo = this.billGui.posTxtBillRoom.Text;
                    String[,] checkingGuestDetails = this.posGetGuestName(new String[] { roomNo, newBillData[7] });
                    String guestName = checkingGuestDetails[0, 0];
                    String noOfPax = "";
                    if (newBillData[7].Equals("GU")) { noOfPax = checkingGuestDetails[0, 1]; }
                    String discount = this.billGui.posTxtBillDiscount.Text;
                    String subTotal = this.billGui.posTxtBillSubTotal.Text;
                    String discountAmount = this.billGui.posTxtBillDiscAmt.Text;
                    // Get Item For Print according order no
                    int billItemCount = this.billGui.posDgvBillTableItemDetails.Rows.Count;
                    String[,] billItem = new String[billItemCount, 4];

                    for (int i = 0; i < billItemCount; i++)
                    {
                        try
                        {
                            billItem[i, 0] = this.billGui.posDgvBillTableItemDetails.Rows[i].Cells[0].Value.ToString();
                            billItem[i, 1] = this.billGui.posDgvBillTableItemDetails.Rows[i].Cells[1].Value.ToString();
                            billItem[i, 2] = this.billGui.posDgvBillTableItemDetails.Rows[i].Cells[2].Value.ToString();
                            billItem[i, 3] = this.billGui.posDgvBillTableItemDetails.Rows[i].Cells[3].Value.ToString();
                        }
                        catch (Exception) { }
                    }

                    String[] printData = new String[] { tableNo, steward, billNo, serialNo, noOfPax, roomNo, guestName, total.ToString(), discount, subTotal,discountAmount ,this.posOldBillNo};

                    this.printer.posPrintBillSplit(printData, billItem, billItemCount, this.bill, this.billGui);
                    this.billGui.posTxtBillOrderNo.Text = data[0]; MessageBox.Show("The order is billed"); this.billGui.posTxtBillOrderNo.Text = ""; return true;
                    //}
                    //else { MessageBox.Show("The order is not billed"); }
                }
                else
                {
                    MessageBox.Show("The order is not billed");
                }
           
            return false;
        }
        private bool posUpdateGuestOrderData(String[] data)
        {
            this.qry = "update FOOrderMaster set OrdM_Status='" + data[0] + "',OrdM_BillNo='" + data[1] + "' where OrdM_Table='" + data[2] + "'"
            + "AND CCent_Code='" + data[3] + "' AND SaTy_Code='" + data[4] + "' AND OrdM_Status='UB'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt32(this.dt[0, 0]) > 0)
            {
                return true;
            }
            return false;
        }
        private String[,] posGetGuestName(String[] data)
        {
            if (data[1].Equals("GU"))
            {
                this.qry = "SELECT   FOGuest.Tit_Code + ' ' + RTRIM(FOGuest.Guest_FirstName) + ' ' + RTRIM(FOGuest.Guest_LastName) AS 'Name',"
                           + " FOCheckinMaster.MChk_NoOfAdults + FOCheckinMaster.MChk_NoOfKids AS PAX  FROM  FOCheckinDetail INNER JOIN"
                         + " FOCheckinMaster ON FOCheckinDetail.MChk_Number = FOCheckinMaster.MChk_Number INNER JOIN"
                         + " FOGuest ON FOCheckinDetail.Guest_Number = FOGuest.Guest_Number "
                         + " WHERE  (FOCheckinDetail.DChk_Main = 1) AND (FOCheckinMaster.MChk_Status = 'I') and  FOCheckinMaster.Room_Code='" + data[0] + "'";
            }
            else { this.qry = "SELECT  Stf_Name as 'Name' FROM dbo.FOExecutive where Stf_Code = '" + data[0] + "'"; }

            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return this.dt; } else { return null; }
        }
        public void posSetHomeObject(posClsHome homeObject) {
            this.home = homeObject;
        }
        private String[,] posGetTaxCode(String[] data)
        {
            this.qry = "select Tax_Tax from FOCostCenterSalesTypeTax where CCent_Code='" + data[0] + "' and SaTy_Code='" + data[1] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return this.dt;
            }
            return null;
        }
        private String[,] posGetTaxProperties(String[] data)
        {
           

            String costCenter = posStaticLoadOnce.PosCostCenterCode[0];
            try
            {
                this.qry = "select Tax_Percentage1,Tax_Percentage2,Tax_Percentage3,Tax_Factor1,Tax_Factor2,Tax_Factor3,Tax_Percentage4,Tax_Factor4,Tax_PriceMethod from MTax where Tax_Tax='" + this.posGetTaxCode(new String[] { costCenter, data[0] })[0, 0] + "'";
            }
            catch (Exception ex) { }
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return this.dt;
            }
            return null;
        }
        //other
        public posGuiBillSeperate posGetGuiBillSeperateObject() {
            return this.guiBillSeperate;
        }
        private void posCreateGuiBillData() {
            this.bill = new posClsBill();
            this.billGui = this.bill.posGetBillGuiObject();
            this.bill.posSetHomeObject(this.home);
            this.bill.posSetNumericKeyPadObject(this.numPad);
            this.bill.posSetFullKeyBoardObject(this.fullKeyBoard);
            this.bill.posSetLoginAdmin(this.admin);
            
        }
        public void posShowBillSeperatePanel(){
           
            try { this.home.posHideGuiHome(); }
            catch (Exception) { }
            //try { this.guiBillSeperate.posDgvBillSeperateExistItem.Rows.Clear(); }
            //catch (Exception) { }
            try { this.guiBillSeperate.posDgvBillSeperateNewItem.Rows.Clear(); }
            catch (Exception) { }
            //try { this.posSetBillForSettleDetails(); }
            //catch (Exception) { }           
            this.guiBillSeperate.Show();           
        }
        public void posHideBillSeperatePanel() {
            this.guiBillSeperate.Hide();
            this.fullKeyBoard.posHideFullKeyBoardPad();
            this.numPad.posHideNumericPad();
        }
        public void posDisposeSeperatePanel() {
            this.guiBillSeperate.Dispose();
        }
    }
}
