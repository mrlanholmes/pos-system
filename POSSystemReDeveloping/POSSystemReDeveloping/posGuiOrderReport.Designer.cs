﻿namespace POSSystemReDeveloping
{
    partial class posGuiOrderReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.posBtnReportOrderPrint = new System.Windows.Forms.Button();
            this.posBtnReportOrderDetailShow = new System.Windows.Forms.Button();
            this.posBtnReportOrderSearch = new System.Windows.Forms.Button();
            this.posDtpReportOrderToDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.posDtpReportOrderFromDate = new System.Windows.Forms.DateTimePicker();
            this.posTxtReportOrderToDate = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.posTxtReportOrderFromDate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.posTxtReportOrderStewardNo = new System.Windows.Forms.TextBox();
            this.posTxtReportOrderStaffNo = new System.Windows.Forms.TextBox();
            this.posTxtReportOrderTableNo = new System.Windows.Forms.TextBox();
            this.posTxtReportOrderRoomNo = new System.Windows.Forms.TextBox();
            this.posTxtReportOrderBillNo = new System.Windows.Forms.TextBox();
            this.posTxtReportOrderNo = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.posDgvReportOrder = new System.Windows.Forms.DataGridView();
            this.btnOderReportBack = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvReportOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.posBtnReportOrderPrint);
            this.groupBox1.Controls.Add(this.posBtnReportOrderDetailShow);
            this.groupBox1.Controls.Add(this.posBtnReportOrderSearch);
            this.groupBox1.Controls.Add(this.posDtpReportOrderToDate);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.posDtpReportOrderFromDate);
            this.groupBox1.Controls.Add(this.posTxtReportOrderToDate);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.posTxtReportOrderFromDate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.posTxtReportOrderStewardNo);
            this.groupBox1.Controls.Add(this.posTxtReportOrderStaffNo);
            this.groupBox1.Controls.Add(this.posTxtReportOrderTableNo);
            this.groupBox1.Controls.Add(this.posTxtReportOrderRoomNo);
            this.groupBox1.Controls.Add(this.posTxtReportOrderBillNo);
            this.groupBox1.Controls.Add(this.posTxtReportOrderNo);
            this.groupBox1.Location = new System.Drawing.Point(115, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(884, 140);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // posBtnReportOrderPrint
            // 
            this.posBtnReportOrderPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnReportOrderPrint.Location = new System.Drawing.Point(757, 78);
            this.posBtnReportOrderPrint.Name = "posBtnReportOrderPrint";
            this.posBtnReportOrderPrint.Size = new System.Drawing.Size(120, 48);
            this.posBtnReportOrderPrint.TabIndex = 18;
            this.posBtnReportOrderPrint.Text = "Print";
            this.posBtnReportOrderPrint.UseVisualStyleBackColor = true;
            this.posBtnReportOrderPrint.Click += new System.EventHandler(this.posBtnReportOrderPrint_Click);
            // 
            // posBtnReportOrderDetailShow
            // 
            this.posBtnReportOrderDetailShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnReportOrderDetailShow.Location = new System.Drawing.Point(618, 82);
            this.posBtnReportOrderDetailShow.Name = "posBtnReportOrderDetailShow";
            this.posBtnReportOrderDetailShow.Size = new System.Drawing.Size(133, 44);
            this.posBtnReportOrderDetailShow.TabIndex = 17;
            this.posBtnReportOrderDetailShow.Text = "Detail";
            this.posBtnReportOrderDetailShow.UseVisualStyleBackColor = true;
            this.posBtnReportOrderDetailShow.Click += new System.EventHandler(this.posBtnReportOrderDetailShow_Click);
            // 
            // posBtnReportOrderSearch
            // 
            this.posBtnReportOrderSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnReportOrderSearch.Location = new System.Drawing.Point(476, 82);
            this.posBtnReportOrderSearch.Name = "posBtnReportOrderSearch";
            this.posBtnReportOrderSearch.Size = new System.Drawing.Size(136, 44);
            this.posBtnReportOrderSearch.TabIndex = 16;
            this.posBtnReportOrderSearch.Text = "Search";
            this.posBtnReportOrderSearch.UseVisualStyleBackColor = true;
            this.posBtnReportOrderSearch.Click += new System.EventHandler(this.posBtnReportOrderSearch_Click);
            // 
            // posDtpReportOrderToDate
            // 
            this.posDtpReportOrderToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posDtpReportOrderToDate.Location = new System.Drawing.Point(452, 92);
            this.posDtpReportOrderToDate.Name = "posDtpReportOrderToDate";
            this.posDtpReportOrderToDate.Size = new System.Drawing.Size(18, 24);
            this.posDtpReportOrderToDate.TabIndex = 15;
            this.posDtpReportOrderToDate.ValueChanged += new System.EventHandler(this.posDtpReportOrderToDate_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "From Date";
            // 
            // posDtpReportOrderFromDate
            // 
            this.posDtpReportOrderFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posDtpReportOrderFromDate.Location = new System.Drawing.Point(219, 92);
            this.posDtpReportOrderFromDate.Name = "posDtpReportOrderFromDate";
            this.posDtpReportOrderFromDate.Size = new System.Drawing.Size(18, 24);
            this.posDtpReportOrderFromDate.TabIndex = 14;
            this.posDtpReportOrderFromDate.ValueChanged += new System.EventHandler(this.posDtpReportOrderFromDate_ValueChanged);
            // 
            // posTxtReportOrderToDate
            // 
            this.posTxtReportOrderToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportOrderToDate.Location = new System.Drawing.Point(242, 92);
            this.posTxtReportOrderToDate.Name = "posTxtReportOrderToDate";
            this.posTxtReportOrderToDate.Size = new System.Drawing.Size(204, 24);
            this.posTxtReportOrderToDate.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(332, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 16);
            this.label8.TabIndex = 13;
            this.label8.Text = "To Date";
            // 
            // posTxtReportOrderFromDate
            // 
            this.posTxtReportOrderFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportOrderFromDate.Location = new System.Drawing.Point(9, 92);
            this.posTxtReportOrderFromDate.Name = "posTxtReportOrderFromDate";
            this.posTxtReportOrderFromDate.Size = new System.Drawing.Size(204, 24);
            this.posTxtReportOrderFromDate.TabIndex = 12;
            this.posTxtReportOrderFromDate.TextChanged += new System.EventHandler(this.posTxtReportOrderFromDate_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(551, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "Staff No";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(718, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Steward No";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(390, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Table No";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(217, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Room No";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(124, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Bill No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Order No";
            // 
            // posTxtReportOrderStewardNo
            // 
            this.posTxtReportOrderStewardNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportOrderStewardNo.Location = new System.Drawing.Point(719, 36);
            this.posTxtReportOrderStewardNo.Name = "posTxtReportOrderStewardNo";
            this.posTxtReportOrderStewardNo.Size = new System.Drawing.Size(158, 23);
            this.posTxtReportOrderStewardNo.TabIndex = 5;
            // 
            // posTxtReportOrderStaffNo
            // 
            this.posTxtReportOrderStaffNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportOrderStaffNo.Location = new System.Drawing.Point(554, 36);
            this.posTxtReportOrderStaffNo.Name = "posTxtReportOrderStaffNo";
            this.posTxtReportOrderStaffNo.Size = new System.Drawing.Size(158, 24);
            this.posTxtReportOrderStaffNo.TabIndex = 4;
            // 
            // posTxtReportOrderTableNo
            // 
            this.posTxtReportOrderTableNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportOrderTableNo.Location = new System.Drawing.Point(387, 36);
            this.posTxtReportOrderTableNo.Name = "posTxtReportOrderTableNo";
            this.posTxtReportOrderTableNo.Size = new System.Drawing.Size(158, 24);
            this.posTxtReportOrderTableNo.TabIndex = 3;
            // 
            // posTxtReportOrderRoomNo
            // 
            this.posTxtReportOrderRoomNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportOrderRoomNo.Location = new System.Drawing.Point(220, 36);
            this.posTxtReportOrderRoomNo.Name = "posTxtReportOrderRoomNo";
            this.posTxtReportOrderRoomNo.Size = new System.Drawing.Size(158, 24);
            this.posTxtReportOrderRoomNo.TabIndex = 2;
            // 
            // posTxtReportOrderBillNo
            // 
            this.posTxtReportOrderBillNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportOrderBillNo.Location = new System.Drawing.Point(127, 36);
            this.posTxtReportOrderBillNo.Name = "posTxtReportOrderBillNo";
            this.posTxtReportOrderBillNo.Size = new System.Drawing.Size(86, 24);
            this.posTxtReportOrderBillNo.TabIndex = 1;
            // 
            // posTxtReportOrderNo
            // 
            this.posTxtReportOrderNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtReportOrderNo.Location = new System.Drawing.Point(7, 36);
            this.posTxtReportOrderNo.Name = "posTxtReportOrderNo";
            this.posTxtReportOrderNo.Size = new System.Drawing.Size(113, 24);
            this.posTxtReportOrderNo.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.posDgvReportOrder);
            this.groupBox2.Location = new System.Drawing.Point(115, 158);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(884, 573);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // posDgvReportOrder
            // 
            this.posDgvReportOrder.AllowUserToAddRows = false;
            this.posDgvReportOrder.AllowUserToDeleteRows = false;
            this.posDgvReportOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.posDgvReportOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvReportOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.posDgvReportOrder.Location = new System.Drawing.Point(3, 16);
            this.posDgvReportOrder.Name = "posDgvReportOrder";
            this.posDgvReportOrder.ReadOnly = true;
            this.posDgvReportOrder.Size = new System.Drawing.Size(878, 554);
            this.posDgvReportOrder.TabIndex = 0;
            // 
            // btnOderReportBack
            // 
            this.btnOderReportBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOderReportBack.Location = new System.Drawing.Point(1, 12);
            this.btnOderReportBack.Name = "btnOderReportBack";
            this.btnOderReportBack.Size = new System.Drawing.Size(95, 92);
            this.btnOderReportBack.TabIndex = 1;
            this.btnOderReportBack.Text = "Back";
            this.btnOderReportBack.UseVisualStyleBackColor = true;
            this.btnOderReportBack.Click += new System.EventHandler(this.btnOderReportBack_Click);
            // 
            // posGuiOrderReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 733);
            this.ControlBox = false;
            this.Controls.Add(this.btnOderReportBack);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "posGuiOrderReport";
            this.Text = "Order Report";
            this.Load += new System.EventHandler(this.posGuiOrderReport_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.posDgvReportOrder)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button posBtnReportOrderPrint;
        private System.Windows.Forms.Button posBtnReportOrderDetailShow;
        private System.Windows.Forms.Button posBtnReportOrderSearch;
        private System.Windows.Forms.DateTimePicker posDtpReportOrderToDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker posDtpReportOrderFromDate;
        private System.Windows.Forms.TextBox posTxtReportOrderToDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox posTxtReportOrderFromDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox posTxtReportOrderStewardNo;
        private System.Windows.Forms.TextBox posTxtReportOrderStaffNo;
        private System.Windows.Forms.TextBox posTxtReportOrderTableNo;
        private System.Windows.Forms.TextBox posTxtReportOrderRoomNo;
        private System.Windows.Forms.TextBox posTxtReportOrderBillNo;
        private System.Windows.Forms.TextBox posTxtReportOrderNo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView posDgvReportOrder;
        private System.Windows.Forms.Button btnOderReportBack;
    }
}