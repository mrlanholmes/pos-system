﻿namespace POSSystemReDeveloping
{
    partial class posCrvReportOrderDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.posCrvOrdeReportDetail = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.btnCrvOrderBack = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.posCrvOrdeReportDetail);
            this.groupBox1.Location = new System.Drawing.Point(115, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(894, 716);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // posCrvOrdeReportDetail
            // 
            this.posCrvOrdeReportDetail.ActiveViewIndex = -1;
            this.posCrvOrdeReportDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posCrvOrdeReportDetail.Cursor = System.Windows.Forms.Cursors.Default;
            this.posCrvOrdeReportDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.posCrvOrdeReportDetail.Location = new System.Drawing.Point(3, 16);
            this.posCrvOrdeReportDetail.Name = "posCrvOrdeReportDetail";
            this.posCrvOrdeReportDetail.Size = new System.Drawing.Size(888, 697);
            this.posCrvOrdeReportDetail.TabIndex = 0;
            this.posCrvOrdeReportDetail.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // btnCrvOrderBack
            // 
            this.btnCrvOrderBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrvOrderBack.Location = new System.Drawing.Point(1, 5);
            this.btnCrvOrderBack.Name = "btnCrvOrderBack";
            this.btnCrvOrderBack.Size = new System.Drawing.Size(111, 95);
            this.btnCrvOrderBack.TabIndex = 1;
            this.btnCrvOrderBack.Text = "Back";
            this.btnCrvOrderBack.UseVisualStyleBackColor = true;
            this.btnCrvOrderBack.Click += new System.EventHandler(this.button1_Click);
            // 
            // posCrvReportOrderDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 733);
            this.ControlBox = false;
            this.Controls.Add(this.btnCrvOrderBack);
            this.Controls.Add(this.groupBox1);
            this.Name = "posCrvReportOrderDetail";
            this.Text = "Print Order Details";
            this.Load += new System.EventHandler(this.posCrvReportOrderDetail_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer posCrvOrdeReportDetail;
        private System.Windows.Forms.Button btnCrvOrderBack;
    }
}