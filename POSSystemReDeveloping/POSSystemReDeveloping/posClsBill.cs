﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    class posClsBill : pos
    {
        private posGuiBill bill;
        private posClsHome home;
        private String[,] orderDetails;
        private Decimal total = 0;
        private posClsPrinter printer;
        private posClsBillSeperate billSeperate;
        private posClsLoginAdmin admin;
        private posClsFullKeyBoard fullKeyBoard;
        private posClsNumericPad numPad;
     
        public posClsBill() {
            this.orderDetails = null;            
            this.bill = new posGuiBill();            
            this.posSetGuiBillProperties();
            this.posSetBtnDiscountProperties();
            this.posSetBtnDiscountCalculateProperties();
            this.posSetBtnBillSaveProperties();

            this.printer = new posClsPrinter();
            this.bill.posTxtBillRemarks.Click += this.posSetTxtRemarkClick;
            this.bill.posTxtBillSerialNo.Click += this.posSetTxtSerialNumberClick;
            this.bill.posBtnBillKeyBoard.Click += this.posSetBtnKeyBoardClick;
            this.bill.posBtnBillBillSeperate.Click += this.posSetBtnBillBillSeperateClick;
            this.bill.posDgvBillTableNumber.Click += this.posSetDgvOrderedTableNumbersClick;
            this.bill.posTxtBillDiscount.Click += this.posSetTxtDiscountClick;
        }
        public void posSetHomeObject(posClsHome homeObject) {
            this.home = homeObject;
        }
        public void posSetLoginAdmin(posClsLoginAdmin adminObject) {
            this.admin = adminObject;
        }
        public void posSetNumericKeyPadObject(posClsNumericPad numPadObject)
        {
            this.numPad = numPadObject;
        }
        public void posSetFullKeyBoardObject(posClsFullKeyBoard fullKeyBoardObject)
        {
            this.fullKeyBoard = fullKeyBoardObject;
        }
        // DB data retrive
        public DataGridView posGetDgvBillTableDetails() {
            return this.bill.posDgvBillTableItemDetails;
        }
        private DataTable posGetOrderedTableNumbers() {
            //this.qry = "SELECT OrdM_Table FROM FOOrderMaster where OrdM_Status = 'UB' and CCent_Code = '"+posStaticLoadOnce.PosCostCenterCode[0]+"' group by OrdM_Table";
            this.qry = "SELECT OrdM_Table FROM FOOrderMaster where FOOrderMaster.OrdM_Status = 'UB' and FOOrderMaster.CCent_Code = '" + posStaticLoadOnce.PosCostCenterCode[0] + "' "
                        + " and FOOrderMaster.OrdM_CreatedOn between '" + DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd") + "' and '" + DateTime.Today.AddDays(+1).ToString("yyyy-MM-dd") + "' "
                        + "  group by FOOrderMaster.OrdM_Table";
            //this.qry = "SELECT OrdM_Table FROM FOOrderMaster where FOOrderMaster.OrdM_Status = 'UB' and FOOrderMaster.CCent_Code = '" + posStaticLoadOnce.PosCostCenterCode[0] + "' and 0 < (select count(*)"
            //            + " FROM FOOrderDetail where  FOOrderDetail.Item_Status ='AC' and FOOrderDetail.OrdM_No = FOOrderMaster.OrdM_No ) "
            //            + " and FOOrderMaster.OrdM_CreatedOn between '" + DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd") + "' and '" + DateTime.Today.AddDays(+1).ToString("yyyy-MM-dd") + "' group by FOOrderMaster.OrdM_Table";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return posStaticLoadOnce.PosDataTable;
            }
            return null;
        }
        private DataTable posGetOrderTableDetails(String[] data) {
            //this.qry = "  select FOOrderMaster.OrdM_No   as 'Total Price',FOOrderMaster.OrdM_Date as 'Order Date' , "
            //+"FOOrderMaster.OrdM_KOTBOT as 'KOT/BOT No',FOOrderMaster.OrdM_Status as 'Status',"
            //+"FOOrderMaster.SaTy_Code as'Sales Type' from FOOrderDetail , FOOrderMaster where FOOrderMaster.OrdM_No = FOOrderDetail.OrdM_No "
            //+"and  FOOrderMaster.OrdM_Table = '"+data[0]+"' and FOOrderMaster.OrdM_Status <> 'BL' and FOOrderMaster.CCent_Code = '"+data[1]+"' "
            //+ "group by FOOrderMaster.OrdM_KOTBOT ,FOOrderMaster.OrdM_No,FOOrderMaster.OrdM_Date ,FOOrderMaster.OrdM_Status,FOOrderMaster.SaTy_Code "
            //+ "order by FOOrderMaster.OrdM_Date desc";
           // this.qry = "select FOOrderMaster.OrdM_No as 'Order No',FOOrderMaster.OrdM_Date as 'Order Date'  "
           // + ",FOOrderMaster.OrdM_Status as 'Status' ,FOOrderMaster.SaTy_Code as'Sales Type' , FOOrderMaster.OrdM_KOTBOT as 'KOT/BOT No'  from FOOrderMaster where "
           // + " FOOrderMaster.OrdM_Table = '" + data[0] + "' and FOOrderMaster.OrdM_Status = 'UB' and FOOrderMaster.CCent_Code = '" + data[1] + "' and 0 < (select count(*)"
           //             + " FROM FOOrderDetail where  FOOrderDetail.Item_Status ='AC' and FOOrderDetail.OrdM_No = FOOrderMaster.OrdM_No )"
           //+ " and FOOrderMaster.OrdM_CreatedOn between '" + DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd") + "' and '" + DateTime.Today.AddDays(+1).ToString("yyyy-MM-dd") + "' order by FOOrderMaster.OrdM_Date desc";
            this.qry = "select FOOrderMaster.OrdM_No as 'Order No',FOOrderMaster.OrdM_Date as 'Order Date'  "
            + ",FOOrderMaster.OrdM_Status as 'Status' ,FOOrderMaster.SaTy_Code as'Sales Type' , FOOrderMaster.OrdM_KOTBOT as 'KOT/BOT No'  from FOOrderMaster where "
            + " FOOrderMaster.OrdM_Table = '" + data[0] + "' and FOOrderMaster.OrdM_Status = 'UB' and FOOrderMaster.CCent_Code = '" + data[1] + "'  "
                        + " "
           + " and FOOrderMaster.OrdM_CreatedOn between '" + DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd") + "' and '" + DateTime.Today.AddDays(+1).ToString("yyyy-MM-dd") + "' order by FOOrderMaster.OrdM_Date desc";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return posStaticLoadOnce.PosDataTable;
            }
            return null;
        }
        private String[,] posGetOrderItemTotal(String[] data) {
            this.qry = "  select  Sum(FOOrderDetail.OrdD_Price*FOOrderDetail.OrdD_Qty) as 'Total' "
            + " from FOOrderMaster,FOOrderDetail,FOMItems where FOOrderDetail.OrdM_No = FOOrderMaster.OrdM_No and "
            + "FOOrderDetail.FOIM_ItemNo = FOMItems.FOIM_ItemNo and FOOrderMaster.OrdM_Table ='" + data[0] + "' "
            + "and FOOrderMaster.OrdM_Status = 'UB' and FOOrderMaster.CCent_Code ='" + posStaticLoadOnce.PosCostCenterCode[0] + "' and FOOrderDetail.Item_Status = 'AC'  "; 
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return this.dt;
            }
            return null;
        }
        public  DataTable posGetOrderItemDetails(String[] data)
        {            
            this.qry = " select FOMItems.FOIM_ItemDes as 'Name' ,FOOrderDetail.OrdD_Cost  ,FOOrderDetail.OrdD_Price as 'Price' ,"
            + " FOOrderDetail.OrdD_Qty as 'Qty',FOMItems.FOIM_Type1 as 'Type' ,"
            + " (FOOrderDetail.OrdD_Price * FOOrderDetail.OrdD_Qty) as 'Total' "
            +" from FOOrderMaster,FOOrderDetail,FOMItems where FOOrderDetail.OrdM_No = FOOrderMaster.OrdM_No and "
            +"FOOrderDetail.FOIM_ItemNo = FOMItems.FOIM_ItemNo and FOOrderMaster.OrdM_Table ='"+data[0]+"' "
            + "and FOOrderMaster.OrdM_Status = 'UB' AND  FOOrderDetail.Item_Status = 'AC' and FOOrderMaster.CCent_Code ='" + posStaticLoadOnce.PosCostCenterCode[0] + "'"
            + " and FOOrderMaster.OrdM_CreatedOn between '" + DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd") + "' and '" + DateTime.Today.AddDays(+1).ToString("yyyy-MM-dd") + "'";
            //and FOOrderMaster.OrdM_No = '"+data[1]+"'
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                
                return posStaticLoadOnce.PosDataTable;
            }
            return null;
        }
        public String[,] posGetOrderItemDetailsPrint(String[] data)
        {
            this.qry = " select FOMItems.FOIM_ItemDes as 'Name' ,FOOrderDetail.OrdD_Cost  ,FOOrderDetail.OrdD_Price as 'Price' ,"
            + " FOOrderDetail.OrdD_Qty as 'Qty',FOMItems.FOIM_Type1 as 'Type' ,"
            + " (FOOrderDetail.OrdD_Price*FOOrderDetail.OrdD_Qty) as 'Total',FOMItems.FOIM_ItemNo "
            + " from FOOrderMaster,FOOrderDetail,FOMItems where FOOrderDetail.OrdM_No = FOOrderMaster.OrdM_No and "
            + "FOOrderDetail.FOIM_ItemNo = FOMItems.FOIM_ItemNo and FOOrderMaster.OrdM_Table ='" + data[0] + "' "
            + "and FOOrderMaster.OrdM_Status = 'BL' and FOOrderMaster.OrdM_No = '" + data[1] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {                
                return this.dt;
            }
            return null;
        }
        private String posGenerateBillNo(String[] costCenterCode)
        {
            this.qry = "select CCent_Prefix from FOCostCenter where CCent_Code='" + costCenterCode[0] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            String costCenterPrefix = this.dt[0, 0].Replace(" ", "").Trim();
            this.qry = "select max(FOMOutletBill.OblM_BillNo) from FOMOutletBill where  Left(FOMOutletBill.OblM_BillNo,2)='" + costCenterPrefix + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt[0, 0] == null) { this.dt[0, 0] = "1"; }
            this.dt[0, 0] = (Convert.ToInt32(this.dt[0, 0].ToString().Trim().Replace(" ", "").Replace("" + costCenterPrefix + "", "")) + 1).ToString();
            return this.dt[0, 0].PadLeft(6, '0').ToString().Insert(0, costCenterPrefix);
        }
        private String[,] posGetOrderDetails(String[] data) {
            this.qry = "SELECT  OrdM_Stw, OrdM_Table,OrdM_CusType ,OrdM_Room,OrdM_Staff,OrdM_Status ,SaTy_Code FROM FOOrderMaster where OrdM_No = '" + data[0] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return this.dt;
            }
            return null;
        }
        private String[,] posGetTaxCode(String[] data)
        {
            this.qry = "select Tax_Tax from FOCostCenterSalesTypeTax where CCent_Code='"+data[0]+"' and SaTy_Code='"+data[1]+"'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return this.dt;
            }
            return null;
        }
        private String[,] posGetTaxProperties() {
            String saleType = "";
            try { saleType = this.bill.posDgvBillTableDetails.CurrentRow.Cells[3].Value.ToString(); }
            catch (Exception ex) { }
             
            String costCenter = posStaticLoadOnce.PosCostCenterCode[0];
            try {
                        this.qry = "select Tax_Percentage1,Tax_Percentage2,Tax_Percentage3,Tax_Factor1,Tax_Factor2,Tax_Factor3,Tax_Percentage4,Tax_Factor4,Tax_PriceMethod from MTax where Tax_Tax='" + this.posGetTaxCode(new String[] { costCenter, saleType })[0, 0] + "'";
            }
            catch (Exception ex) { }
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return this.dt;
            }
            return null;
        }
        private String posGetProcessDate(String code)
        {
            this.qry = "select Com_ProcessDate from Companies where Com_Code='" + code + "' ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            return this.dt[0, 0];
        }
        private String posGetAuditDate()
        {
            String processDate = this.posGetProcessDate("TBH").Substring(0, 10).Trim() + " " + this.getTime();
            return Convert.ToDateTime(processDate).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
        }
        private bool posInsertBillData(String[] data) {
            String CusType = "";
            if (data[7].Equals("OT")) { CusType = "OblM_EStaff"; } else { CusType = "OblM_ERoom"; }
            this.qry = "insert into FOMOutletBill(OblM_BillNo,OblM_SerialNo,Cur_Code,CCent_Code,"+CusType+",OblM_Date,OblM_Stw,OblM_CusType,OblM_Table,OblM_SCusType,"
            +" OblM_Status,OblM_Remark,OblM_CurrRate,OblM_Stot,OblM_Ces,OblM_CesA,OblM_ScP,OblM_SpA,OblM_VatP,OblM_VatA,OblM_DisP,OblM_DisA,OblM_ActDisA,OblM_F_Tot,"
            +"OblM_B_Tot,OblM_C_Tot,OblM_O_Tot,OblM_Billed,OblM_CreatedBy,OblM_CreatedOn,OblM_ModifiedBy,OblM_ModifiedOn,Saty_Code)"
            + "values('" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "','" + data[7] + "',"
            + "'" + data[8] + "','" + data[9] + "','" + data[10] + "','" + data[11] + "'," + data[12] + "," + data[13] + "," + data[14] + "," + data[15] + ","
            + "" + data[ 16] + "," + data[17] + "," + data[18] + "," + data[19] + "," + data[20] + "," + data[21] + "," + data[22] + "," + data[23] + "," + data[24] + ","
            + "" + data[25] + "," + data[26] + ",'" + data[27] + "','" + data[28] + "','" + data[29] + "','" + data[30] + "','" + data[31] + "','" + data[32] + "')";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt32(this.dt[0, 0]) == 1)
            {
                return true;
            }
            return false;
        }
        private bool posUpdateGuestOrderData(String[] data) {
            this.qry = "update FOOrderMaster set OrdM_Status='" + data[0] + "',OrdM_BillNo='" + data[1] + "' where OrdM_Table='" + data[2] + "'"
            + "AND CCent_Code='" + data[3] + "' AND SaTy_Code='" + data[4] + "' AND OrdM_Status='UB'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt32(this.dt[0, 0]) > 0)
            {
                return true;
            }
            return false;
        } 
        private String posGetNextSerialNo() {
            this.qry = "select (OblM_SerialNo ) from FOMOutletBill where OblM_CreatedOn = (select max(OblM_CreatedOn) from FOMOutletBill)";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            try {
                if (Convert.ToDouble(this.dt[0, 0]) > 0) { return (Convert.ToDouble(this.dt[0, 0]) + 1).ToString(); } else { return "1"; }       
            }
            catch (Exception) { }
            return null; 
        }
        private String[,] posGetGuestName(String[] data) {
            if (data[1].Equals("GU")) {                 
                this.qry = "SELECT   FOGuest.Tit_Code + ' ' + RTRIM(FOGuest.Guest_FirstName) + ' ' + RTRIM(FOGuest.Guest_LastName) AS 'Name',"
                           + " FOCheckinMaster.MChk_NoOfAdults + FOCheckinMaster.MChk_NoOfKids AS PAX  FROM  FOCheckinDetail INNER JOIN" 
                         +" FOCheckinMaster ON FOCheckinDetail.MChk_Number = FOCheckinMaster.MChk_Number INNER JOIN" 
                         +" FOGuest ON FOCheckinDetail.Guest_Number = FOGuest.Guest_Number "
                         +" WHERE  (FOCheckinDetail.DChk_Main = 1) AND (FOCheckinMaster.MChk_Status = 'I') and  FOCheckinMaster.Room_Code='"+data[0]+"'";
            }
            else { this.qry = "SELECT  Stf_Name as 'Name' FROM dbo.FOExecutive where Stf_Code='"+data[0]+"'"; }
           
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return this.dt; } else { return null; }
        }
        private bool posInsertFODBill(String[] data) {
            this.qry = "insert into FODOutletBill(OblM_BillNo,FOIM_ItemNo,OblD_Cost,OblD_Price,OblD_Qty,OrdM_KOTBOT) values('" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "')";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt16(this.dt[0, 0]) == 1) {
                return true;
            }
            return false;
        }
        // Events
        public void posSetBtnHomeClick(object sender, EventArgs e)
        {
            this.home.posShowGuiHome();
            this.posBillPanelHide();
        }
        private void posSetDgvOrderedTableNumbersClick(object sender, EventArgs e) {
            try
            {
                int rowCountItemDetails = this.bill.posDgvBillTableItemDetails.Rows.Count;
                int rowCountTableDetails = this.bill.posDgvBillTableDetails.Rows.Count;
                //for (int i = rowCountItemDetails; i > -1; i--) { try { this.bill.posDgvBillTableItemDetails.Rows.RemoveAt(i - 1); } catch (Exception) { } }
                //for (int i = rowCountTableDetails; i > -1; i--) { try { this.bill.posDgvBillTableDetails.Rows.RemoveAt(i - 1); } catch (Exception) { } }              
                String table = this.bill.posDgvBillTableNumber.CurrentRow.Cells[0].Value.ToString();
                String costCenterCode = posStaticLoadOnce.PosCostCenterCode[0];
                this.bill.posDgvBillTableDetails.DataSource = this.posGetOrderTableDetails(new String[] { table, costCenterCode });
                this.posSetDgvOrderedTableDetailsProperties();
                this.posClearText();
                //this.posSetDgvOrderedTableDetailsClick(sender, e);
                this.posSetDgvOrderedTableDetails();
            }
            catch (Exception) { }            
            
        }
        public void posClearText() {
            this.bill.posTxtBillTotal.Text = "";
            this.bill.posTxtBillTableNo.Text = "";
            this.bill.posTxtBillSteward.Text = "";
            this.bill.posTxtBillRoom.Text = "";
            this.bill.posTxtBillRemarks.Text = "";
            this.bill.posTxtBillActDis.Text = "";
            this.bill.posTxtBillCess.Text = "";
            this.bill.posTxtBillCessAmt.Text = "";
            this.bill.posTxtBillDiscAmt.Text = "";
            this.bill.posTxtBillSc.Text = "";
            this.bill.posTxtBillScAmt.Text = "";
            this.bill.posTxtBillStatus.Text = "";
            this.bill.posTxtBillSubTotal.Text = "";
            this.bill.posTxtBillVat.Text = "";
            this.bill.posTxtBillVatAmt.Text = "";
            this.bill.posTxtBillSaleType.Text = "";
            this.bill.posTxtBillDiscount.Text = "";
            this.bill.posTxtBillTableDetailsKOTBOT.Text = "";
            this.bill.posTxtBillDgvItemTotal.Text = "";
        }
        private void posSetDgvOrderedTableDetails() {
            this.total = 0;
            //int rowCountItemDetails = this.bill.posDgvBillTableItemDetails.Rows.Count;
            //for (int i = rowCountItemDetails; i > -1; i--) { try { this.bill.posDgvBillTableItemDetails.Rows.RemoveAt(i - 1); } catch (Exception) { } }

            String orderNo = "";
            String tableNo = "";
            try { orderNo = this.bill.posDgvBillTableDetails.CurrentRow.Cells[0].Value.ToString(); }
            catch (Exception) { }
            try { tableNo = this.bill.posDgvBillTableNumber.CurrentRow.Cells[0].Value.ToString(); }
            catch (Exception) { }

            this.bill.posDgvBillTableItemDetails.DataSource = this.posGetOrderItemDetails(new String[] { tableNo, orderNo });

            this.posSetDgvOrderedTableItemDetailsProperties();
            try { this.total = Convert.ToDecimal(this.posGetOrderItemTotal(new String[] { tableNo })[0, 0]); }
            catch (Exception) { }


            this.total = Math.Round(this.total, 2);
            this.bill.posTxtBillSubTotal.Text = this.total.ToString();
            this.bill.posTxtBillTotal.Text = this.total.ToString();

            try
            {
                this.orderDetails = this.posGetOrderDetails(new String[] { orderNo });
                this.bill.posTxtBillSteward.Text = orderDetails[0, 0];
                this.bill.posTxtBillTableNo.Text = orderDetails[0, 1];
                if (orderDetails[0, 2].Equals("OT")) { this.bill.posLblBillRoom.Text = "Executive :"; this.bill.posTxtBillCustomerType.Text = "Others"; this.bill.posTxtBillRoom.Text = orderDetails[0, 4]; }
                else { this.bill.posLblBillRoom.Text = "      Room :"; this.bill.posTxtBillCustomerType.Text = "Guest"; this.bill.posTxtBillRoom.Text = orderDetails[0, 3]; }
                this.bill.posTxtBillStatus.Text = orderDetails[0, 5];
                this.bill.posTxtBillSaleType.Text = orderDetails[0, 6];
            }
            catch (Exception) { }
            try
            {
                String[,] dataTax = this.posGetTaxProperties();
                this.bill.posTxtBillCess.Text = dataTax[0, 0];
                this.bill.posTxtBillSc.Text = dataTax[0, 1];
                this.bill.posTxtBillVat.Text = dataTax[0, 2];
                Decimal discountTotal = this.posGetDiscountvalue(total);
                if (discountTotal == this.total) { this.bill.posTxtBillDiscAmt.Text = "0"; }
                else { this.bill.posTxtBillDiscAmt.Text = (this.total - discountTotal).ToString(); }

                this.bill.posTxtBillCessAmt.Text = Math.Round(((Convert.ToDecimal(dataTax[0, 3]) * discountTotal) / 100), 2).ToString();
                this.bill.posTxtBillScAmt.Text = Math.Round(((Convert.ToDecimal(dataTax[0, 4]) * discountTotal) / 100), 2).ToString();
                this.bill.posTxtBillVatAmt.Text = Math.Round(((Convert.ToDecimal(dataTax[0, 5]) * discountTotal) / 100), 2).ToString();
                Decimal actDiscount = this.posGetActDis(new String[] { dataTax[0, 3].ToString(), dataTax[0, 4].ToString(), dataTax[0, 5].ToString(), (total - discountTotal).ToString() });
                if (discountTotal != this.total) { this.bill.posTxtBillActDis.Text = actDiscount.ToString(); this.bill.posTxtBillTotal.Text = discountTotal.ToString(); }
                else { this.bill.posTxtBillActDis.Text = "0"; }
            }
            catch (Exception) { }
            try
            {
               // this.posSetDgvOrderedTableItemRowsAdded(sender, e);
            }
            catch (Exception) { }
            try
            {
                String kotbotNo = this.bill.posDgvBillTableDetails.Rows[this.bill.posDgvBillTableDetails.CurrentRow.Index].Cells[4].Value.ToString();
                this.bill.posTxtBillTableDetailsKOTBOT.Text = kotbotNo;
            }
            catch (Exception) { }
        }
        private void posSetDgvOrderedTableDetailsClick(object sender, EventArgs e) {
            //this.posSetDgvOrderedTableDetails();
        }
        private void posSetDgvOrderedTableItemClick(object sender, EventArgs e)
        {
            try
            {
                Decimal itemTotal = Convert.ToDecimal(this.bill.posDgvBillTableItemDetails.Rows[this.bill.posDgvBillTableItemDetails.CurrentRow.Index].Cells[5].Value);
                this.bill.posTxtBillDgvItemTotal.Text =Math.Round(itemTotal,2).ToString();
            }
            catch (Exception) { }
        }
        private void posSetBtnKeyBoardClick(object sender, EventArgs e) {
            this.fullKeyBoard.posSetBillObject(this.bill);
            this.fullKeyBoard.posShowFullKeyBoardPad();

        }
        private void posSetTxtRemarkClick(object sender, EventArgs e)
        {
            this.numPad.posHideNumericPad();
            this.fullKeyBoard.posSetBillObject(this.bill);
            this.fullKeyBoard.posShowFullKeyBoardPad();
            this.bill.posTxtBillRemarks.Focus();
        }
        private void posSetTxtSerialNumberClick(object sender, EventArgs e)
        {
            //this.numPad.posShowNumericPad();\
            
            this.fullKeyBoard.posHideFullKeyBoardPad();
            this.numPad.posSetBillObject(this.bill);
            this.numPad.posShowNumericPad();
            int x = 0;
            int y = 0;
            try {
                 x = this.posGetBillGuiObject().Location.X + this.posGetBillGuiObject().Width - 350; 
                 y = this.posGetBillGuiObject().Location.Y + this.posGetBillGuiObject().Height - 520;
            }
            catch (Exception) { }
            this.numPad.posSetViewLocationNumericPad(new int[] {x,y});
        }
            
        private void posSetTxtDiscountClick(object sender, EventArgs e)
        {
            this.bill.TopMost = false;
            this.fullKeyBoard.posHideFullKeyBoardPad();
            this.numPad.posSetBillObject(this.bill);
            this.numPad.posShowNumericPad();
            int x = 0;
            int y = 0;
            try
            {
                x = this.posGetBillGuiObject().Location.X + this.posGetBillGuiObject().Width - 660;
                y = this.posGetBillGuiObject().Location.Y + this.posGetBillGuiObject().Height - 440;
            }
            catch (Exception) { }
            this.numPad.posSetViewLocationNumericPad(new int[] { x, y });
            this.bill.posTxtBillDiscount.Focus();
        }
        private void posSetDgvOrderedTableItemRowsAdded(object sender, EventArgs e) {
            try {
                this.bill.posDgvBillTableItemDetails.Rows[0].Selected = true;
                this.posSetDgvOrderedTableItemClick(sender,e);
            }catch(Exception){}
        }
        private void posSetDgvOrderedTableDetailsRowsAdded(object sender, EventArgs e)
        {
            try
            {
                this.bill.posDgvBillTableDetails.Rows[0].Selected = true;
                this.posSetDgvOrderedTableDetailsClick(sender, e);
            }
            catch (Exception) { }
        }
        private void posSetBillFormLoad(object sender, EventArgs e)
        {            
            //this.posSetDgvBillTableNumberData();         
        }
        private void posSetDgvBillTableNumberData() {
            this.bill.posDgvBillTableNumber.DataSource = this.posGetOrderedTableNumbers();
            this.posSetDgvOrderedTableNumbersProperties();
            //this.bill.posTxtBillOrderNo.Text = this.posGenerateBillNo(posStaticLoadOnce.PosCostCenterCode);
            this.bill.posTxtBillCashier.Text = posStaticLoadOnce.PosLoggedStewardUserName;
            //this.bill.posTxtBillSteward.Text = posStaticLoadOnce.PosLoggedStewardUserName;
            this.bill.posTxtBillCostCenter.Text = posStaticLoadOnce.PosCostCenterCode[0];
            this.bill.posTxtBillSerialNo.Text = this.posGetNextSerialNo();
            this.bill.posTxtBillDate.Text = this.getDateTime();
            //this.bill.posTxtBillDiscount.ReadOnly = true;
        }
        public void posSetBtnDiscountCalculateClick(object sender, EventArgs e) {           
                
                if (this.admin.isValid())
                {
                    //this.posSetDgvOrderedTableDetailsClick(sender, e);
                    this.posSetDgvOrderedTableDetails();
                    this.admin.isValid(false);
                    
                    this.bill.posTxtBillDiscount.Enabled = false;
                    
                }
                this.bill.posBtnBillCalculateDiscount.Enabled = false;
                this.bill.TopMost = true;
        }
        public void posSetBtnDiscountClick(object sender, EventArgs e)
        {
            if (this.bill.posDgvBillTableNumber.SelectedRows.Count == 0) { MessageBox.Show("Please Select an order"); }
            else {
                this.bill.posTxtBillDiscount.BackColor = SystemColors.ControlLight;
                this.fullKeyBoard.posShowFullKeyBoardPad();
                this.admin.posSetBillVarificationState(true, this.bill);
                this.admin.posShowGuiAdmin();
                
                this.fullKeyBoard.posSetPositionFullKeyBoardPad();
                
            }
        }
        
        public void posSetBtnBillSaveClick(object sender, EventArgs e)
        {
            this.numPad.posHideNumericPad();
            this.fullKeyBoard.posHideFullKeyBoardPad();
            int errCounter =0;

            if (this.bill.posTxtBillSerialNo.Text.Equals("") || this.bill.posTxtBillSerialNo.Text == null) { errCounter++; MessageBox.Show("Please Enter A Serial Number..."); }
            else { try { Convert.ToInt64(this.bill.posTxtBillSerialNo.Text); } catch (Exception ex) { errCounter++; MessageBox.Show("Please Enter A Correct Serial Number..."); } }
            if (errCounter !=1) {
                if (this.bill.posDgvBillTableDetails.Rows.Count > 0) { 
                    if (this.posSaveBillDetails()) {
                        this.posClearText();
                        this.bill.posDgvBillTableNumber.DataSource = null;
                        this.bill.posDgvBillTableDetails.DataSource = null;
                        this.bill.posDgvBillTableItemDetails.DataSource = null;
                    }
                
                } else { MessageBox.Show("Please Select a Guest Table "); }               
            }



            try { this.posSetDgvBillTableNumberData(); }
            catch (Exception) { }
            
        }
        private void posSetBtnBillBillSeperateClick(object sender, EventArgs e)
        {
            if (this.bill.posDgvBillTableNumber.SelectedRows.Count > 0)
            {
                if (this.billSeperate == null)
                {
                    this.billSeperate = new posClsBillSeperate();
                   // this.billSeperate.posSetBillObject(this.bill);
                }
                this.billSeperate.posShowBillSeperatePanel();
            }
            else if (this.bill.posDgvBillTableNumber.SelectedRows.Count == 0)
            {
                MessageBox.Show("Orders are not available now");
            }
            else
            {
                MessageBox.Show("Please select a table");
            }

        }
        //Set Component Properties
        private void posSetDgvOrderedTableNumbersProperties() {
            
            this.bill.posDgvBillTableNumber.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.bill.posDgvBillTableNumber.MultiSelect = false;
            this.bill.posDgvBillTableNumber.RowHeadersVisible = false;

            for (int i = 0; i < this.bill.posDgvBillTableNumber.Rows.Count; i++)
            {
                //if (this.bill.posDgvBillTableNumber.Rows[i].Cells[0].Value.ToString().Equals(""))
                //{
                //    this.bill.posDgvBillTableNumber.Rows.RemoveAt(i);
                //}
                this.bill.posDgvBillTableNumber.Rows[i].Height = 35;
            }               
            this.bill.posDgvBillTableNumber.DefaultCellStyle.ForeColor = Color.Blue;
            this.bill.posDgvBillTableNumber.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            this.bill.posDgvBillTableNumber.AllowUserToResizeRows = false;
            this.bill.posDgvBillTableNumber.ReadOnly = true;
            
            
        }
        private void posSetDgvOrderedTableDetailsProperties()
        {
            this.bill.posDgvBillTableDetails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.bill.posDgvBillTableDetails.MultiSelect = false;
            this.bill.posDgvBillTableDetails.RowHeadersVisible = false;
            for (int i = 0; i < this.bill.posDgvBillTableDetails.Rows.Count; i++)
            {                
                this.bill.posDgvBillTableDetails.Rows[i].Height = 35;
            }
            this.bill.posDgvBillTableDetails.DefaultCellStyle.ForeColor = Color.Blue;
            this.bill.posDgvBillTableDetails.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            this.bill.posDgvBillTableDetails.AllowUserToResizeRows = false;
            this.bill.posDgvBillTableDetails.ReadOnly = true;
            this.bill.posDgvBillTableDetails.Columns[2].Visible = false;
            this.bill.posDgvBillTableDetails.Click += this.posSetDgvOrderedTableDetailsClick;
            this.bill.posDgvBillTableDetails.Columns[0].Width = 110;
            this.bill.posDgvBillTableDetails.Columns[1].Width = 210;
            this.bill.posDgvBillTableDetails.Columns[3].Width = 60;
            this.bill.posDgvBillTableDetails.Columns[4].Width = 110;

        }
        private void posSetDgvOrderedTableItemDetailsProperties()
        {
            this.bill.posDgvBillTableItemDetails.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.bill.posDgvBillTableItemDetails.MultiSelect = false;
            this.bill.posDgvBillTableItemDetails.RowHeadersVisible = false;
            for (int i = 0; i < this.bill.posDgvBillTableItemDetails.Rows.Count; i++)
            {
                this.bill.posDgvBillTableItemDetails.Rows[i].Height = 35;
            }
            this.bill.posDgvBillTableItemDetails.DefaultCellStyle.ForeColor = Color.Blue;
            this.bill.posDgvBillTableItemDetails.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
            this.bill.posDgvBillTableItemDetails.AllowUserToResizeRows = false;
            this.bill.posDgvBillTableItemDetails.ReadOnly = true;
            this.bill.posDgvBillTableItemDetails.Columns[1].Visible = false;
            this.bill.posDgvBillTableItemDetails.Columns[4].Width = 30;
            this.bill.posDgvBillTableItemDetails.Columns[3].Width = 50;
            this.bill.posDgvBillTableItemDetails.Columns[2].Width = 90;
            this.bill.posDgvBillTableItemDetails.Columns[0].Width = 230;
            this.bill.posDgvBillTableItemDetails.Columns[1].Width = 40;
            this.bill.posDgvBillTableItemDetails.Columns[5].Width = 150;
            this.bill.posDgvBillTableItemDetails.CellClick += this.posSetDgvOrderedTableItemClick;
            //this.bill.posDgvBillTableItemDetails.RowsAdded += this.posSetDgvOrderedTableItemRowsAdded;
        }
        private void posSetGuiBillProperties()
        {
            this.bill.posBtnBillHome.Click += posSetBtnHomeClick;
            this.bill.Load += this.posSetBillFormLoad;
        }
        private void posSetBtnDiscountCalculateProperties() {
            this.bill.posBtnBillCalculateDiscount.Click += this.posSetBtnDiscountCalculateClick;
        }
        private void posSetBtnDiscountProperties() {
            this.bill.posBtnBillDiscount.Click += this.posSetBtnDiscountClick;
        }   
        private void posSetBtnBillSaveProperties() {
            this.bill.posBtnBillSave.Click += this.posSetBtnBillSaveClick;
        }
        private bool posSaveBillDetails() {
            String[] data = new String[33];
            data[0] = this.posGenerateBillNo(new String[] {posStaticLoadOnce.PosCostCenterCode[0].ToString()});
            data[1] = this.bill.posTxtBillSerialNo.Text;
            data[2] = "LKR";
            data[3] = posStaticLoadOnce.PosCostCenterCode[0].ToString();
            if (this.orderDetails[0, 2].Equals("OT")) { data[4] = this.orderDetails[0, 4]; } else { data[4] = this.orderDetails[0, 3]; }
            data[5] = this.posGetAuditDate();
            data[6] = this.orderDetails[0, 0];
            data[7] = this.orderDetails[0, 2];
            data[8] = this.orderDetails[0, 1];
            data[9] = this.orderDetails[0, 2];
            data[10] = "PR";
            data[11] = this.bill.posTxtBillRemarks.Text;
            data[12] = "1.00";
            data[13] = this.total.ToString();
            data[14] = this.bill.posTxtBillCess.Text;
            data[15] = this.bill.posTxtBillCessAmt.Text;
            data[16] = this.bill.posTxtBillSc.Text;
            data[17] = this.bill.posTxtBillScAmt.Text;
            data[18] = this.bill.posTxtBillVat.Text;
            data[19] = this.bill.posTxtBillVatAmt.Text;
            data[20] = this.bill.posTxtBillDiscount.Text;
            data[21] = this.bill.posTxtBillDiscAmt.Text;
            data[22] = this.bill.posTxtBillActDis.Text;
            Decimal[] count = new Decimal[4];
            for (int i = 0; i < this.bill.posDgvBillTableItemDetails.Rows.Count; i++)
            {   if (this.bill.posDgvBillTableItemDetails.Rows[i].Cells[4].Value.ToString().Trim().Equals("F")){ 
                count[0]+= Convert.ToDecimal(this.bill.posDgvBillTableItemDetails.Rows[i].Cells[5].Value.ToString());}
            else if (this.bill.posDgvBillTableItemDetails.Rows[i].Cells[4].Value.ToString().Trim().Equals("B"))
            {
                count[1] += Convert.ToDecimal(this.bill.posDgvBillTableItemDetails.Rows[i].Cells[5].Value.ToString());
            }
            else if (this.bill.posDgvBillTableItemDetails.Rows[i].Cells[4].Value.ToString().Trim().Equals("C"))
            {
                count[2] += Convert.ToDecimal(this.bill.posDgvBillTableItemDetails.Rows[i].Cells[5].Value.ToString());
            }
            else if (this.bill.posDgvBillTableItemDetails.Rows[i].Cells[4].Value.ToString().Trim().Equals("O"))
            {
                count[3] += Convert.ToDecimal(this.bill.posDgvBillTableItemDetails.Rows[i].Cells[5].Value.ToString());
            }
            }
            
            data[23] = Math.Round(count[0],2).ToString();
            data[24] = count[1].ToString();
            data[25] = count[2].ToString();
            data[26] = count[3].ToString();
            data[27] = "N";
            data[28] = posStaticLoadOnce.PosLoggedStewardUserId;
            data[29] = this.getDateTime();
            data[30] = posStaticLoadOnce.PosLoggedStewardUserId;
            data[31] = this.getDateTime();           
            data[32] = this.bill.posTxtBillSaleType.Text;
            if (this.posInsertBillData(data))
            {
                String[] dataGuestOrder = new String[5];
                if (this.bill.posTxtBillSaleType.Text.ToString().Trim().Equals("AI")) { dataGuestOrder[0] = "AI"; } else { dataGuestOrder[0] = "BL"; }
                dataGuestOrder[1] = data[0];
                dataGuestOrder[2] = data[8];
                dataGuestOrder[3] = posStaticLoadOnce.PosCostCenterCode[0];
                dataGuestOrder[4] = data[32];
                if (this.posUpdateGuestOrderData(dataGuestOrder)) {
                    String tableNo =this.bill.posTxtBillTableNo.Text;
                    String steward = posStaticLoadOnce.PosLoggedStewardUserId;
                    String billNo = data[0];
                    String serialNo = data[1];              
                    String roomNo = this.bill.posTxtBillRoom.Text;
                    String[,] checkingGuestDetails  =this.posGetGuestName(new String[] { roomNo, this.orderDetails[0, 2] });
                    String guestName = checkingGuestDetails[0,0];
                    String noOfPax="";
                    if (this.orderDetails[0, 2].Equals("GU")) { noOfPax = checkingGuestDetails[0,1]; }
                    String total = this.bill.posTxtBillTotal.Text;
                    String discount = this.bill.posTxtBillDiscount.Text;
                    String discountAmount = this.bill.posTxtBillDiscAmt.Text;
                    String subTotal = this.bill.posTxtBillSubTotal.Text;
                    // Get Item For Print according order no
                    int billItemCount = this.bill.posDgvBillTableItemDetails.Rows.Count;
                    String[,] billItem = new String[billItemCount,4];

                    for (int i = 0; i < billItemCount; i++) { 
                        try{
                            billItem[i, 0] = this.bill.posDgvBillTableItemDetails.Rows[i].Cells[0].Value.ToString();
                            billItem[i, 1] = this.bill.posDgvBillTableItemDetails.Rows[i].Cells[1].Value.ToString();
                            billItem[i, 2] = this.bill.posDgvBillTableItemDetails.Rows[i].Cells[2].Value.ToString();
                            billItem[i, 3] = this.bill.posDgvBillTableItemDetails.Rows[i].Cells[3].Value.ToString();
                        }catch(Exception){}                        
                    }

                    String[] printData = new String[] { tableNo, steward, billNo, serialNo, noOfPax, roomNo, guestName, total, discount, subTotal, discountAmount };
                   
                    this.printer.posPrintBillPrepare(printData,billItem, billItemCount,this,this.bill);
                    this.bill.posTxtBillOrderNo.Text = data[0]; MessageBox.Show("The order is billed"); this.bill.posTxtBillOrderNo.Text = ""; return true; } 
                else { MessageBox.Show("The order is not billed"); }                
            }
            else {
                MessageBox.Show("The order is not billed");
            }            
            return false;
        }
        private Decimal posGetDiscountvalue(Decimal total)
        {
            String billDiscount = this.bill.posTxtBillDiscount.Text;
            if (billDiscount.Equals("") || billDiscount==null)
            {
                billDiscount = "0";
                this.bill.posTxtBillDiscount.Text = "0";
            }
            Decimal discount = Convert.ToDecimal(billDiscount);
            return total - (total * discount) / 100;
        }
        private Decimal posGetActDis(String[] data)
        {
            Decimal discountValue = Convert.ToDecimal(data[3]);
            Decimal taxFactor1 = Convert.ToDecimal(data[0]);
            Decimal taxFactor2 = Convert.ToDecimal(data[1]);
            Decimal taxFactor3 = Convert.ToDecimal(data[2]);
            Decimal actDis = Math.Round(discountValue - (((taxFactor1 / 100 * discountValue)) + ((taxFactor2 / 100 * discountValue)) + ((taxFactor3 / 100 * discountValue))), 2);
            return actDis;
        }        
        public void posBillPanelShow()
        {
            this.bill.Show();
            try { this.bill.posDgvBillTableNumber.DataSource = null; }
            catch (Exception) { }
            try { this.bill.posDgvBillTableDetails.DataSource = null; }
            catch (Exception) { }
            try { this.bill.posDgvBillTableItemDetails.DataSource = null; }
            catch (Exception) { }
            //try { this.posClearText(); }
            //catch (Exception) { }           
            
            try { this.posSetDgvBillTableNumberData(); }
            catch (Exception) { }
        }
        public posGuiBill posGetBillGuiObject() {
            return this.bill;
        }
        public void posBillPanelDispose()
        {
            this.bill.Dispose();
        }
        public void posBillPanelHide() {
            this.bill.Hide();
            this.fullKeyBoard.posHideFullKeyBoardPad();
            this.numPad.posHideNumericPad();
        }
    }
}
