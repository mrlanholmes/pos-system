﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    class posClsSettings : pos
    {
        private posClsHome home;
        private posGuiSettings setting;
        private posClsSettingsUser setUser;
        private posClsSettingsUserRights userRight;
        private posClsSettingsOpenItem openItem;
        private posClsSettingsAddPrinterToItems printerItems;
        private String[] costCenterIndex;
        private String[] userCostCenterIndex;
        private String[] userIndex;
        private String[,] posGetSaleTypeData()
        {
            this.qry = "SELECT SaTy_Code, SaTy_Description FROM dbo.FOSalesType";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return this.dt;
            }
            return null;
        }
        public posClsSettings(posClsHome homeObject)
        {
            this.home = homeObject;
            this.setting = new posGuiSettings();
            this.userRight = new posClsSettingsUserRights(this.setting);
            this.setUser = new posClsSettingsUser(this.setting);
            this.openItem = new posClsSettingsOpenItem();
            this.printerItems = new posClsSettingsAddPrinterToItems(this.setting);
            this.posSetBtnHomeProperties();
        }
        //sql
        private String[,] posGetCostCenters()
        {
            this.qry = "SELECT  CCent_Name,CCent_Code FROM FOCostCenter WHERE CCent_POS = '1'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) { return this.dt; }
            return null;
        }
        private String[,] posGetUsers() {
            this.qry = "SELECT  POSUsers_UserName,POSUsers_UserID FROM POSUsers";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) { return this.dt; }
            return null;
        }
        private DataTable posGetPrinterItem() {
            this.qry = "SELECT DISTINCT FOMItems.FOIM_ItemNo AS 'ITEM NO', FOMItems.FOIM_ItemDes AS 'ITEM DESCRIPTION',"
            +"FOCostCenter.CCent_Code AS 'COST CENTER CODE', FOCostCenter.CCent_Name AS 'COST CENTER',FOCostCenterItemPrice.Printer_Name AS 'PRINTER' "
            +"FROM FOMItems INNER JOIN FOCostCenterItemPrice ON FOMItems.FOIM_ItemNo = FOCostCenterItemPrice.FOIM_ItemNo INNER JOIN FOCostCenter "
            + "ON FOCostCenterItemPrice.CCent_Code = FOCostCenter.CCent_Code WHERE FOMItems.FOIM_Active='True' order by FOMItems.FOIM_ItemNo ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) { return posStaticLoadOnce.PosDataTable; }
            return null;
        }
        private String[,] posGetUserCostCenter(String[] data)
        {
            this.qry = "select (select CCent_Name from FOCostCenter where CCent_Code = userright_ccode),userright_ccode   from [posuserright] where userright_userid='" + data[0] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return this.dt; } else { return null; }
        }
        //Properties
        private void posSetBtnHomeProperties() {
            this.setting.posBtnUserRightHome.Click += this.posSetBtnHomeClick;
            this.setting.posBtnUserSave.Click += this.posSetBtnUserSaveClick;
            this.setting.posBtnUserRightSave.Click += this.posSetBtnUserRightSaveClick;
            this.setting.posCmbUserRightUserId.SelectedIndexChanged += this.posSetCmbUserRightUserIdIndexChange;
            this.setting.posCmbUserRightCostCenter.SelectedIndexChanged += this.posSetCmbUserRightCostCenterIndexChange;
            this.setting.posTabSettingsAPTIBtnSave.Click += this.posSetBtnPrinterToItemSaveClick;
            this.setting.Load += this.posSetSettingFormLoad;
            this.setting.posCmbSettingsOpenItemSaleType.SelectedIndexChanged += this.posSetCmbSettingOpenItemSaleTypeIndexChange;
            this.setting.posBtnSettingsOpenItemSave.Click += this.posSetBtnOpenItemUpdateClick;
        }
        private void posSetDgvSettingOpenItemProperties() {
            this.setting.posDgvSettingsOpenItem.Columns[1].Width = 400;
        }
        //event
        private void posSetBtnHomeClick(object sender, EventArgs e)
        {
            this.posSettingPanelDispose();
            this.home.posShowGuiHome();
        }
        private void posSetBtnUserSaveClick(object sender, EventArgs e) {            
            if (this.setUser.posSetSettingsBtnSaveClick(this.costCenterIndex)) {
                this.setting.posCmbUserRightCostCenter.Items.Clear();
                this.posSetSettingFormLoad(sender, e); }
            
        }
        private void posSetBtnPrinterToItemSaveClick(object sender, EventArgs e)
        {
            String itemCode = this.setting.posTxtSettingsAPTIItemNo.Text;
            String printerName="";
            try { printerName = this.setting.posCmbSettingsAPTIPrinter.SelectedItem.ToString(); }
            catch (Exception) { }
            
            this.printerItems.posUpdateCostCenterPrinterToItem(new String[]{itemCode,printerName});
        }
        private void posSetSettingFormLoad(object sender, EventArgs e) {
            
                String[,] data = null;// this.posGetCostCenters();
                int rowCount = 0;// posStaticLoadOnce.PosRowCount;
                //this.costCenterIndex = new String[rowCount];
                //for (int i = 0; i < rowCount; i++)
                //{
                //    this.setting.posDgvSettingUserCcent.Rows.Add();
                //    this.setting.posDgvSettingUserCcent.Rows[i].Cells[1].Value=(data[i, 0]);
                //    this.setting.posDgvSettingUserCcent.Rows[i].Cells[2].Value = (data[i, 1]);
                //   // this.costCenterIndex[i] = data[i, 1];
                //}
                try
                {
                data = this.posGetUsers();
                rowCount = posStaticLoadOnce.PosRowCount;
                this.userIndex = new String[rowCount];
                for (int i = 0; i < rowCount; i++)
                {
                    this.setting.posCmbUserRightUserId.Items.Add(data[i, 1]);
                    //this.userIndex[i] = data[i, 1];
                }
                data = this.printerItems.posGetCmbPrinter();
                rowCount = posStaticLoadOnce.PosRowCount;
                for (int i = 0; i < rowCount; i++)
                {
                    this.setting.posCmbSettingsAPTIPrinter.Items.Add(data[i, 0]);
                }
                try { this.setting.posCmbSettingsAPTIPrinter.SelectedIndex = 0; }
                catch (Exception) { }
                // Set Dgv data
                this.setting.posTabSettingsAPTIDgvPrinterItem.DataSource = this.posGetPrinterItem();
                for (int i = 0; i < posStaticLoadOnce.PosRowCount; i++)
                {
                    if (!this.setting.posTabSettingsAPTIDgvPrinterItem.Rows[i].Cells[4].Value.Equals(""))
                    {
                        this.setting.posTabSettingsAPTIDgvPrinterItem.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                    }
                }
            }
            catch (Exception) { }
                // set open item combo box
                data = this.openItem.posGetCostCenter("1");
                rowCount = posStaticLoadOnce.PosRowCount;
                //this.userIndex = new String[rowCount];
                for (int i = 0; i < rowCount; i++)
                {
                    this.setting.posCmbSettingsOpenItemCostCenter.Items.Add(data[i, 1] + " " + data[i, 0]);
                    //this.userIndex[i] = data[i, 1];
                }
                // set open item sale type

                data = this.posGetSaleTypeData();
                rowCount = posStaticLoadOnce.PosRowCount;
                //this.userIndex = new String[rowCount];
                for (int i = 0; i < rowCount; i++)
                {
                    this.setting.posCmbSettingsOpenItemSaleType.Items.Add(data[i, 0]);
                    //this.userIndex[i] = data[i, 1];
                }
                // data = this.posGetSaleTypeData();
                // rowCount = posStaticLoadOnce.PosRowCount;
                // for (int i = 0; i < rowCount; i++)
                // {
                //  this.setting.posCmbSettingsAPTISType.Items.Add(data[i, 0]);
                //}
                this.setUser.posSetDgvUserData();
            
        }
        private void posSetBtnUserRightSaveClick(object sender, EventArgs e) {
            if(this.setting.posCmbUserRightUserId.SelectedIndex ==-1){
                MessageBox.Show("Please select an user...");
            }
            else if (this.setting.posCmbUserRightCostCenter.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a Cost center...");
            }
            else {
                String userCode = this.setting.posCmbUserRightUserId.SelectedItem.ToString();
                String costCenter = this.posGetSelectedCostCenterCode( new String[]{this.setting.posCmbUserRightCostCenter.SelectedItem.ToString()});
                if (this.userRight.posGetUserRightTableRowCount(new String[] { userCode, costCenter }) == 0 )
                {
                    String[] data = new String[12];
                    data[0] = userCode;
                    data[1] = costCenter;
                    data[2] = "0";
                    data[3] = "0";
                    data[4] = "0";
                    data[5] = "0";
                    data[6] = "0";
                    data[7] = "0";
                    data[8] = "0";
                    data[9] = "0";
                    data[10] = "0";
                    data[11] = "0";
                    this.userRight.posInsertUserRight(data);
                    this.userRight.posSaveUserRights(userCode, costCenter);
                }
                else { this.userRight.posSaveUserRights(userCode, costCenter); }
               
            }            
        }
        private void posSetBtnOpenItemUpdateClick(object sender, EventArgs e)
        {
            String itemcode = "";
            String itemCostCenter = "";
            String priceValue = "";
            String saleType = "";
            int counter = 0;
            itemCostCenter = this.setting.posCmbSettingsOpenItemCostCenter.SelectedItem.ToString();
            itemCostCenter = this.posGetSelectedCostCenterCode(new String[]{itemCostCenter});
            saleType = this.setting.posCmbSettingsOpenItemSaleType.SelectedItem.ToString();
            for(int i = 0;i<this.setting.posDgvSettingsOpenItem.Rows.Count;i++){
                itemcode = this.setting.posDgvSettingsOpenItem.Rows[i].Cells[0].Value.ToString();               
                priceValue = this.setting.posDgvSettingsOpenItem.Rows[i].Cells[2].Value.ToString();
                if (this.openItem.posUpdateSalesType(new String[] { priceValue, priceValue, itemcode, itemCostCenter,saleType }))
                {
                    counter++;
                    //MessageBox.Show("Open item Price Value is updated...");
                }
               
            }
            if (counter > 0)
            {
                MessageBox.Show("Open item Price Value is updated...");
            }
            else {
                MessageBox.Show("Open item Price Value is not updated...");
            }
            
        }
        private void posSetCmbUserRightUserIdIndexChange(object sender, EventArgs e) {
            try {
                this.setting.posCmbUserRightCostCenter.Items.Clear();
                this.setting.posTxtUserRightEmployeeName.Text = this.setting.posCmbUserRightUserId.SelectedItem.ToString();
            }catch(Exception){}
            try
            {
                
                this.setting.posTxtUserRightCostCenter.Text = "";
                //String[,] data = this.posGetUserCostCenter(new String[] { this.userIndex[this.setting.posCmbUserRightUserId.SelectedIndex] });
                String[,] data = this.posGetCostCenters();
                int rowCount = posStaticLoadOnce.PosRowCount;
                this.userCostCenterIndex = new String[rowCount];
                for (int i = 0; i < rowCount; i++)
                {
                    this.setting.posCmbUserRightCostCenter.Items.Add(data[i, 1] + " " +data[i, 0]);
                    //this.userCostCenterIndex[i] = data[i, 1];
                }
                
            } catch (Exception) { }

            this.posClearCheckBoxValue();
            
        }
        private void posSetCmbSettingOpenItemSaleTypeIndexChange(object sender, EventArgs e)
        {
            try { this.setting.posDgvSettingsOpenItem.Rows.Clear(); }
            catch (Exception) { }
            try { this.setting.posDgvSettingsOpenItem.Columns.Clear(); }
            catch (Exception) { }
            String costCenter = this.setting.posCmbSettingsOpenItemCostCenter.SelectedItem.ToString();
            costCenter = this.posGetSelectedCostCenterCode(new String []{costCenter});
            String saleType = this.setting.posCmbSettingsOpenItemSaleType.SelectedItem.ToString();
            String[,] openItem = this.openItem.posGetOpenItem(new String[]{costCenter,saleType});
            int rowCount = posStaticLoadOnce.PosRowCount;
            this.setting.posDgvSettingsOpenItem.Columns.Add("posSettingOpenItemNo","Item No");
            this.setting.posDgvSettingsOpenItem.Columns.Add("posSettingOpenItemDes", "Description");
            this.setting.posDgvSettingsOpenItem.Columns.Add("posSettingOpenItemPriceValue", "Price");
            for (int i = 0; i < rowCount; i++) {
                this.setting.posDgvSettingsOpenItem.Rows.Add();
                this.setting.posDgvSettingsOpenItem.Rows[i].Cells[0].Value = openItem[i, 0];
                this.setting.posDgvSettingsOpenItem.Rows[i].Cells[1].Value = openItem[i, 1];
                this.setting.posDgvSettingsOpenItem.Rows[i].Cells[2].Value = openItem[i, 2];
            }
            this.posSetDgvSettingOpenItemProperties();
        }
        private void posSetCmbUserRightCostCenterIndexChange(object sender, EventArgs e)
        {
            this.posClearCheckBoxValue();
            try
            {
                this.setting.posTxtUserRightCostCenter.Text = this.setting.posCmbUserRightCostCenter.SelectedItem.ToString();
            }
            catch (Exception) { }
            String userId = this.userIndex[this.setting.posCmbUserRightUserId.SelectedIndex];
            String costCenter = this.posGetSelectedCostCenterCode(new String[]{this.setting.posCmbUserRightCostCenter.SelectedItem.ToString()});
            String[,] data=this.userRight.posGetUserRights(new String[] { userId,costCenter});
            try { this.posSetChkBoxValue(data); }
            catch (Exception) { }
           
        }
        private void posClearCheckBoxValue() {
            String[,] data = new String[1, 10];
            for (int i = 0; i < 10; i++) { data[0, i] = "0"; }
            this.posSetChkBoxValue(data);
        }
        private void posSetChkBoxValue(String[,] data) {
            this.setting.posChkUserRightKotBot.Checked = this.posGetUserStatus(data[0, 0]);
            this.setting.posChkUserRightAI.Checked = this.posGetUserStatus(data[0, 1]);
            this.setting.posChkUserRightBill.Checked = this.posGetUserStatus(data[0, 2]);
            this.setting.posChkUserRightEntitle.Checked = this.posGetUserStatus(data[0, 3]);
            this.setting.posChkUserRightSettlement.Checked = this.posGetUserStatus(data[0, 4]);
            this.setting.posChkUserRightReports.Checked = this.posGetUserStatus(data[0, 5]);
            this.setting.posChkUserRightSettings.Checked = this.posGetUserStatus(data[0, 6]);
            this.setting.posChkUserRightAddPrinter.Checked  = this.posGetUserStatus(data[0, 7]);
            this.setting.posChkUserRightPrinterToItems.Checked = this.posGetUserStatus(data[0, 8]);
            this.setting.posChkUserRightAddItemRemarks.Checked = this.posGetUserStatus(data[0, 9]);
        }
        //function 
        public posGuiSettings posGetGuiSettingObject() {
            return this.setting;
        }
        private String posGetSelectedCostCenterCode(String[] data) {
            return data[0].Substring(0, 4);
        }
        private bool posGetUserStatus(String data)
        {
            if(data.Equals("1")){return true;}else{return false;}
            
        }
        public void posSettingPanelShow()
        {
            this.setting.Show();
        }
        public void posSettingPanelDispose()
        {
            this.setting.Dispose();
        }
        public void posSettingPanelHide()
        {
            this.setting.Hide();
        } 
    }
}
