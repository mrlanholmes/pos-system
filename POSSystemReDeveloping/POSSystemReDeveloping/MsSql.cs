﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
namespace POSSystemReDeveloping
{
    class MsSql : ADbConnection
    {
        // variables
        private SqlConnection con;
        private SqlCommand cmd;
        private SqlDataReader dr;
        private fileRead fr;
        // function
        // Assign database path to variable
        public MsSql() {
            
            if (this.createConnection())
            {
              this.ConState= this.openConnection();
            }
        }
        ~MsSql()
        {
            this.closeConnection();
        }
        private String getConnectionString(){
            fr = new fileRead();
            fr.FilePath = "pos.ipply";
            this.serverName = fr.readLineAccordingLineNo(0);
            this.dbName = fr.readLineAccordingLineNo(1);
            this.dbUserName = fr.readLineAccordingLineNo(2);
            this.dbPassword = fr.readLineAccordingLineNo(3);
            String conStr = "Data Source="+this.serverName+";Initial Catalog="+this.dbName+";User ID="+this.dbUserName+";password="+this.dbPassword+";";
            return conStr;

        }
        public SqlConnection getConnection()
        {
            return this.con;
        }
        protected override Boolean createConnection()
        {            
            try
            {
                this.con = new SqlConnection(this.getConnectionString());
                return true;
            }
            catch (Exception)
            {
               // MessageBox.Show("Connection Error.Error Code:  MsSql#1");
            }
            return false;
        }
        // Open a new connection
        protected override Boolean openConnection()
        {
           
            try {

                if (this.con.State == ConnectionState.Open)
                {
                    return true;
                }
                else {
                    this.con.Open();
                    return true;
                }
            }catch(Exception){
                
                MessageBox.Show("Connection Error.Error Code: MsSql#2" );
               
            }
            return false;
        }
        // Close a connection
        protected override Boolean closeConnection()
        {
            try
            {

                if (this.con.State == ConnectionState.Closed)
                {
                    return true;
                }
                else {
                    this.con.Close();
                    return true;
                }
            }
            catch (Exception)
            {
                //MessageBox.Show("Connection Error.Error Code: Sqlite#3");
            }
            return false;
        }
        // Execute query passing query
        public override String[,] dbExecute(String exeQuery,String ExecutionType)
        {           
            this.qry = exeQuery;
            if (this.ConState) { 
                cmd = this.con.CreateCommand();
                cmd.CommandText = exeQuery;
                if (ExecutionType.ToUpper().Equals("ENQ") )
                {
                    String[,] data = new String[1,1];
                    SqlTransaction sqlTrn;
                    sqlTrn = this.con.BeginTransaction("Trn_Tmp");
                    cmd.Transaction = sqlTrn;
                    try {  
                        data[0, 0] = Convert.ToString(cmd.ExecuteNonQuery());
                        sqlTrn.Commit();
                        return data;
                    }
                    catch (Exception) { sqlTrn.Rollback(); return null; }                    
                    
                }
                else if (ExecutionType.ToUpper().Equals("EQ"))
                {
                    try
                    {
                        //Execute a sql query
                        this.dr = cmd.ExecuteReader();
                        // Set Null to Count again
                        this.dtColumnCount = 0;
                        this.dtRowCount = 0;
                        // Get no of column in dataset
                        this.dtColumnCount = dr.FieldCount;                        
                        //Get no of rows in dataset
                        this.dt = new DataTable(); 
                        this.dt.Load(dr);
                        this.dtRowCount = this.dt.Rows.Count;
                        // Variable declaration
                        String[,] data = new String[this.dtRowCount,this.dtColumnCount];
                        // Set data to array                        
                        for (int i = 0;i<this.dtRowCount ; i++) {
                            for(int j=0;j<this.dtColumnCount;j++){
                                data[i, j] = this.dt.Rows[i][j].ToString();
                            }                            
                        }
                        return data;

                    }
                    catch (Exception)
                    {}
                    
                }
            }
            return null;
            
        }
        public override int getRsRowCount() {
            return this.dtRowCount;
        }
        public override int getRsColumnCount() {
            return this.dtColumnCount;
        }
        public override DataTable getDataTable()
        {
            return this.dt;
        }
    }
}
