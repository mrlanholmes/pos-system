﻿namespace POSSystemReDeveloping
{
    partial class posGuiFullKeyBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(posGuiFullKeyBoard));
            this.posBtnFullKeyBoardClose = new System.Windows.Forms.Button();
            this.posChkFullKeyBoardRightControl = new System.Windows.Forms.CheckBox();
            this.posChkFullKeyBoardRightAlt = new System.Windows.Forms.CheckBox();
            this.posChkFullKeyBoardLeftAlt = new System.Windows.Forms.CheckBox();
            this.posChkFullKeyBoardLeftControl = new System.Windows.Forms.CheckBox();
            this.posChkFullKeyBoardRightShift = new System.Windows.Forms.CheckBox();
            this.posChkFullKeyBoardLeftShift = new System.Windows.Forms.CheckBox();
            this.posBtnFullKeyBoardSpaceBar = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardEqual = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardBackSpace = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardEight = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardNine = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardZero = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardOne = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardTwo = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardThree = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardFour = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardFive = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardSix = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardSeven = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardHyphen = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardCurrentSign = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardZ = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardX = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardC = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardV = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardB = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardN = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardM = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardComma = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardDot = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardFowardSlash = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardEnter = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardQuotation = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardColon = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardK = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardL = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardJ = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardD = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardF = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardG = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardH = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardBackSlash = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardTab = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardA = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardS = new System.Windows.Forms.Button();
            this.posChkFullKeyBoardCapsLock = new System.Windows.Forms.CheckBox();
            this.posBtnFullKeyBoardU = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardI = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardO = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardP = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardLeftSquareBrackets = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardRightSquareBracket = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardW = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardE = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardR = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardT = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardY = new System.Windows.Forms.Button();
            this.posBtnFullKeyBoardQ = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // posBtnFullKeyBoardClose
            // 
            this.posBtnFullKeyBoardClose.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardClose.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardClose.Image = ((System.Drawing.Image)(resources.GetObject("posBtnFullKeyBoardClose.Image")));
            this.posBtnFullKeyBoardClose.Location = new System.Drawing.Point(850, 2);
            this.posBtnFullKeyBoardClose.Name = "posBtnFullKeyBoardClose";
            this.posBtnFullKeyBoardClose.Size = new System.Drawing.Size(77, 65);
            this.posBtnFullKeyBoardClose.TabIndex = 125;
            this.posBtnFullKeyBoardClose.UseVisualStyleBackColor = false;
            // 
            // posChkFullKeyBoardRightControl
            // 
            this.posChkFullKeyBoardRightControl.Appearance = System.Windows.Forms.Appearance.Button;
            this.posChkFullKeyBoardRightControl.BackColor = System.Drawing.Color.Black;
            this.posChkFullKeyBoardRightControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posChkFullKeyBoardRightControl.ForeColor = System.Drawing.Color.White;
            this.posChkFullKeyBoardRightControl.Location = new System.Drawing.Point(837, 264);
            this.posChkFullKeyBoardRightControl.Name = "posChkFullKeyBoardRightControl";
            this.posChkFullKeyBoardRightControl.Size = new System.Drawing.Size(92, 65);
            this.posChkFullKeyBoardRightControl.TabIndex = 124;
            this.posChkFullKeyBoardRightControl.Text = "Ctrl";
            this.posChkFullKeyBoardRightControl.UseVisualStyleBackColor = false;
            // 
            // posChkFullKeyBoardRightAlt
            // 
            this.posChkFullKeyBoardRightAlt.Appearance = System.Windows.Forms.Appearance.Button;
            this.posChkFullKeyBoardRightAlt.BackColor = System.Drawing.Color.Black;
            this.posChkFullKeyBoardRightAlt.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posChkFullKeyBoardRightAlt.ForeColor = System.Drawing.Color.White;
            this.posChkFullKeyBoardRightAlt.Location = new System.Drawing.Point(730, 263);
            this.posChkFullKeyBoardRightAlt.Name = "posChkFullKeyBoardRightAlt";
            this.posChkFullKeyBoardRightAlt.Size = new System.Drawing.Size(107, 65);
            this.posChkFullKeyBoardRightAlt.TabIndex = 123;
            this.posChkFullKeyBoardRightAlt.Text = "Alt";
            this.posChkFullKeyBoardRightAlt.UseVisualStyleBackColor = false;
            // 
            // posChkFullKeyBoardLeftAlt
            // 
            this.posChkFullKeyBoardLeftAlt.Appearance = System.Windows.Forms.Appearance.Button;
            this.posChkFullKeyBoardLeftAlt.BackColor = System.Drawing.Color.Black;
            this.posChkFullKeyBoardLeftAlt.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posChkFullKeyBoardLeftAlt.ForeColor = System.Drawing.Color.White;
            this.posChkFullKeyBoardLeftAlt.Location = new System.Drawing.Point(94, 264);
            this.posChkFullKeyBoardLeftAlt.Name = "posChkFullKeyBoardLeftAlt";
            this.posChkFullKeyBoardLeftAlt.Size = new System.Drawing.Size(109, 65);
            this.posChkFullKeyBoardLeftAlt.TabIndex = 122;
            this.posChkFullKeyBoardLeftAlt.Text = "Alt";
            this.posChkFullKeyBoardLeftAlt.UseVisualStyleBackColor = false;
            // 
            // posChkFullKeyBoardLeftControl
            // 
            this.posChkFullKeyBoardLeftControl.Appearance = System.Windows.Forms.Appearance.Button;
            this.posChkFullKeyBoardLeftControl.BackColor = System.Drawing.Color.Black;
            this.posChkFullKeyBoardLeftControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posChkFullKeyBoardLeftControl.ForeColor = System.Drawing.Color.White;
            this.posChkFullKeyBoardLeftControl.Location = new System.Drawing.Point(2, 264);
            this.posChkFullKeyBoardLeftControl.Name = "posChkFullKeyBoardLeftControl";
            this.posChkFullKeyBoardLeftControl.Size = new System.Drawing.Size(92, 65);
            this.posChkFullKeyBoardLeftControl.TabIndex = 121;
            this.posChkFullKeyBoardLeftControl.Text = "Ctrl";
            this.posChkFullKeyBoardLeftControl.UseVisualStyleBackColor = false;
            // 
            // posChkFullKeyBoardRightShift
            // 
            this.posChkFullKeyBoardRightShift.Appearance = System.Windows.Forms.Appearance.Button;
            this.posChkFullKeyBoardRightShift.BackColor = System.Drawing.Color.Black;
            this.posChkFullKeyBoardRightShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posChkFullKeyBoardRightShift.ForeColor = System.Drawing.Color.White;
            this.posChkFullKeyBoardRightShift.Location = new System.Drawing.Point(794, 198);
            this.posChkFullKeyBoardRightShift.Name = "posChkFullKeyBoardRightShift";
            this.posChkFullKeyBoardRightShift.Size = new System.Drawing.Size(135, 65);
            this.posChkFullKeyBoardRightShift.TabIndex = 120;
            this.posChkFullKeyBoardRightShift.Text = "Shift";
            this.posChkFullKeyBoardRightShift.UseVisualStyleBackColor = false;
            // 
            // posChkFullKeyBoardLeftShift
            // 
            this.posChkFullKeyBoardLeftShift.Appearance = System.Windows.Forms.Appearance.Button;
            this.posChkFullKeyBoardLeftShift.BackColor = System.Drawing.Color.Black;
            this.posChkFullKeyBoardLeftShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posChkFullKeyBoardLeftShift.ForeColor = System.Drawing.Color.White;
            this.posChkFullKeyBoardLeftShift.Location = new System.Drawing.Point(2, 199);
            this.posChkFullKeyBoardLeftShift.Name = "posChkFullKeyBoardLeftShift";
            this.posChkFullKeyBoardLeftShift.Size = new System.Drawing.Size(135, 65);
            this.posChkFullKeyBoardLeftShift.TabIndex = 119;
            this.posChkFullKeyBoardLeftShift.Text = "Shift";
            this.posChkFullKeyBoardLeftShift.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardSpaceBar
            // 
            this.posBtnFullKeyBoardSpaceBar.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardSpaceBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardSpaceBar.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardSpaceBar.Location = new System.Drawing.Point(203, 264);
            this.posBtnFullKeyBoardSpaceBar.Name = "posBtnFullKeyBoardSpaceBar";
            this.posBtnFullKeyBoardSpaceBar.Size = new System.Drawing.Size(395, 65);
            this.posBtnFullKeyBoardSpaceBar.TabIndex = 118;
            this.posBtnFullKeyBoardSpaceBar.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardEqual
            // 
            this.posBtnFullKeyBoardEqual.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardEqual.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardEqual.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardEqual.Location = new System.Drawing.Point(781, 2);
            this.posBtnFullKeyBoardEqual.Name = "posBtnFullKeyBoardEqual";
            this.posBtnFullKeyBoardEqual.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardEqual.TabIndex = 117;
            this.posBtnFullKeyBoardEqual.Text = " +\r\n =";
            this.posBtnFullKeyBoardEqual.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardBackSpace
            // 
            this.posBtnFullKeyBoardBackSpace.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardBackSpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardBackSpace.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardBackSpace.Location = new System.Drawing.Point(599, 264);
            this.posBtnFullKeyBoardBackSpace.Name = "posBtnFullKeyBoardBackSpace";
            this.posBtnFullKeyBoardBackSpace.Size = new System.Drawing.Size(130, 65);
            this.posBtnFullKeyBoardBackSpace.TabIndex = 116;
            this.posBtnFullKeyBoardBackSpace.Text = "Back\nSpace";
            this.posBtnFullKeyBoardBackSpace.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardEight
            // 
            this.posBtnFullKeyBoardEight.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardEight.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardEight.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardEight.Location = new System.Drawing.Point(520, 2);
            this.posBtnFullKeyBoardEight.Name = "posBtnFullKeyBoardEight";
            this.posBtnFullKeyBoardEight.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardEight.TabIndex = 115;
            this.posBtnFullKeyBoardEight.Text = " *\r\n 8";
            this.posBtnFullKeyBoardEight.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardNine
            // 
            this.posBtnFullKeyBoardNine.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardNine.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardNine.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardNine.Location = new System.Drawing.Point(585, 2);
            this.posBtnFullKeyBoardNine.Name = "posBtnFullKeyBoardNine";
            this.posBtnFullKeyBoardNine.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardNine.TabIndex = 114;
            this.posBtnFullKeyBoardNine.Text = " (\r\n 9";
            this.posBtnFullKeyBoardNine.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardZero
            // 
            this.posBtnFullKeyBoardZero.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardZero.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardZero.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardZero.Location = new System.Drawing.Point(651, 2);
            this.posBtnFullKeyBoardZero.Name = "posBtnFullKeyBoardZero";
            this.posBtnFullKeyBoardZero.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardZero.TabIndex = 113;
            this.posBtnFullKeyBoardZero.Text = " )\r\n0";
            this.posBtnFullKeyBoardZero.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardOne
            // 
            this.posBtnFullKeyBoardOne.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardOne.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardOne.Location = new System.Drawing.Point(67, 2);
            this.posBtnFullKeyBoardOne.Name = "posBtnFullKeyBoardOne";
            this.posBtnFullKeyBoardOne.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardOne.TabIndex = 112;
            this.posBtnFullKeyBoardOne.Text = " !\r\n 1";
            this.posBtnFullKeyBoardOne.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardTwo
            // 
            this.posBtnFullKeyBoardTwo.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardTwo.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardTwo.Location = new System.Drawing.Point(132, 2);
            this.posBtnFullKeyBoardTwo.Name = "posBtnFullKeyBoardTwo";
            this.posBtnFullKeyBoardTwo.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardTwo.TabIndex = 111;
            this.posBtnFullKeyBoardTwo.Text = " @\r\n 2";
            this.posBtnFullKeyBoardTwo.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardThree
            // 
            this.posBtnFullKeyBoardThree.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardThree.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardThree.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardThree.Location = new System.Drawing.Point(197, 2);
            this.posBtnFullKeyBoardThree.Name = "posBtnFullKeyBoardThree";
            this.posBtnFullKeyBoardThree.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardThree.TabIndex = 110;
            this.posBtnFullKeyBoardThree.Text = " #\r\n 3";
            this.posBtnFullKeyBoardThree.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardFour
            // 
            this.posBtnFullKeyBoardFour.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardFour.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardFour.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardFour.Location = new System.Drawing.Point(262, 2);
            this.posBtnFullKeyBoardFour.Name = "posBtnFullKeyBoardFour";
            this.posBtnFullKeyBoardFour.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardFour.TabIndex = 109;
            this.posBtnFullKeyBoardFour.Text = " $\r\n 4";
            this.posBtnFullKeyBoardFour.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardFive
            // 
            this.posBtnFullKeyBoardFive.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardFive.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardFive.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardFive.Location = new System.Drawing.Point(326, 2);
            this.posBtnFullKeyBoardFive.Name = "posBtnFullKeyBoardFive";
            this.posBtnFullKeyBoardFive.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardFive.TabIndex = 108;
            this.posBtnFullKeyBoardFive.Text = " %\r\n 5";
            this.posBtnFullKeyBoardFive.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardSix
            // 
            this.posBtnFullKeyBoardSix.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardSix.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardSix.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardSix.Location = new System.Drawing.Point(390, 2);
            this.posBtnFullKeyBoardSix.Name = "posBtnFullKeyBoardSix";
            this.posBtnFullKeyBoardSix.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardSix.TabIndex = 107;
            this.posBtnFullKeyBoardSix.Text = " ^\r\n 6";
            this.posBtnFullKeyBoardSix.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardSeven
            // 
            this.posBtnFullKeyBoardSeven.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardSeven.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardSeven.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardSeven.Location = new System.Drawing.Point(455, 2);
            this.posBtnFullKeyBoardSeven.Name = "posBtnFullKeyBoardSeven";
            this.posBtnFullKeyBoardSeven.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardSeven.TabIndex = 106;
            this.posBtnFullKeyBoardSeven.Text = " &&\r\n 7";
            this.posBtnFullKeyBoardSeven.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardHyphen
            // 
            this.posBtnFullKeyBoardHyphen.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardHyphen.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardHyphen.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardHyphen.Location = new System.Drawing.Point(716, 2);
            this.posBtnFullKeyBoardHyphen.Name = "posBtnFullKeyBoardHyphen";
            this.posBtnFullKeyBoardHyphen.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardHyphen.TabIndex = 105;
            this.posBtnFullKeyBoardHyphen.Text = " _\r\n -";
            this.posBtnFullKeyBoardHyphen.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardCurrentSign
            // 
            this.posBtnFullKeyBoardCurrentSign.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardCurrentSign.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardCurrentSign.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardCurrentSign.Location = new System.Drawing.Point(2, 2);
            this.posBtnFullKeyBoardCurrentSign.Name = "posBtnFullKeyBoardCurrentSign";
            this.posBtnFullKeyBoardCurrentSign.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardCurrentSign.TabIndex = 104;
            this.posBtnFullKeyBoardCurrentSign.Text = " ~\r\n `";
            this.posBtnFullKeyBoardCurrentSign.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardZ
            // 
            this.posBtnFullKeyBoardZ.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardZ.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardZ.Location = new System.Drawing.Point(138, 198);
            this.posBtnFullKeyBoardZ.Name = "posBtnFullKeyBoardZ";
            this.posBtnFullKeyBoardZ.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardZ.TabIndex = 103;
            this.posBtnFullKeyBoardZ.Text = " Z";
            this.posBtnFullKeyBoardZ.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardX
            // 
            this.posBtnFullKeyBoardX.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardX.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardX.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardX.Location = new System.Drawing.Point(203, 198);
            this.posBtnFullKeyBoardX.Name = "posBtnFullKeyBoardX";
            this.posBtnFullKeyBoardX.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardX.TabIndex = 102;
            this.posBtnFullKeyBoardX.Text = " X";
            this.posBtnFullKeyBoardX.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardC
            // 
            this.posBtnFullKeyBoardC.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardC.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardC.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardC.Location = new System.Drawing.Point(269, 198);
            this.posBtnFullKeyBoardC.Name = "posBtnFullKeyBoardC";
            this.posBtnFullKeyBoardC.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardC.TabIndex = 101;
            this.posBtnFullKeyBoardC.Text = " C";
            this.posBtnFullKeyBoardC.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardV
            // 
            this.posBtnFullKeyBoardV.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardV.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardV.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardV.Location = new System.Drawing.Point(335, 198);
            this.posBtnFullKeyBoardV.Name = "posBtnFullKeyBoardV";
            this.posBtnFullKeyBoardV.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardV.TabIndex = 100;
            this.posBtnFullKeyBoardV.Text = " V";
            this.posBtnFullKeyBoardV.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardB
            // 
            this.posBtnFullKeyBoardB.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardB.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardB.Location = new System.Drawing.Point(401, 198);
            this.posBtnFullKeyBoardB.Name = "posBtnFullKeyBoardB";
            this.posBtnFullKeyBoardB.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardB.TabIndex = 99;
            this.posBtnFullKeyBoardB.Text = "B";
            this.posBtnFullKeyBoardB.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardN
            // 
            this.posBtnFullKeyBoardN.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardN.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardN.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardN.Location = new System.Drawing.Point(467, 198);
            this.posBtnFullKeyBoardN.Name = "posBtnFullKeyBoardN";
            this.posBtnFullKeyBoardN.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardN.TabIndex = 98;
            this.posBtnFullKeyBoardN.Text = " N";
            this.posBtnFullKeyBoardN.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardM
            // 
            this.posBtnFullKeyBoardM.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardM.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardM.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardM.Location = new System.Drawing.Point(533, 198);
            this.posBtnFullKeyBoardM.Name = "posBtnFullKeyBoardM";
            this.posBtnFullKeyBoardM.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardM.TabIndex = 97;
            this.posBtnFullKeyBoardM.Text = " M";
            this.posBtnFullKeyBoardM.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardComma
            // 
            this.posBtnFullKeyBoardComma.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardComma.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardComma.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardComma.Location = new System.Drawing.Point(599, 198);
            this.posBtnFullKeyBoardComma.Name = "posBtnFullKeyBoardComma";
            this.posBtnFullKeyBoardComma.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardComma.TabIndex = 96;
            this.posBtnFullKeyBoardComma.Text = " <\r\n ,";
            this.posBtnFullKeyBoardComma.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardDot
            // 
            this.posBtnFullKeyBoardDot.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardDot.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardDot.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardDot.Location = new System.Drawing.Point(664, 198);
            this.posBtnFullKeyBoardDot.Name = "posBtnFullKeyBoardDot";
            this.posBtnFullKeyBoardDot.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardDot.TabIndex = 95;
            this.posBtnFullKeyBoardDot.Text = " >\r\n.";
            this.posBtnFullKeyBoardDot.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardFowardSlash
            // 
            this.posBtnFullKeyBoardFowardSlash.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardFowardSlash.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardFowardSlash.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardFowardSlash.Location = new System.Drawing.Point(730, 198);
            this.posBtnFullKeyBoardFowardSlash.Name = "posBtnFullKeyBoardFowardSlash";
            this.posBtnFullKeyBoardFowardSlash.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardFowardSlash.TabIndex = 94;
            this.posBtnFullKeyBoardFowardSlash.Text = " ?\r\n /";
            this.posBtnFullKeyBoardFowardSlash.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardEnter
            // 
            this.posBtnFullKeyBoardEnter.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardEnter.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardEnter.Location = new System.Drawing.Point(802, 133);
            this.posBtnFullKeyBoardEnter.Name = "posBtnFullKeyBoardEnter";
            this.posBtnFullKeyBoardEnter.Size = new System.Drawing.Size(127, 65);
            this.posBtnFullKeyBoardEnter.TabIndex = 93;
            this.posBtnFullKeyBoardEnter.Text = " Enter";
            this.posBtnFullKeyBoardEnter.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardQuotation
            // 
            this.posBtnFullKeyBoardQuotation.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardQuotation.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardQuotation.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardQuotation.Location = new System.Drawing.Point(737, 133);
            this.posBtnFullKeyBoardQuotation.Name = "posBtnFullKeyBoardQuotation";
            this.posBtnFullKeyBoardQuotation.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardQuotation.TabIndex = 92;
            this.posBtnFullKeyBoardQuotation.Text = " \"\r\n \'";
            this.posBtnFullKeyBoardQuotation.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardColon
            // 
            this.posBtnFullKeyBoardColon.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardColon.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardColon.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardColon.Location = new System.Drawing.Point(671, 133);
            this.posBtnFullKeyBoardColon.Name = "posBtnFullKeyBoardColon";
            this.posBtnFullKeyBoardColon.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardColon.TabIndex = 91;
            this.posBtnFullKeyBoardColon.Text = " :\r\n ;";
            this.posBtnFullKeyBoardColon.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardK
            // 
            this.posBtnFullKeyBoardK.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardK.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardK.Location = new System.Drawing.Point(541, 133);
            this.posBtnFullKeyBoardK.Name = "posBtnFullKeyBoardK";
            this.posBtnFullKeyBoardK.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardK.TabIndex = 90;
            this.posBtnFullKeyBoardK.Text = " K";
            this.posBtnFullKeyBoardK.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardL
            // 
            this.posBtnFullKeyBoardL.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardL.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardL.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardL.Location = new System.Drawing.Point(606, 133);
            this.posBtnFullKeyBoardL.Name = "posBtnFullKeyBoardL";
            this.posBtnFullKeyBoardL.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardL.TabIndex = 89;
            this.posBtnFullKeyBoardL.Text = " L";
            this.posBtnFullKeyBoardL.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardJ
            // 
            this.posBtnFullKeyBoardJ.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardJ.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardJ.Location = new System.Drawing.Point(476, 133);
            this.posBtnFullKeyBoardJ.Name = "posBtnFullKeyBoardJ";
            this.posBtnFullKeyBoardJ.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardJ.TabIndex = 88;
            this.posBtnFullKeyBoardJ.Text = " J";
            this.posBtnFullKeyBoardJ.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardD
            // 
            this.posBtnFullKeyBoardD.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardD.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardD.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardD.Location = new System.Drawing.Point(216, 133);
            this.posBtnFullKeyBoardD.Name = "posBtnFullKeyBoardD";
            this.posBtnFullKeyBoardD.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardD.TabIndex = 87;
            this.posBtnFullKeyBoardD.Text = " D";
            this.posBtnFullKeyBoardD.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardF
            // 
            this.posBtnFullKeyBoardF.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardF.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardF.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardF.Location = new System.Drawing.Point(281, 133);
            this.posBtnFullKeyBoardF.Name = "posBtnFullKeyBoardF";
            this.posBtnFullKeyBoardF.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardF.TabIndex = 86;
            this.posBtnFullKeyBoardF.Text = " F";
            this.posBtnFullKeyBoardF.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardG
            // 
            this.posBtnFullKeyBoardG.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardG.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardG.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardG.Location = new System.Drawing.Point(346, 133);
            this.posBtnFullKeyBoardG.Name = "posBtnFullKeyBoardG";
            this.posBtnFullKeyBoardG.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardG.TabIndex = 85;
            this.posBtnFullKeyBoardG.Text = " G";
            this.posBtnFullKeyBoardG.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardH
            // 
            this.posBtnFullKeyBoardH.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardH.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardH.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardH.Location = new System.Drawing.Point(411, 133);
            this.posBtnFullKeyBoardH.Name = "posBtnFullKeyBoardH";
            this.posBtnFullKeyBoardH.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardH.TabIndex = 84;
            this.posBtnFullKeyBoardH.Text = " H";
            this.posBtnFullKeyBoardH.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardBackSlash
            // 
            this.posBtnFullKeyBoardBackSlash.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardBackSlash.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardBackSlash.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardBackSlash.Location = new System.Drawing.Point(864, 68);
            this.posBtnFullKeyBoardBackSlash.Name = "posBtnFullKeyBoardBackSlash";
            this.posBtnFullKeyBoardBackSlash.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardBackSlash.TabIndex = 83;
            this.posBtnFullKeyBoardBackSlash.Text = "|\r\n\\";
            this.posBtnFullKeyBoardBackSlash.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardTab
            // 
            this.posBtnFullKeyBoardTab.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardTab.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardTab.Location = new System.Drawing.Point(2, 68);
            this.posBtnFullKeyBoardTab.Name = "posBtnFullKeyBoardTab";
            this.posBtnFullKeyBoardTab.Size = new System.Drawing.Size(84, 65);
            this.posBtnFullKeyBoardTab.TabIndex = 82;
            this.posBtnFullKeyBoardTab.Text = "Tab";
            this.posBtnFullKeyBoardTab.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardA
            // 
            this.posBtnFullKeyBoardA.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardA.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardA.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardA.Location = new System.Drawing.Point(86, 133);
            this.posBtnFullKeyBoardA.Name = "posBtnFullKeyBoardA";
            this.posBtnFullKeyBoardA.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardA.TabIndex = 81;
            this.posBtnFullKeyBoardA.Text = "A";
            this.posBtnFullKeyBoardA.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardS
            // 
            this.posBtnFullKeyBoardS.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardS.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardS.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardS.Location = new System.Drawing.Point(151, 133);
            this.posBtnFullKeyBoardS.Name = "posBtnFullKeyBoardS";
            this.posBtnFullKeyBoardS.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardS.TabIndex = 80;
            this.posBtnFullKeyBoardS.Text = " S";
            this.posBtnFullKeyBoardS.UseVisualStyleBackColor = false;
            // 
            // posChkFullKeyBoardCapsLock
            // 
            this.posChkFullKeyBoardCapsLock.Appearance = System.Windows.Forms.Appearance.Button;
            this.posChkFullKeyBoardCapsLock.BackColor = System.Drawing.Color.Black;
            this.posChkFullKeyBoardCapsLock.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posChkFullKeyBoardCapsLock.ForeColor = System.Drawing.Color.White;
            this.posChkFullKeyBoardCapsLock.Location = new System.Drawing.Point(2, 133);
            this.posChkFullKeyBoardCapsLock.Name = "posChkFullKeyBoardCapsLock";
            this.posChkFullKeyBoardCapsLock.Size = new System.Drawing.Size(84, 65);
            this.posChkFullKeyBoardCapsLock.TabIndex = 79;
            this.posChkFullKeyBoardCapsLock.Text = "Caps\r\nLock";
            this.posChkFullKeyBoardCapsLock.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardU
            // 
            this.posBtnFullKeyBoardU.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardU.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardU.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardU.Location = new System.Drawing.Point(474, 68);
            this.posBtnFullKeyBoardU.Name = "posBtnFullKeyBoardU";
            this.posBtnFullKeyBoardU.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardU.TabIndex = 78;
            this.posBtnFullKeyBoardU.Text = "U";
            this.posBtnFullKeyBoardU.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardI
            // 
            this.posBtnFullKeyBoardI.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardI.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardI.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardI.Location = new System.Drawing.Point(539, 68);
            this.posBtnFullKeyBoardI.Name = "posBtnFullKeyBoardI";
            this.posBtnFullKeyBoardI.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardI.TabIndex = 77;
            this.posBtnFullKeyBoardI.Text = "I";
            this.posBtnFullKeyBoardI.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardO
            // 
            this.posBtnFullKeyBoardO.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardO.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardO.Location = new System.Drawing.Point(604, 68);
            this.posBtnFullKeyBoardO.Name = "posBtnFullKeyBoardO";
            this.posBtnFullKeyBoardO.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardO.TabIndex = 76;
            this.posBtnFullKeyBoardO.Text = "O";
            this.posBtnFullKeyBoardO.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardP
            // 
            this.posBtnFullKeyBoardP.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardP.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardP.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardP.Location = new System.Drawing.Point(669, 68);
            this.posBtnFullKeyBoardP.Name = "posBtnFullKeyBoardP";
            this.posBtnFullKeyBoardP.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardP.TabIndex = 75;
            this.posBtnFullKeyBoardP.Text = "P";
            this.posBtnFullKeyBoardP.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardLeftSquareBrackets
            // 
            this.posBtnFullKeyBoardLeftSquareBrackets.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardLeftSquareBrackets.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardLeftSquareBrackets.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardLeftSquareBrackets.Location = new System.Drawing.Point(734, 68);
            this.posBtnFullKeyBoardLeftSquareBrackets.Name = "posBtnFullKeyBoardLeftSquareBrackets";
            this.posBtnFullKeyBoardLeftSquareBrackets.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardLeftSquareBrackets.TabIndex = 74;
            this.posBtnFullKeyBoardLeftSquareBrackets.Text = "{\r\n[";
            this.posBtnFullKeyBoardLeftSquareBrackets.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardRightSquareBracket
            // 
            this.posBtnFullKeyBoardRightSquareBracket.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardRightSquareBracket.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardRightSquareBracket.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardRightSquareBracket.Location = new System.Drawing.Point(799, 68);
            this.posBtnFullKeyBoardRightSquareBracket.Name = "posBtnFullKeyBoardRightSquareBracket";
            this.posBtnFullKeyBoardRightSquareBracket.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardRightSquareBracket.TabIndex = 73;
            this.posBtnFullKeyBoardRightSquareBracket.Text = "}\r\n]";
            this.posBtnFullKeyBoardRightSquareBracket.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardW
            // 
            this.posBtnFullKeyBoardW.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardW.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardW.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardW.Location = new System.Drawing.Point(150, 68);
            this.posBtnFullKeyBoardW.Name = "posBtnFullKeyBoardW";
            this.posBtnFullKeyBoardW.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardW.TabIndex = 72;
            this.posBtnFullKeyBoardW.Text = "W";
            this.posBtnFullKeyBoardW.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardE
            // 
            this.posBtnFullKeyBoardE.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardE.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardE.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardE.Location = new System.Drawing.Point(215, 68);
            this.posBtnFullKeyBoardE.Name = "posBtnFullKeyBoardE";
            this.posBtnFullKeyBoardE.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardE.TabIndex = 71;
            this.posBtnFullKeyBoardE.Text = "E";
            this.posBtnFullKeyBoardE.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardR
            // 
            this.posBtnFullKeyBoardR.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardR.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardR.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardR.Location = new System.Drawing.Point(280, 68);
            this.posBtnFullKeyBoardR.Name = "posBtnFullKeyBoardR";
            this.posBtnFullKeyBoardR.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardR.TabIndex = 70;
            this.posBtnFullKeyBoardR.Text = "R";
            this.posBtnFullKeyBoardR.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardT
            // 
            this.posBtnFullKeyBoardT.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardT.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardT.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardT.Location = new System.Drawing.Point(344, 68);
            this.posBtnFullKeyBoardT.Name = "posBtnFullKeyBoardT";
            this.posBtnFullKeyBoardT.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardT.TabIndex = 69;
            this.posBtnFullKeyBoardT.Text = "T";
            this.posBtnFullKeyBoardT.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardY
            // 
            this.posBtnFullKeyBoardY.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardY.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardY.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardY.Location = new System.Drawing.Point(409, 68);
            this.posBtnFullKeyBoardY.Name = "posBtnFullKeyBoardY";
            this.posBtnFullKeyBoardY.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardY.TabIndex = 68;
            this.posBtnFullKeyBoardY.Text = "Y";
            this.posBtnFullKeyBoardY.UseVisualStyleBackColor = false;
            // 
            // posBtnFullKeyBoardQ
            // 
            this.posBtnFullKeyBoardQ.BackColor = System.Drawing.Color.Black;
            this.posBtnFullKeyBoardQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnFullKeyBoardQ.ForeColor = System.Drawing.Color.White;
            this.posBtnFullKeyBoardQ.Location = new System.Drawing.Point(86, 68);
            this.posBtnFullKeyBoardQ.Name = "posBtnFullKeyBoardQ";
            this.posBtnFullKeyBoardQ.Size = new System.Drawing.Size(65, 65);
            this.posBtnFullKeyBoardQ.TabIndex = 67;
            this.posBtnFullKeyBoardQ.Text = "Q";
            this.posBtnFullKeyBoardQ.UseVisualStyleBackColor = false;
            // 
            // posGuiFullKeyBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 330);
            this.ControlBox = false;
            this.Controls.Add(this.posBtnFullKeyBoardClose);
            this.Controls.Add(this.posChkFullKeyBoardRightControl);
            this.Controls.Add(this.posChkFullKeyBoardRightAlt);
            this.Controls.Add(this.posChkFullKeyBoardLeftAlt);
            this.Controls.Add(this.posChkFullKeyBoardLeftControl);
            this.Controls.Add(this.posChkFullKeyBoardRightShift);
            this.Controls.Add(this.posChkFullKeyBoardLeftShift);
            this.Controls.Add(this.posBtnFullKeyBoardSpaceBar);
            this.Controls.Add(this.posBtnFullKeyBoardEqual);
            this.Controls.Add(this.posBtnFullKeyBoardBackSpace);
            this.Controls.Add(this.posBtnFullKeyBoardEight);
            this.Controls.Add(this.posBtnFullKeyBoardNine);
            this.Controls.Add(this.posBtnFullKeyBoardZero);
            this.Controls.Add(this.posBtnFullKeyBoardOne);
            this.Controls.Add(this.posBtnFullKeyBoardTwo);
            this.Controls.Add(this.posBtnFullKeyBoardThree);
            this.Controls.Add(this.posBtnFullKeyBoardFour);
            this.Controls.Add(this.posBtnFullKeyBoardFive);
            this.Controls.Add(this.posBtnFullKeyBoardSix);
            this.Controls.Add(this.posBtnFullKeyBoardSeven);
            this.Controls.Add(this.posBtnFullKeyBoardHyphen);
            this.Controls.Add(this.posBtnFullKeyBoardCurrentSign);
            this.Controls.Add(this.posBtnFullKeyBoardZ);
            this.Controls.Add(this.posBtnFullKeyBoardX);
            this.Controls.Add(this.posBtnFullKeyBoardC);
            this.Controls.Add(this.posBtnFullKeyBoardV);
            this.Controls.Add(this.posBtnFullKeyBoardB);
            this.Controls.Add(this.posBtnFullKeyBoardN);
            this.Controls.Add(this.posBtnFullKeyBoardM);
            this.Controls.Add(this.posBtnFullKeyBoardComma);
            this.Controls.Add(this.posBtnFullKeyBoardDot);
            this.Controls.Add(this.posBtnFullKeyBoardFowardSlash);
            this.Controls.Add(this.posBtnFullKeyBoardEnter);
            this.Controls.Add(this.posBtnFullKeyBoardQuotation);
            this.Controls.Add(this.posBtnFullKeyBoardColon);
            this.Controls.Add(this.posBtnFullKeyBoardK);
            this.Controls.Add(this.posBtnFullKeyBoardL);
            this.Controls.Add(this.posBtnFullKeyBoardJ);
            this.Controls.Add(this.posBtnFullKeyBoardD);
            this.Controls.Add(this.posBtnFullKeyBoardF);
            this.Controls.Add(this.posBtnFullKeyBoardG);
            this.Controls.Add(this.posBtnFullKeyBoardH);
            this.Controls.Add(this.posBtnFullKeyBoardBackSlash);
            this.Controls.Add(this.posBtnFullKeyBoardTab);
            this.Controls.Add(this.posBtnFullKeyBoardA);
            this.Controls.Add(this.posBtnFullKeyBoardS);
            this.Controls.Add(this.posChkFullKeyBoardCapsLock);
            this.Controls.Add(this.posBtnFullKeyBoardU);
            this.Controls.Add(this.posBtnFullKeyBoardI);
            this.Controls.Add(this.posBtnFullKeyBoardO);
            this.Controls.Add(this.posBtnFullKeyBoardP);
            this.Controls.Add(this.posBtnFullKeyBoardLeftSquareBrackets);
            this.Controls.Add(this.posBtnFullKeyBoardRightSquareBracket);
            this.Controls.Add(this.posBtnFullKeyBoardW);
            this.Controls.Add(this.posBtnFullKeyBoardE);
            this.Controls.Add(this.posBtnFullKeyBoardR);
            this.Controls.Add(this.posBtnFullKeyBoardT);
            this.Controls.Add(this.posBtnFullKeyBoardY);
            this.Controls.Add(this.posBtnFullKeyBoardQ);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "posGuiFullKeyBoard";
            this.ShowInTaskbar = false;
            this.Text = "Full Key Board";
            this.Load += new System.EventHandler(this.posGuiFullKeyBoard_Load);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button posBtnFullKeyBoardClose;
        internal System.Windows.Forms.CheckBox posChkFullKeyBoardRightControl;
        internal System.Windows.Forms.CheckBox posChkFullKeyBoardRightAlt;
        internal System.Windows.Forms.CheckBox posChkFullKeyBoardLeftAlt;
        internal System.Windows.Forms.CheckBox posChkFullKeyBoardLeftControl;
        internal System.Windows.Forms.CheckBox posChkFullKeyBoardRightShift;
        internal System.Windows.Forms.CheckBox posChkFullKeyBoardLeftShift;
        internal System.Windows.Forms.Button posBtnFullKeyBoardSpaceBar;
        internal System.Windows.Forms.Button posBtnFullKeyBoardEqual;
        internal System.Windows.Forms.Button posBtnFullKeyBoardBackSpace;
        internal System.Windows.Forms.Button posBtnFullKeyBoardEight;
        internal System.Windows.Forms.Button posBtnFullKeyBoardNine;
        internal System.Windows.Forms.Button posBtnFullKeyBoardZero;
        internal System.Windows.Forms.Button posBtnFullKeyBoardOne;
        internal System.Windows.Forms.Button posBtnFullKeyBoardTwo;
        internal System.Windows.Forms.Button posBtnFullKeyBoardThree;
        internal System.Windows.Forms.Button posBtnFullKeyBoardFour;
        internal System.Windows.Forms.Button posBtnFullKeyBoardFive;
        internal System.Windows.Forms.Button posBtnFullKeyBoardSix;
        internal System.Windows.Forms.Button posBtnFullKeyBoardSeven;
        internal System.Windows.Forms.Button posBtnFullKeyBoardHyphen;
        internal System.Windows.Forms.Button posBtnFullKeyBoardCurrentSign;
        internal System.Windows.Forms.Button posBtnFullKeyBoardZ;
        internal System.Windows.Forms.Button posBtnFullKeyBoardX;
        internal System.Windows.Forms.Button posBtnFullKeyBoardC;
        internal System.Windows.Forms.Button posBtnFullKeyBoardV;
        internal System.Windows.Forms.Button posBtnFullKeyBoardB;
        internal System.Windows.Forms.Button posBtnFullKeyBoardN;
        internal System.Windows.Forms.Button posBtnFullKeyBoardM;
        internal System.Windows.Forms.Button posBtnFullKeyBoardComma;
        internal System.Windows.Forms.Button posBtnFullKeyBoardDot;
        internal System.Windows.Forms.Button posBtnFullKeyBoardFowardSlash;
        internal System.Windows.Forms.Button posBtnFullKeyBoardEnter;
        internal System.Windows.Forms.Button posBtnFullKeyBoardQuotation;
        internal System.Windows.Forms.Button posBtnFullKeyBoardColon;
        internal System.Windows.Forms.Button posBtnFullKeyBoardK;
        internal System.Windows.Forms.Button posBtnFullKeyBoardL;
        internal System.Windows.Forms.Button posBtnFullKeyBoardJ;
        internal System.Windows.Forms.Button posBtnFullKeyBoardD;
        internal System.Windows.Forms.Button posBtnFullKeyBoardF;
        internal System.Windows.Forms.Button posBtnFullKeyBoardG;
        internal System.Windows.Forms.Button posBtnFullKeyBoardH;
        internal System.Windows.Forms.Button posBtnFullKeyBoardBackSlash;
        internal System.Windows.Forms.Button posBtnFullKeyBoardTab;
        internal System.Windows.Forms.Button posBtnFullKeyBoardA;
        internal System.Windows.Forms.Button posBtnFullKeyBoardS;
        internal System.Windows.Forms.CheckBox posChkFullKeyBoardCapsLock;
        internal System.Windows.Forms.Button posBtnFullKeyBoardU;
        internal System.Windows.Forms.Button posBtnFullKeyBoardI;
        internal System.Windows.Forms.Button posBtnFullKeyBoardO;
        internal System.Windows.Forms.Button posBtnFullKeyBoardP;
        internal System.Windows.Forms.Button posBtnFullKeyBoardLeftSquareBrackets;
        internal System.Windows.Forms.Button posBtnFullKeyBoardRightSquareBracket;
        internal System.Windows.Forms.Button posBtnFullKeyBoardW;
        internal System.Windows.Forms.Button posBtnFullKeyBoardE;
        internal System.Windows.Forms.Button posBtnFullKeyBoardR;
        internal System.Windows.Forms.Button posBtnFullKeyBoardT;
        internal System.Windows.Forms.Button posBtnFullKeyBoardY;
        internal System.Windows.Forms.Button posBtnFullKeyBoardQ;



    }
}