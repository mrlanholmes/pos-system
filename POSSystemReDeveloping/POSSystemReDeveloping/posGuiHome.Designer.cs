﻿namespace POSSystemReDeveloping
{
    partial class posGuiHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnHomeGuestOrder = new System.Windows.Forms.Button();
            this.btnHomeAllInclusive = new System.Windows.Forms.Button();
            this.btnHomeBill = new System.Windows.Forms.Button();
            this.btnHomeEntitle = new System.Windows.Forms.Button();
            this.btnHomeReports = new System.Windows.Forms.Button();
            this.btnHomeBillSplit = new System.Windows.Forms.Button();
            this.btnHomeStewardLogout = new System.Windows.Forms.Button();
            this.btnHomeSettings = new System.Windows.Forms.Button();
            this.btnHomeSwitchOff = new System.Windows.Forms.Button();
            this.btnHomePrinter = new System.Windows.Forms.Button();
            this.btnHomePrinterToItems = new System.Windows.Forms.Button();
            this.btnHomeItemRemarks = new System.Windows.Forms.Button();
            this.posDgvHome = new System.Windows.Forms.DataGridView();
            this.posPnlHome = new System.Windows.Forms.Panel();
            this.btnHomeOpenItem = new System.Windows.Forms.Button();
            this.btnHomeBillReprint = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvHome)).BeginInit();
            this.posPnlHome.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnHomeGuestOrder
            // 
            this.btnHomeGuestOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeGuestOrder.Location = new System.Drawing.Point(3, 3);
            this.btnHomeGuestOrder.Name = "btnHomeGuestOrder";
            this.btnHomeGuestOrder.Size = new System.Drawing.Size(149, 74);
            this.btnHomeGuestOrder.TabIndex = 0;
            this.btnHomeGuestOrder.Text = "Guest Order";
            this.btnHomeGuestOrder.UseVisualStyleBackColor = true;
            this.btnHomeGuestOrder.Click += new System.EventHandler(this.btnHomeGuestOrder_Click);
            // 
            // btnHomeAllInclusive
            // 
            this.btnHomeAllInclusive.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeAllInclusive.Location = new System.Drawing.Point(156, 3);
            this.btnHomeAllInclusive.Name = "btnHomeAllInclusive";
            this.btnHomeAllInclusive.Size = new System.Drawing.Size(149, 74);
            this.btnHomeAllInclusive.TabIndex = 1;
            this.btnHomeAllInclusive.Text = "All Inc(AI)";
            this.btnHomeAllInclusive.UseVisualStyleBackColor = true;
            // 
            // btnHomeBill
            // 
            this.btnHomeBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeBill.Location = new System.Drawing.Point(3, 83);
            this.btnHomeBill.Name = "btnHomeBill";
            this.btnHomeBill.Size = new System.Drawing.Size(149, 74);
            this.btnHomeBill.TabIndex = 2;
            this.btnHomeBill.Text = "Bill";
            this.btnHomeBill.UseVisualStyleBackColor = true;
            this.btnHomeBill.Click += new System.EventHandler(this.btnHomeBill_Click);
            // 
            // btnHomeEntitle
            // 
            this.btnHomeEntitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeEntitle.Location = new System.Drawing.Point(156, 83);
            this.btnHomeEntitle.Name = "btnHomeEntitle";
            this.btnHomeEntitle.Size = new System.Drawing.Size(149, 74);
            this.btnHomeEntitle.TabIndex = 3;
            this.btnHomeEntitle.Text = "Entittle";
            this.btnHomeEntitle.UseVisualStyleBackColor = true;
            // 
            // btnHomeReports
            // 
            this.btnHomeReports.Enabled = false;
            this.btnHomeReports.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeReports.Location = new System.Drawing.Point(156, 163);
            this.btnHomeReports.Name = "btnHomeReports";
            this.btnHomeReports.Size = new System.Drawing.Size(149, 74);
            this.btnHomeReports.TabIndex = 4;
            this.btnHomeReports.Text = "Reports";
            this.btnHomeReports.UseVisualStyleBackColor = true;
            this.btnHomeReports.Click += new System.EventHandler(this.btnHomeReports_Click);
            // 
            // btnHomeBillSplit
            // 
            this.btnHomeBillSplit.Enabled = false;
            this.btnHomeBillSplit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeBillSplit.Location = new System.Drawing.Point(3, 163);
            this.btnHomeBillSplit.Name = "btnHomeBillSplit";
            this.btnHomeBillSplit.Size = new System.Drawing.Size(149, 74);
            this.btnHomeBillSplit.TabIndex = 5;
            this.btnHomeBillSplit.Text = "Bill Split";
            this.btnHomeBillSplit.UseVisualStyleBackColor = true;
            // 
            // btnHomeStewardLogout
            // 
            this.btnHomeStewardLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeStewardLogout.Location = new System.Drawing.Point(3, 244);
            this.btnHomeStewardLogout.Name = "btnHomeStewardLogout";
            this.btnHomeStewardLogout.Size = new System.Drawing.Size(149, 74);
            this.btnHomeStewardLogout.TabIndex = 6;
            this.btnHomeStewardLogout.Text = "Steward Logout";
            this.btnHomeStewardLogout.UseVisualStyleBackColor = true;
            this.btnHomeStewardLogout.Click += new System.EventHandler(this.btnHomeStewardLogout_Click);
            // 
            // btnHomeSettings
            // 
            this.btnHomeSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeSettings.Location = new System.Drawing.Point(156, 244);
            this.btnHomeSettings.Name = "btnHomeSettings";
            this.btnHomeSettings.Size = new System.Drawing.Size(149, 74);
            this.btnHomeSettings.TabIndex = 7;
            this.btnHomeSettings.Text = "Settings";
            this.btnHomeSettings.UseVisualStyleBackColor = true;
            // 
            // btnHomeSwitchOff
            // 
            this.btnHomeSwitchOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeSwitchOff.Location = new System.Drawing.Point(3, 324);
            this.btnHomeSwitchOff.Name = "btnHomeSwitchOff";
            this.btnHomeSwitchOff.Size = new System.Drawing.Size(149, 74);
            this.btnHomeSwitchOff.TabIndex = 8;
            this.btnHomeSwitchOff.Text = "Switch Off";
            this.btnHomeSwitchOff.UseVisualStyleBackColor = true;
            this.btnHomeSwitchOff.Click += new System.EventHandler(this.btnHomeSwitchOff_Click);
            // 
            // btnHomePrinter
            // 
            this.btnHomePrinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomePrinter.Location = new System.Drawing.Point(156, 324);
            this.btnHomePrinter.Name = "btnHomePrinter";
            this.btnHomePrinter.Size = new System.Drawing.Size(149, 74);
            this.btnHomePrinter.TabIndex = 9;
            this.btnHomePrinter.Text = "Printer";
            this.btnHomePrinter.UseVisualStyleBackColor = true;
            // 
            // btnHomePrinterToItems
            // 
            this.btnHomePrinterToItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomePrinterToItems.Location = new System.Drawing.Point(3, 404);
            this.btnHomePrinterToItems.Name = "btnHomePrinterToItems";
            this.btnHomePrinterToItems.Size = new System.Drawing.Size(149, 74);
            this.btnHomePrinterToItems.TabIndex = 10;
            this.btnHomePrinterToItems.Text = "Printer To Items";
            this.btnHomePrinterToItems.UseVisualStyleBackColor = true;
            // 
            // btnHomeItemRemarks
            // 
            this.btnHomeItemRemarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeItemRemarks.Location = new System.Drawing.Point(156, 404);
            this.btnHomeItemRemarks.Name = "btnHomeItemRemarks";
            this.btnHomeItemRemarks.Size = new System.Drawing.Size(149, 74);
            this.btnHomeItemRemarks.TabIndex = 11;
            this.btnHomeItemRemarks.Text = "Item Remarks";
            this.btnHomeItemRemarks.UseVisualStyleBackColor = true;
            // 
            // posDgvHome
            // 
            this.posDgvHome.AllowUserToAddRows = false;
            this.posDgvHome.AllowUserToDeleteRows = false;
            this.posDgvHome.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.posDgvHome.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.posDgvHome.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvHome.Location = new System.Drawing.Point(1, 3);
            this.posDgvHome.Name = "posDgvHome";
            this.posDgvHome.ReadOnly = true;
            this.posDgvHome.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.posDgvHome.Size = new System.Drawing.Size(403, 558);
            this.posDgvHome.TabIndex = 12;
            this.posDgvHome.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.posDgvHome_CellContentClick);
            // 
            // posPnlHome
            // 
            this.posPnlHome.Controls.Add(this.btnHomeGuestOrder);
            this.posPnlHome.Controls.Add(this.btnHomeAllInclusive);
            this.posPnlHome.Controls.Add(this.btnHomeItemRemarks);
            this.posPnlHome.Controls.Add(this.btnHomeBill);
            this.posPnlHome.Controls.Add(this.btnHomePrinterToItems);
            this.posPnlHome.Controls.Add(this.btnHomeEntitle);
            this.posPnlHome.Controls.Add(this.btnHomePrinter);
            this.posPnlHome.Controls.Add(this.btnHomeBillSplit);
            this.posPnlHome.Controls.Add(this.btnHomeSwitchOff);
            this.posPnlHome.Controls.Add(this.btnHomeReports);
            this.posPnlHome.Controls.Add(this.btnHomeSettings);
            this.posPnlHome.Controls.Add(this.btnHomeStewardLogout);
            this.posPnlHome.Location = new System.Drawing.Point(404, 3);
            this.posPnlHome.Name = "posPnlHome";
            this.posPnlHome.Size = new System.Drawing.Size(308, 483);
            this.posPnlHome.TabIndex = 13;
            // 
            // btnHomeOpenItem
            // 
            this.btnHomeOpenItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeOpenItem.Location = new System.Drawing.Point(560, 487);
            this.btnHomeOpenItem.Name = "btnHomeOpenItem";
            this.btnHomeOpenItem.Size = new System.Drawing.Size(149, 74);
            this.btnHomeOpenItem.TabIndex = 15;
            this.btnHomeOpenItem.Text = "Open Item";
            this.btnHomeOpenItem.UseVisualStyleBackColor = true;
            // 
            // btnHomeBillReprint
            // 
            this.btnHomeBillReprint.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeBillReprint.Location = new System.Drawing.Point(407, 487);
            this.btnHomeBillReprint.Name = "btnHomeBillReprint";
            this.btnHomeBillReprint.Size = new System.Drawing.Size(149, 74);
            this.btnHomeBillReprint.TabIndex = 14;
            this.btnHomeBillReprint.Text = "Bill Reprint";
            this.btnHomeBillReprint.UseVisualStyleBackColor = true;
            // 
            // posGuiHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 563);
            this.ControlBox = false;
            this.Controls.Add(this.btnHomeOpenItem);
            this.Controls.Add(this.btnHomeBillReprint);
            this.Controls.Add(this.posPnlHome);
            this.Controls.Add(this.posDgvHome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "posGuiHome";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HOME";
            this.Load += new System.EventHandler(this.posGuiHome_Load);
            ((System.ComponentModel.ISupportInitialize)(this.posDgvHome)).EndInit();
            this.posPnlHome.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.DataGridView posDgvHome;
        internal System.Windows.Forms.Button btnHomeGuestOrder;
        internal System.Windows.Forms.Button btnHomeBill;
        internal System.Windows.Forms.Button btnHomeSettings;
        internal System.Windows.Forms.Button btnHomeAllInclusive;
        internal System.Windows.Forms.Button btnHomeEntitle;
        internal System.Windows.Forms.Button btnHomeReports;
        internal System.Windows.Forms.Button btnHomeBillSplit;
        internal System.Windows.Forms.Button btnHomeStewardLogout;
        internal System.Windows.Forms.Button btnHomeSwitchOff;
        internal System.Windows.Forms.Button btnHomePrinter;
        internal System.Windows.Forms.Button btnHomePrinterToItems;
        internal System.Windows.Forms.Button btnHomeItemRemarks;
        internal System.Windows.Forms.Panel posPnlHome;
        internal System.Windows.Forms.Button btnHomeOpenItem;
        internal System.Windows.Forms.Button btnHomeBillReprint;
    }
}