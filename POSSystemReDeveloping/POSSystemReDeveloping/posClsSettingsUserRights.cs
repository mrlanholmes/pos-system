﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    class posClsSettingsUserRights :pos
    {

        private posGuiSettings setting;
        public posClsSettingsUserRights(posGuiSettings settingObject) {
            this.setting = settingObject;
        }
        
        //sql
        public int posGetUserRightTableRowCount(String[] data)
        {
            this.qry = "select count(*) from [posuserright] where userright_userid='"+data[0]+"' and userright_ccode ='"+data[1]+"'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) {
                return Convert.ToInt16(this.dt[0, 0]);
            }
            return 0;
        }
        public String[,] posGetUserRights(String[] data) {
            this.qry = "SELECT [userright_kotbot]  ,[userright_ai]  ,[userright_bill]  ,[userright_entitle] ,[userright_settlement]"
                        + ",[userright_reports]  ,[userright_settings]  ,[userright_addprinter] ,[userright_itemtoprinter]"
                        + ",[userright_additemremarks] FROM [posuserright] where userright_userid='" + data[0] + "' and [userright_ccode] = '" + data[1] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return this.dt; } else { return null; }
        }
        public bool posInsertUserRight(String[] data)
        {
            this.qry = "INSERT INTO posuserright(userright_userid,userright_ccode,userright_kotbot,userright_ai,userright_entitle,userright_bill"
            + ",userright_reports,userright_settlement,userright_addprinter,userright_itemtoprinter,userright_additemremarks"
            + ",userright_settings) VALUES('" + data[0] + "','" + data[1] + "'," + data[2] + "," + data[3] + "," + data[4] + "," +
            data[5] + "," + data[6] + "," + data[7] + "," + data[8] + "," + data[9] + "," + data[10] + "," + data[11] + ")";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt32(this.dt[0, 0]) == 1)
            {
                return true;
            }
            return false;
        }      
        private bool posUpdateUserRight(String[] data)
        {
            this.qry = "UPDATE posuserright SET userright_kotbot=" + data[0]
            + ",userright_ai=" + data[1] + ",userright_bill=" + data[2] + ","
            + "userright_entitle=" + data[3] + ",userright_settlement=" + data[4] + ","
            + "userright_reports=" + data[5] + ",userright_settings=" + data[6] + ","
            + "userright_addprinter=" + data[7] + ",userright_itemtoprinter=" + data[8] + ","
            + "userright_additemremarks=" + data[9] + " WHERE  userright_userid='" + data[10] + "'"
            +" AND userright_ccode='" + data[11] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt32(this.dt[0, 0]) ==1)
            {
                return true;
            }
            return false;
        }
        // event

        //function
        private String posIsChecked(CheckBox chkbx) {
            if (chkbx.Checked == true) { return "1"; } else { return "0"; }
        }
        public bool posSaveUserRights(String userCode,String costCenterCode) {
            String[] data = new String[12];
            data[0] = this.posIsChecked(this.setting.posChkUserRightKotBot);
            data[1] = this.posIsChecked(this.setting.posChkUserRightAI);
            data[2] = this.posIsChecked(this.setting.posChkUserRightBill);
            data[3] = this.posIsChecked(this.setting.posChkUserRightEntitle);
            data[4] = this.posIsChecked(this.setting.posChkUserRightSettlement);
            data[5] = this.posIsChecked(this.setting.posChkUserRightReports);
            data[6] = this.posIsChecked(this.setting.posChkUserRightSettings);
            data[7] = this.posIsChecked(this.setting.posChkUserRightAddPrinter);
            data[8] = this.posIsChecked(this.setting.posChkUserRightPrinterToItems);
            data[9] = this.posIsChecked(this.setting.posChkUserRightAddItemRemarks);
            data[10] = userCode;
            data[11] = costCenterCode;
            if (this.posUpdateUserRight(data)) { MessageBox.Show("Userrights are Created"); return true; }
            else { MessageBox.Show("Userrights are not Created"); } 
            return false;
        }
        
    }
}
