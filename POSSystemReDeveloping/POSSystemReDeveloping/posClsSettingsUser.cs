﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    class posClsSettingsUser : pos
    {
        private posGuiSettings setting;
       
        public posClsSettingsUser(posGuiSettings settingObject) {
            this.setting = settingObject;
        }
        // Sql 
        
        private bool posIsUserDetailsExist(String[] data)
        {
            this.qry = "SELECT count(*) FROM POSUsers WHERE POSUsers_UserID = '" + data[1] + " ' OR POSUsers_UserPassword='" + data[2] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (Convert.ToInt32(this.dt[0, 0]) > 0)
            {
                return true;
            }
            return false;
        }
        private bool posInsertUserDetails(String[] data)
        {
            this.qry = "insert into POSUsers(POSUsers_UserName,POSUsers_UserID,POSUsers_UserPassword,POSUsers_PasswordExp,POSUsers_UserType"
             + ",POSUsers_UserStatus,POSUsers_CreatedBy,POSUsers_CreatedOn) values('" + data[0] + "','" + data[1] + "','" + data[2] + "','"
             + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "','" + data[7] + "')";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt32(this.dt[0, 0]) == 1)
            {
                return true;
            }
            return false;
        }
          

        //Event
        public bool posSetSettingsBtnSaveClick(String[] costCenterIndex) {
            if (this.setting.posTxtUserName.Text.Trim().Equals("")) { MessageBox.Show("Please Enter Username..."); }
            else if (this.setting.posTxtUserUserId.Text.Trim().Equals("")) { MessageBox.Show("Please Enter User id..."); }
            else if (this.setting.posTxtUserPassword.Text.Trim().Equals("")) { MessageBox.Show("Please Enter Password..."); }
            else if (this.setting.posTxtUserUserId.Text.Trim().Equals("")) { MessageBox.Show("Please Enter User id..."); }
            else {
                if (this.SaveUserDetails(costCenterIndex)) { return true; }
                else { return false; }
            }
            return false;
        }
        
        private DataTable posGetUserDetails() {
            this.qry = "select POSUsers_UserID,POSUsers_UserName,POSUsers_PasswordExp,POSUsers_UserType,POSUsers_UserStatus from [POSUsers] ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) {
                return posStaticLoadOnce.PosDataTable;
            }
            return null;
        }
        public void posSetDgvUserData() {
            this.setting.posDgvSettingUser.DataSource = null;
            this.setting.posDgvSettingUser.DataSource = this.posGetUserDetails();
        }
        private bool SaveUserDetails(String[] costCenterIndex)
        {
            String[] data = new String[8];
            data[0] = this.setting.posTxtUserName.Text.Trim();
            data[1] = this.setting.posTxtUserUserId.Text.Trim();
            data[2] = this.setting.posTxtUserPassword.Text;
            data[3] = this.setting.posDtpUserExpireDate.Value.Date.ToString("dd/MM/yyyy") ;
            data[4] = this.setting.posCmbUserUserType.SelectedItem.ToString();
            data[5] = this.setting.posCmbUserAllowPwChange.SelectedItem.ToString() ;
            data[6] = posStaticLoadOnce.PosLoggedStewardUserId;
            data[7] = this.getDateTime();
            if (!this.posIsUserDetailsExist(data))
            {
                if (this.posInsertUserDetails(data))
                {
                    MessageBox.Show("User Details are saved...");
                    return true; 
                    //String[] userRight = new String[12];
                   // int counter = 0;
                    //if (this.setting.posDgvSettingUserCcent.Rows.Count > 0)
                    //{
                        //for (int i = 0; i < this.setting.posDgvSettingUserCcent.Rows.Count; i++)
                        //{
                            //userRight[0] = data[1];
                            ////this.setting.posCmbUserCostCenter.Items[i].Selected;
                            //userRight[1] = costCenterIndex[i];
                            //userRight[2] = "0";
                            //userRight[3] = "0";
                            //userRight[4] = "0";
                            //userRight[5] = "0";
                            //userRight[6] = "0";
                            //userRight[7] = "0";
                            //userRight[8] = "0";
                            //userRight[9] = "0";
                            //userRight[10] = "0";
                            //userRight[11] = "0";

                            //if (this.posInsertUserRight(userRight))
                            //{
                            //    counter++;
                            //}
                        //}
                    //}
                   // if (counter == this.setting.posDgvSettingUserCcent.Rows.Count) { MessageBox.Show("User Details are saved..."); this.posClrComponent(); return true; }{ MessageBox.Show("User Details are not saved..."); return false; }
                }
                else { MessageBox.Show("User Details are not saved..."); return false; }
            }
            else { MessageBox.Show("This User's  is exist..."); return false; }
            
             //return false;
        }
        public void posClrComponent() {
            this.setting.posTxtUserName.Text = "";
            this.setting.posTxtUserPassword.Text = "";
            this.setting.posTxtUserUserId.Text = "";
            
            this.setting.posDtpUserExpireDate.Text = "";
            try { 
                //this.setting.posCmbUserCostCenter.SelectedIndex = -1;
            }
            catch (Exception) { }
            try
            {
                this.setting.posCmbUserUserType.SelectedIndex = -1;
            }
            catch (Exception) { }
            try
            {
                this.setting.posCmbUserAllowPwChange.SelectedIndex = -1;
            }
            catch (Exception) { }
            
        }
        public void posSettingPanelShow()
        {
            this.setting.Show();
        }
        public void posSettingPanelDispose()
        {
            this.setting.Dispose();
        }
        public void posSettingPanelHide() {
            this.setting.Hide();
        } 
    }
}
