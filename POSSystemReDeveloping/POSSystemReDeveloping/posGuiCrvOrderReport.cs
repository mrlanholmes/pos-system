﻿using CrystalDecisions.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public partial class posGuiCrvOrderReport : Form
    {
        public posGuiCrvOrderReport()
        {
            InitializeComponent();
        }

        private void posGuiCrvOrderReport_Load(object sender, EventArgs e)
        {

        }
        public CrystalReportViewer posGetOrderReportViewer()
        {
            return posCrvOrderReport;
        }

        private void btnCrvOrderReportBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
