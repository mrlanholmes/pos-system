﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public partial class posGuiReportCategory : Form
    {
        private posGuiOrderReport orderReport;
        private posGuiHome home;
        public posGuiReportCategory()
        {
            InitializeComponent();
        }

        private void btnReportBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.home.Show();
        }

        private void btnReportCategoryGeustReport_Click(object sender, EventArgs e)
        {
            this.orderReport = null;
            if (this.orderReport == null) {
                this.orderReport = new  posGuiOrderReport();
            }
            this.orderReport.Show();
        }

        private void posGuiReportCategory_Load(object sender, EventArgs e)
        {
            
        }
        public void posSetHomeObject(posGuiHome homeObject) {
            this.home = homeObject;
            this.home.Hide();
        }
        
    }
}
