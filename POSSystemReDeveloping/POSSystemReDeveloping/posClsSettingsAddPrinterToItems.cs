﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    class posClsSettingsAddPrinterToItems : pos
    {
        private posGuiSettings settings;
        public posClsSettingsAddPrinterToItems(posGuiSettings settingsObject) {
            this.settings = settingsObject;
            this.settings.posTabSettingsAPTIDgvPrinterItem.Click += this.posSetDgvPrinterItemClick;
        }
        //Sql
        public String[,] posGetCmbPrinter() {
            this.qry = "select [Printer Name] from PrinterProperties";
            String[,] data =posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return data; }
            return null;
        }
        private void posSetDgvPrinterItemClick(object sender, EventArgs e)
        {
            this.settings.posTxtSettingsAPTIItemNo.Text = this.settings.posTabSettingsAPTIDgvPrinterItem.Rows[this.settings.posTabSettingsAPTIDgvPrinterItem.CurrentRow.Index].Cells[0].Value.ToString();
            this.settings.posTxtSettingsAPTIName.Text = this.settings.posTabSettingsAPTIDgvPrinterItem.Rows[this.settings.posTabSettingsAPTIDgvPrinterItem.CurrentRow.Index].Cells[1].Value.ToString();
            this.settings.posTxtSettingsAPTICCenter.Text = this.settings.posTabSettingsAPTIDgvPrinterItem.Rows[this.settings.posTabSettingsAPTIDgvPrinterItem.CurrentRow.Index].Cells[3].Value.ToString();
        }
        public bool posUpdateCostCenterPrinterToItem(String[] data) {
            this.qry = "UPDATE FOCostCenterItemPrice   SET  Printer_Name ='"+data[1]+"' WHERE FOIM_ItemNo ='"+data[0]+"'";
             String[,] tmpData = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
             if (Convert.ToInt16(tmpData[0, 0]) > 0) { return true ; }
            return false;
        }

    }
}
