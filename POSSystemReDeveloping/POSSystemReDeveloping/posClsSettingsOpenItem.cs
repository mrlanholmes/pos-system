﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POSSystemReDeveloping
{
    class posClsSettingsOpenItem : pos
    {
        public String[,] posGetCostCenter(String selector)
        {
            this.qry = "SELECT  CCent_Name,CCent_Code FROM FOCostCenter where CCent_POS = '" + selector + "' ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) { return this.dt; }
            return null;
        }
        public String[,] posGetOpenItem(String[] data) {
            this.qry = "select FOMItems.FOIM_ItemNo, FOMItems.FOIM_ItemDes,FOCostCenterItemPrice.CCP_Value from FOMItems " 
                        +" join FOCostCenterItemPrice on FOMItems.FOIM_ItemNo= FOCostCenterItemPrice.FOIM_ItemNo "
                        + "and FOMItems.FOIM_Open = 1 and FOCostCenterItemPrice.CCent_Code = '"+data[0]+"' and FOCostCenterItemPrice.SaTy_Code ='"+data[1]+"' ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) {
                return this.dt;
            }
            return null;
        }
        public String[,] posGetSalesType(String selector)
        {
            this.qry = "SELECT SaTy_Code as 'Code', SaTy_Description as 'Name' FROM dbo.FOSalesType";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null)
            {
                return this.dt ;
            }
            return null;
        }
        public bool posUpdateSalesType(String[] data)
        {
            this.qry = "update FOCostCenterItemPrice set CCP_Value = '" + data[0] + "' , CCP_ValueF = '" + data[1] + "' where FOIM_ItemNo = '" + data[2] + "' and CCent_Code = '" + data[3] + "' and SaTy_Code = '"+ data[4] +"'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt16(this.dt[0,0]) == 1)
            {
                return true;
            }
            return false ;
        }
        

    }
}
