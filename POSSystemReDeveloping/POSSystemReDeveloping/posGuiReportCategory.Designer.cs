﻿namespace POSSystemReDeveloping
{
    partial class posGuiReportCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnReportCategoryGeustReport = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnReportBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnReportCategoryGeustReport
            // 
            this.btnReportCategoryGeustReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportCategoryGeustReport.Location = new System.Drawing.Point(126, 12);
            this.btnReportCategoryGeustReport.Name = "btnReportCategoryGeustReport";
            this.btnReportCategoryGeustReport.Size = new System.Drawing.Size(358, 41);
            this.btnReportCategoryGeustReport.TabIndex = 0;
            this.btnReportCategoryGeustReport.Text = "Guest Order Report";
            this.btnReportCategoryGeustReport.UseVisualStyleBackColor = true;
            this.btnReportCategoryGeustReport.Click += new System.EventHandler(this.btnReportCategoryGeustReport_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(126, 59);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(358, 41);
            this.button2.TabIndex = 1;
            this.button2.Text = "Bill Report";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(126, 106);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(358, 41);
            this.button4.TabIndex = 3;
            this.button4.Text = "Bill Split Report";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // btnReportBack
            // 
            this.btnReportBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportBack.Location = new System.Drawing.Point(5, 8);
            this.btnReportBack.Name = "btnReportBack";
            this.btnReportBack.Size = new System.Drawing.Size(91, 90);
            this.btnReportBack.TabIndex = 4;
            this.btnReportBack.Text = "Back";
            this.btnReportBack.UseVisualStyleBackColor = true;
            this.btnReportBack.Click += new System.EventHandler(this.btnReportBack_Click);
            // 
            // posGuiReportCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 157);
            this.ControlBox = false;
            this.Controls.Add(this.btnReportBack);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnReportCategoryGeustReport);
            this.Name = "posGuiReportCategory";
            this.Text = "Report";
            this.Load += new System.EventHandler(this.posGuiReportCategory_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnReportCategoryGeustReport;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnReportBack;
    }
}