﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace POSSystemReDeveloping
{
    class posStaticLoadOnce 
    { 
        private static MsSql msql;        
        private static String posLoggedUserName;
        private static String posLoggedUserType;
        private static String posLoggedUserId;
        private static String posLoggedExpireDate;
        private static int posSystemNoOfUsers;
        private static String posSystemExpireDate;
        private static String posLoggedStewardUserId;       
        private static String posLoggedStewardUserName;      
        private static String posLoggedStewardUserType;       
        private static String posLoggedStewardExpireDate;
        private static posClsLog log;
        private static String[] posCostCenterCode;
        private static Boolean[] posUserRightPermission;
        private static posClsSystem posSystem;
        // UserRight
        // Get Company Name
        public static String[,] posGetCompanyName(){
            if(posStaticLoadOnce.posSystem==null){
                posStaticLoadOnce.posSystem = new posClsSystem();
            }
            return posStaticLoadOnce.posSystem.posGetCompanyName();
        }
        public  posStaticLoadOnce() {
            new posClsSystem().posSetSystemData();            
        }
        public static String[,] dbExecute(String exeQuery, String ExecutionType) {
            if (msql == null )
            {
                msql = new MsSql();
            }
            return msql.dbExecute(exeQuery,ExecutionType);
        }
        private static bool createPosLogObject()
        {
            if (log == null)
            {
                try
                {
                    log = new posClsLog();
                    return true;
                }
                catch (Exception)
                { return false; }
            }
            else return true;            
        }
        public static SqlConnection getConnection()
        {
            if (msql == null || msql.Equals(""))
            {
                msql = new MsSql();
            }
            return msql.getConnection();
        }
        public static bool posLogExecutive(String[] data)
        {
            if (createPosLogObject()) { return log.posWriteExecutiveLogin(data); }
            return false;            
        }
        public static bool posLogSystem(String[] data) {
            if (createPosLogObject()) { return log.posWriteSystemLogin(data); }
            return false; }
        public static int PosColumnCount
        {
            get { return posStaticLoadOnce.msql.getRsColumnCount(); }            
        }
        public static int PosRowCount
        {
            get { return posStaticLoadOnce.msql.getRsRowCount(); }           
        }
        public static DataTable PosDataTable {             
            get { return posStaticLoadOnce.msql.getDataTable(); }
        }
        public static String PosLoggedUserName
        {
            get { return posStaticLoadOnce.posLoggedUserName.ToUpper(); }
            set { posStaticLoadOnce.posLoggedUserName = value; }
        }
        public static String PosLoggedUserType
        {
            get { return posStaticLoadOnce.posLoggedUserType; }
            set { posStaticLoadOnce.posLoggedUserType = value; }
        }
        public static bool isPosLoggedUserExecutive()
        {
            if (posStaticLoadOnce.PosLoggedUserType.Equals("EXECUTIVE")) { return true; } else { return false; }
        }
        public static bool isPosLoggedUserOther() {
            if (posStaticLoadOnce.PosLoggedUserType.Equals("OTHER")) { return true; } else { return false; }
        }
        public static bool isPosLoggedUserAdmin() {
            if (posStaticLoadOnce.PosLoggedUserType.Equals("ADMIN")) { return true; } else { return false; }
        }
        public static String PosLoggedUserId
        {
            get { return posStaticLoadOnce.posLoggedUserId; }
            set { posStaticLoadOnce.posLoggedUserId = value; }
        }
        public static String PosLoggedExpireDate
        {
            get { return posStaticLoadOnce.posLoggedExpireDate; }
            set { posStaticLoadOnce.posLoggedExpireDate = value; }
        }
        public static int PosSystemNoOfUsers
        {
            get { return posStaticLoadOnce.posSystemNoOfUsers; }
            set { posStaticLoadOnce.posSystemNoOfUsers = value; }
        }
        public static String PosSystemExpireDate
        {
            get { return posStaticLoadOnce.posSystemExpireDate; }
            set { posStaticLoadOnce.posSystemExpireDate = value; }
        }
        public static String PosLoggedStewardUserId
        {
            get { return posStaticLoadOnce.posLoggedStewardUserId; }
            set { posStaticLoadOnce.posLoggedStewardUserId = value; }
        }
        public static String PosLoggedStewardUserName
        {
            get { return posStaticLoadOnce.posLoggedStewardUserName.ToUpper(); }
            set { posStaticLoadOnce.posLoggedStewardUserName = value; }
        }
        public static String PosLoggedStewardUserType
        {
            get { return posStaticLoadOnce.posLoggedStewardUserType; }
            set { posStaticLoadOnce.posLoggedStewardUserType = value; }
        }
        public static String PosLoggedStewardExpireDate
        {
            get { return posStaticLoadOnce.posLoggedStewardExpireDate; }
            set { posStaticLoadOnce.posLoggedStewardExpireDate = value; }
        }
        public static void setCostCenterCode(int index,String val) {
            if (posStaticLoadOnce.posCostCenterCode == null) { posStaticLoadOnce.posCostCenterCode = new String[2]; }
            
            posStaticLoadOnce.posCostCenterCode[index] = val;
        }
        public static String[] PosCostCenterCode {
            get { return posStaticLoadOnce.posCostCenterCode; }
        }
        public static Boolean[] PosUserRightPermission() {
            if (posStaticLoadOnce.posUserRightPermission == null) { posStaticLoadOnce.posUserRightPermission = new Boolean[10]; }
            posClsSettingsUserRights userRights = new posClsSettingsUserRights(null);
            String[,] data= userRights.posGetUserRights(new String[]{posStaticLoadOnce.PosLoggedStewardUserId,posStaticLoadOnce.PosCostCenterCode[0]});

            if (data != null)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (data[0, i].Equals("1"))
                    {
                        posStaticLoadOnce.posUserRightPermission[i] = true;
                    }
                    if (data[0, i].Equals("0") || data[0, i].Equals("") || data[0, i] == null)
                    {
                        posStaticLoadOnce.posUserRightPermission[i] = false;
                    }
                }
            }
            else {
                for (int i = 0; i < 10; i++)
                {
                    
                        posStaticLoadOnce.posUserRightPermission[i] = false;
                    
                }
            }
                return posStaticLoadOnce.posUserRightPermission;
        }
    }
}
