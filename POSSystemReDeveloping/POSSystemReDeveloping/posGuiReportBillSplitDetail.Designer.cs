﻿namespace POSSystemReDeveloping
{
    partial class posGuiReportBillSplitDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.posBtnReportBillSplitDetailPrint = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.posDgvBillSplitDetailReport = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillSplitDetailReport)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.posBtnReportBillSplitDetailPrint);
            this.groupBox1.Location = new System.Drawing.Point(2, -2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1009, 114);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // posBtnReportBillSplitDetailPrint
            // 
            this.posBtnReportBillSplitDetailPrint.Location = new System.Drawing.Point(406, 38);
            this.posBtnReportBillSplitDetailPrint.Name = "posBtnReportBillSplitDetailPrint";
            this.posBtnReportBillSplitDetailPrint.Size = new System.Drawing.Size(203, 39);
            this.posBtnReportBillSplitDetailPrint.TabIndex = 0;
            this.posBtnReportBillSplitDetailPrint.Text = "Print";
            this.posBtnReportBillSplitDetailPrint.UseVisualStyleBackColor = true;
            this.posBtnReportBillSplitDetailPrint.Click += new System.EventHandler(this.posBtnReportBillSplitDetailPrint_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.posDgvBillSplitDetailReport);
            this.groupBox2.Location = new System.Drawing.Point(2, 112);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1008, 616);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // posDgvBillSplitDetailReport
            // 
            this.posDgvBillSplitDetailReport.AllowUserToAddRows = false;
            this.posDgvBillSplitDetailReport.AllowUserToDeleteRows = false;
            this.posDgvBillSplitDetailReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvBillSplitDetailReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.posDgvBillSplitDetailReport.Location = new System.Drawing.Point(3, 16);
            this.posDgvBillSplitDetailReport.Name = "posDgvBillSplitDetailReport";
            this.posDgvBillSplitDetailReport.ReadOnly = true;
            this.posDgvBillSplitDetailReport.Size = new System.Drawing.Size(1002, 597);
            this.posDgvBillSplitDetailReport.TabIndex = 0;
            // 
            // posGuiReportBillSplitDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 733);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "posGuiReportBillSplitDetail";
            this.Text = "posGuiReportBillSplitDetail";
            this.Load += new System.EventHandler(this.posGuiReportBillSplitDetail_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillSplitDetailReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button posBtnReportBillSplitDetailPrint;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView posDgvBillSplitDetailReport;
    }
}