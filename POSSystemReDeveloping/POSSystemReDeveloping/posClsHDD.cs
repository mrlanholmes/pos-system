﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Management;
namespace POSSystemReDeveloping
{
    class posClsHDD : pos
    {
        public  bool checkHDDSerialNumberValidation()
        {
            String hddSerialNumber = this.getHDDSerialNumber();
            if (hddSerialNumber != "")
            {
                return this.checkExistsHDDSerial(hddSerialNumber);
            }
            return false;
        }

        public  String getHDDSerialNumber()
        {
            this.qry = "select * from Win32_PhysicalMedia";
            this.sch = new ManagementObjectSearcher(this.qry);
            foreach (ManagementObject schDt in this.sch.Get())
            {
                if (schDt["SerialNumber"] != null || !schDt["SerialNumber"].Equals(""))
                {
                    return schDt["SerialNumber"].ToString();
                }
            }
            return "";
        }
        private bool checkExistsHDDSerial(String serialNumber)
        {
            this.dt = posStaticLoadOnce.dbExecute("SELECT  count(*) FROM POSHDDSerial where HDDSerial_No = '" + serialNumber.Trim() + "'", "EQ");
            if (Convert.ToInt16(this.dt[0, 0]) == 1)
            {
                return true;
            }
            return false;
        }

        public  bool setHDDSerial()
        {
            this.dt = posStaticLoadOnce.dbExecute("insert into POSHDDSerial(HDDSerial_No,HDDSerial_Machine,HDDSerial_CreatedBy,HDDSerial_CreatedOn) values('" + this.getHDDSerialNumber().Trim() + "','" + this.getMachineName() + "','" + posStaticLoadOnce.PosLoggedUserId + "','" + this.getDateTime()+ "')", "ENQ");
            if (Convert.ToInt16(this.dt[0, 0]) == 1)
            {
                return true;
            }
            return false;
        }
    }
}
