﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public partial class posGuiReportBillSplitDetail : Form
    {
        String newBillNo;
        posDsReportBillDetail dsBillSplitDet;
        public posGuiReportBillSplitDetail()
        {
            InitializeComponent();
            newBillNo = "";
            this.dsBillSplitDet = new posDsReportBillDetail();
        }

        public void posSetBillNo(String[] Data)
        {
            this.newBillNo = Data[0];

        }
            
        private void posGuiReportBillSplitDetail_Load(object sender, EventArgs e)
        {
            posStaticLoadOnce.getConnection();
            String sqlBillSplitDet = null;
            sqlBillSplitDet = "SELECT FODOutletBill.OblM_BillNo,FOMItems.FOIM_ItemDes,FODOutletBill.OblD_Qty,FODOutletBill.OblD_Price,FODOutletBill.OrdM_KOTBOT,"
                        + "FOMOutletBill.OblM_F_Tot,FOMOutletBill.OblM_B_Tot,FOMOutletBill.OblM_C_Tot,FOMOutletBill.OblM_O_Tot,FOMOutletBill.OblM_Stot"
                        + " FROM FODOutletBill INNER JOIN "
                        + "FOMOutletBill ON FODOutletBill.OblM_BillNo = FOMOutletBill.OblM_BillNo INNER JOIN "
                         + "FOMItems ON FODOutletBill.FOIM_ItemNo = FOMItems.FOIM_ItemNo WHERE FODOutletBill.OblM_BillNo ='" + this.newBillNo + "'";

            SqlDataAdapter AdpBillSplitDet = new SqlDataAdapter(sqlBillSplitDet, posStaticLoadOnce.getConnection());

            AdpBillSplitDet.Fill(dsBillSplitDet, "orderDetail");
            posDgvBillSplitDetailReport.DataSource = dsBillSplitDet.Tables[1];

        }

        private void posBtnReportBillSplitDetailPrint_Click(object sender, EventArgs e)
        {
            posCrvBillSplitDetailReport CrvBillSplitDet = new posCrvBillSplitDetailReport();


            CrvBillSplitDet.Show();
            ReportDocument cryRpt = new ReportDocument();
            posCrystalReportBillSplitDetail crBillSplitDetail = new posCrystalReportBillSplitDetail();
            crBillSplitDetail.SetDataSource(dsBillSplitDet.Tables[1]);
            CrvBillSplitDet.posGetBillSplitDetailReportViewer().ReportSource = crBillSplitDetail;
            CrvBillSplitDet.posGetBillSplitDetailReportViewer().Refresh();
        }
    }
}
