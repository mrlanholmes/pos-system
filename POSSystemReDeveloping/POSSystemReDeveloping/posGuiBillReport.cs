﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    public partial class posGuiBillReport : Form
    {
        posGuiReportBillDetail billDetail;
        posDsReportBill dsBill;
        public posGuiBillReport()
        {
            InitializeComponent();
            //this.CmbDs = new posDsC_centerLoad();
            c_centerLoad();
            this.dsBill = new posDsReportBill();
            this.billDetail = new posGuiReportBillDetail();

        }

        private void posCmbReportBillCcenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (posCmbReportBillCcenter.SelectedIndex == 0)
            {
                posTxtBillReportHiddenCenter.Text = "0063";
            }
            else
            {
                posTxtBillReportHiddenCenter.Text = "0080";
            }


            //MessageBox.Show(posTxtBillReportHiddenCenter.Text); 

        }

        void c_centerLoad() {
            posStaticLoadOnce.getConnection();
            String sqlCmb = null;
            sqlCmb = "Select CCent_Code,CCent_Name From FOCostCenter Where CCent_POS = '1'";
            SqlDataAdapter Adp = new SqlDataAdapter(sqlCmb, posStaticLoadOnce.getConnection());
            //Adp.Fill(this.CmbDs, "c_center");
            DataTable dt =new DataTable();
            Adp.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++) {
                //posCmbReportBillCcenter.ValueMember = dt.Columns["CCent_Code"].ToString();
                posCmbReportBillCcenter.Items.Add(dt.Rows[i]["CCent_Name"]);
            }
            posCmbReportBillCcenter.SelectedIndex = 0;
        }

        private void posDtpReportBillFromDate_ValueChanged(object sender, EventArgs e)
        {
            posTxtReportBillFromDate.Text = posDtpReportBillFromDate.Value.Date.ToString("yyyy-MM-dd");
        }

        private void posDtpBillReportToDate_ValueChanged(object sender, EventArgs e)
        {
            posTxtReportBillToDate.Text = posDtpBillReportToDate.Value.Date.ToString("yyyy-MM-dd");
        }

        private void posBtnBillReportSearch_Click(object sender, EventArgs e)
        {
            if (posTxtBillReportHiddenCenter.Text != "" || posTxtReportBillNo.Text != "" || posTxtReportBillSerialNo.Text != "" || posTxtReportBillRoomNo.Text != "" || posTxtReportBillStaffNo.Text != "" || posTxtReportBillStewardNo.Text != "" || posTxtReportBillFromDate.Text != "" || posTxtReportBillToDate.Text != "" && DateTime.Parse(posTxtReportBillFromDate.Text) >= DateTime.Parse(posTxtReportBillToDate.Text))
            {
                ReportDocument cryRpt = new ReportDocument();
                TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
                TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
                ConnectionInfo crConnectionInfo = new ConnectionInfo();
                Tables CrTables;

                crConnectionInfo.ServerName = "SAJANI\\MSSQLSAJANI";
                crConnectionInfo.DatabaseName = "SMARTTBH";
                crConnectionInfo.UserID = "sa";
                crConnectionInfo.Password = "123";

                posStaticLoadOnce.getConnection();
                String sqlBill = null;
                int counter = 0;
                sqlBill = " SELECT FOMOutletBill.OblM_BillNo,FOMOutletBill.OblM_SerialNo,FOMOutletBill.OblM_ERoom,FOMOutletBill.OblM_EStaff,"
                           + "FOMOutletBill.OblM_Date,FOMOutletBill.OblM_Stw,FOMOutletBill.OblM_CusType,FOMOutletBill.OblM_Status,FOMOutletBill.OblM_Stot,"
                           + "FOCostCenter.CCent_Name, FOMOutletBill.POS_SplitStatus FROM FOMOutletBill,FOCostCenter "
                           + "WHERE FOMOutletBill.CCent_Code = FOCostCenter.CCent_Code AND ";

                if (!posTxtBillReportHiddenCenter.Text.Equals(""))
                {
                    sqlBill += "FOMOutletBill.CCent_Code='" + posTxtBillReportHiddenCenter.Text + "'";
                    counter++;
                }

                if (!posTxtReportBillNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sqlBill += " and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_BillNo='" + posTxtReportBillNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportBillSerialNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sqlBill += " and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_SerialNo='" + posTxtReportBillSerialNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportBillRoomNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sqlBill += " and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_ERoom ='" + posTxtReportBillRoomNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportBillStaffNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sqlBill += " and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_EStaff ='" + posTxtReportBillStaffNo.Text + "'";
                    counter++;
                }

                if (!posTxtReportBillStewardNo.Text.Equals(""))
                {
                    if (counter == 1)
                    {
                        sqlBill += "and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_Stw='" + posTxtReportBillStewardNo.Text + "'";
                    counter++;
                }

                if ((!posTxtReportBillFromDate.Text.Equals("")) && (!posTxtReportBillToDate.Text.Equals("")))
                {
                    if (counter == 1)
                    {
                        sqlBill += " and ";
                        counter = 0;
                    }
                    sqlBill += "OblM_Date BETWEEN '" + posTxtReportBillFromDate.Text.ToString() + "' and '" + posTxtReportBillToDate.Text + "'";
                }


                SqlDataAdapter AdpBill = new SqlDataAdapter(sqlBill, posStaticLoadOnce.getConnection());

                //dscmd.Fill(this.ds, "order");

                posStaticLoadOnce.getConnection().Close();
                AdpBill.Fill(this.dsBill, "Bill");
                posDgvReportBill.DataSource = this.dsBill.Tables[1];

               
                
            }
        }

        private void posBtnBillReportPrint_Click(object sender, EventArgs e)
        {
            posGuiCrvReportBill billCrv = new posGuiCrvReportBill();
            billCrv.Show();
            ReportDocument cryRpt = new ReportDocument();
            posCrystalReportBill billRpt = new posCrystalReportBill();
            billRpt.SetDataSource(dsBill.Tables[1]);
            billCrv.posGetBillReportViewer().ReportSource = billRpt;
            billCrv.posGetBillReportViewer().Refresh();


        }

        private void posBtnReportBillDetail_Click(object sender, EventArgs e)
        {
            if (billDetail == null)
            {
                billDetail = new posGuiReportBillDetail();

            }
            String BillId = posDgvReportBill.CurrentRow.Cells[0].Value.ToString();
            this.billDetail.posSetBillNo(new String[] { BillId });
            this.billDetail.Show();
        }

        

       
    }
}
