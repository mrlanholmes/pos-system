﻿namespace POSSystemReDeveloping
{
    partial class posGuiCrvBillSplitReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.posCrvReportBillSplit = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.posCrvReportBillSplit);
            this.groupBox1.Location = new System.Drawing.Point(3, -3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1006, 735);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // posCrvReportBillSplit
            // 
            this.posCrvReportBillSplit.ActiveViewIndex = -1;
            this.posCrvReportBillSplit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posCrvReportBillSplit.Cursor = System.Windows.Forms.Cursors.Default;
            this.posCrvReportBillSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.posCrvReportBillSplit.Location = new System.Drawing.Point(3, 16);
            this.posCrvReportBillSplit.Name = "posCrvReportBillSplit";
            this.posCrvReportBillSplit.Size = new System.Drawing.Size(1000, 716);
            this.posCrvReportBillSplit.TabIndex = 0;
            this.posCrvReportBillSplit.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // posGuiCrvBillSplitReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 733);
            this.Controls.Add(this.groupBox1);
            this.Name = "posGuiCrvBillSplitReport";
            this.Text = "posGuiCrvBillSplitReport";
            this.Load += new System.EventHandler(this.posGuiCrvBillSplitReport_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer posCrvReportBillSplit;
    }
}