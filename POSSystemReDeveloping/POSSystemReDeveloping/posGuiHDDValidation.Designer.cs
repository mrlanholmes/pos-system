﻿namespace POSSystemReDeveloping
{
    partial class posGuiHDDValidation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnHddYes = new System.Windows.Forms.Button();
            this.btnHddNo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnHddYes
            // 
            this.btnHddYes.Location = new System.Drawing.Point(352, 64);
            this.btnHddYes.Name = "btnHddYes";
            this.btnHddYes.Size = new System.Drawing.Size(75, 23);
            this.btnHddYes.TabIndex = 0;
            this.btnHddYes.Text = "Yes";
            this.btnHddYes.UseVisualStyleBackColor = true;
            this.btnHddYes.Click += new System.EventHandler(this.btnHddYes_Click);
            // 
            // btnHddNo
            // 
            this.btnHddNo.Location = new System.Drawing.Point(433, 64);
            this.btnHddNo.Name = "btnHddNo";
            this.btnHddNo.Size = new System.Drawing.Size(75, 23);
            this.btnHddNo.TabIndex = 1;
            this.btnHddNo.Text = "No";
            this.btnHddNo.UseVisualStyleBackColor = true;
            this.btnHddNo.Click += new System.EventHandler(this.btnHddNo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(364, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Do you want to save your Hard Diskserial number?";
            // 
            // posGuiHDDValidation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 89);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnHddNo);
            this.Controls.Add(this.btnHddYes);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "posGuiHDDValidation";
            this.ShowIcon = false;
            this.Text = "posGuiHDDValidation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnHddYes;
        private System.Windows.Forms.Button btnHddNo;
        private System.Windows.Forms.Label label1;
    }
}