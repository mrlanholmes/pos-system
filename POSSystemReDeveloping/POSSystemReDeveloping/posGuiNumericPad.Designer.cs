﻿namespace POSSystemReDeveloping
{
    partial class posGuiNumericPad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.posBtnNumericPadFullKeyBoard = new System.Windows.Forms.Button();
            this.posBtnNumericPadZero = new System.Windows.Forms.Button();
            this.posBtnNumericPadBackSpace = new System.Windows.Forms.Button();
            this.posBtnNumericPadDecimal = new System.Windows.Forms.Button();
            this.posBtnNumericPadThree = new System.Windows.Forms.Button();
            this.posBtnNumericPadTwo = new System.Windows.Forms.Button();
            this.posBtnNumericPadEight = new System.Windows.Forms.Button();
            this.posBtnNumericPadNine = new System.Windows.Forms.Button();
            this.posBtnNumericPadEnter = new System.Windows.Forms.Button();
            this.posBtnNumericPadOne = new System.Windows.Forms.Button();
            this.posBtnNumericPadFour = new System.Windows.Forms.Button();
            this.posBtnNumericPadFive = new System.Windows.Forms.Button();
            this.posBtnNumericPadSix = new System.Windows.Forms.Button();
            this.posBtnNumericPadSeven = new System.Windows.Forms.Button();
            this.posBtnNumericPadExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // posBtnNumericPadFullKeyBoard
            // 
            this.posBtnNumericPadFullKeyBoard.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadFullKeyBoard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadFullKeyBoard.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadFullKeyBoard.Location = new System.Drawing.Point(-2, 256);
            this.posBtnNumericPadFullKeyBoard.Name = "posBtnNumericPadFullKeyBoard";
            this.posBtnNumericPadFullKeyBoard.Size = new System.Drawing.Size(327, 65);
            this.posBtnNumericPadFullKeyBoard.TabIndex = 27;
            this.posBtnNumericPadFullKeyBoard.Text = "Full Key Board";
            this.posBtnNumericPadFullKeyBoard.UseVisualStyleBackColor = false;
            this.posBtnNumericPadFullKeyBoard.Click += new System.EventHandler(this.posBtnNumericPadFullKeyBoard_Click);
            // 
            // posBtnNumericPadZero
            // 
            this.posBtnNumericPadZero.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadZero.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadZero.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadZero.Location = new System.Drawing.Point(62, 192);
            this.posBtnNumericPadZero.Name = "posBtnNumericPadZero";
            this.posBtnNumericPadZero.Size = new System.Drawing.Size(65, 65);
            this.posBtnNumericPadZero.TabIndex = 26;
            this.posBtnNumericPadZero.Text = "0";
            this.posBtnNumericPadZero.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadBackSpace
            // 
            this.posBtnNumericPadBackSpace.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadBackSpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadBackSpace.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadBackSpace.Location = new System.Drawing.Point(126, 192);
            this.posBtnNumericPadBackSpace.Name = "posBtnNumericPadBackSpace";
            this.posBtnNumericPadBackSpace.Size = new System.Drawing.Size(199, 65);
            this.posBtnNumericPadBackSpace.TabIndex = 25;
            this.posBtnNumericPadBackSpace.Text = "Backspace";
            this.posBtnNumericPadBackSpace.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadDecimal
            // 
            this.posBtnNumericPadDecimal.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadDecimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadDecimal.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadDecimal.Location = new System.Drawing.Point(-2, 193);
            this.posBtnNumericPadDecimal.Name = "posBtnNumericPadDecimal";
            this.posBtnNumericPadDecimal.Size = new System.Drawing.Size(65, 65);
            this.posBtnNumericPadDecimal.TabIndex = 24;
            this.posBtnNumericPadDecimal.Text = ".";
            this.posBtnNumericPadDecimal.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadThree
            // 
            this.posBtnNumericPadThree.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadThree.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadThree.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadThree.Location = new System.Drawing.Point(126, 128);
            this.posBtnNumericPadThree.Name = "posBtnNumericPadThree";
            this.posBtnNumericPadThree.Size = new System.Drawing.Size(65, 65);
            this.posBtnNumericPadThree.TabIndex = 23;
            this.posBtnNumericPadThree.Text = "3";
            this.posBtnNumericPadThree.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadTwo
            // 
            this.posBtnNumericPadTwo.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadTwo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadTwo.Location = new System.Drawing.Point(62, 128);
            this.posBtnNumericPadTwo.Name = "posBtnNumericPadTwo";
            this.posBtnNumericPadTwo.Size = new System.Drawing.Size(65, 65);
            this.posBtnNumericPadTwo.TabIndex = 22;
            this.posBtnNumericPadTwo.Text = "2";
            this.posBtnNumericPadTwo.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadEight
            // 
            this.posBtnNumericPadEight.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadEight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadEight.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadEight.Location = new System.Drawing.Point(62, 1);
            this.posBtnNumericPadEight.Name = "posBtnNumericPadEight";
            this.posBtnNumericPadEight.Size = new System.Drawing.Size(65, 65);
            this.posBtnNumericPadEight.TabIndex = 21;
            this.posBtnNumericPadEight.Text = "8";
            this.posBtnNumericPadEight.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadNine
            // 
            this.posBtnNumericPadNine.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadNine.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadNine.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadNine.Location = new System.Drawing.Point(126, 1);
            this.posBtnNumericPadNine.Name = "posBtnNumericPadNine";
            this.posBtnNumericPadNine.Size = new System.Drawing.Size(65, 65);
            this.posBtnNumericPadNine.TabIndex = 20;
            this.posBtnNumericPadNine.Text = "9";
            this.posBtnNumericPadNine.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadEnter
            // 
            this.posBtnNumericPadEnter.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadEnter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadEnter.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadEnter.Location = new System.Drawing.Point(190, 64);
            this.posBtnNumericPadEnter.Name = "posBtnNumericPadEnter";
            this.posBtnNumericPadEnter.Size = new System.Drawing.Size(135, 129);
            this.posBtnNumericPadEnter.TabIndex = 19;
            this.posBtnNumericPadEnter.Text = "Enter";
            this.posBtnNumericPadEnter.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadOne
            // 
            this.posBtnNumericPadOne.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadOne.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadOne.Location = new System.Drawing.Point(-2, 129);
            this.posBtnNumericPadOne.Name = "posBtnNumericPadOne";
            this.posBtnNumericPadOne.Size = new System.Drawing.Size(65, 65);
            this.posBtnNumericPadOne.TabIndex = 18;
            this.posBtnNumericPadOne.Text = "1";
            this.posBtnNumericPadOne.UseVisualStyleBackColor = false;
            this.posBtnNumericPadOne.Click += new System.EventHandler(this.posBtnNumericPadOne_Click);
            // 
            // posBtnNumericPadFour
            // 
            this.posBtnNumericPadFour.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadFour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadFour.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadFour.Location = new System.Drawing.Point(-2, 65);
            this.posBtnNumericPadFour.Name = "posBtnNumericPadFour";
            this.posBtnNumericPadFour.Size = new System.Drawing.Size(65, 65);
            this.posBtnNumericPadFour.TabIndex = 17;
            this.posBtnNumericPadFour.Text = "4";
            this.posBtnNumericPadFour.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadFive
            // 
            this.posBtnNumericPadFive.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadFive.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadFive.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadFive.Location = new System.Drawing.Point(62, 64);
            this.posBtnNumericPadFive.Name = "posBtnNumericPadFive";
            this.posBtnNumericPadFive.Size = new System.Drawing.Size(65, 65);
            this.posBtnNumericPadFive.TabIndex = 16;
            this.posBtnNumericPadFive.Text = "5";
            this.posBtnNumericPadFive.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadSix
            // 
            this.posBtnNumericPadSix.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadSix.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadSix.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadSix.Location = new System.Drawing.Point(126, 64);
            this.posBtnNumericPadSix.Name = "posBtnNumericPadSix";
            this.posBtnNumericPadSix.Size = new System.Drawing.Size(65, 65);
            this.posBtnNumericPadSix.TabIndex = 15;
            this.posBtnNumericPadSix.Text = "6";
            this.posBtnNumericPadSix.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadSeven
            // 
            this.posBtnNumericPadSeven.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadSeven.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadSeven.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadSeven.Location = new System.Drawing.Point(-2, 1);
            this.posBtnNumericPadSeven.Name = "posBtnNumericPadSeven";
            this.posBtnNumericPadSeven.Size = new System.Drawing.Size(65, 65);
            this.posBtnNumericPadSeven.TabIndex = 14;
            this.posBtnNumericPadSeven.Text = "7";
            this.posBtnNumericPadSeven.UseVisualStyleBackColor = false;
            // 
            // posBtnNumericPadExit
            // 
            this.posBtnNumericPadExit.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.posBtnNumericPadExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnNumericPadExit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.posBtnNumericPadExit.Location = new System.Drawing.Point(190, 1);
            this.posBtnNumericPadExit.Name = "posBtnNumericPadExit";
            this.posBtnNumericPadExit.Size = new System.Drawing.Size(135, 65);
            this.posBtnNumericPadExit.TabIndex = 28;
            this.posBtnNumericPadExit.Text = "Exit";
            this.posBtnNumericPadExit.UseVisualStyleBackColor = false;
            // 
            // posGuiNumericPad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 323);
            this.ControlBox = false;
            this.Controls.Add(this.posBtnNumericPadExit);
            this.Controls.Add(this.posBtnNumericPadFullKeyBoard);
            this.Controls.Add(this.posBtnNumericPadZero);
            this.Controls.Add(this.posBtnNumericPadBackSpace);
            this.Controls.Add(this.posBtnNumericPadDecimal);
            this.Controls.Add(this.posBtnNumericPadThree);
            this.Controls.Add(this.posBtnNumericPadTwo);
            this.Controls.Add(this.posBtnNumericPadEight);
            this.Controls.Add(this.posBtnNumericPadNine);
            this.Controls.Add(this.posBtnNumericPadEnter);
            this.Controls.Add(this.posBtnNumericPadOne);
            this.Controls.Add(this.posBtnNumericPadFour);
            this.Controls.Add(this.posBtnNumericPadFive);
            this.Controls.Add(this.posBtnNumericPadSix);
            this.Controls.Add(this.posBtnNumericPadSeven);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "posGuiNumericPad";
            this.ShowInTaskbar = false;
            this.Text = "Numeric Key Pad";
            this.Load += new System.EventHandler(this.posGuiNumericPad_Load);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button posBtnNumericPadFullKeyBoard;
        internal System.Windows.Forms.Button posBtnNumericPadZero;
        internal System.Windows.Forms.Button posBtnNumericPadBackSpace;
        internal System.Windows.Forms.Button posBtnNumericPadDecimal;
        internal System.Windows.Forms.Button posBtnNumericPadThree;
        internal System.Windows.Forms.Button posBtnNumericPadTwo;
        internal System.Windows.Forms.Button posBtnNumericPadEight;
        internal System.Windows.Forms.Button posBtnNumericPadNine;
        internal System.Windows.Forms.Button posBtnNumericPadEnter;
        internal System.Windows.Forms.Button posBtnNumericPadOne;
        internal System.Windows.Forms.Button posBtnNumericPadFour;
        internal System.Windows.Forms.Button posBtnNumericPadFive;
        internal System.Windows.Forms.Button posBtnNumericPadSix;
        internal System.Windows.Forms.Button posBtnNumericPadSeven;
        internal System.Windows.Forms.Button posBtnNumericPadExit;

    }
}