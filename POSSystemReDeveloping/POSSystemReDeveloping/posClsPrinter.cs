﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace POSSystemReDeveloping
{
    class posClsPrinter : pos
    {
        private posClsLog log;
        public posClsPrinter() {
            log = new posClsLog();
        }
        // Get Printer name from FOCostCenterItemPrice
        private String[,] posGetPrinterName(String[] data) {
            this.qry = "select Printer_Name from FOCostCenterItemPrice where FOIM_ItemNo='" + data[0] + "' AND CCent_Code='" + data[1] + "' and [SaTy_Code] = '" + data[2] + "' and Printer_Name is not NULL";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0) {
                return this.dt;
            }
            return null;
        }
        // Get printer properties from PrinterProperties
        private String[,] posGetPrinterProperties(String[] data)
        {
            this.qry = "select * from [PrinterProperties] where [Printer Name] = '"+data[0]+"' ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0)
            {
                return this.dt;
            }
            return null;
        }
        // Insert temporary data into Temperary_ItemData
        private bool posInsertTempItemData(String[] data) {
            this.qry = "insert into Temperary_ItemData(Temperary_ItemData_OrderNo,Temperary_ItemData_ItemCode,"
            +"Temperary_ItemData_ItemName,Temperary_ItemData_ItemQty,Temperary_ItemData_ItemPrinter,Temperary_ItemData_ItemRemarks)"
            + "values('" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "')";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt16(this.dt[0, 0]) == 1) { return true; }
            return false;
        }
        // Get Temporary Item Data from Temperary_ItemData
        private String[,] posGetTempItemData(String[] data) {
            this.qry = "select * from Temperary_ItemData where Temperary_ItemData_OrderNo='"+data[0]+"' and Temperary_ItemData_ItemPrinter='"+data[1]+"'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0)
            {
                return this.dt;
            }
            return null;
        }
        // Get IP number and port from PrinterProperties
        private String[,] posGetPrinterIPAndPort(String[] data)
        {
            this.qry = "select [IP Address],Port from PrinterProperties where [Printer Name]='"+data[0]+"'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0)
            {
                return this.dt;
            }
            return null;
        }
        // Print Status update
        private bool posUpdateOrderPrintStatus(String[] data) {
            this.qry = "update FOOrderMaster set OrdM_POSPrintStatus='PR' where OrdM_No='"+data[0]+"'";
            if (Convert.ToInt16(posStaticLoadOnce.dbExecute(this.qry, "ENQ")[0, 0]) == 1 ) { return true; }
            return false;
        }
        private bool posUpdateOrderItemPrintStatus(String[] data)
        {
            this.qry = "update FOOrderDetail set Printed_IP='"+data[0]+"' where OrdM_No='"+data[1]+"' and FOIM_ItemNo='"+data[2]+"'";
            if (Convert.ToInt16(posStaticLoadOnce.dbExecute(this.qry, "ENQ")[0, 0]) == 1) { return true; }
            return false;
        }
        //private bool posUpdateOrderPrintStatus(String[] data)
        //{insert into POSLog values(@user,@status,@createdby,@createdon
        //    this.qry = "update FOOrderMaster set OrdM_POSPrintStatus='PR' where OrdM_No='" + data[0] + "'";
        //    if (Convert.ToInt16(posStaticLoadOnce.dbExecute(this.qry, "ENQ")[0, 0]) == 1) { return true; }
        //    return false;
        //}
        // get print details according printer
        private String[,] posGetPrinterDetails(String[] data)
        {
            this.qry = "select * from [Temperary_ItemData] where [Temperary_ItemData_ItemPrinter] ='" + data[0] + "' and [Temperary_ItemData_Status] = 'Y'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0)
            {
                return this.dt;
            }
            return null;
        }
        //  get print details from temperary item data
        private String[,] posGetPrinterFromTempItemDataTable() {
            this.qry = "  select Temperary_ItemData.Temperary_ItemData_ItemPrinter from [Temperary_ItemData] where [Temperary_ItemData_Status] = 'Y' group by Temperary_ItemData_ItemPrinter ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry,"EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return this.dt; }
            return null;
        }
        private bool posUpdatePrinterTempDataStatus(String[] data)
        {
            this.qry = "  update [Temperary_ItemData] set [Temperary_ItemData_Status] = 'N' where [Temperary_ItemData_ItemPrinter] ='"+data[0]+"'; ";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt16(this.dt[0, 0]) == 1) { return true; }
            return false;
        }
        private bool posUpdateOrderCancelTemp(String[] data)
        {
            this.qry = "  update [OrdCancel_Temp] set [OrdCancel_Status]='CN' where [OrdCancel_OrdM_No]='" + data[0] + "' and [OrdCancel_Printed_IP] ='" + data[1] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt16(this.dt[0, 0]) == 1) { return true; }
            return false;
        }
        // set FOD details
        private bool posInsertFODBill(String[] data)
        {
            this.qry = "insert into FODOutletBill(OblD_Cost,OblM_BillNo,FOIM_ItemNo,OblD_Price,OblD_Qty,OrdM_KOTBOT) values(" +data[0] +",'" + data[1] + "','" + data[2] + "'," + data[3] + "," + data[4] + ",'" + data[5] + "')";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
            if (Convert.ToInt16(this.dt[0, 0]) == 1)
            {
                return true;
            }
            return false;
        }
        private String posGetBillItemOrdDCost(String[] data)
        {
            this.qry = "select OrdD_Cost  from [FOOrderDetail] where OrdM_No='" + data[0] + "' and FOIM_ItemNo = '" + data[1] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) {
                return this.dt[0, 0];
            }
            return null;
        }
        private String posGetOrderKOTBOTNo(String[] data) {
            this.qry = "  select OrdM_KOTBOT from FOOrderMaster where OrdM_No = '"+data[0]+"'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (this.dt != null) {
                return this.dt[0, 0];
            }
            return null;
        }
        // Print a Order
        public bool posPrintOrderItems(String[] data,String[,] itemData,int printItemCounter) {
            String[,] itemPrinterDetails = new String[printItemCounter,3];            
            int printerInsertCount = 0;            

            for (int i = 0; i < printItemCounter; i++) {   
                // Set Printer Name
                try {
                    itemPrinterDetails[0, 0] = this.posGetPrinterName(new String[] { itemData[i, 0].Trim(), posStaticLoadOnce.PosCostCenterCode[0], data[17] })[0, 0];

                    this.qry = " insert into [Temperary_ItemData]([Temperary_ItemData_OrderNo],[Temperary_ItemData_ItemCode],[Temperary_ItemData_ItemName]"
                                + ",[Temperary_ItemData_ItemQty],[Temperary_ItemData_ItemPrinter] ,[Temperary_ItemData_ItemRemarks],[Temperary_ItemData_Status]) values "
                                + "('" + data[6] + "','" + itemData[i, 0] + "','" + itemData[i, 1] + "','" + itemData[i, 2] + "','" + itemPrinterDetails[0, 0] + "','" + itemData[i, 4] + "','Y')";
                    this.dt = posStaticLoadOnce.dbExecute(this.qry, "ENQ");
                    if (Convert.ToInt16(this.dt[0, 0]) == 1)
                    {
                        printerInsertCount++;
                    }
                }
                catch (Exception) { }
                
            }
            
            String[,] printer = this.posGetPrinterFromTempItemDataTable();
            int printerCount = posStaticLoadOnce.PosRowCount;

            for (int i = 0; i < printerCount; i++) {
                String[,] printerDetails = posGetPrinterProperties(new String[] {printer[i,0]});
                String[,] printPrinterItem = this.posGetPrinterDetails(new String[]{printer[i,0]});
                int printPrinterItemCount = posStaticLoadOnce.PosRowCount;
                try { this.posSendToPrintPrinterDetails(printerDetails, data, printPrinterItem, printPrinterItemCount); }
                catch (Exception) { }
                for (int k = 0; k < printPrinterItemCount; k++) {
                    this.posUpdateOrderItemPrintStatus(new String[] { printerDetails[0, 1], data[0], printPrinterItem[k, 1] });
                }
                    this.posUpdatePrinterTempDataStatus(new String[] { printer[i, 0] });             
            }           
            return false;
        }
        private String[,] posGetOrderDetails(String[] data) {
            this.qry = "  select OrdM_KOTBOT,OrdM_Table,OrdM_Room, SaTy_Code from FOOrderMaster where OrdM_No='" + data[0] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return this.dt; }
            return null;
        }
        private String[,] posGetOrderCancelIP(String[] data) {
            this.qry = "  select Printed_IP from FOOrderDetail where OrdM_No='"+data[0]+"' group by Printed_IP";
            this.dt = posStaticLoadOnce.dbExecute(this.qry,"EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return this.dt; }
            return null;
        }
        private String[,] posGetOrderItemCancelIP(String[] data)
        {
            this.qry = "   select [OrdCancel_Printed_IP] from [OrdCancel_Temp] where [OrdCancel_OrdM_No]='" + data[0] + "' and [OrdCancel_Status] ='AC' and [OrdCancel_Printed_IP] is not NULL group by [OrdCancel_Printed_IP]";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return this.dt; }
            return null;
        }
        private String[,] posGetOrderItemAccordingIp(String[] data) {
            this.qry = "select FOOrderDetail.FOIM_ItemNo,( select FOIM_ItemDes from FOMItems where FOIM_ItemNo= FOOrderDetail.FOIM_ItemNo)"
                        + " as ItemName,FOOrderDetail.OrdD_Qty,FOOrderDetail.OrdD_Remark,"
                        +"(select PrinterProperties.Port from PrinterProperties where PrinterProperties.[IP Address] = FOOrderDetail.Printed_IP"
                        + " group by PrinterProperties.[IP Address],PrinterProperties.Port) from FOOrderDetail where FOOrderDetail.OrdM_No='" + data[0] + "' and FOOrderDetail.Printed_IP='" + data[1] + "' and FOOrderDetail.Item_Status <> 'CN'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return this.dt; }
            return null;
        }
        private String[,] posGetOrderCancelItemAccordingIp(String[] data)
        {
            this.qry = "select [OrdCancel_Item_No],( select FOIM_ItemDes from FOMItems where FOIM_ItemNo= [OrdCancel_Temp].[OrdCancel_Item_No]) as ItemName,"
                        +"[OrdCancel_Qty],(select PrinterProperties.Port from PrinterProperties where PrinterProperties.[IP Address] = [OrdCancel_Temp].[OrdCancel_Printed_IP]"
                        + "group by PrinterProperties.[IP Address],PrinterProperties.Port) from [OrdCancel_Temp] where [OrdCancel_Temp].[OrdCancel_Status] <> 'CN' and [OrdCancel_OrdM_No] = '" + data[0] + "' and [OrdCancel_Printed_IP]='" + data[1] + "'";
            this.dt = posStaticLoadOnce.dbExecute(this.qry, "EQ");
            if (posStaticLoadOnce.PosRowCount > 0) { return this.dt; }
            return null;
        }
        public bool posPrintOrderCancel(String[] data) {
            String[,] printedIp = this.posGetOrderCancelIP(new String[] { data[0] });
            int printedIpCount = posStaticLoadOnce.PosRowCount;
            String[,] orderDetails = this.posGetOrderDetails(new String[]{data[0]});
            String[,] printedItem;
            int printedItemCount;
            for (int i = 0; i < printedIpCount; i++) {
                printedItem = this.posGetOrderItemAccordingIp(new String[]{data[0],printedIp[i,0]});
                printedItemCount = posStaticLoadOnce.PosRowCount;
                this.posPrintOrderCancelPrint(new String[,] {{printedIp[i,0],printedItem[i,4]}},orderDetails,printedItem,printedItemCount);
            }
                return false;
        }
        public bool posPrintOrderItemCancel(String[] data,String[] cancelitem,int cancelItemCount){
            String[,] printedIp= this.posGetOrderItemCancelIP(new String[]{data[0]});           
            int printedIpCount = posStaticLoadOnce.PosRowCount;
            String[,] orderDetails = this.posGetOrderDetails(new String[] { data[0] });
            String[,] printedItem;
            int printedItemCount;
            for (int i = 0; i < printedIpCount; i++)
            {
                printedItem = this.posGetOrderCancelItemAccordingIp(new String[] { data[0], printedIp[i, 0] });
                printedItemCount = posStaticLoadOnce.PosRowCount;
                try { this.posPrintOrderItemCancelPrint(new String[,] { { printedIp[i, 0], printedItem[i, 3] } }, orderDetails, printedItem, printedItemCount); }
                catch (Exception) { }
                
                this.posUpdateOrderCancelTemp(new String[] {data[0],printedIp[i,0]});
            }
            return false;
        }
        private bool posPrintOrderItemCancelPrint(String[,] PrinterData, String[,] data, String[,] itemData, int printItemCounter)
        {
            TcpClient client = new TcpClient();
            client.Connect(PrinterData[0, 0].Replace(" ",""), Convert.ToInt16(PrinterData[0, 1]));
            //client.Connect("192.168.10.79", 9100);
            //client.Connect("192.168.123.64", 9100);
            StreamWriter writer = new StreamWriter(client.GetStream());
            //StreamWriter writer = new StreamWriter("test.txt");
            writer.WriteLine("\t" + posStaticLoadOnce.posGetCompanyName()[0, 0]);
            writer.WriteLine("\t\t" + posStaticLoadOnce.PosCostCenterCode[1] + " " + "(" + data[0, 3] + ")");
            writer.WriteLine();
            writer.Write("########--Canceled Ordered Item--#######");
            writer.WriteLine();
            writer.Write("KOT/BOT No    : ");
            writer.WriteLine(data[0, 0]);
            writer.Write("Table No      : ");
            writer.WriteLine(data[0, 1]);
            writer.Write("Room No       : ");
            writer.WriteLine(data[0, 2]);
            writer.Write("Steward Name  : ");
            writer.WriteLine(posStaticLoadOnce.PosLoggedStewardUserId);
            writer.WriteLine();
            writer.Write("Order Items");
            writer.Write("\t");
            writer.Write("\t");
            writer.Write("\t");
            writer.WriteLine("Qty");
            writer.WriteLine("----------------------------------------");
            int i;
            String txtConcat = "";
            String str = "";
            String remainTxt = "";
            for (i = 0; i < printItemCounter; i++)
            {
                // itemcode = Cost_Center.TempItem_Grid.Rows(i).Cells(0).Value;
                //update_itemprIP(ipAddress, itemcode);
                writer.WriteLine(itemData[i, 0] + "(Canceled Ordered Item)");
                str = itemData[i, 1];
                if (str.ToString().Length > 30)
                {
                    writer.Write("(" + str.Substring(0, 30));
                    writer.WriteLine();
                    remainTxt = str.Substring(30, (str.Length - 30)) + ")";
                    writer.Write(remainTxt);
                    for (int p = 0; p < (33 - (remainTxt.Length)); p++) { txtConcat += " "; }
                    writer.Write(txtConcat + itemData[i, 2]);
                }
                else
                {
                    writer.Write("(" + str + ")");

                    for (int p = 0; p < (31 - str.Length); p++) { txtConcat += " "; }

                    writer.Write(txtConcat + itemData[i, 2]);
                }
                //writer.WriteLine("(" + itemData[i, 1] + ")\t\t" + itemData[i, 2]);
                writer.WriteLine();
                txtConcat = "";
            }
            writer.WriteLine("----------------------------------------");

            writer.Write("Remarks: ");
            writer.WriteLine("#### Canceled Ordered Item ####");

            writer.WriteLine();
            writer.WriteLine();
            writer.WriteLine("         ---------------------          ");
            writer.WriteLine("            Guest Signature            ");
            writer.WriteLine();
            writer.Write("Date/Time: ");
            writer.WriteLine(this.getDateTime());
            writer.WriteLine("----------------------------------------");
            writer.Write("         [ i-Smart POS...  ] ");
            writer.WriteLine("\n");
            writer.WriteLine();
            writer.WriteLine();
            writer.WriteLine();
            writer.WriteLine("\n");
            writer.Flush();
            writer.Close();

            this.log.posWriteSystemLogin(new String[] { "#Canceled Ordered Item# Printed to Ip:" + PrinterData[0, 1] + " ItemCount:" + printItemCounter });
            return false;
        }
        private bool posPrintOrderCancelPrint(String[,] PrinterData, String[,] data, String[,] itemData, int printItemCounter)
        {
            TcpClient client = new TcpClient();
            client.Connect(PrinterData[0, 0].Replace(" ",""), Convert.ToInt16(PrinterData[0,1]));
            //client.Connect("192.168.123.64", 9100);
            StreamWriter writer = new StreamWriter(client.GetStream());
            writer.WriteLine("\t" + posStaticLoadOnce.posGetCompanyName()[0, 0]);
            writer.WriteLine("\t\t" + posStaticLoadOnce.PosCostCenterCode[1] + " " + "(" + data[0,3] + ")");
            writer.WriteLine();
            writer.Write(" Canceled Order####Canceled Order####");
            writer.WriteLine();
            writer.Write("KOT/BOT No    : ");
            writer.WriteLine(data[0,0]);
            writer.Write("Table No      : ");
            writer.WriteLine(data[0,1]);
            writer.Write("Room No       : ");
            writer.WriteLine(data[0,2]);
            writer.Write("Steward Name  : ");
            writer.WriteLine(posStaticLoadOnce.PosLoggedStewardUserId);
            writer.WriteLine();
            writer.Write("Order Items");
            writer.Write("\t");
            writer.Write("\t");
            writer.Write("\t");
            writer.WriteLine("Qty");
            writer.WriteLine("----------------------------------------");
            int i;
            String txtConcat = "";
            String str = "";
            String remainTxt = "";
            for (i = 0; i < printItemCounter; i++)
            {
                // itemcode = Cost_Center.TempItem_Grid.Rows(i).Cells(0).Value;
                //update_itemprIP(ipAddress, itemcode);
                writer.WriteLine(itemData[i, 0] + "(Canceled Order Item)");
                str = itemData[i,1];
                if (str.ToString().Length > 30)
                {
                    writer.Write("(" + str.Substring(0, 30));
                    writer.WriteLine();
                    remainTxt = str.Substring(30, (str.Length - 30)) + ")";
                    writer.Write(remainTxt);
                    for (int p = 0; p < (33 - (remainTxt.Length)); p++) { txtConcat += " "; }
                    writer.Write(txtConcat + itemData[i, 2]);
                }
                else
                {
                    writer.Write("(" + str + ")");

                    for (int p = 0; p < (31 - str.Length); p++) { txtConcat += " "; }

                    writer.Write(txtConcat + itemData[i, 2]);
                }
                //writer.WriteLine("(" + itemData[i, 1] + ")\t\t" + itemData[i, 2]);
                writer.WriteLine();
                txtConcat = "";
            }
            writer.WriteLine("----------------------------------------");

            writer.Write("Remarks: ");
            writer.WriteLine("#####Canceled Order#####");

            writer.WriteLine();
            writer.WriteLine();
            writer.WriteLine("          ----------------------          ");
            writer.WriteLine("             Guest Signature            ");
            writer.WriteLine();
            writer.Write("Date/Time: ");
            writer.WriteLine(this.getDateTime());
            writer.WriteLine("----------------------------------------");
            writer.Write("         [ i-Smart POS...  ] ");
            writer.WriteLine("\n");
            writer.WriteLine();
            writer.WriteLine();
            writer.WriteLine();
            writer.WriteLine("\n");
            writer.Flush();
            writer.Close();

            this.log.posWriteSystemLogin(new String[] { "#Canceled Order# Printed to Ip:" + PrinterData[0, 1] + " ItemCount:" + printItemCounter });
            return false;
        }
        //  m
        private bool posSendToPrintPrinterDetails(String[,] PrinterData,String[] data,String[,] itemData,int printItemCounter) {
                TcpClient client = new TcpClient();
                client.Connect(PrinterData[0,1].Replace(" ",""), Convert.ToInt16(PrinterData[0,3])); 
                //client.Connect("192.168.123.64", 9100);
                StreamWriter writer = new StreamWriter(client.GetStream());
               // StreamWriter writer = new StreamWriter("test.txt");
                writer.WriteLine("\t" + posStaticLoadOnce.posGetCompanyName()[0,0]);
                writer.WriteLine("\t\t" + posStaticLoadOnce.PosCostCenterCode[1] + " " + "(" + data[17] + ")");
                writer.WriteLine();
                writer.Write("KOT/BOT No    : ");
                writer.WriteLine(data[6]);
                writer.Write("Table No      : ");
                writer.WriteLine(data[5]);
                writer.Write("Room No       : ");
                writer.WriteLine(data[10]);
                writer.Write("Steward Name  : ");
                writer.WriteLine(posStaticLoadOnce.PosLoggedStewardUserId);
                writer.Write("         PAX  : ");
                writer.WriteLine(data[19]);
                writer.WriteLine();
                writer.Write("Order Items");
                writer.Write("\t");
                writer.Write("\t");
                writer.Write("\t");
                writer.WriteLine("Qty");
                writer.WriteLine("---------------------------------");
                int i;
                String txtConcat = "";
                String str = "";
                String remainTxt = "";
               // int txtCount = 0;
                String basicTxt = "";
                for(i=0;i<printItemCounter;i++){
                   // itemcode = Cost_Center.TempItem_Grid.Rows(i).Cells(0).Value;
                    //update_itemprIP(ipAddress, itemcode);
                    writer.Write(itemData[i,1] +"(" );

                                                
                    str = itemData[i,5];

                    basicTxt = str;
                    for (int pt = 1; pt >0; pt++) {
                        if (str.ToString().Length > 25)
                        {
                            writer.Write(str.Substring(0, 25));
                            writer.WriteLine();
                            if ((str.Length - 25) <= 25) { writer.Write(str); break; }
                            str = basicTxt.Substring(26, str.Length-26);
                        }
                        else { writer.Write(str); break; }                                 
                    }
                        
                    writer.WriteLine(")");
                    txtConcat = "";
//############################################################################################
                    str = itemData[i, 2];
                    if (str.ToString().Length > 30)
                    {                        
                         writer.Write("(" + str.Substring(0, 30) );
                         writer.WriteLine();
                         remainTxt = str.Substring(30, (str.Length - 30)) + ")";
                         writer.Write(remainTxt);                        
                         for (int p = 0; p < (33 - (remainTxt.Length)); p++) { txtConcat += " "; }
                         writer.Write(txtConcat + itemData[i, 3]);
                    }
                    else
                    {                        
                        writer.Write("(" + str + ")");
                        
                            for (int p = 0; p < (31 - str.Length); p++) { txtConcat += " "; }
                        
                        writer.Write(txtConcat + itemData[i, 3]);
                    }
                    writer.WriteLine();
                    txtConcat = "";
                    this.posUpdateOrderItemPrintStatus(new String[] { PrinterData[0, 1], data[0],itemData[i,1] });
                    
                }        
                writer.WriteLine("---------------------------------");
               
                writer.Write("Remarks: ");
                writer.WriteLine(data[18]);
                
                writer.WriteLine();
                writer.WriteLine();
                writer.WriteLine("          --------------------          ");
                writer.WriteLine("             Guest Signature            ");
                writer.WriteLine();
                writer.Write("Date/Time: ");
                writer.WriteLine(this.getDateTime());
                writer.WriteLine("---------------------------------");
                writer.Write("          [ i-Smart POS...  ] ");
                writer.WriteLine("\n");
                writer.WriteLine();
                writer.WriteLine();
                writer.WriteLine();
                writer.WriteLine("\n");
                writer.Flush();
                writer.Close();
                this.posUpdateOrderPrintStatus(new String[]{data[0]});
                this.log.posWriteSystemLogin(new String[] { "Printed to Ip:" + PrinterData[0, 1] + " ItemCount:" + printItemCounter });
            return false;
        }
        
        public bool posPrintBillPrepare(String[] data, String[,] itemData, int printItemCounter,posClsBill billObject,posGuiBill guiBillObject)
        {
            this.posPrintBill(data,itemData,printItemCounter,billObject,guiBillObject);
            return false;
        }
        private String posSetCurrencyFormat(String cur, int length)
        {
            int lenghtCur = cur.Length;
            String str = "";
            for (int i = 0; i < length-lenghtCur; i++)
            {
                
                    str += " ";
                
            }
            str += cur;
            return str;
        }
        private String posGetEditedString(String str) {
            if (str.Length > 16) {
                str=str.Substring(0, 16);
            }
            return str;
        }
        public bool posPrintBill( String[] data, String[,] itemData, int printItemCounter,posClsBill billObject,posGuiBill guiBill)
        {
            //TcpClient client = new TcpClient();
            //client.Connect("192.168.123.64", 9100);

            ////client.Connect("192.168.123.64", 9100);
            //StreamWriter writer = new StreamWriter(client.GetStream());
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.Write(""+this.getDate());
            //writer.Write(" " + data[0]);// table no
            //writer.Write("      " + data[1]);// steward code
            //writer.Write("  " + data[2]); // bill no
            //writer.Write(" " + data[3]); // serial No
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.Write("       "+posStaticLoadOnce.PosCostCenterCode[0]);
            //writer.Write("       "+data[4]); // no of pax
            //writer.Write("              " + data[5]);//room no

            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //for (int i = 0; i < printItemCounter; i++)
            //{
            //    writer.Write("     "+itemData[i, 0]);
            //    writer.Write(" "+ itemData[i, 1]);
            //    writer.Write(" "+ itemData[i, 2]);
            //    writer.Write(" "+ itemData[i, 3]);
            //    writer.WriteLine();
                
            //}

            
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            
            //writer.WriteLine();
            //writer.Write("                                    "+data[7]);//total
            //writer.WriteLine();
            //writer.Write("                                    "+data[8]);// discount
            //writer.WriteLine(); 
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.Write("         " + data[6]);//Guest name
            //writer.Write("           " + data[5]);//room no
            
            //writer.Flush();
            //writer.Close();
            PrintDocument p = new PrintDocument();
            p.PrintPage += delegate(object sender, PrintPageEventArgs e)
            {               
                e.Graphics.DrawString( this.getDate(), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(5,120));
                e.Graphics.DrawString( data[0], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(120,120));// table no
                e.Graphics.DrawString( data[1], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(200, 120));// steward code
                e.Graphics.DrawString( data[2], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(265, 120)); // bill no
                e.Graphics.DrawString( data[3], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, 120)); // serial No                
                
                e.Graphics.DrawString( posStaticLoadOnce.PosCostCenterCode[1], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(35,150));
                e.Graphics.DrawString( data[4], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(210, 150)); // no of pax
                e.Graphics.DrawString("(Split-" + ")", new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(225, 150)); // Split bill comment
                e.Graphics.DrawString( data[5], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, 150));//room no

                int startPostionCounter = 205;
                 String tableNo = guiBill.posDgvBillTableNumber.CurrentRow.Cells[0].Value.ToString();
                 String orderno = "";                 
                int itemCount =0;

                 for (int i = 0; i < guiBill.posDgvBillTableDetails.Rows.Count; i++)
                 {
                     orderno = guiBill.posDgvBillTableDetails.Rows[i].Cells[0].Value.ToString();
                     e.Graphics.DrawString(orderno, new Font("Times New Roman", 10), new SolidBrush(Color.Black), new Point(0, startPostionCounter));
                     itemData = billObject.posGetOrderItemDetailsPrint(new String[] { tableNo, orderno });
                     itemCount = posStaticLoadOnce.PosRowCount;
                     for ( int j = 0; j < itemCount; j++)
                     {
                         e.Graphics.DrawString(this.posGetEditedString(itemData[j, 0]), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(65, startPostionCounter)); // item description
                         e.Graphics.DrawString(this.posSetCurrencyFormat(Math.Round(Convert.ToDecimal(itemData[j, 3]), 2).ToString().Replace(".00", ""), 4), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(245, startPostionCounter)); // item qty
                         e.Graphics.DrawString(this.posSetCurrencyFormat(itemData[j, 2], 8), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(285, startPostionCounter)); // item price
                         e.Graphics.DrawString(this.posSetCurrencyFormat(Convert.ToString(Math.Round(Convert.ToDecimal(itemData[j, 3]) * Convert.ToDecimal(itemData[j, 2]), 2)), 10), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, startPostionCounter));// item total
                         startPostionCounter += 15;
                     }
                     for (int j = 0; j < itemCount; j++) {
                         this.posInsertFODBill(new String[] { this.posGetBillItemOrdDCost(new String[] { orderno, itemData[j, 6] }), data[2], itemData[j, 6], itemData[j, 2], itemData[j, 3], this.posGetOrderKOTBOTNo(new String[] { orderno }) });
                     }
                 }
                e.Graphics.DrawString(this.posSetCurrencyFormat(data[9],10), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, 380));//sub total
                if (data[8].Equals("0")) {
                    data[8] = "";
                }
                e.Graphics.DrawString( data[8], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(270, 395));// discount
                e.Graphics.DrawString((data[9]).ToString(), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(365, 395));// Discount Value
                e.Graphics.DrawString(this.posSetCurrencyFormat(data[7],10), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, 455));//total
                e.Graphics.DrawString("Including Service Charge & VAT", new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(5, 455));//including service charge & vat
                e.Graphics.DrawString( data[6], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(120, 550));//Guest name
                e.Graphics.DrawString( data[5], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, 550));//room no

            };
               try
            {
                p.Print();
            }
            catch (Exception)
            {
                
            }
                
           
            return false;
        }
        public bool posPrintBillSplit(String[] data, String[,] itemData, int printItemCounter, posClsBill billObject, posGuiBill guiBill)
        {
            //TcpClient client = new TcpClient();
            //client.Connect("192.168.123.64", 9100);

            ////client.Connect("192.168.123.64", 9100);
            //StreamWriter writer = new StreamWriter(client.GetStream());
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.Write(""+this.getDate());
            //writer.Write(" " + data[0]);// table no
            //writer.Write("      " + data[1]);// steward code
            //writer.Write("  " + data[2]); // bill no
            //writer.Write(" " + data[3]); // serial No
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.Write("       "+posStaticLoadOnce.PosCostCenterCode[0]);
            //writer.Write("       "+data[4]); // no of pax
            //writer.Write("              " + data[5]);//room no

            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //for (int i = 0; i < printItemCounter; i++)
            //{
            //    writer.Write("     "+itemData[i, 0]);
            //    writer.Write(" "+ itemData[i, 1]);
            //    writer.Write(" "+ itemData[i, 2]);
            //    writer.Write(" "+ itemData[i, 3]);
            //    writer.WriteLine();

            //}


            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();

            //writer.WriteLine();
            //writer.Write("                                    "+data[7]);//total
            //writer.WriteLine();
            //writer.Write("                                    "+data[8]);// discount
            //writer.WriteLine(); 
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.WriteLine();
            //writer.Write("         " + data[6]);//Guest name
            //writer.Write("           " + data[5]);//room no

            //writer.Flush();
            //writer.Close();
            PrintDocument p = new PrintDocument();
            p.PrintPage += delegate(object sender, PrintPageEventArgs e)
            {
                e.Graphics.DrawString(this.getDate(), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(5, 120));
                e.Graphics.DrawString(data[0], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(120, 120));// table no
                e.Graphics.DrawString(data[1], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(200, 120));// steward code
                e.Graphics.DrawString(data[2], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(265, 120)); // bill no
                e.Graphics.DrawString(data[3], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, 120)); // serial No                

                e.Graphics.DrawString(posStaticLoadOnce.PosCostCenterCode[1], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(35, 150));
                e.Graphics.DrawString(data[4], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(210, 150)); // no of pax
                e.Graphics.DrawString("(Split- "+ data[11] +")", new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(225, 150)); // Split bill comment
                e.Graphics.DrawString(data[5], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, 150));//room no

                int startPostionCounter = 205;
                //String tableNo = guiBill.posDgvBillTableNumber.CurrentRow.Cells[0].Value.ToString();
                String orderno = "";
                int itemCount = 0;

                
                   // orderno = guiBill.posDgvBillTableDetails.Rows[i].Cells[0].Value.ToString();
                   // e.Graphics.DrawString(orderno, new Font("Times New Roman", 10), new SolidBrush(Color.Black), new Point(0, startPostionCounter));
                   // itemData = billObject.posGetOrderItemDetailsPrint(new String[] { tableNo, orderno });
                   // itemCount = posStaticLoadOnce.PosRowCount;
                    for (int j = 0; j < guiBill.posDgvBillTableItemDetails.Rows.Count; j++)
                    {
                        e.Graphics.DrawString(this.posGetEditedString(itemData[j, 0]), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(65, startPostionCounter)); // item description
                        e.Graphics.DrawString(this.posSetCurrencyFormat(Math.Round(Convert.ToDecimal(itemData[j, 1]), 2).ToString().Replace(".00", ""), 4), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(245, startPostionCounter)); // item qty
                        e.Graphics.DrawString(this.posSetCurrencyFormat(itemData[j, 2], 8), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(285, startPostionCounter)); // item price
                        e.Graphics.DrawString(this.posSetCurrencyFormat(Convert.ToString(Math.Round(Convert.ToDecimal(itemData[j, 1]) * Convert.ToDecimal(itemData[j, 2]), 2)), 10), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, startPostionCounter));// item total
                        startPostionCounter += 15;
                    }
                    for (int j = 0; j < itemCount; j++)
                    {
                        this.posInsertFODBill(new String[] { this.posGetBillItemOrdDCost(new String[] { orderno, itemData[j, 6] }), data[2], itemData[j, 6], itemData[j, 2], itemData[j, 3], this.posGetOrderKOTBOTNo(new String[] { orderno }) });
                    }
                
                e.Graphics.DrawString(this.posSetCurrencyFormat(data[9], 10), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, 380));//sub total
                if (data[8].Equals("0"))
                {
                    data[8] = "";
                }
                else {
                    data[8] += ".00 %";
                }
                if (data[10].Equals("0"))
                {
                    data[10] = "";
                }
                e.Graphics.DrawString(data[8]  , new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(270, 395));// discount
                e.Graphics.DrawString((data[10]).ToString(), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(365, 395));// Discount Value
                e.Graphics.DrawString(this.posSetCurrencyFormat(data[7], 10), new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, 455));//total
                
                e.Graphics.DrawString("Including Service Charge & VAT", new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(5, 455));//including service charge & vat
                e.Graphics.DrawString(data[6], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(120, 550));//Guest name
                e.Graphics.DrawString(data[5], new Font("Times New Roman", 12), new SolidBrush(Color.Black), new Point(350, 550));//room no

            };
            try
            {
                p.Print();
            }
            catch (Exception)
            {

            }


            return false;
        }
    }
}
