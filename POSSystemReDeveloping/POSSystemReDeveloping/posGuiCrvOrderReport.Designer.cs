﻿namespace POSSystemReDeveloping
{
    partial class posGuiCrvOrderReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.posCrvOrderReport = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.btnCrvOrderReportBack = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.posCrvOrderReport);
            this.groupBox1.Location = new System.Drawing.Point(110, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(897, 607);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // posCrvOrderReport
            // 
            this.posCrvOrderReport.ActiveViewIndex = -1;
            this.posCrvOrderReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.posCrvOrderReport.Cursor = System.Windows.Forms.Cursors.Default;
            this.posCrvOrderReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.posCrvOrderReport.Location = new System.Drawing.Point(3, 16);
            this.posCrvOrderReport.Name = "posCrvOrderReport";
            this.posCrvOrderReport.Size = new System.Drawing.Size(891, 588);
            this.posCrvOrderReport.TabIndex = 0;
            this.posCrvOrderReport.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // btnCrvOrderReportBack
            // 
            this.btnCrvOrderReportBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrvOrderReportBack.Location = new System.Drawing.Point(3, 3);
            this.btnCrvOrderReportBack.Name = "btnCrvOrderReportBack";
            this.btnCrvOrderReportBack.Size = new System.Drawing.Size(101, 98);
            this.btnCrvOrderReportBack.TabIndex = 1;
            this.btnCrvOrderReportBack.Text = "Back";
            this.btnCrvOrderReportBack.UseVisualStyleBackColor = true;
            this.btnCrvOrderReportBack.Click += new System.EventHandler(this.btnCrvOrderReportBack_Click);
            // 
            // posGuiCrvOrderReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 733);
            this.ControlBox = false;
            this.Controls.Add(this.btnCrvOrderReportBack);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "posGuiCrvOrderReport";
            this.Text = "posGuiCrvOrderReport";
            this.Load += new System.EventHandler(this.posGuiCrvOrderReport_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer posCrvOrderReport;
        private System.Windows.Forms.Button btnCrvOrderReportBack;
    }
}