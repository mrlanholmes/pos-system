﻿namespace POSSystemReDeveloping
{
    partial class posGuiReportBillDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.posBtnReportBillDetailPrint = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.posDgvReportBillDetail = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvReportBillDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.posBtnReportBillDetailPrint);
            this.groupBox1.Location = new System.Drawing.Point(2, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1007, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // posBtnReportBillDetailPrint
            // 
            this.posBtnReportBillDetailPrint.Location = new System.Drawing.Point(361, 29);
            this.posBtnReportBillDetailPrint.Name = "posBtnReportBillDetailPrint";
            this.posBtnReportBillDetailPrint.Size = new System.Drawing.Size(251, 46);
            this.posBtnReportBillDetailPrint.TabIndex = 0;
            this.posBtnReportBillDetailPrint.Text = "Print";
            this.posBtnReportBillDetailPrint.UseVisualStyleBackColor = true;
            this.posBtnReportBillDetailPrint.Click += new System.EventHandler(this.posBtnReportBillDetailPrint_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.posDgvReportBillDetail);
            this.groupBox2.Location = new System.Drawing.Point(2, 106);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1007, 625);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // posDgvReportBillDetail
            // 
            this.posDgvReportBillDetail.AllowUserToAddRows = false;
            this.posDgvReportBillDetail.AllowUserToDeleteRows = false;
            this.posDgvReportBillDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvReportBillDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.posDgvReportBillDetail.Location = new System.Drawing.Point(3, 16);
            this.posDgvReportBillDetail.Name = "posDgvReportBillDetail";
            this.posDgvReportBillDetail.ReadOnly = true;
            this.posDgvReportBillDetail.Size = new System.Drawing.Size(1001, 606);
            this.posDgvReportBillDetail.TabIndex = 0;
            // 
            // posGuiReportBillDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 733);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "posGuiReportBillDetail";
            this.Text = "posGuiReportBillDetail";
            this.Load += new System.EventHandler(this.posGuiReportBillDetail_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.posDgvReportBillDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button posBtnReportBillDetailPrint;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView posDgvReportBillDetail;
    }
}