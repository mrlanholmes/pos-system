﻿namespace POSSystemReDeveloping
{
    partial class posGuiBillSeperate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.posDgvBillSeperateExistItem = new System.Windows.Forms.DataGridView();
            this.posDgvBillSeperateOldDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posDgvBillSeperateOldQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posDgvBillSeperateOldPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posDgvBillSeperateOldTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posDgvBillSeperateOldKotbot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posDgvBillSeperateOldItemCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posDgvBillSeperateNewItem = new System.Windows.Forms.DataGridView();
            this.posDgvBillSeperateNewDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posDgvBillSeperateNewQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posDgvBillSeperateNewPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posDgvBillSeperateNewType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posDgvBillSeperateNewKotBot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posBillSeperateNewItemItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posBtnBillSeperateRightSide = new System.Windows.Forms.Button();
            this.posBtnBillSeperateLeftSide = new System.Windows.Forms.Button();
            this.posTxtBillSeperateValue = new System.Windows.Forms.TextBox();
            this.posBtnBillSeperateBack = new System.Windows.Forms.Button();
            this.posBtnBillSeperateBill = new System.Windows.Forms.Button();
            this.posDgvBillSeperateBillForSettle = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillSeperateExistItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillSeperateNewItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillSeperateBillForSettle)).BeginInit();
            this.SuspendLayout();
            // 
            // posDgvBillSeperateExistItem
            // 
            this.posDgvBillSeperateExistItem.AllowUserToAddRows = false;
            this.posDgvBillSeperateExistItem.AllowUserToDeleteRows = false;
            this.posDgvBillSeperateExistItem.BackgroundColor = System.Drawing.SystemColors.Control;
            this.posDgvBillSeperateExistItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvBillSeperateExistItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.posDgvBillSeperateOldDescription,
            this.posDgvBillSeperateOldQty,
            this.posDgvBillSeperateOldPrice,
            this.posDgvBillSeperateOldTotal,
            this.posDgvBillSeperateOldKotbot,
            this.posDgvBillSeperateOldItemCode});
            this.posDgvBillSeperateExistItem.Location = new System.Drawing.Point(98, 283);
            this.posDgvBillSeperateExistItem.Name = "posDgvBillSeperateExistItem";
            this.posDgvBillSeperateExistItem.ReadOnly = true;
            this.posDgvBillSeperateExistItem.RowHeadersVisible = false;
            this.posDgvBillSeperateExistItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.posDgvBillSeperateExistItem.Size = new System.Drawing.Size(412, 389);
            this.posDgvBillSeperateExistItem.TabIndex = 4;
            // 
            // posDgvBillSeperateOldDescription
            // 
            this.posDgvBillSeperateOldDescription.HeaderText = "Description";
            this.posDgvBillSeperateOldDescription.Name = "posDgvBillSeperateOldDescription";
            this.posDgvBillSeperateOldDescription.ReadOnly = true;
            // 
            // posDgvBillSeperateOldQty
            // 
            this.posDgvBillSeperateOldQty.HeaderText = "Qty";
            this.posDgvBillSeperateOldQty.Name = "posDgvBillSeperateOldQty";
            this.posDgvBillSeperateOldQty.ReadOnly = true;
            // 
            // posDgvBillSeperateOldPrice
            // 
            this.posDgvBillSeperateOldPrice.HeaderText = "Price";
            this.posDgvBillSeperateOldPrice.Name = "posDgvBillSeperateOldPrice";
            this.posDgvBillSeperateOldPrice.ReadOnly = true;
            // 
            // posDgvBillSeperateOldTotal
            // 
            this.posDgvBillSeperateOldTotal.HeaderText = "Total";
            this.posDgvBillSeperateOldTotal.Name = "posDgvBillSeperateOldTotal";
            this.posDgvBillSeperateOldTotal.ReadOnly = true;
            // 
            // posDgvBillSeperateOldKotbot
            // 
            this.posDgvBillSeperateOldKotbot.HeaderText = "KotBotNo";
            this.posDgvBillSeperateOldKotbot.Name = "posDgvBillSeperateOldKotbot";
            this.posDgvBillSeperateOldKotbot.ReadOnly = true;
            // 
            // posDgvBillSeperateOldItemCode
            // 
            this.posDgvBillSeperateOldItemCode.HeaderText = "ItemCodeNo";
            this.posDgvBillSeperateOldItemCode.Name = "posDgvBillSeperateOldItemCode";
            this.posDgvBillSeperateOldItemCode.ReadOnly = true;
            // 
            // posDgvBillSeperateNewItem
            // 
            this.posDgvBillSeperateNewItem.AllowUserToAddRows = false;
            this.posDgvBillSeperateNewItem.AllowUserToDeleteRows = false;
            this.posDgvBillSeperateNewItem.BackgroundColor = System.Drawing.SystemColors.Control;
            this.posDgvBillSeperateNewItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvBillSeperateNewItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.posDgvBillSeperateNewDescription,
            this.posDgvBillSeperateNewQty,
            this.posDgvBillSeperateNewPrice,
            this.posDgvBillSeperateNewType,
            this.posDgvBillSeperateNewKotBot,
            this.posBillSeperateNewItemItemName});
            this.posDgvBillSeperateNewItem.Location = new System.Drawing.Point(599, 283);
            this.posDgvBillSeperateNewItem.Name = "posDgvBillSeperateNewItem";
            this.posDgvBillSeperateNewItem.ReadOnly = true;
            this.posDgvBillSeperateNewItem.RowHeadersVisible = false;
            this.posDgvBillSeperateNewItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.posDgvBillSeperateNewItem.Size = new System.Drawing.Size(412, 389);
            this.posDgvBillSeperateNewItem.TabIndex = 5;
            // 
            // posDgvBillSeperateNewDescription
            // 
            this.posDgvBillSeperateNewDescription.HeaderText = "Description";
            this.posDgvBillSeperateNewDescription.Name = "posDgvBillSeperateNewDescription";
            this.posDgvBillSeperateNewDescription.ReadOnly = true;
            this.posDgvBillSeperateNewDescription.Width = 160;
            // 
            // posDgvBillSeperateNewQty
            // 
            this.posDgvBillSeperateNewQty.HeaderText = "Qty";
            this.posDgvBillSeperateNewQty.Name = "posDgvBillSeperateNewQty";
            this.posDgvBillSeperateNewQty.ReadOnly = true;
            this.posDgvBillSeperateNewQty.Width = 40;
            // 
            // posDgvBillSeperateNewPrice
            // 
            this.posDgvBillSeperateNewPrice.HeaderText = "Price";
            this.posDgvBillSeperateNewPrice.Name = "posDgvBillSeperateNewPrice";
            this.posDgvBillSeperateNewPrice.ReadOnly = true;
            this.posDgvBillSeperateNewPrice.Width = 50;
            // 
            // posDgvBillSeperateNewType
            // 
            this.posDgvBillSeperateNewType.HeaderText = "Total";
            this.posDgvBillSeperateNewType.Name = "posDgvBillSeperateNewType";
            this.posDgvBillSeperateNewType.ReadOnly = true;
            this.posDgvBillSeperateNewType.Width = 40;
            // 
            // posDgvBillSeperateNewKotBot
            // 
            this.posDgvBillSeperateNewKotBot.HeaderText = "KotBotNo";
            this.posDgvBillSeperateNewKotBot.Name = "posDgvBillSeperateNewKotBot";
            this.posDgvBillSeperateNewKotBot.ReadOnly = true;
            // 
            // posBillSeperateNewItemItemName
            // 
            this.posBillSeperateNewItemItemName.HeaderText = "Code";
            this.posBillSeperateNewItemItemName.Name = "posBillSeperateNewItemItemName";
            this.posBillSeperateNewItemItemName.ReadOnly = true;
            // 
            // posBtnBillSeperateRightSide
            // 
            this.posBtnBillSeperateRightSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnBillSeperateRightSide.Location = new System.Drawing.Point(521, 370);
            this.posBtnBillSeperateRightSide.Name = "posBtnBillSeperateRightSide";
            this.posBtnBillSeperateRightSide.Size = new System.Drawing.Size(65, 62);
            this.posBtnBillSeperateRightSide.TabIndex = 2;
            this.posBtnBillSeperateRightSide.Text = ">";
            this.posBtnBillSeperateRightSide.UseVisualStyleBackColor = true;
            this.posBtnBillSeperateRightSide.Click += new System.EventHandler(this.posBtnBillSeperateRightSide_Click);
            // 
            // posBtnBillSeperateLeftSide
            // 
            this.posBtnBillSeperateLeftSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnBillSeperateLeftSide.Location = new System.Drawing.Point(521, 508);
            this.posBtnBillSeperateLeftSide.Name = "posBtnBillSeperateLeftSide";
            this.posBtnBillSeperateLeftSide.Size = new System.Drawing.Size(65, 62);
            this.posBtnBillSeperateLeftSide.TabIndex = 3;
            this.posBtnBillSeperateLeftSide.Text = "<";
            this.posBtnBillSeperateLeftSide.UseVisualStyleBackColor = true;
            // 
            // posTxtBillSeperateValue
            // 
            this.posTxtBillSeperateValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posTxtBillSeperateValue.Location = new System.Drawing.Point(521, 439);
            this.posTxtBillSeperateValue.MaxLength = 3;
            this.posTxtBillSeperateValue.Multiline = true;
            this.posTxtBillSeperateValue.Name = "posTxtBillSeperateValue";
            this.posTxtBillSeperateValue.Size = new System.Drawing.Size(65, 62);
            this.posTxtBillSeperateValue.TabIndex = 1;
            this.posTxtBillSeperateValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // posBtnBillSeperateBack
            // 
            this.posBtnBillSeperateBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnBillSeperateBack.Location = new System.Drawing.Point(3, 1);
            this.posBtnBillSeperateBack.Name = "posBtnBillSeperateBack";
            this.posBtnBillSeperateBack.Size = new System.Drawing.Size(89, 88);
            this.posBtnBillSeperateBack.TabIndex = 6;
            this.posBtnBillSeperateBack.Text = "Back";
            this.posBtnBillSeperateBack.UseVisualStyleBackColor = true;
            // 
            // posBtnBillSeperateBill
            // 
            this.posBtnBillSeperateBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.posBtnBillSeperateBill.Location = new System.Drawing.Point(3, 95);
            this.posBtnBillSeperateBill.Name = "posBtnBillSeperateBill";
            this.posBtnBillSeperateBill.Size = new System.Drawing.Size(89, 88);
            this.posBtnBillSeperateBill.TabIndex = 7;
            this.posBtnBillSeperateBill.Text = "Bill";
            this.posBtnBillSeperateBill.UseVisualStyleBackColor = true;
            this.posBtnBillSeperateBill.Click += new System.EventHandler(this.posBtnBillSeperateBill_Click);
            // 
            // posDgvBillSeperateBillForSettle
            // 
            this.posDgvBillSeperateBillForSettle.AllowUserToAddRows = false;
            this.posDgvBillSeperateBillForSettle.AllowUserToDeleteRows = false;
            this.posDgvBillSeperateBillForSettle.BackgroundColor = System.Drawing.SystemColors.Control;
            this.posDgvBillSeperateBillForSettle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.posDgvBillSeperateBillForSettle.Location = new System.Drawing.Point(98, 1);
            this.posDgvBillSeperateBillForSettle.Name = "posDgvBillSeperateBillForSettle";
            this.posDgvBillSeperateBillForSettle.ReadOnly = true;
            this.posDgvBillSeperateBillForSettle.RowHeadersVisible = false;
            this.posDgvBillSeperateBillForSettle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.posDgvBillSeperateBillForSettle.Size = new System.Drawing.Size(913, 276);
            this.posDgvBillSeperateBillForSettle.TabIndex = 8;
            this.posDgvBillSeperateBillForSettle.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.posDgvBillSeperateBillForSettle_CellContentClick);
            // 
            // posGuiBillSeperate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 734);
            this.ControlBox = false;
            this.Controls.Add(this.posDgvBillSeperateBillForSettle);
            this.Controls.Add(this.posBtnBillSeperateBill);
            this.Controls.Add(this.posBtnBillSeperateBack);
            this.Controls.Add(this.posTxtBillSeperateValue);
            this.Controls.Add(this.posBtnBillSeperateLeftSide);
            this.Controls.Add(this.posBtnBillSeperateRightSide);
            this.Controls.Add(this.posDgvBillSeperateNewItem);
            this.Controls.Add(this.posDgvBillSeperateExistItem);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "posGuiBillSeperate";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill Seperation";
            this.Load += new System.EventHandler(this.posGuiBillSeperate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillSeperateExistItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillSeperateNewItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.posDgvBillSeperateBillForSettle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.DataGridView posDgvBillSeperateNewItem;
        internal System.Windows.Forms.Button posBtnBillSeperateRightSide;
        internal System.Windows.Forms.Button posBtnBillSeperateLeftSide;
        internal System.Windows.Forms.TextBox posTxtBillSeperateValue;
        internal System.Windows.Forms.DataGridView posDgvBillSeperateExistItem;
        internal System.Windows.Forms.Button posBtnBillSeperateBack;
        internal System.Windows.Forms.Button posBtnBillSeperateBill;
        internal System.Windows.Forms.DataGridView posDgvBillSeperateBillForSettle;
        private System.Windows.Forms.DataGridViewTextBoxColumn posDgvBillSeperateNewDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn posDgvBillSeperateNewQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn posDgvBillSeperateNewPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn posDgvBillSeperateNewType;
        private System.Windows.Forms.DataGridViewTextBoxColumn posDgvBillSeperateNewKotBot;
        private System.Windows.Forms.DataGridViewTextBoxColumn posBillSeperateNewItemItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn posDgvBillSeperateOldDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn posDgvBillSeperateOldQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn posDgvBillSeperateOldPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn posDgvBillSeperateOldTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn posDgvBillSeperateOldKotbot;
        private System.Windows.Forms.DataGridViewTextBoxColumn posDgvBillSeperateOldItemCode;

    }
}